export let homeListDef = [
    {
        id: 1,
        imgPath: 'assets/imgs/esselworldHome.jpg',
        cardHeader: 'EsselWorld',
        cardBody: 'Essel World is an amusement park located in Gorai, Mumbai and established in 1989. The park is owned by EsselWorld Leisure Pvt. Ltd. ',
    }, {
        id: 2,
        imgPath: 'assets/imgs/waterkingdomHome.jpg',
        cardHeader: 'Water Kingdom',
        cardBody: 'Family-friendly water park featuring high-speed slides, wave pools, lazy river & kiddie attractions.',
    },
]
