import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { LocationInfo } from './LocationInfo';
import {ENUM} from './ENUM';
/*
  Generated class for the MenuServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocationInfoService {
    list:any = [];

    constructor(private http: Http ) {
        //this.list = LocationInfo;
        //this.list = [];
    }

    getLocationList(){
        return LocationInfo;
    }

    setLocationList(list){
      this.list = list;
      //return null;
    }

    getCloudLocationList(){
        return this.list;
    }
}
