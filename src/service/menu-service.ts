// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
/*
  Generated class for the MenuServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MenuServiceProvider {

  constructor( public menuCtrl : MenuController) {
    console.log('Hello MenuServiceProvider Provider');
  }



  enableMenu(variable){
    switch (variable) {
      case "login":
	        this.menuCtrl.enable(false, "logoutMenu");
	        this.menuCtrl.enable(true, "loginMenu");
        break;

        case "logout":
	        this.menuCtrl.enable(false, "loginMenu");
		    this.menuCtrl.enable(true, "logoutMenu");
        break;
      	
      default:
        this.menuCtrl.enable(false, "loginMenu");
	      this.menuCtrl.enable(true, "logoutMenu");
        break;
    }
    
  }
}
