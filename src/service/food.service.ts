import {Injectable} from "@angular/core";
import { food } from './food'

@Injectable()
export class FoodsService {

	foodsArray : any = [];

	constructor(){
		this.foodsArray= food;
		console.log('serviceAllFoods:',this.foodsArray)
	}

	getAll(){
		return this.foodsArray;
	}
}