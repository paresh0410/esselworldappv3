export let tktCategory:any = [{
    id:1,
    title:'Basic',
    subCat : [{
        id:1,
        name:'Basic(Adult) ',
        disc : 'Entry to park @ ₹ 90 and ₹ 300 redeemable on rides, food & merchandize',
        price:390,
        Qnt : 0
    },{
        id:2,
        name:'Basic(Clild)',
        disc : 'Entry to park @ ₹ 90 and ₹ 300 redeemable on rides, food & merchandize',
        price:300,
        Qnt : 0
    }]
},
{
    id:2,
    title:'Silver',
    subCat : [{
        id:3,
        name:'Silver (Adult)',
        disc : 'Access to all Adult & Family rides',
        price:1050,
        Qnt : 0
    },{
        id:4,
        name:'Silver (Child)',
        disc : 'Access to all Kid & Family rides',
        price:750,
        Qnt : 0
    }] 
},
{
    id:3,
    title:'Fast Track + Silver',
    subCat : [{
        id:5,
        name:'Fast Track + Silver (Adult)',
        disc : 'Price @ ₹ 450 + ₹ 1050. Privilege entry to park & no queue to rides. Access to all Adult & Family rides',
        price: 1500,
        Qnt : 0
    },{
        id:6,
        name:'Fast Track + Silver (Child)',
        disc : 'Price @ ₹ 250 + ₹ 750. Privilege entry to park & no queue to rides. Access to all Kid & Family rides',
        price: 1000,
        Qnt : 0
    }]
},
{
    id:4,
    title:'Combine',
    subCat : [{
        id:7,
        name:'Combine (Adult)',
        disc : 'Entry to EsselWorld + Water Kingdom on same day of visit',
        price: 1390,
        Qnt : 0
    },{
        id:8,
        name:'Combine (Clild)',
        disc : 'Entry to EsselWorld + Water Kingdom on same day of visit',
        price: 950,
        Qnt : 0
    }]
},
{
    id:5,
    title:'Annual Pass - Passport Next',
    subCat : [{
        id:9,
        name:'Annual Pass - Passport Next',
        disc : 'Entry to either of the parks three times a year',
        price: 1590,
        Qnt : 0
    }]
}];