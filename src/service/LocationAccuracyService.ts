import { Injectable } from '@angular/core';
import {ENUM} from './ENUM';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
/*
  Generated class for the EntryRatesServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable()
export class LocationAccuracyService{

    constructor(private locationAccuracy: LocationAccuracy) {
    }

    canRequest(cb){
        this.locationAccuracy.canRequest().then((canRequest: boolean) => {

          if(canRequest) {

          }

          // the accuracy option will be ignored by iOS
            this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
                cb(true);
                console.log('Request successful')
            },
              error => {
                  console.log('Error requesting location permissions', error);
                  cb(false);
              }
            );
        });
    }

}