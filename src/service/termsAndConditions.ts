export let termsAndConditions: any = [{
	desc: 'There are certain rules and regulations one has to follow to ensure optimum safety and enjoyment at both the parks – EsselWorld and Water Kingdom. Following are the rules are to be observed and adhered:',
	List: [{
		list: 'Sr. Citizens need to carry valid age proof photo ID to avail the benefit.'
	},
	{
		list: 'Special rates apply only for New Year Eve event and for special events.'
	},
	{
		list: 'Park entry ticket prices are subject to change without prior notice.'
	},
	{
		list: 'Charges for F&B, Riki’s Rocking Alley, Arctic Circle and other specified attractions are in addition to normal entry ticket rate, hence called paid attractions.'
	},
	{
		list: 'Nylon or lycra swimwear is mandatory for Water Kingdom. Cotton clothes are not advisable. Swimwear is also available on rent and for purchase at Water Kingdom.'
	},
	{
		list: 'Baggage facility at EsselWorld & Lockers facility at Water Kingdom is provided at nominal rates.'
    },
    {
		list: 'Patrons using various rides / slides and other facilities at EsselWorld / Water Kingdom will do so with due caution taking note of the various warning notices displayed at appropriate places and shall utilise the facilities/rides at their own risks.'
    },
    {
		list: 'All bags, backpacks & personal items will be inspected prior to entering the parks.'
    },
    {
		list: 'Picnic lunches, large duffle bags, suitcases, coolers, bags with wheels, weapons, outside food & beverages, alcohol, illegal drugs (BIG NO!) strictly not allowed within premises.'
    },
    {
		list: 'The management shall not owe any responsibility for lost valuables, mishap or any other damages.'
    },
    {
		list: 'If you are driving to our parks, we recommend you leave back unessential articles and secure valuables in your car trunk.'
    },
    {
		list: 'All the rides are upto the extent possible are being kept fully operational. However sometimes it may happen that due to technical reasons certain rides may be non-operational and the management does not owe any liability towards it and is not answerable for such situations.'
    },
    {
		list: 'Guests are informed that we may photograph /videograph, record or reproduce the images and/or voice during their visit to our parks. Such photographs/video recordings may be used for promotional purposes for which no payment will be made to any such person.'
    },
    {
		list: 'Rights of admission reserved.'
    },
    {
		list: 'Please carry the printout of the E-Ticket and exchange it for a valid ticket at our parks.'
    },
    {
		list: 'In case of Credit / Debit Card payments, it is also MANDATORY to carry the same card from which booking was made.'
    },
    {
		list: 'Tickets once booked cannot be exchanged, cancelled or refunded.'
    },
    {
		list: 'In-case of non-availability of a paid attraction (game) due to technical or any other reasons, a substitute game will be provided. No cash refund in such cases. All game rules apply.'
    },
    {
		list: 'No two promotions/ offers can be clubbed or combined.'
    },
    {
		list: 'Prices are standard for Child, Adult or Sr. Citizens.'
    },
    {
		list: 'Car Combo service can be availed from morning to midnight. Additional charges applicable post midnight.'
    },
    {
		list: 'Prices are inclusive of entry ticket to EsselWorld OR Water Kingdom (single park entry visit).'
    },
    {
		list: 'All Car Combo bookings and confirmations are subject to availability.'
    },
    {
		list: 'Prices are inclusive of transfer from single pick-up point to our parks and back along with driver allowance, toll & car park charges.'
    },
    {
		list: 'There will be no refund on cancellations. Re-scheduling subject to prior 48 hours intimation.'
    },
    {
		list: 'Rates & terms are subject to change, without prior notice.'
    },
    {
		list: 'All booking terms & conditions of EsselWorld Leisure Pvt Ltd. & First Cars will apply.'
    },
    {
		list: 'There will be single pick-up point as confirmed at the time of booking. Multiple pick-ups points are not allowed.'
    },
    {
		list: 'The driver details will be sent and emailed to the guest in the night at 23:00 Hrs. for the next day bookings.'
    },
    {
		list: 'For reservations related queries - 24x7 CRS no. 022-40774277 / 8655433660.'
    },
    {
		list: 'For Car & driver details related queries - 24x7 Mumbai operations desk can be reached on 9004555345 / 8655433770.'
    },
    {
		list: 'By using the mobile application to book tickets and all other products listed, the customer agrees to all terms and conditions mentioned above.'
    },
]
}]