export let fnbCategory:any = [{
    id:1,
    title:'FOOD VOUCHERS',
    subCat : [{
        id:1,
        name:'FOOD VOUCHERS',
        disc : 'This food voucher is redeemable at any food outlet at EsselWorld',
        price:180,
        Qnt : 0
    }]
},
 {
    id:2,
    title:'COMBO',
    subCat : [
    {
        id:2,
        name:'PANEER COMBO',
        disc : 'Shahi paneer,Imli chutney,2 pcs Lachha paratha',
        price:180,
        Qnt : 0
    },
    {
        id:3,
        name:'CHOLE COMBO',
        disc : 'Chole,2 pcs kulcha,Imli chutney',
        price:180,
        Qnt : 0
    },
    {
        id:4,
        name:'ALOO COMBO',
        disc : 'Bedmi aloo sabzi,3 pcs Poori,Imli chutney',
        price:135,
        Qnt : 0
    },
    {
        id:5,
        name:'BLACK DAL COMBO',
        disc : 'Black dal,Imli chutney,3 pcs Tandoor roti',
        price:180,
        Qnt : 0
    }] 
},
{
    id:3,
    title:'BUFFET LUNCH',
    subCat : [
    {
        id:6,
        name:'LUNCH BUFFET CHILD',
        disc : 'Frencg fries,White pasta,Margerita pizza,Sandwiches,(cheese/jam/chutney),Pav Bhaji,Mix veg,Dal rice,Ice cream(cup),Packed juice and many more...',
        price: 171,
        Qnt : 0
    },
    {
        id:7,
        name:'LUNCH BUFFET ADULT',
        disc : 'Tomato soup,Mix bhajiya,Pani poori,Green salad/Veg raita,Veg crispy,Veg kolhapuri/Shahi paneer,Dal tadka,Tandoori roti/naan,Veg pulav/Plain rice,Papad,Pickles,Gulab jamoon,Moonge dal sheera,SoftDrink,Tea,Coffee and many more...',
        price: 405,
        Qnt : 0
    }]
},
{
    id:4,
    title:'MEALS',
    subCat : [
    {
        id:8,
        name:'MEAL',
        disc : 'Aloo sabzi,Chole,Mix veg,Dal rice,Salad,Papad,5 pcs Poori',
        price: 189,
        Qnt : 0
    }]
}];