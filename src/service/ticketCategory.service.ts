import {Injectable} from "@angular/core";
import { tktCategory } from './ticketCategory'

@Injectable()
export class TicketCategoryService {

	TktCategoryArray : any = [];

	constructor(){
		this.TktCategoryArray= tktCategory;
		console.log('result:',this.TktCategoryArray)
	}

	getAll(){
		return this.TktCategoryArray;
	}
}