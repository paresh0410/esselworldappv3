export let instructions: any = [{
	type: 'ENTRY TICKETS',
	List: [{
		list: 'Tickets of EsselWorld & Water Kingdom are separate.'
	},
	{
		list: 'Ticket rate include entry fees and access to all rides.'
	},
	{
		list: 'Additional charges are applicable for ‘Paid Attractions.'
	},
	{
		list: 'Tickets once sold will not be refunded.'
	},
	{
		list: 'Re-sale of tickets or discounted offer vouchers outside the ticket counter is strictly prohibited. If found, management reserves the right to dishonor such tickets.'
	},
	{
		list: 'Re-entry to the park is not permitted on used tickets.'
	}
	]
},
{
	type: 'RIDES',
	List: [{
		list: 'Kindly read the ride instructions carefully before enjoying the rides. Some rides might not be advisable for heart patients, blood pressure patients & pregnant ladies.'
	},
	{
		list: 'Seek help/advice from ride operators if needed.'
	},
	{
		list: 'Kindly follow the height restrictions on the respective rides.'
	},
	{
		list: 'Some rides may be shut down for technical or safety purpose, inconvenience caused is regretted.'
	},
	{
		list: 'Kindly use the defined entry & exit areas at the rides and follow queue rules.'
	},
	{
		list: 'For enjoying the rides in Water Kingdom, Nylon / Lycra swim wear is compulsory. Swimming costumes are available on hire purchase.'
	}
	]
},
{
	type: 'FOOD & BEVERAGES',
	List: [{
		list: 'Outside food and drinks are not permitted within the parks. '
	},
	{
		list: 'Alcoholic drinks are strictly prohibited in and around park premises. '
	},
	]
},
{
	type: 'BAGGAGE ASSISTANCE',
	List: [{
		list: 'Kindly deposit your baggage at baggage counters.'
	},
	{
		list: 'Avoid keeping your belongings unattended.'
	},
	{
		list:'Locker facility is also available on hire for your convenience.'
	}
	]
},
{
	type: 'SUGGESTIONS',
	List: [{
		list: 'Your suggestions and feedbacks are most welcome. Kindly record them in the suggestion books. Visit Ticket Counter / Control Tower for further assistance.Admission rights reserved with management.'
	},
	]
}
] 