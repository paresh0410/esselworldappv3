export let rides=[
					{
						//ADULT RIDES
						id:'1',
						subRides :  [{
						  id:2,
					      imgpath:'assets/imgs/RidesImages/shot-n-drop2.jpg',
					      name:'Shot-N-Drop',
					      desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'
					  },
					  {   
					  	  id:6,
					      imgpath:'assets/imgs/RidesImages/topspin.jpg',
					      name:'Top Spin',
					      desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'
					  },
					  {   
					  	  id:7,
					      imgpath:'assets/imgs/RidesImages/enterprise.jpg',
					      name:'Enterprise',
					      desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'
					  },
					  {		 
					  	  id:3,
					      imgpath:'assets/imgs/RidesImages/Hoola-loop.jpg',
					      name:'Hoola -Loop',
					      desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'
					  },
					  {   
					  	  id:8,
					      imgpath:'assets/imgs/RidesImages/slambob.jpg',
					      name:'Slam Bob',
					      desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'
					  },
					  {
					  	  id:1,
					      imgpath:'assets/imgs/RidesImages/Senior-Dodgem-Cars.jpg',
					      name:'Sr. Dodgem',
					      desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'
					  },
					  {   
					  	  id:10,
					      imgpath:'assets/imgs/RidesImages/sr.telecombat.jpg',
					      name:'Super Telecombat',
					      desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'
					  },
					  {   
					  	  id:9,
					      imgpath:'assets/imgs/RidesImages/Thunder.jpg',
					      name:'Thunder',
					      desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'
					  },
					  {   
					  	  id:5,
					      imgpath:'assets/imgs/RidesImages/Zyclone.png',
					      name:'Zyclone',
					      desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'
					  },
					  {   
					  	  id:4,
					      imgpath:'assets/imgs/RidesImages/Rainbow.jpg',
					      name:'Rainbow',
					      desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'
					  },
					  {
					  	  id:23,
					      imgpath:'assets/imgs/RidesImages/Tunnel-Twister.jpg',
					      name:'Tunnel Twister',
					      desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'
					  }]
					},
					{
						//FAMILY RIDES
						id: '2',
						subRides : [{
						  	id:26,
						    imgpath:'assets/imgs/RidesImages/Alibaba-Mirror-maze.jpg',
						    name:'Alibaba Mirror Maze',
						    desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'

						  },
						  {
						  	id:20,
						    imgpath:'assets/imgs/RidesImages/aqua-dive.jpg',
						    name:'Aque Dive',
						    desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'

						  },
						  {
						  	id:28,
						    imgpath:'assets/imgs/RidesImages/Copper-chopper.jpg',
						    name:'Copper Chopper',
						    desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'

						  },
						  {
						  	id:24,
						    imgpath:'assets/imgs/RidesImages/Crazy-cup.jpg',
						    name:'Crazy Cup',
						    desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'

						  },
						  {
						  	id:25,
						    imgpath:'assets/imgs/RidesImages/High-way-cars.jpg',
						    name:'Highway Cars',
						    desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'

						  },
						  {
						  	id:27,
						    imgpath:'assets/imgs/RidesImages/Monster.jpg',
						    name:'Monster',
						    desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'

						  },
						  {
						  	id:21,
						    imgpath:'assets/imgs/RidesImages/zipper-dipper.jpg',
						    name:'Zipper Dipper',
						    desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'

						  },
						  {
						  	id:22,
						    imgpath:'assets/imgs/RidesImages/Monster-In-the-Mist.jpg',
						    name:'Monster In The Mist',
						    desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'

						  },{
							id:29,
						    imgpath:'assets/imgs/RidesImages/Aeroswinger.png',
						    name:'Aero swinger',
						    desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'

						  }]
					},
					{
						//Child Rides
						id: '3',
						subRides : [{
						  id:14,
					      imgpath:'assets/imgs/RidesImages/Happy-sky.jpg',
					      name:'Happy Sky',
					      desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'
					  },
					  {   
					  	  id:13,
					      imgpath:'assets/imgs/RidesImages/kangaroo-hop.jpg',
					      name:'Kangaroo Hop',
					      desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'
					  },
					  {   
					  	  id:16,
					      imgpath:'assets/imgs/RidesImages/Children-boat-ride.jpg',
					      name:'Children Boat Ride',
					      desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'
					  },
					  {
					  	  id:18,
					      imgpath:'assets/imgs/RidesImages/Cater-piller.jpg',
					      name:'Cater Piller',
					      desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'
					  },
					  {
					  	  id:11,
					      imgpath:'assets/imgs/RidesImages/Jr.Dodgem.jpg',
					      name:'Jr Dodgem',
					      desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'
					  },
					  {
					  	  id:12,
					      imgpath:'assets/imgs/RidesImages/Jr-Go-Cart.jpg',
					      name:'Jr Go Kart',
					      desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'
					  },
					  {
					  	  id:19,
					      imgpath:'assets/imgs/RidesImages/Rio-grand-train.jpg',
					      name:'Rio Grand Train',
					      desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'
					  },
					  {
			              id:17,
					      imgpath:'assets/imgs/RidesImages/mini-tele-combat.jpg',
					      name:'Mini Tele Combat',
					      desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'
					  },
					   {
					   	  id:15,
					      imgpath:'assets/imgs/RidesImages/Yard-train.jpg',
					      name:'Yard Train',
					      desc: 'The most popular industrial group ever, and largely.responsible for bringing the music to a mass audience.'
					  }]
					}
				]
