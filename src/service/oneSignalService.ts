import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import {ENUM} from './ENUM';
import { OneSignal } from '@ionic-native/onesignal';
import { Device } from '@ionic-native/device';
/*
 Generated class for the EntryRatesServiceProvider provider.

 See https://angular.io/guide/dependency-injection for more info on providers
 and Angular DI.
*/

@Injectable()
export class OneSignalService {
  
  pushIds: any = {
    pushToken:null,
    userId: null
  }

   constructor(private http: Http, public oneSignal : OneSignal, public device: Device ) {
   }

   getToken(cb){
    this.oneSignal.getIds().then((result)=>{
       this.pushIds = result;
       cb(this.pushIds);
    }).catch((error)=>{
        console.log("error for oneSignal getIds", error);
       cb(this.pushIds);
    });
   }
   getPushIds(){
       return this.pushIds;
   }

   startInit(){
    this.oneSignal.startInit(ENUM.oneSignal.onesignalappkey,ENUM.oneSignal.senderId);
	
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
   }

   endInit(){
    this.oneSignal.endInit();
   }

   registerUserTokenInfo(userId, cb){

    let url = "";
    let param: any = {
        uId: null,
        appId: ENUM.oneSignal.configId,
        model: this.device.model,
        platforms: this.device.platform,
        version: this.device.version,
        pushId: this.pushIds.pushToken, // This token varies 
        pushUserId: this.pushIds.userId, //This ID is constant
        uuId: this.device.uuid
    };
    if(userId){
        param.uId = userId;
        url = ENUM.domain + ENUM.url.insertPushUser;

    }
    else{
        url = ENUM.domain + ENUM.url.insertPushUser;
    }

    return new Promise(resolve => {
        this.http.post(url, param)
        .map(res => res.json())
        .subscribe(data => {
            resolve(data);
        },
        err => {
            resolve('err');
        });
    });
    
   }

//    fetchBookingNotification(userId, pgtxn cb){

//     let url = '';
//     let param: any = {

//     }

//    }
}