import {Injectable} from "@angular/core";
import { games } from './game'

@Injectable()
export class GameService {

	gameArray : any = [];

	constructor(){
		this.gameArray= games;
		console.log('serviceAllGames:',this.gameArray)
	}

	getAll(){
		return this.gameArray;
	}
}