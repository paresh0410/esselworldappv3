import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { homeListDef } from './homeList';
import {ENUM} from './ENUM';
import { AlertController, LoadingController } from 'ionic-angular';
/*
  Generated class for the MenuServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HomeServiceProvider {
    list:any = [];
    url:any = {
        homelisturl:ENUM.domain+ENUM.url.homeList
    }

    constructor(private http: Http, private alertCtrl: AlertController, public loadingCtrl: LoadingController) {
        this.list = homeListDef;

    }

    getLocalList(){
        return this.list;    
    }

    getHomeListService(param){
        // don't have the data yet
        return new Promise(resolve => {
            // We're using Angular HTTP provider to request the data,
            // then on the response, it'll map the JSON data to a parsed JS object.
            // Next, we process the data and resolve the promise with the new data.
            this.http.post(this.url.homelisturl, param)
            .map(res => res.json())
            .subscribe(data => {
                // we've got back the raw data, now generate the core schedule data
                // and save the data for later reference
                resolve(data);
            },
            err => {
                resolve('err');
            });
        });
    }

}
