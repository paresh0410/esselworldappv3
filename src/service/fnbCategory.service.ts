import {Injectable} from "@angular/core";
import { fnbCategory } from './fnbCategory'

@Injectable()
export class fnbCategoryService {

	fnbCategoryArray : any = [];

	constructor(){
		this.fnbCategoryArray= fnbCategory;
		console.log('result:',this.fnbCategoryArray)
	}

	getAll(){
		return this.fnbCategoryArray;
	}
}