import { Injectable } from "@angular/core";
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
// import { Observable } from 'rxjs';
import { ENUM } from './ENUM';
import { HttpClient, HttpHeaders, HttpInterceptor, HttpRequest, HttpResponse,
        HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
@Injectable()
export class webAPIService {
    url: any;
    enum: any = ENUM;
    headers = new HttpHeaders();
    constructor(public http: Http, private httph: HttpClient) {
        this.url = ENUM.url;
    }

    pushService(url, param) {
        // don't have the data yet
        return new Promise(resolve => {
            // We're using Angular HTTP provider to request the data,
            // then on the response, it'll map the JSON data to a parsed JS object.
            // Next, we process the data and resolve the promise with the new data.
            this.http.post(url, param)
                .map(res => res.json())
                .subscribe(data => {
                    // we've got back the raw data, now generate the core schedule data
                    // and save the data for later reference
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getService(url, param) {

        return new Promise(resolve => {
            this.http.post(url, param)
                .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }


    serviceforUPI_old(url, param) {
        this.headers = new HttpHeaders();
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('X-CHANNEL', 'API');
        this.headers.set('X-EMAIL', 'cashlesstechnologies123@gmail.com');
        this.headers.set('AuthorizationKey', 'eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL21vYmlsZXBheW1lbnRzLmJlbm93LmluLyIsInN1YiI6ImNhc2hsZXNzdGVjaG5vbG9naWVzMTIzQGdtYWlsLmNvbSIsImRhdGEiOnsibWVyY2hhbnRJZCI6IjExNDYwIiwibWNjQ29kZSI6IjU0OTkiLCJtb2JpbGVOdW1iZXIiOiI3MDQ1MDczMzUxIiwiZGlzcGxheU5hbWUiOiJJTlRFR1JBVElPTiBURVNUIE1FUkNIQU5UIiwibWVyY2hhbnRDb2RlIjoiQUY4WTEiLCJwcml2YXRlSWQiOiI5OTMifSwiaWF0IjoxNTE2NzAyOTQyfQ.GqLkpZgv_YIULLM7kuUmYDeyRYZ8hNg2mGhkHlGl1A8');

        const URL = url; // site that doesn’t send Access-Control-*
        fetch(URL)
            .then(response => response.text())
            .then(contents => console.log(contents))
            .catch(() => console.log("Can’t access " + URL + " response. Blocked by browser?"));

        const proxyurl = "https://cors-anywhere.herokuapp.com/";
        fetch(proxyurl + url) // https://cors-anywhere.herokuapp.com/https://example.com`
            .then(response => response.text())
            .then(contents => {
                console.log(contents);
            })
            .catch(() => {
                console.log("Can’t access " + url + " response. Blocked by browser?")
            });
        console.log('Headers are ', this.headers);
        let options = {
            headers: this.headers
        }

        return new Promise(resolve => {
            this.httph.post(URL, param, options)
                .toPromise().then(response => {
                    console.log('response ', response);
                    console.log('resolve(response) ', resolve(response));
                    resolve(response);
                }).catch((err) => {
                    console.log('err ', err);
                    resolve(err);
                })
        });
    }


    serviceforUPI(url, param) {
        let header = new Headers();
        header.append('Content-Type', 'application/json');
        header.append('X-CHANNEL', 'API');
        header.append('X-EMAIL', 'cashlesstechnologies123@gmail.com');
        header.append('AuthorizationKey', 'eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL21vYmlsZXBheW1lbnRzLmJlbm93LmluLyIsInN1YiI6ImNhc2hsZXNzdGVjaG5vbG9naWVzMTIzQGdtYWlsLmNvbSIsImRhdGEiOnsibWVyY2hhbnRJZCI6IjExNDYwIiwibWNjQ29kZSI6IjU0OTkiLCJtb2JpbGVOdW1iZXIiOiI3MDQ1MDczMzUxIiwiZGlzcGxheU5hbWUiOiJJTlRFR1JBVElPTiBURVNUIE1FUkNIQU5UIiwibWVyY2hhbnRDb2RlIjoiQUY4WTEiLCJwcml2YXRlSWQiOiI5OTMifSwiaWF0IjoxNTE2NzAyOTQyfQ.GqLkpZgv_YIULLM7kuUmYDeyRYZ8hNg2mGhkHlGl1A8');
        let RequestOption = new RequestOptions({headers : header});
        const URL = url; // site that doesn’t send Access-Control-*
        // fetch(URL)
        //     .then(response => response.text())
        //     .then(contents => console.log(contents))
        //     .catch(() => console.log("Can’t access " + URL + " response. Blocked by browser?"));

        // const proxyurl = "https://cors-anywhere.herokuapp.com/";
        // fetch(proxyurl + url, param, RequestOption) // https://cors-anywhere.herokuapp.com/https://example.com`
        //     .then(response => response.text())
        //     .then(contents => {
        //         console.log(contents);
        //     })
        //     .catch(() => {
        //         console.log("Can’t access " + url + " response. Blocked by browser?")
        //     });
        // console.log('Headers are ', this.headers);
        
        
        const proxyurl = "https://cors-anywhere.herokuapp.com/";
        // let header = new Headers();
        // header.append('Content-Type', 'application/json');
        // header.append( 'Authorization' , 'Bearer ' + 'token');
        
          return new Promise(resolve => {
            this.http.post(url, param, RequestOption)
              .map(res => res.json())
              .subscribe(data => {
                resolve(data);
              });
          });
    }

    // getServiceWithHeaders(url, param) {
    //     this.headers = new HttpHeaders().append('Content-Type', 'application/json')
    //         .append('X-CHANNEL', 'API')
    //         .append('X-EMAIL', 'cashlesstechnologies123@gmail.com')
    //         .append('AuthorizationKey', 'eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL21vYmlsZXBheW1lbnRzLmJlbm93LmluLyIsInN1YiI6ImNhc2hsZXNzdGVjaG5vbG9naWVzMTIzQGdtYWlsLmNvbSIsImRhdGEiOnsibWVyY2hhbnRJZCI6IjExNDYwIiwibWNjQ29kZSI6IjU0OTkiLCJtb2JpbGVOdW1iZXIiOiI3MDQ1MDczMzUxIiwiZGlzcGxheU5hbWUiOiJJTlRFR1JBVElPTiBURVNUIE1FUkNIQU5UIiwibWVyY2hhbnRDb2RlIjoiQUY4WTEiLCJwcml2YXRlSWQiOiI5OTMifSwiaWF0IjoxNTE2NzAyOTQyfQ.GqLkpZgv_YIULLM7kuUmYDeyRYZ8hNg2mGhkHlGl1A8');

    //     var options = {
    //         headers: this.headers
    //     }
    //     console.log('Headers are ', this.headers);
    //     return new Promise(resolve => {

    //         this.httph.post(url, param, options)
    //             .toPromise()
    //             .then(response => {
    //                 console.log('response ', response);
    //                 console.log('resolve(response) ', resolve(response));
    //                 resolve(response);
    //             })
    //             .catch((result) => {
    //                 resolve(result);
    //             });
    //     })

    // }

    otpService(url, param) {
        // don't have the data yet
        return new Promise(resolve => {
            // We're using Angular HTTP provider to request the data,
            // then on the response, it'll map the JSON data to a parsed JS object.
            // Next, we process the data and resolve the promise with the new data.
            this.http.post(url, param)
                .map(res => res.json())
                .subscribe(data => {
                    // we've got back the raw data, now generate the core schedule data
                    // and save the data for later reference
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    getLocation(url, param) {
        return new Promise(resolve => {
            this.http.post(url, param)
                .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }

    //  getRides(url){
    //         return new Promise(resolve => {
    //         this.http.post(url,'')
    //         .map(res => res.json())
    //         .subscribe(data => {
    //             resolve(data);
    //         },
    //         err => {
    //             resolve('err');
    //         });
    //     });
    // }

    getImages(param) {
        // don't have the data yet
        return new Promise(resolve => {
            // We're using Angular HTTP provider to request the data,
            // then on the response, it'll map the JSON data to a parsed JS object.
            // Next, we process the data and resolve the promise with the new data.
            this.http.get(this.enum.domain + this.url.imgUrl + param)
                .map(res => res.json())
                .subscribe(data => {
                    // we've got back the raw data, now generate the core schedule data
                    // and save the data for later reference
                    resolve(data);
                },
                    err => {
                        resolve('err');
                    });
        });
    }
}