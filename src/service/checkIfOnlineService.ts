import {Injectable} from "@angular/core";
import 'rxjs/add/operator/map';
import { Network } from '@ionic-native/network';
import { Platform } from 'ionic-angular';

@Injectable()
export class checkIfOnlineService {

    public ifOnline: boolean;

    constructor(public platform: Platform, public network: Network){

    }

    getIfOnline(){
        return this.ifOnline;
    }

    setOnlineFlag(onlineFlag){
        this.ifOnline = onlineFlag;
    }

}