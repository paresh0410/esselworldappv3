import {Injectable} from "@angular/core";
import { termsAndConditions } from './termsAndConditions'

@Injectable()
export class TermsAndConditionsService {

	termsAndConditionsArray : any = [];

	constructor(){
		this.termsAndConditionsArray= termsAndConditions;
		// console.log('xyz:',this.instructionsArray)
	}

	getAll(){
		return this.termsAndConditionsArray;
	}
}