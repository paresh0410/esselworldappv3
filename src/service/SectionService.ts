import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
/*
  Generated class for the MenuServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SectionService {

    list:any = [];

    setSection(list){
        this.list = list;
    }

    getSection(){
        return this.list;
    }

}