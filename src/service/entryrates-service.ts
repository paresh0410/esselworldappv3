import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import {ENUM} from './ENUM';

/*
  Generated class for the EntryRatesServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable()
export class EntryRatesServiceProvider{
    list:any = [];
    url:any = {
        entryRatesurl:ENUM.domain+ENUM.url.entryRates
    }

    constructor(private http: Http ) {
    }

    getentryRatesService(){
        // don't have the data yet
        return new Promise(resolve => {
            // We're using Angular HTTP provider to request the data,
            // then on the response, it'll map the JSON data to a parsed JS object.
            // Next, we process the data and resolve the promise with the new data.
            this.http.post(this.url.entryRatesurl,'')
            .map(res => res.json())
            .subscribe(data => {
                // we've got back the raw data, now generate the core schedule data
                // and save the data for later reference
                resolve(data);
            },
            err => {
                resolve('err');
            });
        });
    }

}