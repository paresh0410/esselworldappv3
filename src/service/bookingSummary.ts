export let bookingSummary:any = [{
    "id": "EW1551520287019",
    "userDetails": {
        "userid": 47,
        "regusername": "Paresh",
        "mobileno": "8898715436",
        "email": "paresh@mobeserv.com",
        "gender": 1,
        "bookingdate": "2019-03-26T18:30:00.000Z"
    },
    "tickets": [{
        "ticketId": 8,
        "ticketname": "Holi Bash - Adult",
        "ticketprice": "1171",
        "ticCount": 1,
        "totalPirce": 1171,
        "ticketimg": null,
        "ticketcatname": "Holi Bash",
        "hctitle": "EsselWorld",
        "ttname": "Basic Ticket"
    }],
    "FandB": [{
        "ticketId": 22,
        "ticketname": "Thaali",
        "ticketprice": "200",
        "ticCount": 2,
        "totalPirce": 400,
        "ticketimg": "/img/ticket/file-1551358958832.png",
        "ticketcatname": "Food Vouchers",
        "hctitle": "EsselWorld",
        "ttname": "Food And Beverages"
    }],
    "paidAttr": [{
        "ticketId": 19,
        "ticketname": "Cricket",
        "ticketprice": "150",
        "ticCount": 3,
        "totalPirce": 450,
        "ticketimg": "/img/ticket/file-1551358458469.png",
        "ticketcatname": "Paid Attractions I",
        "hctitle": "EsselWorld",
        "ttname": "Paid Attractions"
    }]
}]