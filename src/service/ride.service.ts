import {Injectable} from "@angular/core";
import { rides } from './rides'

@Injectable()
export class RidesService {

	ridesArray : any = [];

	constructor(){
		this.ridesArray= rides;
		console.log('serviceAllRides:',this.ridesArray)
	}

	getAll(){
		return this.ridesArray;
	}
}