import {Injectable} from "@angular/core";
import { instructions } from './instructions'

@Injectable()
export class InstructionsService {

	instructionsArray : any = [];

	constructor(){
		this.instructionsArray= instructions;
		// console.log('xyz:',this.instructionsArray)
	}

	getAll(){
		return this.instructionsArray;
	}
}