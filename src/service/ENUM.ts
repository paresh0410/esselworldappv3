
export let ENUM = {
    // domain: 'http://35.154.55.154:9772/',                  //DEV
    // domain: 'http://35.154.55.154:9775/',                      //TEST
    // domain: 'http://13.232.192.230:9772/',                  //New DEV
    // domain: 'http://13.232.192.230:9775/',               //New TEST
    // domain: 'http://144.76.154.179:9772/',                   //ESSEL TEST                      
    domain: 'http://144.76.154.179:9775/',               //ESSEL LIVE
    url: {
        registerUser: 'api/ewapp/register',
        loginUser: 'api/ewapp/login',
        homeList: 'api/ewapp/home',
        loginHistory: 'api/ewapp/loginhistory',
        logoutHistory: 'api/ewapp/logouthistory',
        sendOtp : 'api/ewapp/send2factorotp',
        verifyOtp : 'api/ewapp/verify2factorotp',
        imgUrl : 'api/ewapp/getimage?path=',
       // ifalreadyReg :'api/ewapp/registerVerify',
        updateOtpStatus: 'api/ewapp/updateOtp',
        fetchLocations : 'api/ewapp/fetchLocations',
        entryRates: 'api/ewapp/entryRates',
        eventList: 'api/ewapp/events',
        offerList: 'api/ewapp/offers',
        groupList: 'api/ewapp/groups',
        intro: 'api/ewapp/intro',
        attractions:'api/ewapp/fetchAttractions',
        mySchedule: 'api/ewapp/mySchedule',
        updateMySchedule: 'api/ewapp/updateMySchedule',
        updateLikeStatus: 'api/ewapp/updateLikeStatus',
        fetchSections: 'api/ewapp/section',
        getLikeStatus:'api/ewapp/GetLikeStatus',
        fetchParkTimings:'api/ewapp/parktimings',
        insertPushUser: 'api/ewweb/insertpushnotitable',
        fetchlocationcat: 'api/ewapp/fetchloccat',
        fetchGuidMapHcid : 'api/ewapp/guidemap', 
        insertUUID: 'api/ewapp/updateUUID',
        // notification: 'api/ewapp/oneSignalAllUsers1'
        myNotifications: 'api/ewapp/fetchmynotifications',
        updateprofile : 'api/ewapp/updateprofile',
        // updateprofileimg : 'api/ewapp/uploadprofilepic',
        fetchProfileImg : 'api/ewapp/getProimage?path=',
        saveuserdetails : 'api/ewapp/booking/saveUserDetails',
        fetchuserdetails : 'api/ewapp/booking/fetchUserBookingDetails',
        fetchbookingsummary : 'api/ewapp/booking/fetchbookingSummary',
        insertTrackerId: 'api/ewapp/booking/trackerid',
        fetchTickets: 'api/ewapp/booking/fetchTickets',
        saveUnconfirmed: 'api/ewapp/booking/saveUnconfirmed',
        fetchUnconfirmedItems: 'api/ewapp/booking/fetchUnconfirmedItems',
        saveConfirmed :'api/ewapp/booking/saveConfirmed',
        fetchConfirmedItems: 'api/ewapp/booking/fetchConfirmedList',
        fetchhConfirmedItemsDetails:'api/ewapp/booking/fetchConfirmedDetails',
        addNewTicket : 'api/ewapp/booking/AddNewTicket',
        deleteOneTicket : 'api/ewapp/booking/DeleteOneTicket',
        removeAllTicket : 'api/ewapp/booking/RemoveTicketCate',
        savetxnId: 'api/ewapp/booking/saveTxn',
        savetxnNoti: 'api/ewapp/booking/savenotifications',
        fetchtxnSMS: 'api/ewapp/booking/paymentSMS',
        fetchtxnNotification: 'api/ewapp/booking/paymentNotification',
        fetchtxnMail: 'api/ewapp/booking/PaymentMail',
        // upilink: 'api/ewapp/booking/upiLinkGenerate',
        upilink: 'https://merchant.benow.in/sdk/createBillString',
        fetchSectionBanners :'api/ewapp/sectionBanners',
        encrypt: 'api/ewapp/booking/encrypt',
        fetchPoints: 'api/ewapp/fetchPoints',
        checkMainTicket: 'api/ewapp/booking/checkMainTicket',
        DeleteAllTicketsOnMain: 'api/ewapp/booking/DeleteAllTicketsOnMain'
    }, 
    display: {
        login: 'Login',
        registration: 'Registration',
        loginMsg: 'Logged In Successfully',
        errorTitle1: 'Server Error!!!',
        errorTitle2: 'Login Error!!!',
        errorTitle3: 'Authentication Error!!!',
        noInternet: 'No Internet Connection!!!',
        serverError: 'Server not responding. Please try again later.',
        tryAgainMsg: 'Something went wrong. Please try again.',
        alreadyExist: "User already registered. Tap 'OK' for login",
        notRegistered: "User not registered. Tap 'OK' for registration",
        noEvents: 'No Events Available.',
        noOffers: 'No Offers Available.',
        noRide: 'No Rides Available.',
        noFNB: 'No Foods & Beverages Stall Available.',
        noGames: 'No Games Available.',
        noData: 'No data Available.',
        invalidOTP: 'Invalid OTP',
        addToScheduleRes: 'Attraction added to the schedule.',
        attractionExists: 'Attraction already exists in the schedule.',
        addToScheduleServer: 'Cannot connect to server! Action cannot be completed.',
        addToScheduleNoConn: 'Internet connection lost! Action cannot be completed.',
        loginToCont: 'Login to avial this funtionality.',
    },
    oneSignal: {
        senderId: "367010904539", //"26327932925", //From Firebase
        onesignalappkey: "146b8594-112b-4d81-954f-cf74943c8831",//"db7bd806-fd67-4495-915d-b76c19c9e9de",  //"554c1929-611b-453e-8376-e76c1eeb1d7b",  
        configId:"com.esselworld.official"
    },
    alertMessage :'<div class="popupMsgdiv"><img src="./assets/imgs/TicketIcon.png" class="popupIcon"><div class="popupmsg"><p class="para">####</p></div></div>'
    

}
// AIzaSyC1NrO2ifijvdwO_jpSW1jlAT2SP0eYE04
export let NUMBER: any ;
