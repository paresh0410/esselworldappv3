import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import {
	Nav, Platform, NavController, ToastController,
	MenuController, AlertController, Events, Card, ModalController, Modal, ModalOptions
} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';
///import { NavController } from 'ionic-angular';
import { ModalpagePage } from '../pages/modalpage/modalpage';
import { IntroPage } from '../pages/intro/intro'
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { BookingHistoryPage } from '../pages/booking-history/booking-history';
import { SchedulePage } from '../pages/schedule/schedule';
import { GeofencePage } from '../pages/geofence/geofence';
import { RegistrationPage } from '../pages/registration/registration';
import { LoginPage } from '../pages/login/login';
import { OffersPage } from '../pages/offers/offers';
import { ProfilePage } from '../pages/profile/profile';
import { BookTicketPage } from '../pages/book-ticket/book-ticket'
import { ParkTimingsPage } from '../pages/park-timings/park-timings';
import { FoodBeveragesPage } from '../pages/food-beverages/food-beverages';
import { GamesPage } from '../pages/games/games';
import { EventsPage } from '../pages/events/events';
import { GroupsNschoolsPage } from '../pages/groups-nschools/groups-nschools';
import { GuideMapPage } from '../pages/guide-map/guide-map';
import { OtpPage } from '../pages/otp/otp';
import { RegisterNewPage } from '../pages/register-new/register-new';
// import { TabsPage } from '../pages/tabs/tabs';
import { TodoPage } from '../pages/todo/todo';
import { RidePage } from '../pages/ride/ride';
import { RideDetailsPage } from '../pages/ride-details/ride-details';
import { NoInternetConnectionPage } from '../pages/no-internet-connection/no-internet-connection';
import { NotificationPage } from '../pages/notification/notification';
import { NotificationService } from '../service/notification.service';
import { webAPIService } from '../service/webAPIService';
import { TicketCategoryPage } from '../pages/ticket-category/ticket-category';
import { FAndBCategoryPage } from '../pages/f-and-b-category/f-and-b-category';
import { PaidAttractionsPage } from '../pages/paid-attractions/paid-attractions';
import { BookingSummaryPage } from '../pages/booking-summary/booking-summary';
import { CartPage } from '../pages/cart/cart';
import { checkIfOnlineService } from '../service/checkIfOnlineService';
import { ENUM, NUMBER } from '../service/ENUM';
import { getLocationService } from '../service/getlocation.service'
import { LocationInfoService } from '../service/LocationInfoService';
import { LoggedInUserService } from '../service/loggedInUser.service'
import { LocationAccuracyService } from '../service/LocationAccuracyService';
import { SectionService } from '../service/SectionService'
import { SectionPage } from '../pages/section/section';
import { SaftyInstructionPage } from '../pages/safty-instruction/safty-instruction';
import { TermsAndConditionsPage } from '../pages/terms-and-conditions/terms-and-conditions';
import { OneSignalService } from '../service/oneSignalService';
// import { FCM } from '@ionic-native/fcm';
// import { FCMService } from '../service/fcmservice';
import { OneSignal } from '@ionic-native/onesignal';
import { Device } from '@ionic-native/device';
import { UserDetailsPage } from '../pages/user-details/user-details';
import { BookingHistoryListPage } from '../pages/booking-history-list/booking-history-list';
// import { Geofence } from '@ionic-native/geofence';
import { ThemeServiceProvider } from '../providers/theme-service/theme-service';
import { BookhomecategoryPage } from '../pages/bookhomecategory/bookhomecategory';
import { BookingPage } from '../pages/booking/booking';
import { PaymentPage } from '../pages/payment/payment';
import { RewardPage } from '../pages/reward/reward';
@Component({
	templateUrl: 'app.html'
})
export class MyApp {
	@ViewChild(Nav) nav: NavController;
	logoutBtn: boolean = false;
	loginBtn: boolean = true;
	rootPage: any = LoginPage;
	// UserDetail: any = {
	// 	FirstName: "Niket",
	// 	LastName: "Bhave",
	// 	LastLogin: "2018-05-29T11:29:08.467",
	// 	EndDate: 1606674600000,
	// 	ProfilePic: "assets/imgs/avatar-han.png",
	// 	// ProfilePic : {
	// 	//   large : "https://randomuser.me/api/portraits/men/14.jpg",
	// 	//   medium : "https://randomuser.me/api/portraits/med/men/14.jpg",
	// 	//   small : "https://randomuser.me/api/portraits/thumb/men/14.jpg"
	// 	// },
	// 	CustomerContactId: "c0512a3a23f8090f1e072ca424ed2ff59b15aa8a82dff7524440cbc7fe075e84",
	// 	Email: "niket@mobeserv.com"
	// };
	NUMBER: any = NUMBER;
	number: any = 0;
	UserDetails: any = {}
	senderId: any;
	onesignalappkey: any;
	logoutpages: Array<{ title: string, component: any, icon: any }>;
	loginpages: Array<{ title: string, component: any, icon: any }>;
	windowlocal: any;
	alert: any;
	public pushTokenIds: any;
	theme: String = 'blue-theme';//keep default theme as blue
	ThemeC: any;
	mobile: any;
	value: any;
	name: any;
	init_points: any;
	constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,
		public webAPI: webAPIService, private network: Network, private checkOnline: checkIfOnlineService,
		public toastCtrl: ToastController, public menu: MenuController, public getlocationservice: getLocationService,
		public locationService: LocationInfoService, public loggedinuserservice: LoggedInUserService,
		public LocationAccService: LocationAccuracyService, public alertCtrl: AlertController,
		public sectionService: SectionService, private oneSignal: OneSignal,
		private device: Device, private onesignalservice: OneSignalService,
		public events: Events, public notiService: NotificationService,
		public themeServices: ThemeServiceProvider, public changeDetRef: ChangeDetectorRef, private modal: ModalController) {
			this.fetchPoints();
		// localStorage.removeItem('trackerId');
		// localStorage.removeItem('bookingDt');

		// Initializing Geofence
		// this.changeDetRef.detectChanges();
		this.ThemeC = this.themeServices.getTheme();
		console.log('ThemeC', this.ThemeC);
		this.events.subscribe('theme', () => {
			this.ThemeC = this.themeServices.getTheme();
		});
		console.log('ThemeC', this.ThemeC);


		//  platform.ready().then(()=>{
		//        platform.registerBackButtonAction(()=>this.myHandlerFunction());
		// })
		this.senderId = '367010904539';
		this.onesignalappkey = 'db7bd806-fd67-4495-915d-b76c19c9e9de';
		// this.senderId=senderId;
		// this.onesignalappkey=onesignalappkey;
		this.platform.registerBackButtonAction(() => {
			console.log('Nav', this.nav);
			// get current active page
			let view = this.nav.getActive();
			console.log('VIEW--------->', view.component.name);
			let previousView = this.nav.getPrevious(view);
			if (previousView !== null) {
				console.log('PREVIOUSVIEW--------->', previousView.component.name);
			}
			if (this.menu.isOpen()) {
				this.menu.close();
			} else {
				if (view.component.name === 'HomePage') {
					// this.platform.exitApp();
					this.showAlert();
				} else if (previousView != null) {
					this.nav.setRoot(previousView);
				} else if (view.component.name === 'SchedulePage') {
					this.nav.setRoot(HomePage);
				} else if (view.component.name === 'GeofencePage') {
					this.nav.setRoot(HomePage);
				} else if (view.component.name === 'ListPage') {
					this.nav.setRoot(HomePage);
				} else if (view.component.name === 'GroupsNschoolsPage') {
					this.nav.setRoot(HomePage);
				} else if (view.component.name === 'EventsPage') {
					this.nav.setRoot(HomePage);
				} else if (view.component.name === 'OffersPage') {
					this.nav.setRoot(HomePage);
				} else if (view.component.name === 'GuideMapPage') {
					this.nav.setRoot(SectionPage);
				} else if (view.component.name === 'FoodBeveragesPage') {
					this.nav.setRoot(SectionPage);
				} else if (view.component.name === 'GamesPage') {
					this.nav.setRoot(SectionPage);
				} else if (view.component.name === 'ParkTimingsPage') {
					this.nav.setRoot(SectionPage);
				} else if (view.component.name === 'RidePage') {
					this.nav.setRoot(SectionPage);
				} else if (view.component.name === 'LoginPage') {
					this.platform.exitApp();	
				}
				else if (view.component.name === 'RegisterNewPage') {
					this.platform.exitApp();
				}
				else if (view.component.name === 'SectionPage') {
					this.nav.setRoot(HomePage);
				} else if (view.component.name === 'BookTicketPage') {
					this.nav.setRoot(SectionPage);
					// } else if( view.component.name === 'BookingPage'){
					// 	let previousView = this.nav.last().name;
					// 	if(previousView === 'HomePage'){
					// 		this.nav.setRoot(previousView);
					// 	}
					// 	else if(previousView === 'SectionPage'){
					// 		this.nav.setRoot(SectionPage);
					// 	}
				} else if (view.component.name === 'TicketCategoryPage') {
					this.nav.setRoot(HomePage);
				} else if (view.component.name === 'FAndBCategoryPage') {
					let previousView = this.nav.last().name;
					this.nav.setRoot(TicketCategoryPage);
				} else if (view.component.name === 'PaidAttractionsPage') {
					let previousView = this.nav.last().name;
					this.nav.setRoot(FAndBCategoryPage);
				} else if (view.component.name === 'UserDetailsPage') {
					let previousView = this.nav.last().name;
					this.nav.setRoot(PaidAttractionsPage);
				} else if (view.component.name === 'BookingSummaryPage') {
					let previousView = this.nav.last().name;
					this.nav.setRoot(UserDetailsPage);
				} else if (view.component.name === 'BookingPage') {
					this.nav.setRoot(BookhomecategoryPage);
				} else if (view.component.name === 'BookhomecategoryPage') {
					this.nav.setRoot(HomePage);
				} else if(view.component.name === 'RewardPage'){
					this.nav.setRoot(HomePage);
				}
				else {
					this.platform.exitApp();
				}
			}
		});
		this.initializeApp();
		// this.allServices();
		this.fetchLocations();
		this.fetchSections();
		//this.UserDetails = this.loggedinuserservice.userdetails[0]
		//console.log('sideMenuUserDetails:',this.loggedinuserservice.userdetails)

		// used for an example of ngFor and navigation
		// this.UserDetails = localStorage.getItem('UserDetails');
		// console.log('userdetailsApp:',this.UserDetails)
		if (localStorage.getItem('login') == 'Y') {
			this.UserDetails = localStorage.getItem('UserDetails');
			console.log('userdetailsApp:', this.UserDetails)
		}

		this.logoutpages = [
			{ title: 'Home', component: HomePage, icon: 'fa fa-home' },
			{ title: 'Near By', component: GeofencePage, icon: 'fa fa-map-marker' },
			{ title: 'Entry Rates', component: ListPage, icon: 'fa fa-rupee' },
			{ title: 'Group & Schools', component: GroupsNschoolsPage, icon: 'fa fa-users' },
			{ title: 'Events', component: EventsPage, icon: 'fa fa-calendar-o' },
			{ title: 'Offers', component: OffersPage, icon: 'fa fa-gift' },
			{ title: 'My Schedule', component: SchedulePage, icon: 'fa fa-clock-o' },
			{ title: 'Vouchers', component: BookingHistoryListPage, icon: 'fa fa-ticket' },
			{ title: 'Safety Instructions', component: SaftyInstructionPage, icon: 'fa fa-medkit', },
			// { title: 'Ticket Category', component: TicketCategoryPage, icon: 'fa fa-ticket' },
			// { title: 'F & B Category', component: FAndBCategoryPage, icon: 'fa fa-cutlery' },
			// {title: 'Paid Attractions', component: PaidAttractionsPage, icon: 'fa fa-gamepad', },
			// {title: 'Booking Summary', component: BookingSummaryPage, icon: 'fa fa-book', }
			// { title: 'Cart', component: CartPage, icon: 'fa fa-shopping-cart' }
			// {title: 'Payment', component: PaymentPage, icon: 'fa fa-rupee'}
		];

		this.loginpages = [
			{ title: 'Home', component: HomePage, icon: 'fa fa-home' },
			{ title: 'Near By', component: GeofencePage, icon: 'fa fa-map-marker' },
			{ title: 'Entry Rates', component: ListPage, icon: 'fa fa-rupee' },
			{ title: 'Group & Schools', component: GroupsNschoolsPage, icon: 'fa fa-users' },
			{ title: 'Events', component: EventsPage, icon: 'fa fa-calendar-o' },
			{ title: 'Offers', component: OffersPage, icon: 'fa fa-gift' },
			{ title: 'My Schedule', component: SchedulePage, icon: 'fa fa-clock-o' },
			{ title: 'Vouchers', component: BookingHistoryListPage, icon: 'fa fa-ticket' },
			{ title: 'Safety Instructions', component: SaftyInstructionPage, icon: 'fa fa-medkit' },
			// { title: 'Ticket Category', component: TicketCategoryPage, icon: 'fa fa-ticket' },
			// { title: 'Booking History', component: null, icon: 'fa fa-history'},
			{title: 'Reward Center', component: RewardPage, icon: 'fa fa-trophy'},
			{ title: 'Profile', component: ProfilePage, icon: 'fa fa-user' },
			// { title: 'F & B Category', component: FAndBCategoryPage, icon: 'fa fa-cutlery' },
			// {title: 'Paid Attractions', component: PaidAttractionsPage, icon: 'fa fa-gamepad', },
			// {title: 'User Details', component: UserDetailsPage, icon: 'fa fa-book', },
			// {title: 'Booking Summary', component: BookingSummaryPage, icon: 'fa fa-book', }
			// { title: 'Cart', component: CartPage, icon: 'fa fa-shopping-cart' }
			// {title: 'Payment', component: PaymentPage, icon: 'fa fa-rupee'}
		];

		// if (localStorage.getItem('login') == 'Y') {
		// 	this.logoutBtn = true;
		// 	this.loginBtn = false;
		// }

		if ((<any>window).cordova) {


			//Event Listener for cordova/native app
			// watch network for a disconnection
			let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
				console.log('network was disconnected :-(');
				this.checkOnline.setOnlineFlag(false);
			});

			// watch network for a connection
			let connectSubscription = this.network.onConnect().subscribe(() => {
				console.log('network connected!');
				this.checkOnline.setOnlineFlag(true);
			});
		} else {
			//Event Listener For browser
			this.windowlocal = window;
			window.addEventListener('load', () => {
				window.addEventListener('online', (event) => {
					this.checkOnline.setOnlineFlag(true);

				});
				window.addEventListener('offline', (event) => {
					this.checkOnline.setOnlineFlag(false);
				});
			});
		}

		// if(this.platform.is('cordova')){
		// 	this.fcmservice.getToken();
		// 	this.fcm.onNotification().subscribe(data => {
		// 	if(data.wasTapped){
		// 		console.log("Received in background");
		// 	} else {
		// 		console.log("Received in foreground");
		// 	};
		// 	});


		// }

	}

	// myHandlerFunction(){
	//      let toast = this.toastCtrl.create({
	//       message: "Press Again to Confirm Exit",
	//       duration: 3000
	//     });
	//     toast.present(); 
	//     }

	initializeApp() {
		// var onlineStatus = this.checkOnline.getIfOnline();
		// if (onlineStatus) {
		// 	localStorage.setItem('counter', '1');
		// }
		this.platform.ready().then(() => {
			if ((<any>window).cordova) {
				if (this.network.type === 'none' || this.network.type === 'unknown') {
					this.checkOnline.setOnlineFlag(false);
					if (localStorage.getItem('login') == 'Y') {
						this.rootPage = HomePage;
					}
					else {
						this.rootPage = LoginPage;
					}
				} else {
					this.checkOnline.setOnlineFlag(true);
					this.rootPage = IntroPage;
				}
			} else {
				var networkState = navigator.onLine;
				console.log('NS=====>', networkState);
				if (networkState) {
					this.checkOnline.setOnlineFlag(true);
					this.rootPage = IntroPage;
				} else {
					this.checkOnline.setOnlineFlag(false);
					if (localStorage.getItem('login') == 'Y') {
						this.rootPage = HomePage;
					}
					else {
						this.rootPage = LoginPage;
					}
				}
			}

			if (this.platform.is("cordova")) {
				this.LocationAccService.canRequest((result) => {
					if (result) {
						console.log("Location is on");
					} else {
						console.log("Location is off");
					}
				});
				// var notificationOpenedCallback = function(jsonData) {
				// 	console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
				//   };

				//   window["plugins"].OneSignal
				// 	.startInit("554c1929-611b-453e-8376-e76c1eeb1d7b", "26327932925")
				// 	.handleNotificationOpened(notificationOpenedCallback)
				// 	.endInit();
				// OneSignal Code start:
				// Enable to debug issues:
				// window["plugins"].OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});
				console.log('Device UUID is: ' + this.device.uuid);

				this.onesignalservice.getToken((result) => {
					this.pushTokenIds = result;
					var userDetails;
					var userID = null;
					if (localStorage.getItem('UserDetails')) {
						userDetails = JSON.parse(localStorage.getItem('UserDetails'));
						if (userDetails.length > 0) {
							userID = userDetails[0].reguserid;
						}
					}

					this.onesignalservice.registerUserTokenInfo(userID, (result) => {
						console.log('registerUserTokenInfo', result);

					});
				});

				this.onesignalservice.startInit();

				this.oneSignal.handleNotificationReceived().subscribe((result) => {
					this.events.publish('number', () => {
						this.number = this.number + 1;
						// user and time are the same arguments passed in `events.publish(user, time)`
						this.NUMBER = this.number;
						this.notiService.setNumber(this.NUMBER);
					});
				});


				this.oneSignal.handleNotificationOpened().subscribe((result) => {
					// do something when a notification is opened
					this.NUMBER = 0;

					// this.number = NUMBER ++;
					// this.number --;
					var title;
					var body;
					var res;
					var date;
					console.log('this.pushTokenIds   ', this.pushTokenIds);
					console.log('Notification Opened', result);
					res = result;
					title = res.notification.payload.title;
					body = res.notification.payload.body;
					this.nav.push(NotificationPage, { 'res': res });


				});

				this.onesignalservice.endInit();

			}

			// Okay, so the platform is ready and our plugins are available.
			// Here you can do any higher level native things you might need.
			// this.statusBar.styleDefault();
			this.splashScreen.hide();
			this.statusBar.overlaysWebView(false);
			// this.statusBar.backgroundColorByHexString('#4d83e4;');
			this.statusBar.styleLightContent();

		});

	}

	openPage(page) {
		// Reset the content nav to have just this page
		// we wouldn't want the back button to show in this scenario
		this.nav.setRoot(page.component);
	}

	gotoHome() {
		var onlineStatus = this.checkOnline.getIfOnline();
		if (onlineStatus == true) {
			var userDetails = JSON.parse(localStorage.getItem('UserDetails'));
			var param = {
				ireguserid: userDetails[0].reguserid,
			}
			this.webAPI.getService(ENUM.domain + ENUM.url.logoutHistory, param)
				.then(result => {
					console.log("Success :", result);
					this.nav.setRoot(LoginPage);
					localStorage.clear();
				}).catch(result => {
					console.log("Error :", result);
				})
		} else {
			this.presentToast('Connection Lost!!!');
		}

	}

	gotoProfile() {
		this.nav.setRoot(ProfilePage);
	}

	gotoLogin() {
		var onlineStatus = this.checkOnline.getIfOnline();
		if (onlineStatus == true) {
			this.nav.setRoot(LoginPage);
		} else {
			this.presentToast('Connection Lost!!!');
		}
	}

	presentToast(text) {
		let toast = this.toastCtrl.create({
			message: text,
			duration: 2000,
			position: 'top'
		});

		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});

		toast.present();
	}

	showAlert() {
		if (this.alert) {
			this.alert.dismiss();
			this.alert = null;
		}
		else {
			this.alert = this.alertCtrl.create({
				title: 'Exit?',
				message: 'Do you want to exit the app?',
				buttons: [{
					text: 'Cancel',
					role: 'cancel',
					handler: () => {
						this.alert = null;
					}
				}, {
					text: 'Exit',
					handler: () => {
						this.platform.exitApp();
					}
				}
				]
			});
			this.alert.present();
		}
	}

	fetchLocations() {
		var userDetails = JSON.parse(localStorage.getItem('UserDetails'));
		var userId = {
			"userId": 0
		}
		if (userDetails) {
			console.log('userDetails', userDetails)
			userId.userId = userDetails[0].reguserid
			console.log('var userid = ', userId);
		}
		this.webAPI.getLocation(ENUM.domain + ENUM.url.fetchLocations, userId)
			.then(result => {
				var temp: any = result
				console.log('getLocationResponse:', result);
				this.getlocationservice.locations = temp.data;
				this.locationService.setLocationList(temp.data);
			}).catch(error => {
				console.log('fetchlocation => ', error);
				this.locationService.setLocationList([]);
			})
	}

	fetchSections() {

		if (this.network.type === undefined || this.network.type === "none" || this.network.type === "unknown") {
			console.log('network was disconnected :-(');
			this.checkOnline.setOnlineFlag(false);
			var localsection
			if (!localStorage.getItem('localsection')) {
				this.presentToast('No Internet Connection! Please connect to Internet.');
			}
			else if (this.network.type === "ethernet" || this.network.type === "wifi" || this.network.type === "2g" || this.network.type === "3g" || this.network.type === "4g" || this.network.type === "cellular") {
				console.log('network connected!');
				this.checkOnline.setOnlineFlag(true);
				var url = ENUM.domain + ENUM.url.fetchSections
				this.webAPI.getService(url, '')
					.then(result => {
						var temp: any = result;
						this.sectionService.setSection(temp.data);
						console.log('temp.data ==>', this.sectionService.getSection());
						localStorage.setItem('localsection', JSON.stringify(temp.data));
					}).catch(error => {
						console.log('fetchSections => ', error);
						this.sectionService.setSection([]);
					});
			}
			else if (localStorage.getItem('localsection')) {
				localsection = JSON.parse(localStorage.getItem('localsection'));
				this.sectionService.setSection(localsection);
				this.presentToast('No Internet Connection! Please connect to Internet.');
			}
		}
	}

	// watch network for a connection

	// if (this.network.type === "ethernet" || this.network.type === "wifi" || this.network.type === "2g" || this.network.type === "3g" || this.network.type === "4g" || this.network.type === "cellular") {
	// console.log('network connected!');
	// this.checkOnline.setOnlineFlag(true);
	// var url = ENUM.domain + ENUM.url.fetchSections
	// this.webAPI.getService(url, '')
	// 	.then(result => {
	// 		var temp: any = result;
	// 		this.sectionService.setSection(temp.data);
	// 		console.log('temp.data ==>', this.sectionService.getSection());
	// 		localStorage.setItem('localsection', JSON.stringify(temp.data));
	// 	}).catch(error => {
	// 		console.log('fetchSections => ', error);
	// 		this.sectionService.setSection([]);
	// 	});
	// }
	// else{
	// 	this.presentToast('No Internet Connection! Please connect to Internet.');
	// }


	allServices() {
		if (this.network.type === "ethernet" || this.network.type === "wifi" || this.network.type === "2g" || this.network.type === "3g" || this.network.type === "4g" || this.network.type === "cellular") {
			this.checkOnline.setOnlineFlag(true);
			var hcId = localStorage.getItem('hcId');
			var url = ENUM.domain + ENUM.url.entryRates;
			let param = {
				"hId": hcId
			}
			this.webAPI.getService(url, param)
				.then(result => {
					var temp: any = result;
					localStorage.setItem('localentryrates', JSON.stringify(temp.data));
					console.log('allServices temp for entryRates ', temp.data);
				}).catch(error => {
					console.log('entryRates => ', error);

				});
		}
	}

	fetchPoints(){
		let mob;
		mob = localStorage.getItem('mobileNumber');
		let param = {
		  mob: mob
		}
		var url = ENUM.domain + ENUM.url.fetchPoints;
		this.webAPI.getService(url, param)
		.then(res =>{
		  console.log('points fetched ',res);
		  let result;
		  result = res;
		  result = result.data[0];
		  this.mobile = result.mobileno;
		  this.init_points = result.totalP;
		  this.value = result.value;
		  this.name = result.regusername;
		  let param = {
			name: this.name,
			value: this.value
		  }
		  console.log('result ',result);
		  if(this.value > this.init_points){
			this.openModal(param);
		  }
		  
		}).catch(err =>{
		  console.log('points fetched ',err);
		});
	
	  }

	  openModal(param){

		  const value = param;
		  const myModal: Modal = this.modal.create(ModalpagePage, {value: param}, { cssClass: 'modalPop' });
		  myModal.present();
	  }

}
