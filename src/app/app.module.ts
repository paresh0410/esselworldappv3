import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, ElementRef } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';


//import { Geofence } from '@ionic-native/geofence';
import { Geolocation } from '@ionic-native/geolocation';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { ScrollableTabs } from '../components/scrollable-tabs/scrollable-tabs';
import { SwipeTabComponent } from '../components/swipe-tab/swipe-tab';
import { EpandableComponent } from '../components/epandable/epandable';
// import { PinchZoomModule, PinchZoomComponent } from 'ngx-pinch-zoom';
import { HttpModule ,Http } from '@angular/http';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { Network } from '@ionic-native/network';
import { SocialSharing } from '@ionic-native/social-sharing';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MyApp } from './app.component';
import { IonicTabSliderModule } from 'ionic-tab-slider';
// import { SuperTabsModule } from 'ionic2-super-tabs';
import { IonAffixModule } from 'ion-affix';
import { ImgFallbackModule } from 'ngx-img-fallback';

import { IntroPage } from '../pages/intro/intro';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { BookingHistoryPage } from '../pages/booking-history/booking-history';
import { SchedulePage } from '../pages/schedule/schedule';
import { GeofencePage } from '../pages/geofence/geofence';
import { RegistrationPage } from '../pages/registration/registration';
import { LoginPage } from '../pages/login/login';
import { NotificationPage } from '../pages/notification/notification';
import { NotificationService } from '../service/notification.service';
import { OffersPage } from '../pages/offers/offers';
import { TabsPage } from '../pages/tabs/tabs';
import { TodoPage } from '../pages/todo/todo';
import { ProfilePage } from '../pages/profile/profile';
import { BookTicketPage } from '../pages/book-ticket/book-ticket'
import { ParkTimingsPage } from '../pages/park-timings/park-timings';
import { FoodBeveragesPage } from '../pages/food-beverages/food-beverages';
import { GamesPage } from '../pages/games/games';
import { EventsPage } from '../pages/events/events';
import { GroupsNschoolsPage } from '../pages/groups-nschools/groups-nschools';
import { GuideMapPage } from '../pages/guide-map/guide-map';
import { OtpPage } from '../pages/otp/otp';
import { RegisterNewPage } from '../pages/register-new/register-new';
import { SectionPage } from '../pages/section/section';
import { RidePage } from '../pages/ride/ride';
import { RideDetailsPage } from '../pages/ride-details/ride-details';
import {NoInternetConnectionPage} from '../pages/no-internet-connection/no-internet-connection';
import { GroupsNschoolsDetailsPage } from '../pages/groups-nschools-details/groups-nschools-details';
import { EventsDetailsPage } from '../pages/events-details/events-details';
import { OffersDetailsPage } from '../pages/offers-details/offers-details';
import { TicketCategoryPage } from '../pages/ticket-category/ticket-category';
import { FAndBCategoryPage } from '../pages/f-and-b-category/f-and-b-category';
import { PaidAttractionsPage } from '../pages/paid-attractions/paid-attractions';
import { BookingSummaryPage } from '../pages/booking-summary/booking-summary';
import { UserDetailsPage } from '../pages/user-details/user-details';
import { CartPage } from '../pages/cart/cart';
import { BookingPage } from '../pages/booking/booking';
import { BookingHistoryListPage } from '../pages/booking-history-list/booking-history-list';
import { BookhomecategoryPage } from '../pages/bookhomecategory/bookhomecategory';
import { SaftyInstructionPage } from '../pages/safty-instruction/safty-instruction';
// import { PaymentPage } from '../pages/payment/payment';
import { SelectParkPage } from '../pages/select-park/select-park';
import { TermsAndConditionsPage } from '../pages/terms-and-conditions/terms-and-conditions';
import { InstructionsService } from '../service/instructions.service';

import { ScrollableTabComponent } from '../components/scrollable-tab';
import { SwipeSegmentDirective } from '../directives/swipe-segment/swipe-segment';
import { MenuServiceProvider } from '../service/menu-service';
import { webAPIService } from '../service/webAPIService';
import { checkIfOnlineService } from '../service/checkIfOnlineService';
import { HomeServiceProvider } from '../service/home-service';
import { getLocationService } from '../service/getlocation.service';
// import { EntryRatesServiceProvider } from '../service/entryrates-service'
import {LocationInfoService} from '../service/LocationInfoService';
import { ParallaxHeader } from '../directives/parallax-header/parallax-header';
import { Diagnostic } from '@ionic-native/diagnostic';
import { EntryRatesServiceProvider } from '../service/entryrates-service';
import { RidesService } from '../service/ride.service';
import { LoggedInUserService } from '../service/loggedInUser.service';
import { LocationAccuracyService } from '../service/LocationAccuracyService';
import {LocationAccuracy} from '@ionic-native/location-accuracy';
import { LaunchNavigator } from '@ionic-native/launch-navigator'
import { FoodsService } from '../service/food.service';
import { GameService } from '../service/game.service';
import { SectionService } from '../service/SectionService';
import { bookingService } from '../service/bookingService';
import { YoutubePipe } from '../pipes/youtube/youtube';
import { Safe } from '../pipes/safeHtml/safeHtml';
import { PinchZoomModule, PinchZoomComponent } from 'ngx-pinch-zoom';
import { AppAvailability } from '@ionic-native/app-availability';
// import { FCM } from '@ionic-native/fcm';
// import { FCMService } from '../service/fcmservice';
import { Camera } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
import { Base64 } from '@ionic-native/base64/ngx';
import { OneSignal } from '@ionic-native/onesignal';
import { Device } from '@ionic-native/device';
import{ OneSignalService } from '../service/oneSignalService';
import {TicketCategoryService} from '../service/ticketCategory.service';
import { fnbCategoryService } from '../service/fnbCategory.service';
import { paidAttractionsService } from '../service/paidAttractions.service';
import { bookingSummaryService } from '../service/bookingSummary.service';
import { TermsAndConditionsService } from '../service/termsAndConditions.service';
import { ZoomAreaModule } from 'ionic2-zoom-area';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicSwipeAllModule } from 'ionic-swipe-all';
import { TruncateModule } from 'ng2-truncate';
import { DatePickerModule } from 'ionic-calendar-date-picker';
import { WebView } from '@ionic-native/ionic-webview';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { WebIntent } from '@ionic-native/web-intent';
import { AES256 } from '@ionic-native/aes-256';
import { ThemeServiceProvider } from '../providers/theme-service/theme-service';
import { PaymentPage } from '../pages/payment/payment';
import { RewardPage } from '../pages/reward/reward';
import { ModalpagePage } from '../pages/modalpage/modalpage';
import { DoorgetsTruncateModule } from 'doorgets-ng-truncate';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    IntroPage,
    ListPage,
    BookingHistoryPage,
    SchedulePage,
    GeofencePage,
    NotificationPage,
    RegistrationPage,
    LoginPage,
    OffersPage,
    OffersDetailsPage,
    ScrollableTabComponent,
    ScrollableTabs,
    SwipeTabComponent,
    EpandableComponent,
    TabsPage,
    TodoPage,
    ProfilePage,
    BookTicketPage,
    ParkTimingsPage,
    FoodBeveragesPage,
    GamesPage,
    EventsPage,
    EventsDetailsPage,
    GroupsNschoolsPage,
    GroupsNschoolsDetailsPage,
    GuideMapPage,
    SectionPage,
    OtpPage,
    RegisterNewPage,
    SwipeSegmentDirective ,
    RidePage,
    RideDetailsPage,
    NoInternetConnectionPage,
    TicketCategoryPage,
    YoutubePipe,
    Safe,
    FAndBCategoryPage,
    PaidAttractionsPage,
    BookingSummaryPage,
    UserDetailsPage,
    CartPage,
    BookingPage,
    SaftyInstructionPage,
    BookingHistoryListPage,
    SelectParkPage,
    TermsAndConditionsPage,
    BookhomecategoryPage,
    PaymentPage,
    RewardPage,
    ModalpagePage
  ],
  imports: [
    
    BrowserModule,
    HttpClientModule,
    IonAffixModule,
    IonicModule.forRoot(MyApp),
    IonicTabSliderModule,
    PinchZoomModule,
    // SuperTabsModule.forRoot(),
    NgxQRCodeModule,
    // PinchZoomModule,
    // PinchZoomComponent,
    HttpModule,
    AngularFontAwesomeModule,
    ImgFallbackModule,
    ZoomAreaModule.forRoot(),
    BrowserAnimationsModule,
    IonicSwipeAllModule,
    TruncateModule,
    DatePickerModule,
    DoorgetsTruncateModule
  ],
  exports: [
    ScrollableTabs,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    IntroPage,
    HomePage,
    ListPage,
    BookingHistoryPage,
    SchedulePage,
    GeofencePage,
    NotificationPage,
    RegistrationPage,
    LoginPage,
    OffersPage,
    OffersDetailsPage,
    TabsPage,
    TodoPage,
    ProfilePage,
    BookTicketPage,
    ParkTimingsPage,
    FoodBeveragesPage,
    GamesPage,
    EventsPage,
    EventsDetailsPage,
    GroupsNschoolsPage,
    GroupsNschoolsDetailsPage,
    GuideMapPage,
    SectionPage,
    OtpPage,
    RegisterNewPage,
    RidePage,
    RideDetailsPage,
    NoInternetConnectionPage,
    TicketCategoryPage,
    FAndBCategoryPage,
    PaidAttractionsPage,
    UserDetailsPage,
    CartPage,
    BookingSummaryPage,
    BookingPage,
    SaftyInstructionPage,
    BookingHistoryListPage,
    SelectParkPage,
    TermsAndConditionsPage,
    BookhomecategoryPage,
    PaymentPage,
    RewardPage,
    ModalpagePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    //Geofence,
    MenuServiceProvider,
    Geolocation,
    webAPIService,
    checkIfOnlineService,
  	HomeServiceProvider,
    Network,
    getLocationService,
    EntryRatesServiceProvider,
    LocationInfoService,
    SectionService,
    ParallaxHeader,
    Diagnostic,
    RidesService,
    LoggedInUserService,
    LocationAccuracyService,
    LocationAccuracy,
    FoodsService,
    GameService,
    SocialSharing,
    LaunchNavigator,
    YoutubeVideoPlayer,
    // FCM,
    // FCMService,
    // PinchZoomModule,
    // PinchZoomModule,
    // PinchZoomComponent,
    Camera,
    ImagePicker,
    Base64,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    OneSignal,
    Device,
    OneSignalService,
    AppAvailability,
    NotificationService,
    TicketCategoryService,
    fnbCategoryService,
    paidAttractionsService,
    bookingSummaryService,
    bookingService,
    InstructionsService,
    TermsAndConditionsService,
    InAppBrowser,
    WebIntent,
    AES256,
    HttpClient,
    HttpClientModule,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ThemeServiceProvider,
    
  ]
})
export class AppModule {}
