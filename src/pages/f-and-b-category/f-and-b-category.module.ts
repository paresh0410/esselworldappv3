import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FAndBCategoryPage } from './f-and-b-category';

@NgModule({
  declarations: [
    FAndBCategoryPage,
  ],
  imports: [
    IonicPageModule.forChild(FAndBCategoryPage),
  ],
})
export class FAndBCategoryPageModule {}
