import { Component, ViewChild, ElementRef, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ActionSheetController, AlertController, Events } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { NotificationPage } from '../notification/notification';
import { HomePage } from '../home/home';
import { Camera, CameraOptions } from '@ionic-native/camera'
import { ImagePicker } from '@ionic-native/image-picker';
import { Base64 } from '@ionic-native/base64/ngx';
import { AppAvailability } from '@ionic-native/app-availability/';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Environment,
  HtmlInfoWindow,
  BaseArrayClass,
  MyLocation,
  GoogleMapsAnimation,
  MyLocationOptions,
  LatLng,
  Circle,
  CircleOptions,
  MarkerIcon,
  LocationService
} from '@ionic-native/google-maps';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationInfoService } from '../../service/LocationInfoService';
import { webAPIService } from '../../service/webAPIService';
import { ENUM } from '../../service/ENUM';
import { LocationAccuracyService } from '../../service/LocationAccuracyService';
import { SocialSharing } from '@ionic-native/social-sharing';
import { CartPage } from '../cart/cart';
import { ThemeServiceProvider } from '../../providers/theme-service/theme-service';

/**
 * Generated class for the GeofencePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;

@IonicPage()
@Component({
  selector: 'page-geofence',
  templateUrl: 'geofence.html',
})
export class GeofencePage implements OnDestroy {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  // htmlInfoWindow = new HtmlInfoWindow();
  GeoMarker: any;
  sunCircle: any;
  // public number: any = 2;
  locations = [];
  infoWindows: any = [];
  content: any;
  navigationid: any;
  watchID: any;
  pos: any;
  Mymarker: any;
  cityCircle: any;
  gotoloc: any = null;
  km: any;
  Area: any;
  items: any = [];
  markers: any = [];
  marker: any;
  currentpositionimage: any;
  currentnativepositionimage: MarkerIcon;
  previousMarkerOpen: any;
  nativeCircle: Circle;
  data = [];
  htmlNativeInfoWindow: any;
  locationdetail: any = {};
  locationshow: boolean = false;
  previousMarker: any;
  tempMarkerLength: number = 0;
  previousPosition: any;
  itemlike: boolean;
  itemdislike: boolean;
  currentIndex: number = 0;
  seenStatus: boolean = false;
  resetImg: any;
  shareActionSheet: any;
  stable: boolean = false;
  ThemeC: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public geolocation: Geolocation,
    public locationinfoservice: LocationInfoService, public plt: Platform,
    public diagnostic: Diagnostic, public webapi: webAPIService, public changeDetRef: ChangeDetectorRef,
    public LocationAccService: LocationAccuracyService, public actionSheetController: ActionSheetController,
    private socialSharing: SocialSharing, private alertCtrl: AlertController, public themeServices: ThemeServiceProvider,
    private camera: Camera, private imagePicker: ImagePicker,public events: Events,
     private base64: Base64,private appAvailability: AppAvailability) {
      this.ThemeC = this.themeServices.getTheme();
      this.events.subscribe('theme', () => {
        this.ThemeC = this.themeServices.getTheme();
      });
    //this.data = locationinfoservice.getLocationList();
    if (localStorage.getItem('login') == 'Y') {
      this.data = locationinfoservice.getCloudLocationList();
      for (var i = 0; i < this.data.length; i++) {

        if (this.data[i].icon) {
          this.data[i].icon = ENUM.domain + ENUM.url.imgUrl + this.data[i].icon;
        }
      }
    } else {
      this.alertforLogin();
    }
  }

  getLikeStatus(item) {

    this.locationdetail.locname = item.locname;
    this.locationdetail.loctext = item.loctext;
    this.locationdetail.isactive = item.isactive;
    this.locationdetail.locimgpath = item.locimgpath;
    this.locationdetail.loclat = item.loclat;
    this.locationdetail.loclong = item.loclong;
    this.locationdetail.locseen = item.locseen;
    this.locationdetail.share = item.share;
    this.locationdetail.lcid = item.lcid;
    this.locationdetail.locid = item.locid;
    var userDetails = JSON.parse(localStorage.getItem('UserDetails'));
    var userid = userDetails[0].reguserid
    let param = {

      "lId": this.locationdetail.locid,
      "userId": userid,
    }
    var url = ENUM.domain + ENUM.url.getLikeStatus;
    this.webapi.getService(url, param)
      .then(result => {
        console.log('result =>', result);
        var res: any = result;
        var temp = res.data[0];
        var temp2 = res.data[1];
        console.log('temp ======>', temp[0]);
        if (temp[0].like == null) {
          this.locationdetail.like = 0;
          this.locationdetail.RegLikeStatus = null;
          //this.locationdetail.dislikes = 0;
        } else {
          this.locationdetail.like = temp[0].like;
          this.locationdetail.dislike = temp[0].dislike;
          this.locationdetail.RegLikeStatus = temp[0].RegLikeStatus;
        }
        if (temp[0].dislike == null) {
          //this.locationdetail.likes = 0;
          this.locationdetail.dislike = 0;
          this.locationdetail.RegLikeStatus = null;
        } else {
          this.locationdetail.like = temp[0].like;
          this.locationdetail.dislike = temp[0].dislike;
          this.locationdetail.RegLikeStatus = temp[0].RegLikeStatus;
        }

        console.log('reglikestatus', this.locationdetail.RegLikeStatus);

        if (this.locationdetail.RegLikeStatus == "0") {
          this.itemlike = false;
          this.itemdislike = true;
        } else if (this.locationdetail.RegLikeStatus == "1") {
          this.itemlike = true;
          this.itemdislike = false;
        } else {
          this.itemlike = false;
          this.itemdislike = false;

        }
        if (temp2[0].locseen) {
          this.seenStatus = true;
        }
        else {
          this.seenStatus = false;
        }
        this.changeDetRef.detectChanges();
      });


  }

  ionViewDidLoad() {
    this.currentpositionimage = {
      path: "M-20,0a20,20 0 1,0 40,0a20,20 0 1,0 -40,0",
      fillColor: '#488aff',
      fillOpacity: .8,
      anchor: new google.maps.Point(0, 0),
      strokeWeight: 0,
      scale: 0.5
    };

    //this.loadMap();
    // if(this.plt.is("cordova")){
    //   // this.currentnativepositionimage = {
    //   //   path: "M-20,0a20,20 0 1,0 40,0a20,20 0 1,0 -40,0",
    //   //   fillColor: '#488aff',
    //   //   fillOpacity: .8,
    //   //   anchor: new google.maps.Point(0,0),
    //   //   strokeWeight: 0,
    //   //   scale: 0.5
    //   // };
    //   //this.startWatcher();
    //   //this.initializeNative();
    // }else{
    //   this.initialize();  
    // }
    this.startWatcher();
  }
  checkLocation(cb) {
    this.plt.ready().then((readySource) => {

      this.diagnostic.isLocationEnabled().then(
        (isAvailable) => {
          cb(true);
        }).catch((e) => {
          console.log(e);
          cb(false);
          alert(JSON.stringify(e));
        });
    });
  }
  ngOnDestroy() {

    if (this.plt.is("cordova")) {
      //this.initializeNative();
      if (this.map) {
        this.map.remove();
        this.map = null;
      }
      // if(this.watchID){
      //   navigator.geolocation.clearWatch(this.watchID);
      // }
    } else {
      if (this.watchID) {
        navigator.geolocation.clearWatch(this.watchID);
      }
    }
    this.watchID = null;
    return null;
  }
  startWatcher() {

    if (this.plt.is("cordova")) {
      if (this.map) {
        this.map.remove();
      }
      if (this.watchID) {
        navigator.geolocation.clearWatch(this.watchID);
      }
      this.LocationAccService.canRequest((result) => {
        if (result) {
          this.initializeNative();
        }
      })
      // this.checkLocation(data=>{
      //   if(data){
      //     this.initializeNative();
      //   }  
      // })
      //this.initializeNative();
    } else {
      if (this.watchID) {
        navigator.geolocation.clearWatch(this.watchID);
      }
      this.watchID = null;

      this.initialize();
    }


  }
  // loadMap() {

  //   // This code is necessary for browser
  // Environment.setEnv({
  //   'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyDEb15bVQB-4mzlNoC7LXhrmoQpFArVs6Y',
  //   'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyDEb15bVQB-4mzlNoC7LXhrmoQpFArVs6Y'
  // });

  //   this.geolocation.getCurrentPosition().then((position) => {

  //     //let latLng = new google.maps.LatLng(19.2312954, 72.8049919);
  //     let latLng = new google.maps.LatLng(19.2593211,72.9723);
  //     let mapOptions = {
  //       center: latLng,
  //       zoom: 17,
  //       mapTypeControl: false,
  //       draggable: true,
  //       // scaleControl: false,
  //       // scrollwheel: false,
  //       // navigationControl: false,
  //       // streetViewControl: false,
  //       mapTypeId: google.maps.MapTypeId.ROADMAP,
  //       disableDefaultUI: true
  //     }

  //     this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
  //   //   let mapOptions: GoogleMapOptions = {
  //   //   controls: {
  //   //     //'myLocationButton': true,
  //   //     'myLocation': true,   // (blue dot)
  //   //   },
  //   //   camera: {
  //   //      target: {
  //   //        lat: 19.2329813,
  //   //        lng: 72.8043836
  //   //      },
  //   //      zoom: 14,
  //   //      tilt: 30
  //   //    }
  //   // };

  //   //this.map = GoogleMaps.create('map_canvas', mapOptions);
  //     this.sunCircle = {
  //       strokeColor: "#41518b",
  //       strokeOpacity: 0.8,
  //       strokeWeight: 2,
  //       fillColor: "#a2e2b8",
  //       fillOpacity: 0.35,
  //       map: this.map,
  //       center: latLng,
  //       radius: 150 // in meters
  //     };
  //     this.getCurrentLocation();
  //     //this.addMarker(latLng);
  //   }, (err) => {
  //     console.log(err);
  //   });

  // }

  // addMarker(currentlatlong) {

  //   let marker:any;

  //   let this_ref = this;

  //   let classname:any = {
  //       thumbsup:'',
  //       thumbsdown:'',
  //       share:'',
  //       seen:'',
  //     }
  //   this.data.forEach((item: any) => {
  //     classname = {
  //       thumbsup:'',
  //       thumbsdown:'',
  //       share:'share',
  //       seen:'',
  //     }
  //     // if(item.like){
  //     //   classname.thumbsup = 'thumbsup';
  //     // }
  //     // if(item.dislike){
  //     //   classname.thumbsdown = 'thumbsdown';
  //     // }
  //     if(item.seen){
  //       classname.seen = 'seen';
  //     }



  //     this.content = `
  //    <div style="width= 280px;">
  //   <div " class="infowindow" id="` + item.lcid + `" class="item-thumbnail-left item-text-wrap" style="display:inline-flex;width=280px;"> 
  //         <img style="height:166px; float:left; width:100px; text-align:center;" src = "`+ item.locimgpath + `" onError="this.src='assets/imgs/nopic.png'"></img>
  //       <div style="float:left; height:166px; margin-top= width:max-content; padding-left:5px; font-size:1.0rem; " >
  //         <p style="font-size: 1.3em !important;font-weight:bold;margin-top: 0 !important;">`+ item.locname + `</p> 
  //        <p style="margin-top: -7px !important;font-size: 1.3em !important;width: 25vw;">`+ item.loctext + `</p>
  //     </div>
  //   </div>
  //   </div>
  //   <div class="footer" style=" height:25px;margin-top:10px;text-align: center;"">
  //   <!--<img style="height: 20px;float: left;width: 20px;text-align: center;" src = "`+ item.icon1 + `" onError="this.src='assets/imgs/nopic.png'"></img>
  //   <img style="height: 20px;width: 20px;text-align: center;margin-left: 10vw !important;" src = "`+ item.icon2 + `" onError="this.src='assets/imgs/nopic.png'"></img>
  //   <img style="height: 20px;width: 20px;text-align: center;margin-left: 10vw !important;" src = "`+ item.icon3 + `" onError="this.src='assets/imgs/nopic.png'"></img>
  //   <img style="height: 20px;float: right;width: 20px;text-align: center;margin-left: 10vw !important;" src = "`+ item.icon4 + `" onError="this.src='assets/imgs/nopic.png'"></img> -->
  //   <span class="infoicons">
  //   <i class="fa fa-thumbs-o-up thumbsup"></i>
  //   <span class="number">`+item.like+`</span>
  //   </span>
  //   <span class="infoicons"><i class="fa fa-thumbs-o-down thumbsdown"></i> <span class="number">`+item.dislike+`</span></span>
  //   <span class="infoicons"><i class="fa fa-share-alt `+classname.share+`"></i></span>
  //   <span class="infoicons"><i class="fa fa-check-circle `+classname.seen+`"></i></span>
  //   </div>`;
  //     let latlng = {
  //       'lat': Number(item.loclat), 'lng': Number(item.loclong), 'info': this.content, id: item.lcid,
  //       'price': {
  //         text: item.locname,
  //         color: 'white',
  //       },
  //     };
  //     this_ref.locations.push(latlng);
  //     var position = new google.maps.LatLng(item.loclat, item.loclong);
  //       marker = new google.maps.Marker({
  //               position: position,
  //               map: this.map,
  //               title: item.locname
  //           });
  //       this.markers.push(marker);
  //     this.addInfoWindow(marker, this.content);
  //   });

  //   marker = new google.maps.Marker({
  //     map: this.map,
  //     animation: google.maps.Animation.DROP,
  //     position: currentlatlong,
  //     draggable: false,
  //     visible: true,
  //     icon:this.currentpositionimage//'../assets/imgs/currentlocation.png'
  //   });
  //   this.markers.push(marker);
  //   marker.setMap(this.map);
  //   let cityCircle = new google.maps.Circle(this.sunCircle);
  //   cityCircle.bindTo('center', marker, 'position');

  // }

  // addInfoWindow(marker, content) {


  //   let infoWindow = new google.maps.InfoWindow({
  //     content: this.content
  //   });

  //   google.maps.event.addListener(marker, 'click', () => {
  //     infoWindow.open(this.map, marker);
  //   });

  // }


  // // addCluster() {


  // //   let markers = this.locations.map((location, i) => {

  // //     let price = location.price;
  // //     let color = '#93c01f';
  // //     let marker: Marker = this.map.addMarkerSync({
  // //       position: location,
  // //       label: {
  // //         text: price.text,
  // //         color: 'white',
  // //         id: location.id
  // //       },
  // //       icon: { url: this.img(price.text, color) },
  // //     });
  // //     // icon: { url: 'assets/icon/icon1.png' }
  // //     //  this.addinfowindow(location.info);
  // //     marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe((data) => {
  // //       this.addinfowindow(location.info, location.id);
  // //       this.htmlInfoWindow.open(marker);
  // //     });
  // //     return marker;
  // //   });


  // // }

  // // addinfowindow(info, id) {

  // //   let frame: HTMLElement = document.createElement('div');
  // //   frame.innerHTML = [
  // //     info
  // //   ].join("");
  // //   frame.addEventListener("click", () => {
  // //     console.log('marker clicked ', id);
  // //     this.closeAllInfoWindows();
  // //   });

  // //   this.htmlInfoWindow.setContent(frame, {
  // //     width: "280px",
  // //     height: "128px"
  // //   });
  // //   this.infoWindows.push(this.htmlInfoWindow);
  // // }

  // // closeAllInfoWindows() {
  // //   for (let window of this.infoWindows) {
  // //     window.close();
  // //   }
  // // }

  img(name, color) {
    //console.log(name);
    let canvas, context, str = '';

    canvas = document.createElement("canvas");
    canvas.width = 61;
    canvas.height = 35;
    let x = 1, y = 1, width = 60, height = 25, radius = 0, stroke = true;
    context = canvas.getContext("2d");
    if (typeof stroke == "undefined") {
      stroke = true;
    }
    if (typeof radius == "undefined") {
      radius = 5;
    }
    context.beginPath();
    context.moveTo(x + radius, y);
    context.lineTo(x + width - radius, y);
    context.quadraticCurveTo(x + width, y, x + width, y + radius);
    context.lineTo(x + width, y + height - radius);
    context.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
    // context.lineTo(x + radius, y + height);
    context.lineTo(30, y + height);
    context.lineTo(25, y + height + 5);
    context.lineTo(20, y + height);
    context.lineTo(0, y + height);
    context.quadraticCurveTo(x, y + height, x, y + height - radius);
    context.lineTo(x, y + radius);
    context.quadraticCurveTo(x, y, x + radius, y);
    context.fillStyle = color;
    context.fill();
    context.closePath();
    // if (stroke) {
    //   context.stroke();
    // }
    context.lineWidth = 1;
    context.strokeStyle = "#ffffff";
    context.font = "12px Arial";
    context.textAlign = "center";
    context.fillStyle = "white";
    context.fillText(name, 25, 18);
    str = canvas.toDataURL("image/jpg");
    //console.log(str);
    return str;
  }


  gotornotificationPage() {
    this.navCtrl.push(NotificationPage);
    console.log('notification.');
  }


  // getCurrentLocation(){
  //   // Get the location of you
  //   let options = {
  //       enableHighAccuracy: false,
  //       timeout: 5000,
  //       maximumAge: 0
  //     };
  //   if(navigator){
  //     this.navigationid = navigator.geolocation.watchPosition((pos)=>{
  //       // Move the map camera to the location with animation
  //       var crd = pos.coords;
  //       let latLng = new google.maps.LatLng(crd.latitude, crd.longitude);
  //       // this.map.animateCamera({
  //       //   target: latLng,
  //       //   zoom: 17,
  //       //   tilt: 30
  //       // })

  //       console.log('Congratulations, you reached the target');
  //       //navigator.geolocation.clearWatch(this.navigationid);
  //     }, 
  //     (err)=>{
  //       console.warn('ERROR(' + err.code + '): ' + err.message);
  //     }, options);  
  //   }

  //   // this.map.getMyLocation()
  //   //   .then((location: MyLocation) => {
  //   //     console.log(JSON.stringify(location, null ,2)); 
  //   //     // Move the map camera to the location with animation
  //   //     this.map.animateCamera({
  //   //       target: location.latLng,
  //   //       zoom: 17,
  //   //       tilt: 30
  //   //     })
  //   //     .then(() => {
  //   //       this.addMarker(location.latLng);
  //   //     });
  //   //   });
  // }




  /******* init code start *******/
  initialize() {
    /********* delcaration starts *********/

    let icon = {
      //url: 'img/map-marker.png', // url
      scaledSize: new google.maps.Size(50, 50), // scaled size
      //origin: new google.maps.Point(0,0), // origin
      //anchor: new google.maps.Point(0, 0) // anchor
    };

    /*var neighborhoods = [
          {lat: 19.213250, lng: 72.83515},
          {lat: 19.213210, lng: 72.8361},
          {lat: 19.213285, lng: 72.8352},
          {lat: 19.213265, lng: 72.8334}
       ];*/
    let neighborhoods = [];
    //var image = 'https://cdn4.iconfinder.com/data/icons/flat-icons-in-blue/201/location-people.png';
    //let myLatlng = new google.maps.LatLng(-34.397,150.644);  ///coord's to be draw
    let myLatlng = new google.maps.LatLng(19.2593211, 72.9723);
    this.km = 0.1;
    this.Area = 700;

    //var geocoder = new google.maps.Geocoder;   /// Gecoder define for reverse coding location and more.
    //var infowindow = new google.maps.InfoWindow; /// Info windows define for all windows to be open.


    let mapOptions = [{              /// Basic map options used by the map/s 
      //streetViewControl:false,
      //center: pos,            
      zoom: 20,
      mapTypeId: google.maps.MapTypeId.SATELLITE,
      myLocation: true,
      //backgroundColor:"transparent"
      // disableDefaultUI: true,
      //zoomControl: true,
      //zoomControlOptions: {
      //style: google.maps.ZoomControlStyle.SMALL,
      //position: google.maps.ControlPosition.LEFT_CENTER
      //},
      //disableDoubleClickZoom: true,
      //scrollwheel: false,
    },
    {
      "elementType": "labels",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "administrative.neighborhood",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "transit.station.rail",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "visibility": "on"
        }
      ]
    }
    ]

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);    ///map init/instance/define to use map on html.

    /*var infowindow = new google.maps.InfoWindow({  /// reinitizing the map info windows with defalut content in it "made as an optional".
      content:'Click to zoom'
      });
        */
    var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';  ///labels are declared for furture use.
    var labelIndex = 20;              ///index to be init.

    // this.Mymarker = new google.maps.Marker({      ///marker init and loaded with default options.
    //       //position: pos ,
    //       map: this.map,
    //   //label: labels[labelIndex++ % labels.length],    
    //       draggable:false,
    //       animation: google.maps.Animation.DROP,
    //       title: 'Click to zoom',
    //   //icon: icon
    //     });

    this.cityCircle = new google.maps.Circle({
      strokeColor: '#FFF',
      strokeOpacity: 0.8,
      strokeWeight: 0,
      fillColor: '#79bdbd',
      fillOpacity: 0.35,
      map: this.map,
      center: myLatlng,
      radius: this.km * 1000
    });


    // 3 seconds after the center of the map has changed, pan back to the marker
    google.maps.event.addListener(this.map, 'center_changed', function () {
      // setTimeout(()=> {

      // },3000);
      //this.map.panTo(this.gotoloc);
    });

    // Zoom to 16 when clicking on marker
    // google.maps.event.addListener(this.Mymarker,'click',function() {
    //   this.map.setZoom(16);
    //   this.map.setCenter(this.Mymarker.getPosition());
    //    if (this.infowindow) {
    //      this.infowindow.close(this.map,this.Mymarker);
    //      this.gotoloc=this.Mymarker.getPosition();
    //    }
    //   });

    /*google.maps.event.addListener(marker,'click',function() {
      map.setZoom(16);
      map.setCenter(marker.getPosition());
      });  */

    /*google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
      });*/

    /********* All Listener list ends *********/


    /********* geolocation code starts *********/
    // Try HTML5 geolocation.
    var posOptions = {
      enableHighAccuracy: true,
      timeout: 20000,
      maximumAge: 0
    };
    if (navigator.geolocation) {
      //navigator.geolocation.getCurrentPosition(this.onMapSuccess, this.onMapError,posOptions);
      // navigator.geolocation.getCurrentPosition((position)=>{
      //       this.pos = {
      //       lat: position.coords.latitude,
      //       lng: position.coords.longitude
      //   };
      // this.watchID = navigator.geolocation.watchPosition((position)=>{
      //       this.pos = {
      //       lat: position.coords.latitude,
      //       lng: position.coords.longitude
      //   };

      // this.getMap(this.pos.lat, this.pos.lng,this.km);
      // },
      // (error)=>{
      //   console.log('code: ' + error.code + '\n' +
      //     'message: ' + error.message + '\n');
      //     alert('Location is off');
      // },posOptions)

      this.watchID = navigator.geolocation.watchPosition((position) => {
        this.pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        setTimeout(() => {
          this.clearMarkers();
          this.setCityCircle(this.pos);
          this.getMap(this.pos.lat, this.pos.lng, this.km);
        }, 500);
        //this.addMarker(latLng);

      },
        (error) => {
          console.log('code: ' + error.code + '\n' +
            'message: ' + error.message + '\n');
          alert('Location is off');
        }, posOptions);
    } else {
      // Browser doesn't support Geolocation
      console.log("Browser doesn't support Geolocation")
      //this.handleLocationError(false, this.map.getCenter());
    }
  }
  setCityCircle(myLatlng) {
    this.cityCircle = new google.maps.Circle({
      strokeColor: '#FFF',
      strokeOpacity: 0.8,
      strokeWeight: 0,
      fillColor: '#79bdbd',
      fillOpacity: 0.35,
      map: this.map,
      center: myLatlng,
      radius: this.km * 1000
    });
  }
  // pinSymbol(color) {
  //   return {
  //     path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z M -2,-30 a 2,2 0 1,1 4,0 2,2 0 1,1 -4,0',
  //     //path: 'M 0,0 -1,-2 V -43 H 1 V -2 z M 1,-40 H 30 V -20 H 1 z',
  //     fillColor: color,
  //     fillOpacity: 1,
  //     strokeColor: '#000',
  //     strokeWeight: 0.9,
  //     scale: 1,
  //   };
  // }


  onMapSuccess(position) {

    this.pos = {
      lat: position.coords.latitude,
      lng: position.coords.longitude
    };

    this.getMap(this.pos.lat, this.pos.lng, this.km);
    window.setTimeout(function () {
      //this.watchMapPosition();
    }, 5000)

  }
  // Get map by using coordinates

  getMap(latitude, longitude, km) {
    var remaped = new google.maps.LatLng(this.pos.lat, this.pos.lng);  ///coord's remaped for My marker
    this.items = [];
    console.debug("position:", this.pos);
    //pos = remaped;
    let marker = new google.maps.Marker({
      map: this.map,
      //animation: google.maps.Animation.DROP,
      position: remaped,
      draggable: false,
      icon: this.currentpositionimage//'../assets/imgs/currentlocation.png'
    });
    this.markers.push(marker);
    //infowindow.setPosition(pos);
    //infowindow.setContent('<b class="positive">Location found</b>.<br> <i class="assertive">Click any to zoom.</i>');
    this.map.setCenter(this.pos);
    //this.Mymarker.setPosition(remaped);
    //this.cityCircle.visible = false;
    this.cityCircle.setCenter(remaped);
    //this.gotoloc=this.Mymarker.getPosition();
    //this.drop(this.items)
    //this.map.clearOverlays();

    for (var i = 0; i < this.data.length; i++) {
      this.getDistanceFromLatLonInKm(this.pos.lat, this.pos.lng, this.data[i].loclat, this.data[i].loclong, (d, lat, lng) => {
        let item = this.data[i];
        if (d <= km) {

          this.items.push(item);
          this.addNewMarker(item);
          //Scope.itemDetails.push({name:Scope.itemsData[i].name,id:Scope.itemsData[i].id,area:Scope.itemsData[i].area,phone : Scope.itemsData[i].phone, address : Scope.itemsData[i].address, inpat : Scope.itemsData[i].inpat, outpat : Scope.itemsData[i].outpat})

        }
      })

    }
    console.log('itemDetails', this.items);
    /*$scope.itemDetails.sort(function(a,b){
            var textA = a.name.toUpperCase();
            var textB = b.name.toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
          })*/
    //console.log('items',$scope.items);

    //this.drop(this.items) /// droping the markers by timeout function
    // this.changeDetRef.detectChanges();
  };


  // Success callback for watching your changing position

  onMapWatchSuccess(position) {

    var updatedLatitude = position.coords.latitude;
    var updatedLongitude = position.coords.longitude;

    if (updatedLatitude != this.pos.lat && updatedLongitude != this.pos.lng) {

      this.pos = {
        lat: updatedLatitude,
        lng: updatedLongitude
      }
      //Scope.itemDetails = [];
      //itemDetails=[];
      this.getMap(this.pos.lat, this.pos.lng, this.km);
    }
  }

  // Error callback

  onMapError(error) {
    console.log('code: ' + error.code + '\n' +
      'message: ' + error.message + '\n');
    alert('Location is off');
  }

  // Watch your changing position

  getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2, callback) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2 - lon1);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2)
      ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    //return d;
    callback(d, lat2, lon2);

  }

  deg2rad(deg) {
    return deg * (Math.PI / 180)
  }
  /********* clear/add/drop marker code starts *********/
  drop(res) {
    this.clearMarkers();
    for (var i = 0; i < this.items.length; i++) {
      //var myLatlng = new google.maps.LatLng($scope.items[i].lat,$scope.items[i].lng);
      this.addMarkerWithTimeout(this.items[i], i * 200, i, res);
      //console.log('check',$scope.items[i].hosp_lat,$scope.items[i].hosp_lon);
      //console.log('items',$scope.items[i]);
    }
    console.log('items length', this.items.length);
  }
  addMarkerWithTimeout(position, timeout, i, res) {

    setTimeout(() => {
      this.marker = new google.maps.Marker({
        position: position,
        map: this.map,
        draggable: false,
        id: i++,
        //label: labels[labelIndex++ % labels.length],
        //icon: pinSymbol("#FFF"),
        //label: labels[labelIndex++ % labels.length],
        //animation: google.maps.Animation.DROP
      })
      this.markers.push(this.marker);


      ((marker, itemDetails) => {
        //console.log(marker)
        //console.log("markers :", markers)
        //Addind some activites 
        google.maps.event.addListener(marker, 'click', () => {
          if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
          } else {
            this.clearEffect()
            marker.setAnimation(google.maps.Animation.BOUNCE);
            this.map.setZoom(16);
            this.map.setCenter(marker.getPosition());
            this.gotoloc = marker.getPosition();
          }
        });
        //         console.log(markers)
        this.markers = this.markers;

      }).bind(this)(this.marker, this.items);
    }, timeout);
    // Scope.itemDetails = itemDetails;
    // itemDetails.sort(function(a,b){
    //   var textA = a.name.toUpperCase();
    //   var textB = b.name.toUpperCase();
    //   return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    // })
  }

  clearEffect() {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setAnimation(null);
    }
  }
  clearMarkers() {
    for (var i = 0; i < this.markers.length; i++) {
      //this.markers[i].setMap(null);
      if (this.markers[i] && this.markers[i].setMap) {
        this.markers[i].setMap(null);
      }
    }
    if (this.cityCircle) {
      this.cityCircle.setMap(null);
    }
    this.markers = [];
  }
  watchMapPosition() {

    return navigator.geolocation.watchPosition
      (this.onMapWatchSuccess, this.onMapError, { enableHighAccuracy: false, timeout: 20000, maximumAge: 0 });
  }

  handleLocationError(browserHasGeolocation, infowindow, pos) {
    //infowindow.setPosition(pos);
    //infowindow.setContent(browserHasGeolocation ?
    //                              'Error: The Geolocation service failed.' :
    //                             'Error: Your browser doesn\'t support geolocation.');
    console.log(browserHasGeolocation ?
      'Error: The Geolocation service failed.' :
      'Error: Your browser doesn\'t support geolocation.');
  }

  addNewMarker(item) {


    let classname: any = {
      thumbsup: '',
      thumbsdown: '',
      share: '',
      seen: '',
    }

    classname = {
      thumbsup: '',
      thumbsdown: '',
      share: 'share',
      seen: '',
    }
    // if(item.like){
    //   classname.thumbsup = 'thumbsup';
    // }
    // if(item.dislike){
    //   classname.thumbsdown = 'thumbsdown';
    // }
    if (item.seen) {
      classname.seen = 'seen';
    }



    this.content = `
     <div style="width= 280px;">
    <div " class="infowindow" id="` + item.lcid + `" class="item-thumbnail-left item-text-wrap" style="display:inline-flex;width=280px;"> 
          <img style="height:166px; float:left; width:100px; text-align:center;" src = "`+ item.locimgpath + `" onError="this.src='assets/imgs/nopic.png'"></img>
        <div style="float:left; height:166px; margin-top= width:max-content; padding-left:5px; font-size:1.0rem; " >
          <p style="font-size: 1.3em !important;font-weight:bold;margin-top: 0 !important;">`+ item.locname + `</p> 
         <p style="margin-top: -7px !important;font-size: 1.3em !important;width: 25vw;">`+ item.loctext + `</p>
      </div>
    </div>
    </div>
    <div class="footer" style=" height:25px;margin-top:10px;text-align: center;"">
    <!--<img style="height: 20px;float: left;width: 20px;text-align: center;" src = "`+ item.icon1 + `" onError="this.src='assets/imgs/nopic.png'"></img>
    <img style="height: 20px;width: 20px;text-align: center;margin-left: 10vw !important;" src = "`+ item.icon2 + `" onError="this.src='assets/imgs/nopic.png'"></img>
    <img style="height: 20px;width: 20px;text-align: center;margin-left: 10vw !important;" src = "`+ item.icon3 + `" onError="this.src='assets/imgs/nopic.png'"></img>
    <img style="height: 20px;float: right;width: 20px;text-align: center;margin-left: 10vw !important;" src = "`+ item.icon4 + `" onError="this.src='assets/imgs/nopic.png'"></img> -->
    <span class="infoicons">
    <i class="fa fa-thumbs-o-up thumbsup"></i>
    <span class="number">`+ item.like + `</span>
    </span>
    <span class="infoicons"><i class="fa fa-thumbs-o-down thumbsdown"></i> <span class="number">`+ item.dislike + `</span></span>
    <span class="infoicons"><i class="fa fa-share-alt `+ classname.share + `"></i></span>
    <span class="infoicons"><i class="fa fa-check-circle `+ classname.seen + `"></i></span>
    </div>`;

    let latlng = new google.maps.LatLng(item.loclat, item.loclong);
    let marker = new google.maps.Marker({
      map: this.map,
      //animation: google.maps.Animation.DROP,
      position: latlng,
      draggable: false,
      visible: true,
      //icon:this.sunCircle//'../assets/imgs/currentlocation.png'
    });
    this.markers.push(marker);
    marker.setMap(this.map);
    this.addNewInfoWindow(marker, this.content);

  }

  addNewInfoWindow(marker, content) {


    let infoWindow = new google.maps.InfoWindow({
      content: this.content
    });

    google.maps.event.addListener(marker, 'click', () => {
      if (this.previousMarkerOpen) {
        this.previousMarkerOpen.close();
      }
      if (marker.getAnimation() !== null) {
        marker.setAnimation(null);
      } else {
        this.clearEffect()
        marker.setAnimation(google.maps.Animation.BOUNCE);
        //this.map.setZoom(16);
        //this.map.setCenter(marker.getPosition());
        this.gotoloc = marker.getPosition();
      }
      infoWindow.open(this.map, marker);
      if (infoWindow) {
        this.previousMarkerOpen = infoWindow;
      }

    });

  }

  /* native Code */
  initializeNative() {


    // This code is necessary for browser
    if (this.plt.is("cordova")) {
      Environment.setEnv({
        'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyAYTQPRrvGlCNFLuPDKrkORMsFOubFzoiA',//'AIzaSyDEb15bVQB-4mzlNoC7LXhrmoQpFArVs6Y',
        'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyAYTQPRrvGlCNFLuPDKrkORMsFOubFzoiA'//'AIzaSyDEb15bVQB-4mzlNoC7LXhrmoQpFArVs6Y'
      });
    }

    LocationService.getMyLocation().then((mylocation: MyLocation) => {
      let mapOptions: GoogleMapOptions =
      {
        camera: {
          target: mylocation.latLng,
          zoom: 18,
          tilt: 30
        },
        controls: {
          compass: true,
          myLocationButton: true,
          indoorPicker: true,
          zoom: true,
          myLocation: true,
        },
        mapTypeId: google.maps.MapTypeId.SATELLITE,
        styles: [{
          "elementType": "labels",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "administrative.neighborhood",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "transit.station.rail",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "visibility": "on"
            }
          ]
        }]
      }



      this.km = 0.15;
      this.Area = 700;
      this.map = GoogleMaps.create(this.mapElement.nativeElement, mapOptions);

      // Wait the MAP_READY before using any methods.
      this.map.one(GoogleMapsEvent.MAP_READY)
        .then(() => {
          console.log('Map is ready!');
          this.setNativeWatcher();
        }).catch((error) => {
          alert("Unable to open map!");
        })
    })


    // let marker: Marker = this.map.addMarkerSync({
    //   title: 'Ionic',
    //   icon: 'blue',
    //   animation: 'DROP',
    //   position: {
    //     lat: 43.0741904,
    //     lng: -89.3809802
    //   }
    // });
    // marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
    //   alert('clicked');
    // });

  }
  setNativeWatcher() {
    console.log('this.stable =======>',this.stable);
    this.watchID = this.geolocation.watchPosition();

    this.watchID.subscribe((position) => {
      // position can be a set of coordinates, or an error (if an error occurred).
      // position.coords.latitude
      // position.coords.longitude
      this.pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      if (this.stable == undefined || this.stable == false) {
        setTimeout(() => {
          if (this.previousPosition != JSON.stringify(this.pos)) {
            this.previousPosition = JSON.stringify(this.pos);
            this.clearNativeMarkers();
            //this.setNativeCityCircle(this.pos);
            this.getNativeMap(this.pos.lat, this.pos.lng, this.km);
          }
        }, 500);
      }
    }, (error) => {
      console.log("Error ", error);
      alert("Location is off");
    });

  }

  getNativeMap(latitude, longitude, km) {
    let remaped: LatLng = new LatLng(this.pos.lat, this.pos.lng);  ///coord's remaped for My marker
    this.items = [];
    // let icon: MarkerIcon = {
    //   url: new google.maps.MarkerImage('assets/imgs/maps/mobileimgs2.png', new google.maps.Point(0, 0)), //The anchor for this image
      // size: {
      //   width: 34,
      //   height: 25
      // }
    // };
    let options: MarkerOptions = {
      map: this.map,
      //animation: google.maps.Animation.DROP,
      position: remaped,
      draggable: false,
      myLocation: true,
      // icon: icon,
      // icon: 'assets/imgs/maps/gmcutemyloc.png',
      //icon:this.currentpositionimage//'../assets/imgs/currentlocation.png'
    }
    let target: any;
    if (this.map) {
      target = this.map.getCameraPosition();
    } else {
      target.lat = this.pos.lat;
      target.lng = this.pos.lng;
    }

    let mapOptions: GoogleMapOptions = {
      camera: {
        // target: {
        //   lat: this.map.position.lat, //this.pos.lat,
        //   lng: this.map.position.lang //this.pos.lng
        // },
        target: target,
        zoom: this.map.getCameraZoom(),
        tilt: 30
      },
      controls: {
        compass: true,
        myLocationButton: true,
        //indoorPicker: true,
        zoom: true,
        myLocation: true,
      },
      styles: [
        {
          "elementType": "labels",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "administrative.neighborhood",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "transit.station.rail",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "visibility": "on"
            }
          ]
        }
      ]
    }

    this.map.addMarker(options).then((marker: Marker) => {
      //this.markers.push(marker);
      // this.nativeCircle = this.map.addCircleSync({
      //   center: marker.getPosition(),
      //   strokeColor: '#abc8fa',
      //   strokeOpacity: 0.25,
      //   strokeWeight: 0,
      //   fillColor: '#abc8fa',
      //   fillOpacity: 0.05,
      //   map: this.map,
      //   radius: this.km * 1000
      // });
      this.markers.push(marker);
      // circle.center = marker.position
      // marker.bindTo("position", this.nativeCircle, "center");
      this.map.setOptions(mapOptions);
      // this.changeDetRef.detectChanges();
    });
    // let marker = new google.maps.Marker({
    //         map: this.map,
    //         //animation: google.maps.Animation.DROP,
    //         position: remaped,
    //         draggable: false,
    //         icon:this.currentpositionimage//'../assets/imgs/currentlocation.png'
    //       });
    // this.markers.push(marker);
    // this.map.setCenter(this.pos);
    // this.cityCircle.setCenter(remaped);

    // for (var i = 0; i < this.data.length; i++) {
    //   this.getDistanceFromLatLonInKm(this.pos.lat, this.pos.lng, this.data[i].loclat, this.data[i].loclong, (d, lat, lng) => {
    //     let item = this.data[i];
    //     if (d <= km) {

    //       this.items.push(item);
    //       this.addNativeMarker(item);
    //       //Scope.itemDetails.push({name:Scope.itemsData[i].name,id:Scope.itemsData[i].id,area:Scope.itemsData[i].area,phone : Scope.itemsData[i].phone, address : Scope.itemsData[i].address, inpat : Scope.itemsData[i].inpat, outpat : Scope.itemsData[i].outpat})

    //     }
    //   })

    // }
    for (var i = 0; i < this.data.length; i++) {
      let item = this.data[i];
      this.addNativeMarker(item);
    }
    // if(this.items.length != 0 && this.tempMarkerLength != this.items.length){
    //       this.items.forEach(item=>{
    //         this.addNativeMarker(item);
    //       })
    //     }
    this.tempMarkerLength = this.items.length;
    console.log('itemDetails', this.items);
    /*$scope.itemDetails.sort(function(a,b){
            var textA = a.name.toUpperCase();
            var textB = b.name.toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
          })*/
    //console.log('items',$scope.items);

    //this.drop(this.items) /// droping the markers by timeout function
    this.changeDetRef.detectChanges();
  };


  addNativeMarker(item) {
    let icon;
    // if(item.icon){
    //   icon = item.icon;
    // }
    //if(!item.icon){
    if (item.lcid == 1) {
      //icon = "assets/imgs/maps/roller-coaster-50.png";
      icon = "assets/imgs/maps/ferris-wheel-50.png";
    } else if (item.lcid == 3) {
      icon = "assets/imgs/maps/video-game-controller-outline-50.png"
    } else if (item.lcid == 2) {
      icon = "assets/imgs/maps/fork-and-knife-crossed-48.png";
    } else if (item.lcid == 5) {
      icon = "assets/imgs/maps/public_utility.png";
    } else if (item.lcid == 4) {
      icon = "assets/imgs/maps/bag.png";
    }
    //}
    let latlng: LatLng = new LatLng(item.loclat, item.loclong);
    let options: MarkerOptions = {
      map: this.map,
      //animation: google.maps.Animation.DROP,
      myLocation: true,
      position: latlng,
      draggable: false,
      icon: icon,
      title: item.locname,
      flat: false,
    }
    this.map.addMarker(options).then((marker: Marker) => {
      this.markers.push(marker);
      marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe((data) => {
        this.openParallax(item);
        if (this.previousMarker) {
          this.previousMarker.setAnimation(null);
          //this.previousMarker.setDisableAutoPan(true);
        }
        if (data.length > 1) {
          this.previousMarker = data[1];
          this.previousMarker.setAnimation('BOUNCE');
          //this.previousMarker.setDisableAutoPan(false);
        }
        // this.locationdetail=null;
        // this.locationshow = true;
        // item.locimgpath = ENUM.domain+ENUM.url.imgUrl+item.locimgpath;
        // this.locationdetail = item;

        //this.addNativeInfoWindow(marker, item, content);
      });

    });
    //    let latlng = new google.maps.LatLng(item.loclat, item.loclong);
    // let marker = new google.maps.Marker({
    //             map: this.map,
    //             //animation: google.maps.Animation.DROP,
    //             position: latlng,
    //             draggable: false,
    //             visible: true,
    //             //icon:this.sunCircle//'../assets/imgs/currentlocation.png'
    //           });
    //   this.markers.push(marker);
    //marker.setMap(this.map);
  }

  addNativeInfoWindow(marker, item, content) {


    // let infoWindow = new google.maps.InfoWindow({
    //   content: this.content
    // });

    // google.maps.event.addListener(marker, 'click', () => {
    //   if(this.previousMarkerOpen){
    //       this.previousMarkerOpen.close();
    //   }
    //   if (marker.getAnimation() !== null) {
    //         marker.setAnimation(null);
    //   } else {
    //   this.clearEffect()
    //   marker.setAnimation(google.maps.Animation.BOUNCE);
    //     //this.map.setZoom(16);
    //     //this.map.setCenter(marker.getPosition());
    //     this.gotoloc=marker.getPosition();
    //   }
    //   infoWindow.open(this.map, marker);
    //   if(infoWindow){
    //     this.previousMarkerOpen = infoWindow;  
    //   }

    // });

    if (this.htmlNativeInfoWindow) {
      this.htmlNativeInfoWindow.close();
    }
    this.htmlNativeInfoWindow = new HtmlInfoWindow();

    let frame: HTMLElement = document.createElement('div');
    frame.innerHTML = [content].join("");
    // frame.getElementsByTagName("img")[0].addEventListener("click", () => {
    //   this.htmlNativeInfoWindow.setBackgroundColor('red');
    //   this.htmlNativeInfoWindow.open(marker);
    // });
    this.htmlNativeInfoWindow.setContent(frame,
      //  {
      //   width: "100px",
      //   height: "100px"
      // }
    );

    this.htmlNativeInfoWindow.open(marker);

  }
  clearNativeMarkers() {
    for (var i = 0; i < this.markers.length; i++) {
      //this.markers[i].setMap(null);
      if (this.markers[i]) {
        this.markers[i].remove();
      }
    }
    if (this.nativeCircle) {
      this.nativeCircle.remove();
    }
    this.markers = [];
  }

  setNativeCityCircle(myLatlng) {
    this.nativeCircle = new google.maps.Circle({
      strokeColor: '#FFF',
      strokeOpacity: 0.8,
      strokeWeight: 0,
      fillColor: '#79bdbd',
      fillOpacity: 0.35,
      map: this.map,
      center: myLatlng,
      radius: this.km * 1000
    });
  }
  /*End Native Code*/

  removeParallax() {

    this.locationshow = false;
    this.stable = false;
  }
  getImage(path) {
    if (path) {
      this.webapi.getImages(path).then(image => {
        return image;
      });
    }
  }

  openParallax(item) {
    this.stable = true;
    console.log('Parallax Opened')
    this.getIndex(item);
    //this.locationdetail = null;
    this.locationshow = true;
    if (item.locimgpath !== undefined || item.locimgpath !== null || item.locimgpath !== "") {
      var checkImgpath = item.locimgpath.includes(ENUM.domain + ENUM.url.imgUrl); //"http://35.154.55.154:9772/api/ewapp/getimage?path="
      if (checkImgpath == true) {
        // item.locimgpath = "";
        // item.locimgpath = ENUM.domain+ENUM.url.imgUrl+item.locimgpath;
      }
      else {
        item.locimgpath = ENUM.domain + ENUM.url.imgUrl + item.locimgpath;
      }
    }
    this.getLikeStatus(item);
    // this.locationdetail.locname = item.locname;
    // this.locationdetail.loctext = item.loctext;
    // this.locationdetail.isactive = item.isactive;
    // this.locationdetail.locimgpath = item.locimgpath;
    // this.locationdetail.loclat = item.loclat;
    // this.locationdetail.loclong = item.loclong;
    // this.locationdetail.locseen = item.locseen;
    // this.locationdetail.share = item.share;
    // this.locationdetail.lcid = item.lcid;
    // this.locationdetail.locid = item.locid;


    // this.changeDetRef.detectChanges();
  }
  getIndex(item) {
    if (this.data.length > 0) {
      this.data.forEach((value, key) => {
        if (value.locid == item.locid) {
          this.currentIndex = key;
        }
      });
    }
  }
  setItem(item) {
    this.data[this.currentIndex] = item;
    this.locationinfoservice.setLocationList(this.data);
  }
  like(id) {

    if (localStorage.getItem('login') == 'Y') {
      var userDetails = JSON.parse(localStorage.getItem('UserDetails'));
      console.log('userDetails', userDetails)
      var userid = userDetails[0].reguserid
      let param = {
        "userId": userid,
        "locId": id,
        "likeStatus": 1,
      }
      console.log('params', param)
      var url = ENUM.domain + ENUM.url.updateLikeStatus;
      this.webapi.getService(url, param)
        .then(result => {
          var data: any = result
          console.log('UpdateLikeStatusResponse for Like:', result)
          this.locationdetail.like = data.data[0].like
          this.locationdetail.dislike = data.data[0].dislike
          this.locationdetail.reglikestatus = data.data[0].RegLikeStatus
          if (this.locationdetail.reglikestatus == "0") {
            this.itemlike = false;
            this.itemdislike = true;
          } else if (this.locationdetail.reglikestatus == "1") {
            this.itemlike = true;
            this.itemdislike = false;
          } else {
            this.itemlike = false;
            this.itemdislike = false;
          }
          this.setItem(this.locationdetail);
          this.changeDetRef.detectChanges();
          console.log('updated Like and Dislike:', this.locationdetail.like, this.locationdetail.dislike)
        })
        .catch(result => {
          console.log("ServerResponseError :", result);
        })
    } else {
      this.alertforLogin();
    }


  }

  dislike(id) {
    if (localStorage.getItem('login') == 'Y') {
      var userDetails = JSON.parse(localStorage.getItem('UserDetails'));
      console.log('userDetails', userDetails)
      var userid = userDetails[0].reguserid
      let param = {
        "userId": userid,
        "locId": id,
        "likeStatus": 0,
      }
      console.log('params', param)
      var url = ENUM.domain + ENUM.url.updateLikeStatus;
      this.webapi.getService(url, param)
        .then(result => {
          var data: any = result
          console.log('UpdateLikeStatusResponse for Dislike:', result)
          this.locationdetail.like = data.data[0].like
          this.locationdetail.dislike = data.data[0].dislike
          this.locationdetail.reglikestatus = data.data[0].RegLikeStatus
          if (this.locationdetail.reglikestatus == "0") {
            this.itemlike = false;
            this.itemdislike = true;
          } else if (this.locationdetail.reglikestatus == "1") {
            this.itemlike = true;
            this.itemdislike = false;
          } else {
            this.itemlike = false;
            this.itemdislike = false;
          }
          this.setItem(this.locationdetail);
          this.changeDetRef.detectChanges();
          console.log('updated Like and Dislike:', this.locationdetail.like, this.locationdetail.dislike);
        })
        .catch(result => {
          console.log("ServerResponseError :", result);
        })
    } else {
      this.alertforLogin();
    }
  }

  openCamera() {

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: true,
    }
    this.resetImg = this.locationdetail.locimgpath;
    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      console.log('imageData ===>', imageData);
      if (base64Image) {
        console.log('base64Image ===>', base64Image);

        let detail = {
          loctext: this.locationdetail.loctext,
          locname: this.locationdetail.locname,
          locimgpath: base64Image
        };
        //detail.imgpath = base64Image;

        this.Socialshare(detail);
        // this.navCtrl.getActive();
      }
    }, (err) => {
      //  Handle error
      console.log('error :-', err);
    });

  }

  openGallery() {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false,
      destinationType: this.camera.DestinationType.DATA_URL,

    }
    this.resetImg = this.locationdetail.locimgpath;
    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      console.log('imageData for Image Picker ==>', imageData);
      if (base64Image) {
        console.log('base64Image ===>', base64Image);
        let detail = {
          loctext: this.locationdetail.loctext,
          locname: this.locationdetail.locname,
          locimgpath: base64Image
        };
        //detail.imgpath = base64Image;
        this.Socialshare(detail);

        // this.navCtrl.getActive();
      }
    }, (err) => {
      console.log('error :-', err);
    });

  }
  checkAppAvailability(app){
    this.appAvailability.check(app)
      .then(
        (yes: boolean) => console.log(app + ' is available'),
        (no: boolean) => window.location.href = 'http://play.google.com/store/apps/details?id='+app
        ); 
  }
  Socialshare(locationdetail) {
    let twitter;
    let fb;
    let whatsApp;
    let instagram;
    if (this.plt.is('ios')) {
      twitter = 'twitter://';
      fb = 'facebook://';
      whatsApp = 'whatsapp://';
      instagram = 'instagram://';
    } else if (this.plt.is('android')) {
      twitter = 'com.twitter.android';
      fb = 'com.facebook.katana';
      whatsApp = 'com.whatsapp';
      instagram = 'com.instagram.android';
    }
    this.shareActionSheet = this.actionSheetController.create({
      buttons: [
        {
          text: "Share on Facebook",
          icon: "logo-facebook",
          handler: () => {
            this.socialSharing.shareViaFacebookWithPasteMessageHint(locationdetail.loctext, locationdetail.locimgpath, locationdetail.locname, locationdetail.loctext)
              .catch((err) => {
                this.checkAppAvailability(fb);
                console.log('Facebook Unavailabe   ',err);
              });
          }
        },
        {
          text: "Share on Whatsapp",
          icon: "logo-whatsapp",
          handler: () => {
            this.socialSharing.shareViaWhatsApp(locationdetail.locname, locationdetail.locimgpath, locationdetail.loctext)
              .catch((err) => {
                this.checkAppAvailability(whatsApp);
              });
          }
        },
        {
          text: "Share on Instagram",
          icon: "logo-instagram",
          handler: () => {
            this.socialSharing.shareViaInstagram(locationdetail.loctext, locationdetail.locimgpath)
              .catch((err) => {
                this.checkAppAvailability(instagram);
                console.log('Instagram Unavailabe   ',err);
              });
          }
        },
        {
          text: "Share on Twitter",
          icon: "logo-twitter",
          handler: () => {
            this.socialSharing.shareViaTwitter(locationdetail.locname, locationdetail.locimgpath, locationdetail.loctext)
              .catch((err) => {
                this.checkAppAvailability(twitter);
                console.log('Twitter Unavailabe   ',err);
              });
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: 'close-circle',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    this.shareActionSheet.present();

  }

  share() {
    let detail = this.locationdetail;
    this.shareActionSheet = this.actionSheetController.create({
      buttons: [
        {
          text: "Social Share",
          icon: "share",
          handler: () => {
            this.Socialshare(detail)
            // .catch(() => {
            //   //error
            // });
          }
        },
        // {
        //   text: "Click picture and share",
        //   icon: 'camera',
        //   handler: () => {
        //     this.openCamera();
        //     // .catch(() => {
        //     //   //error
        //     // });
        //   }
        // },
        {
          text: "Select from gallery and share",
          icon: 'images',
          handler: () => {
            this.openGallery();
            // .catch(() => {
            //   //error
            // });
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: 'close-circle',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    this.shareActionSheet.present();
  }

  Updateseen(id) {
    if (localStorage.getItem('login') == 'Y') {
      var userDetails = JSON.parse(localStorage.getItem('UserDetails'));
      console.log('userDetails', userDetails)
      var userid = userDetails[0].reguserid
      let param2 = {
        "userId": userid,
        "locId": id,
        "lSeen": 1,
        "actv": 1,
      }
      console.log('params', param2)
      var url2 = ENUM.domain + ENUM.url.updateMySchedule;
      this.webapi.getService(url2, param2)
        .then(result => {
          var data1: any = result
          console.log('SeenStatus for seen:', data1)
          var data2: any = data1.data[2];
          if (data2[0].locseen) {
            this.seenStatus = true;
          }

          console.log('this.seenStatus after calling updateSeen :-', this.seenStatus);
          this.changeDetRef.detectChanges();
        })
        .catch(result => {
          console.log("ServerResponseError :", result);
        })
    } else {
      this.alertforLogin();
    }
  }

  alertforLogin() {
    let alert = this.alertCtrl.create({
      title: 'Authentication Required!',
      message: 'Please login to access NearBy Map',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            //console.log('Cancel clicked');
            this.navCtrl.setRoot(HomePage);
          }
        },
      ]
    });
    alert.present();
  }
  ionViewWillLeave(){
    if(this.shareActionSheet){
      this.shareActionSheet.dismiss();
    }
    if (this.watchID) {
      navigator.geolocation.clearWatch(this.watchID);
    }
    this.watchID = null;
  }
  gotocart(){
    this.navCtrl.push(CartPage);
  }
	gotohomePage() {
		// this.navCtrl.push(HomePage,{});
		this.navCtrl.setRoot(HomePage);

	}
}

