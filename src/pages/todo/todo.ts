import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Slides } from 'ionic-angular';
import { SwipeTabComponent } from '../../components/swipe-tab/swipe-tab'; 

// import { IScrollTab, ScrollableTabComponent } from '../../components/scrollable-tab';
// import {  } from 'ionic-angular';
/**
 * Generated class for the TodoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-todo',
	templateUrl: 'todo.html',
})

export class TodoPage {
	@ViewChild(Slides) slides: Slides;

	public number: any = 2;
	relationship: string = '1';
	array: any = []
	status: boolean;
	public category: string = 'suggested';

	constructor(public navCtrl: NavController) {
		
		this.array = [
			{
				id: '1',
				label: 'Friends - 1',
				value: 'Hello to Friends - 1'
			},
			{
				id: '2',
				label: 'Friends - 2',
				value: 'Hello to Friends - 2'
			},
			{
				id: '3',
				label: 'Friends - 3',
				value: 'Hello to Friends - 3'
			},
			{
				id: '4',
				label: 'Friends - 4',
				value: 'Hello to Friends - 4'
			},
			{
				id: '5',
				label: 'Friends - 5',
				value: 'Hello to Friends - 5'
			},
			{
				id: '6',
				label: 'Friends - 6',
				value: 'Hello to Friends - 6'
			},
			{
				id: '7',
				label: 'Friends - 7',
				value: 'Hello to Friends - 7'
			},
			{
				id: '8',
				label: 'Friends - 8',
				value: 'Hello to Friends - 8'
			},
		]

	}
	
}
