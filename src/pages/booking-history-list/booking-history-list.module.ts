import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookingHistoryListPage } from './booking-history-list';

@NgModule({
  declarations: [
    BookingHistoryListPage,
  ],
  imports: [
    IonicPageModule.forChild(BookingHistoryListPage),
  ],
})
export class BookingHistoryListPageModule {}
