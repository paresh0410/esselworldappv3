import { Component } from '@angular/core';
import {
  IonicPage, NavController, NavParams, AlertController,
  LoadingController, ToastController, Platform, Nav
} from 'ionic-angular';
import { CartPage } from '../cart/cart';
import { NotificationPage } from '../notification/notification';
import { HomePage } from '../home/home';
import { BookingHistoryPage } from '../booking-history/booking-history'
import { ENUM } from '../../service/ENUM';
import { webAPIService } from '../../service/webAPIService';
import { LoginPage } from '../login/login';
/**
 * Generated class for the BookingHistoryListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-booking-history-list',
  templateUrl: 'booking-history-list.html',
})
export class BookingHistoryListPage {
  bookingSummary: any;
  bookdetails: any = [];
  TotaltPrice: any = 0;
  TTP: any = 0;
  TotaltfPrice: any = 0;
  TFP: any = 0;
  TotaltaPrice: any = 0;
  TAP: any = 0;
  loader: any;
  nodataAvailable: boolean = false;
  totalPrice: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public webAPI: webAPIService,
    private alertCtrl: AlertController, public loadingCtrl: LoadingController) {
      let UserDetails;
      UserDetails = localStorage.getItem('UserDetails');
      if (UserDetails) {
        this.fetchBookingSummary();
      }
      else {
        this.alertforLogin();
      }
  }

  ionViewDidEnter() {

  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad BookingHistoryListPage');

  }

  fetchBookingSummary() {
    let UserDetails = JSON.parse(localStorage.getItem('UserDetails'));
    let UserId: any;
    UserId = UserDetails[0].reguserid
    let param = {
      userid: UserId
    }

    var url = ENUM.domain + ENUM.url.fetchConfirmedItems;
    this.presentLoader();
    console.log('param   ', param);
    this.webAPI.getService(url, param)
      .then(result => {
        console.log('Details:', result);
        let temp: any = result;
        console.log('temp  ', temp);
        if (!temp.data[0].length || temp.data[0].length == 0) {
          this.nodataAvailable = true;
          console.log('this.nodataAvailable  ', this.nodataAvailable);
          this.closeLoader();
        }
        else {
          this.nodataAvailable = false;
          console.log('this.nodataAvailable  ', this.nodataAvailable);
          let userdata: any = temp.data[0];
          console.log('fetchConfirmedItems:', userdata);
          this.bookingSummary = userdata;
          console.log('this.bookingSummary', this.bookingSummary);
          // this.bookdetails = this.bookingSummary;
          // console.log('this.bookdetails ',this.bookdetails);
          // this.calculateTicket();
          // this.calculateFnB();
          // this.calculateAttr();
          // console.log('this.TAP + this.TFP + this.TTP;',this.TAP + this.TFP + this.TTP);
          // this.totalPrice = this.TAP + this.TFP + this.TTP;
          this.closeLoader();
        }
      }).catch(result => {
        console.log("Server Response Error :", result);
        this.closeLoader();
      });
  }

  calculateTicket() {
    let Tprice = 0;
    let TQuantity = 0;

    for (let i = 0; i < this.bookingSummary[0].tickets.length; i++) {
      console.log('this.bookingSummary[0].tickets.length ',this.bookingSummary[0].tickets.length);
      // for(let j = 0; j < this.bookingSummary[0].tickets[i].qty;){
      Tprice = this.bookingSummary[0].tickets[i].ticketprice;
      TQuantity = this.bookingSummary[0].tickets[i].ticCount;
      this.TotaltPrice = Tprice * TQuantity;
      this.TTP = this.TTP + this.TotaltPrice;
    }
    // return TTP;
  }

  calculateFnB() {
    let Fprice = 0;
    let FQuantity = 0;


    for (let i = 0; i < this.bookingSummary[0].FandB.length; i++) {
      console.log('this.bookingSummary[0].FandB.length ',this.bookingSummary[0].FandB.length);
      // for(let j = 0; j < this.bookingSummary[0].FandB[i].qty;){
      Fprice = this.bookingSummary[0].FandB[i].ticketprice;
      FQuantity = this.bookingSummary[0].FandB[i].ticCount;
      this.TotaltfPrice = Fprice * FQuantity;
      this.TFP = this.TFP + this.TotaltfPrice;
    }
    // return TTP;
  }

  calculateAttr() {
    let Aprice = 0;
    let AQuantity = 0;


    for (let i = 0; i < this.bookingSummary[0].paidAttr.length; i++) {
      console.log('this.bookingSummary[0].paidAttr.length ',this.bookingSummary[0].paidAttr.length);
      // for(let j = 0; j < this.bookingSummary[0].FandB[i].qty;){
      Aprice = this.bookingSummary[0].paidAttr[i].ticketprice;
      AQuantity = this.bookingSummary[0].paidAttr[i].ticCount;
      this.TotaltaPrice = Aprice * AQuantity;
      this.TAP = this.TAP + this.TotaltaPrice;
    }
    // return TTP;
  }

  // totalPrice(){
  //   // console.log(this.TAP + this.TFP + this.TTP);
  //   return this.TAP + this.TFP + this.TTP;
  // }

  goToHistoryDetails(id) {
    var temp = false;
    var tempTrack;
    for (let i = 0; i <= this.bookingSummary.length; i++) {
      if (i == id) {
        temp = true;
        tempTrack = this.bookingSummary[i].trackerID;
      }
    }
    if(temp == true){
      this.navCtrl.push(BookingHistoryPage, { 'trackerid': tempTrack });
    }

  }

  gotornotificationPage() {
    this.navCtrl.push(NotificationPage);
    console.log('notification.');
  }

  gotocart() {
    this.navCtrl.push(CartPage);
  }

  presentLoader() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
      showBackdrop: true,
    });
    this.loader.present();
  }

  closeLoader() {

    //this.offers = this.array;
    this.loader.dismiss();
  }

  alertforLogin() {
    const confirm = this.alertCtrl.create({
      title: 'Alert!',
      message: 'Please login to check your Booking History',
      buttons: [
        {
          text: 'Cancel',
          // role: 'cancel',
          handler: () => {
            console.log('Disagree clicked');
            this.navCtrl.setRoot(HomePage);
          }
        },
        {
          text: 'Login',
          handler: () => {
            console.log('Agree clicked');
            this.navCtrl.setRoot(LoginPage);
          }
        }
      ]
    });
    confirm.present();
  }
}
