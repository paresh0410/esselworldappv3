import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookhomecategoryPage } from './bookhomecategory';

@NgModule({
  declarations: [
    BookhomecategoryPage,
  ],
  imports: [
    IonicPageModule.forChild(BookhomecategoryPage),
  ],
})
export class BookhomecategoryPageModule {}
