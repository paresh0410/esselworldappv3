import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Events } from 'ionic-angular';
import { HomePage } from '../home/home';
import { CartPage } from '../cart/cart';
import { NotificationPage } from '../notification/notification';
import { checkIfOnlineService } from '../../service/checkIfOnlineService';
import { HomeServiceProvider } from '../../service/home-service';
import { ENUM } from '../../service/ENUM';
import { LoginPage } from '../login/login';
import { BookingPage } from '../booking/booking';
import { TicketCategoryPage } from '../ticket-category/ticket-category';
import { ThemeServiceProvider } from '../../providers/theme-service/theme-service';

/**
 * Generated class for the BookhomecategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bookhomecategory',
  templateUrl: 'bookhomecategory.html',
})
export class BookhomecategoryPage {
  number: any;
  homelist: any = [];
	public alertPresented: any;
	local: boolean = false;
  pushToBook: { bookHCID: any; bookHead: any; };
  forBackButton: boolean;
  nodataAvailable: boolean = false;
  ThemeC: any;
	iconImg: { icon: string; }[];

  constructor(public navCtrl: NavController, public navParams: NavParams,public checkOnline: checkIfOnlineService,public homeservice: HomeServiceProvider,
    private alertCtrl: AlertController, public events: Events,public themeServices : ThemeServiceProvider) {
			this.iconImg = [
				{
					icon: "/assets/imgs/ewNew.png"
				},
			{
				icon: "/assets/imgs/wkNew1.png"
			},{
				icon: "/assets/imgs/bpNew.png"
			}]
      ////////  Chnage for theme   ///////////////
				this.ThemeC = this.themeServices.getTheme();
				this.events.subscribe('theme', () => {
					this.ThemeC = this.themeServices.getTheme();
				});
    let param = {};
    var onlineStatus = this.checkOnline.getIfOnline();
    if (onlineStatus == true) {
      this.homeservice.getHomeListService(param)
        .then(result => {
          if (result != 'err') {
            console.log("SuccessHomeService :", result);
            let temp: any = result;
            let temp2 = temp.data
            for (let i = 0; i < temp2.length; i++) {
              temp2[i].imgPath = ENUM.domain + ENUM.url.imgUrl + temp2[i].imgPath
            }
            this.homelist = temp2;
						console.log('this.homelist',this.homelist);
						for(let i = 0; i < this.homelist.length; i++){
							this.homelist[i].icon = this.iconImg[i].icon;
						}
						console.log('this.homeList',this.homelist);
            // this.categoryList = temp.data[1];
            // this.homelist[0].imgPath = 'assets/imgs/esselworldHome.jpg';
            // this.homelist[1].imgPath = 'assets/imgs/waterkingdomHome.jpg';
            // loader.dismiss();
          } else {
            // loader.dismiss();
            this.serverErrorAlert();
            // this.homelist = this.homeservice.getLocalList();
          }
        }).catch(result => {
          console.log("Error :", result);
          this.local = false;
          // this.homelist = this.homeservice.getLocalList();
          // loader.dismiss();
        })
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookhomecategoryPage');
  }

  gotornotificationPage() {
    this.navCtrl.push(NotificationPage);
    console.log('notification.');
  }

  gotocart() {
    this.navCtrl.push(CartPage);
  }

  gotohomePage() {
    // this.navCtrl.push(HomePage,{});
    console.log('gotohomePage function called');
    this.navCtrl.popToRoot();

  }
  serverErrorAlert() {
		let vm = this;
		if (!vm.alertPresented) {
			vm.alertPresented = true;
			vm.alertCtrl.create({
				title: 'Server Error!!!',
				message: 'Server Not Responding. Please try again later.',
				buttons: [
					{
						text: 'OK',
						role: 'cancel',
						handler: () => {
							this.navCtrl.setRoot(HomePage);
							console.log('Cancel clicked');
						}
					},
				]
			}).present();
		}
	}
	
  booking(hcid) {
    console.log('hcid ',hcid);
		if (localStorage.getItem('UserDetails')) {
			for (let i = 0; i <= this.homelist.length; i++) {
				if (i == hcid) {
					this.pushToBook = {
						bookHCID: this.homelist[hcid].id,
						bookHead: this.homelist[hcid].cardHeader
          }
          if(hcid === 0){
            this.themeServices.setTheme('red');
          }else if(hcid === 1){
            this.themeServices.setTheme('blue');
          }else if(hcid === 2){
            this.themeServices.setTheme('green');
          }
					var bookHCID = JSON.stringify(this.homelist[hcid]);
					let ew: any = 1
					this.forBackButton = true;
					if (localStorage.getItem('trackerId')) {
						// this.resetTicket();
						localStorage.removeItem('trackerId');
						let bookingDt = localStorage.getItem('bookingDt');
						localStorage.removeItem('bookingDt');
						this.navCtrl.push(BookingPage, { 'pushToBook': this.pushToBook });
					}
					else {
						this.navCtrl.push(BookingPage, { 'pushToBook': this.pushToBook });
					}
					break;
				}
			}
		}
		else {
			var checkLogin: boolean = true;
			this.forBackButton = true;
			this.navCtrl.push(LoginPage, { 'checkLogin': checkLogin, 'pushToBook': this.pushToBook, 'forBackButton': this.forBackButton });
		}
  }
  resetTicket() {
		
		let alert = this.alertCtrl.create({
		  title: 'Create a new booking?',
		  buttons: [{
			text: 'No',
			handler: () => {
			  // user has clicked the alert button
			  // begin the alert's dismiss transition
			  let navTransition = alert.dismiss();
	
			  // start some async method
			  // someAsyncOperation().then(() => {
			  // once the async operation has completed
			  // then run the next nav transition after the
			  // first transition has finished animating out
	
			  navTransition.then(() => {
				// this.alertPark();
				// this.navCtrl.push(TicketCategoryPage, { 'pushToBook': this.pushToBook, 'forBackButton': this.forBackButton });
			  });
			  // });
			  return false;
			}
		  },
		  {
			text: 'Yes',
			handler: () => {
        // this.presentModal();
        this.navCtrl.push(BookingPage, { 'pushToBook': this.pushToBook });
			//   this.navCtrl.push(UserDetailsPage, { 'pushToBook': this.pushToBook, 'ticketDate': this.tcktDt, 'trackerId': this.trackerId, 'totalTicketPrice': this.totalTicketPrice, 'totalTicketCount': this.totalTicketCount });
			}
		  }]
		});
	
		alert.present();
	  }
}
