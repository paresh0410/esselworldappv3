import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events } from 'ionic-angular';
import { CartPage } from '../cart/cart';
import { NotificationPage } from '../notification/notification';
import { ThemeServiceProvider } from '../../providers/theme-service/theme-service';
import { HomePage } from '../home/home';
/**
 * Generated class for the EventsDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-events-details',
  templateUrl: 'events-details.html',
})
export class EventsDetailsPage {
  eventgroupofferId: any;
  contentImg: any;
  contentHead: any;
  subContentP1: any;
  ThemeC: any;
	red: boolean;
	blue: boolean;
	green: boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,public event: Events,
		public themeServices: ThemeServiceProvider) {
    if(localStorage.getItem('theme')){
      var previous = this.navCtrl.getPrevious();
      console.log('previous ', previous);
      this.ThemeC = localStorage.getItem('theme');
    }
    else{
              this.ThemeC = this.themeServices.getTheme();
          this.event.subscribe('theme', () => {
            this.ThemeC = this.themeServices.getTheme();
          });
          if(this.ThemeC == 'red'){
            this.red = true;
            this.blue = false;
            this.green = false;
          }else if(this.ThemeC == 'blue'){
            this.blue = true;
            this.red = false;
            this.green = false;
          }else if(this.ThemeC == 'green'){
            this.green = true;
            this.red = false;
            this.blue = false;
          }
    }
    
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });     
    loader.present();
    this.eventgroupofferId = this.navParams.get('eventgroupofferId');
    this.contentImg = this.navParams.get('contentImg');
    this.contentHead = this.navParams.get('contentHead');
    this.subContentP1 = this.navParams.get('subContentP1');
    loader.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventsDetailsPage');
  }
  // ionViewWillLeave(){
	// 	this.ThemeC = 'red';
	// 	localStorage.setItem('theme',this.ThemeC);
	// }
  gotohomePage(){
    // this.navCtrl.push(HomePage,{});
    console.log('gotohomePage function called');
    this.navCtrl.popToRoot();
  }		

  	gotornotificationPage(){
  		this.navCtrl.push(NotificationPage);
  		console.log('notification.');
		}
    gotocart(){
      this.navCtrl.push(CartPage);
    }
  
}
