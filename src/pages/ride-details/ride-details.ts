import { Component, ViewChild, state } from '@angular/core';
import {
  IonicPage, NavController, NavParams, LoadingController, ActionSheetController, Slides,
  AlertController, ToastController, Platform
} from 'ionic-angular';
import { getLocationService } from '../../service/getlocation.service';
import { FoodsService } from '../../service/food.service';
import { GameService } from '../../service/game.service';
import { SocialSharing } from '@ionic-native/social-sharing';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { ENUM } from '../../service/ENUM';
import { webAPIService } from '../../service/webAPIService';
import { LoginPage } from '../login/login';
import { SchedulePage } from '../schedule/schedule';
import { checkIfOnlineService } from '../../service/checkIfOnlineService';
import { Camera, CameraOptions } from '@ionic-native/camera'
import { ImagePicker } from '@ionic-native/image-picker';
import { Base64 } from '@ionic-native/base64/ngx';
import { AppAvailability } from '@ionic-native/app-availability/';
import { NotificationPage } from '../notification/notification';
import { CartPage } from '../cart/cart';
/**
 * Generated class for the RideDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ride-details',
  templateUrl: 'ride-details.html',
})
export class RideDetailsPage {
  @ViewChild(Slides) slides: Slides;

  attractionDetails: any = {
    id: '',
    imgPath: '',
    name: '',
    desc: '',
    likes: '',
    dislikes: '',
    ytlink: '',
  }

  id: any;
  imgPath: any;
  name: any;
  desc: any;
  likes: any;
  dislikes: any;
  ytlink: any;
  rideDetails: any = [];
  foodDetails: any = [];
  gameDetails: any = [];
  Desc: any = {};
  foodHeader: boolean = false;
  rideHeader: boolean = false;
  gameHeader: boolean = false;
  showVideo: boolean = false;
  itemlike: any;
  itemdislike: any;
  passParams: any;
  scheduleBtn: boolean = false;
  youtubebtn: boolean = false;
  base64Image: string;
  headerName: any;
  resetImg: any;
  locCat : any[];
  shareActionSheet :any;
  ThemeC: any;
  red: boolean = false;
  blue: boolean = false;
  green: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public getlocationservice: getLocationService, public loadingCtrl: LoadingController,
    public foodservice: FoodsService, public gameservice: GameService,
    private socialSharing: SocialSharing, public actionSheetController: ActionSheetController,
    private alertCtrl: AlertController, private youtube: YoutubeVideoPlayer, public webAPI: webAPIService,
    public checkOnline: checkIfOnlineService, private toastCtrl: ToastController,
    private camera: Camera, private imagePicker: ImagePicker, private base64: Base64,
    public platform: Platform, private appAvailability: AppAvailability) {
    this.fetchLocationCategories();
    this.ThemeC = localStorage.getItem('theme');
    if(this.ThemeC == 'red'){
      this.red = true;
      this.blue = false;
      this.green = false;
    }else if(this.ThemeC == 'blue'){
      this.blue = true;
      this.red = false;
      this.green = false;
    }else if(this.ThemeC == 'green'){
      this.green = true;
      this.red = false;
      this.blue = false;
    }
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();
    console.log('this.navParams.get(catId)', this.navParams.get('catId'));
    console.log('this.navParams.get(item)', this.navParams.get('item'));
    if (this.navParams.get('catId') == 2) {
      this.foodHeader = true;
      this.rideHeader = false;
      this.gameHeader = false;
      this.foodDetails = this.foodservice.getAll();
      console.log('FoodDetails:', this.foodDetails)
      this.Desc.catId = this.navParams.get('catId');
      this.Desc.title = this.navParams.get('title');
      this.headerName = this.Desc.title;
      this.Desc.id = this.navParams.get('item');
      this.Desc.imgpath = this.navParams.get('img')
      this.Desc.name = this.navParams.get('name')
      this.Desc.desc = this.navParams.get('desc')
      this.Desc.likes = this.navParams.get('likes')
      this.Desc.dislikes = this.navParams.get('dislikes')
      this.Desc.ytlink = this.navParams.get('youtubelink')
      console.log(' this.Desc ==>', this.Desc);

      if (this.Desc.ytlink == null && this.Desc.ytlink == undefined && this.Desc.ytlink =='') {
        this.youtubebtn = false;
      }

    } else if (this.navParams.get('catId') == 3) {
      this.foodHeader = false;
      this.rideHeader = false;
      this.gameHeader = true;
      this.gameDetails = this.gameservice.getAll();
      this.Desc.catId = this.navParams.get('catId');
      this.Desc.title = this.navParams.get('title');
      this.headerName = this.Desc.title;
      this.Desc.id = this.navParams.get('item');
      this.Desc.imgpath = this.navParams.get('img')
      this.Desc.name = this.navParams.get('name')
      this.Desc.desc = this.navParams.get('desc')
      this.Desc.likes = this.navParams.get('likes')
      this.Desc.dislikes = this.navParams.get('dislikes')
      this.Desc.ytlink = this.navParams.get('youtubelink')
      console.log(' this.Desc ==>', this.Desc);

      if (this.Desc.ytlink === null && this.Desc.ytlink === undefined && this.Desc.ytlink === '') {
        this.youtubebtn = false;
      }
    }
    else if(this.navParams.get('catId') == 7){
      this.gameDetails = this.gameservice.getAll();
      this.Desc.catId = this.navParams.get('catId');
      this.Desc.title = this.navParams.get('title');
      this.headerName = this.Desc.title;
      this.Desc.id = this.navParams.get('item');
      this.Desc.imgpath = this.navParams.get('img')
      this.Desc.name = this.navParams.get('name')
      this.Desc.desc = this.navParams.get('desc')
      this.Desc.likes = this.navParams.get('likes')
      this.Desc.dislikes = this.navParams.get('dislikes')
      this.Desc.ytlink = this.navParams.get('youtubelink')
      console.log(' this.Desc ==>', this.Desc);

      if (this.Desc.ytlink === null && this.Desc.ytlink === undefined && this.Desc.ytlink === '') {
        this.youtubebtn = false;
      }
    } else if (this.navParams.get('catId') == 1) {
      this.foodHeader = false;
      this.rideHeader = true;
      this.gameHeader = false;
      this.Desc.catId = this.navParams.get('catId');
      this.Desc.title = this.navParams.get('title');
      this.headerName = this.Desc.title;
      this.Desc.id = this.navParams.get('item');
      this.Desc.imgpath = this.navParams.get('img')
      this.Desc.name = this.navParams.get('name')
      this.Desc.desc = this.navParams.get('desc')
      this.Desc.ytlink = this.navParams.get('youtubelink')
      //this.Desc.likes=this.navParams.get('likes')
      //this.Desc.dislikes=this.navParams.get('dislikes')
      //this.Desc.reglikestatus=this.navParams.get('reglikestatus')
      console.log(' this.Desc ==>', this.Desc);
      if (this.Desc.ytlink === null && this.Desc.ytlink === undefined && this.Desc.ytlink ==='') {
        this.youtubebtn = false;
      }

      if (localStorage.getItem('login') == 'Y') {
        var userDetails = JSON.parse(localStorage.getItem('UserDetails'));
        console.log('userDetails', userDetails)
        var userid = userDetails[0].reguserid

        let param = {

          "lId": this.Desc.id,
          "userId": userid,
        }
        this.passParams = param
      } else {
        let param = {

          "lId": this.Desc.id,
          "userId": 0,

        }
        this.passParams = param
      }


      var url = ENUM.domain + ENUM.url.getLikeStatus;
      this.webAPI.getService(url, this.passParams)
        .then(result => {
          console.log('likeDislike', result)
          var data: any = result;
          var temp = data.data[0]
          if (temp[0].like == null) {
            this.Desc.likes = 0;
            this.Desc.reglikestatus = null;
            //this.Desc.dislikes = 0;
          } else {
            this.Desc.likes = temp[0].like;
            this.Desc.dislikes = temp[0].dislike;
            this.Desc.reglikestatus = temp[0].RegLikeStatus;
          }
          if (temp[0].dislike == null) {
            //this.Desc.likes = 0;
            this.Desc.dislikes = 0;
            this.Desc.reglikestatus = null;
          } else {
            this.Desc.likes = temp[0].like;
            this.Desc.dislikes = temp[0].dislike;
            this.Desc.reglikestatus = temp[0].RegLikeStatus;
          }

          console.log('reglikestatus', this.Desc.reglikestatus);

          if (this.Desc.reglikestatus == "0") {
            this.itemlike = false;
            this.itemdislike = true;
          } else if (this.Desc.reglikestatus == "1") {
            this.itemlike = true;
            this.itemdislike = false;
          } else {
            this.itemlike = false;
            this.itemdislike = false;
          }
        });



      console.log('this.desc =============> =======>',this.Desc);
      console.log('RidesDetails:', this.Desc, this.itemlike, this.itemdislike)

      // this.Desc.id=this.attractionDetails.id
      // this.Desc.imgpath = this.attractionDetails.imgPath,
      // this.Desc.name = this.attractionDetails.name,
      // this.Desc.desc = this.attractionDetails.desc,
      // this.Desc.likes = this.attractionDetails.likes,
      // this.Desc.dislikes = this.attractionDetails.dislikes

      console.log('rideDesc:', this.Desc);
    }
    loader.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RideDetailsPage');
  }

  ionViewWillEnter(){
        if (localStorage.getItem('login') == 'Y') {
      this.scheduleBtn = true
      console.log('this.scheduleBtn',this.scheduleBtn);
    } else {
      this.scheduleBtn = false
      console.log('this.scheduleBtn',this.scheduleBtn);
    }
  }
  //Open video in Youtube App just by calling this function and passing the videoid.
  /*  openYoutube(id){
	    this.youtube.openVideo('vm0dDbYg-Ws');
    } */

  //Unhide DOM for VideoPlayer
  openYoutube() {
    this.showVideo = true; //changing the showVideo value to true and using it to show video player in the DOM.
  }
  closeshowVideo() {
    this.showVideo = false;
  }

  fetchLocationCategories(){
     var url = ENUM.domain + ENUM.url.fetchlocationcat;
     this.webAPI.getService(url,'')
    .then(result => {
    console.log('LocationCategories', result);
     var temp : any = result;
     this.locCat = temp.data;
    console.log('this.locCat',this.locCat)  
   
        });
    }

  openCamera() {

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: true,
    }
    this.resetImg = this.Desc.imgpath;
    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      let loader = this.loadingCtrl.create({
        content: "Please wait..."
      });
      loader.present();
      console.log('imageData ===>', imageData);
      if (base64Image) {
        console.log('base64Image ===>', base64Image);
        
        let detail ={
          desc: this.Desc.desc,
          name: this.Desc.name,
          imgpath: base64Image
        };
        //detail.imgpath = base64Image;
        loader.dismiss();
        this.Socialshare(detail);
        // this.navCtrl.getActive();
      }
    }, (err) => {
      //  Handle error
      console.log('error :-', err);
    });

  }

  openGallery() {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false,
      destinationType: this.camera.DestinationType.DATA_URL,

    }
    this.resetImg = this.Desc.imgpath;
    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      console.log('imageData for Image Picker ==>', imageData);
      if (base64Image) {
        console.log('base64Image ===>', base64Image);
        let detail ={
          desc: this.Desc.desc,
          name: this.Desc.name,
          imgpath: base64Image
        };
        //detail.imgpath = base64Image;
        this.Socialshare(detail);

        // this.navCtrl.getActive();
      }
    }, (err) => {
      console.log('error :-', err);
    });
    // this.Desc.imgpath = resetImg;
    // this.imagePicker.getPictures(options).then((imageData) => {
    // var detail = this.Desc;
    // var resetImg = this.Desc.imgpath;
    // detail.imgpath = imageData;
    // this.Socialshare(detail);
    // this.navCtrl.getActive();
    // detail.imgpath = resetImg;
    // }, (err) => { 
    //   console.log('err in imagePicker', err);
    // });
  }
  checkAppAvailability(app){
    this.appAvailability.check(app)
      .then(
        (yes: boolean) => console.log(app + ' is available'),
        (no: boolean) => window.location.href = 'http://play.google.com/store/apps/details?id='+app
        ); 
  }
  Socialshare(detail) {
    // this.openCamera();
    //var checkImgpath = this.resetImg.includes(ENUM.domain+ENUM.url.imgUrl);
    // if (this.resetImg) {
    //   this.Desc.imgpath = this.resetImg;
    // }
    let twitter;
    let fb;
    let whatsApp;
    let instagram;
    if (this.platform.is('ios')) {
      twitter = 'twitter://';
      fb = 'facebook://';
      whatsApp = 'whatsapp://';
      instagram = 'instagram://';
    } else if (this.platform.is('android')) {
      twitter = 'com.twitter.android';
      fb = 'com.facebook.katana';
      whatsApp = 'com.whatsapp';
      instagram = 'com.instagram.android';
    }
     this.shareActionSheet = this.actionSheetController.create({
      buttons: [
        {
          text: "Share on Facebook",
          icon: "logo-facebook",
          handler: () => {
            this.socialSharing.shareViaFacebook(detail.desc, detail.imgpath, detail.ytlink)
              .catch((err) => {
                this.checkAppAvailability(fb);
                console.log('Facebook Unavailabe   ',err);
              });
          }
        },
        {
          text: "Share on Whatsapp",
          icon: "logo-whatsapp",
          handler: () => {
            
            this.socialSharing.shareViaWhatsApp(detail.name, detail.imgpath, detail.desc)
              .catch((err) => {
                this.checkAppAvailability(whatsApp);
                console.log('WhatsApp Unavailabe   ',err);
              });
          }
        },
        {
          text: "Share on Instagram",
          icon: "logo-instagram",
          handler: () => {
            
            this.socialSharing.shareViaInstagram(detail.name, detail.imgpath)
              .catch((err) => {
                this.checkAppAvailability(instagram);
                console.log('Instagram Unavailabe   ',err);
              });
          }
        },
        {
          text: "Share on Twitter",
          icon: "logo-twitter",
          handler: () => {
            
            this.socialSharing.shareViaTwitter(detail.name, detail.imgpath, detail.desc)
              .catch((err) => {
                this.checkAppAvailability(twitter);
                console.log('Twitter Unavailabe   ',err);
              });
          },
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: 'close-circle',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    this.shareActionSheet.present();
    


  }

  share() {
    let detail = this.Desc;

    // this.appArray = [twitter, fb, whatsApp, instagram];
   
      // this.appAvailability.check(twitter)
      // .then(
      //   (yes: boolean) => console.log( + ' is available'),
      //   (no: boolean) => console.log( + ' is NOT available')
      // );

   
    this.shareActionSheet = this.actionSheetController.create({
      buttons: [
        {
          text: "Social share",
          icon: "share",
          handler: () => {
            this.Socialshare(detail)
            // .catch(() => {
            //   //error
            // });
          }
        },
        {
          text: "Click picture and share",
          icon: 'camera',
          handler: () => {
            this.openCamera();
            // .catch(() => {
            //   //error
            // });
          }
        },
        {
          text: "Select from gallery and share",
          icon: 'images',
          handler: () => {
            this.openGallery();
            // .catch(() => {
            //   //error
            // });
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: 'close-circle',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    this.shareActionSheet.present();
    
  }

  next() {
    this.slides.slideNext();
  }

  prev() {
    this.slides.slidePrev();
  }

  like(id) {

    if (localStorage.getItem('login') == 'Y') {
      var userDetails = JSON.parse(localStorage.getItem('UserDetails'));
      console.log('userDetails', userDetails)
      var userid = userDetails[0].reguserid
      let param = {
        "userId": userid,
        "locId": id,
        "likeStatus": 1,
      }
      console.log('params', param)
      var url = ENUM.domain + ENUM.url.updateLikeStatus;
      this.webAPI.getService(url, param)
        .then(result => {
          var data: any = result
          console.log('UpdateLikeStatusResponse:', result)
          this.Desc.likes = data.data[0].like
          this.Desc.dislikes = data.data[0].dislike
          this.Desc.reglikestatus = data.data[0].RegLikeStatus
          if (this.Desc.reglikestatus == "0") {
            this.itemlike = false;
            this.itemdislike = true;
          } else if (this.Desc.reglikestatus == "1") {
            this.itemlike = true;
            this.itemdislike = false;
          } else {
            this.itemlike = false;
            this.itemdislike = false;
          }
          console.log('updated Like and Dislike:', this.Desc.likes, this.Desc.dislikes)
        })
        .catch(result => {
          console.log("ServerResponseError :", result);
        })
    } else {
      this.alertforLogin();
    }


  }

  dislike(id) {
    if (localStorage.getItem('login') == 'Y') {
      var userDetails = JSON.parse(localStorage.getItem('UserDetails'));
      console.log('userDetails', userDetails)
      var userid = userDetails[0].reguserid
      let param = {
        "userId": userid,
        "locId": id,
        "likeStatus": 0,
      }
      console.log('params', param)
      var url = ENUM.domain + ENUM.url.updateLikeStatus;
      this.webAPI.getService(url, param)
        .then(result => {
          var data: any = result
          console.log('UpdateLikeStatusResponse:', result)
          this.Desc.likes = data.data[0].like
          this.Desc.dislikes = data.data[0].dislike
          this.Desc.reglikestatus = data.data[0].RegLikeStatus
          if (this.Desc.reglikestatus == "0") {
            this.itemlike = false;
            this.itemdislike = true;
          } else if (this.Desc.reglikestatus == "1") {
            this.itemlike = true;
            this.itemdislike = false;
          } else {
            this.itemlike = false;
            this.itemdislike = false;
          }
          console.log('updated Like and Dislike:', this.Desc.likes, this.Desc.dislikes)
        })
        .catch(result => {
          console.log("ServerResponseError :", result);
        })
    } else {
      this.alertforLogin();
    }
  }

  gotoschedule(item) {
    console.log('Some Item--->>', item);
    var UD: any = JSON.parse(localStorage.getItem('UserDetails'));
    var param = {
      userId: UD[0].reguserid,
      locId: item.id,
      lSeen: 0,
      actv: 1
    }
    var url = ENUM.domain + ENUM.url.updateMySchedule;
    var onlineStatus = this.checkOnline.getIfOnline();
    if (onlineStatus == true) {
      this.webAPI.pushService(url, param)
        .then(result => {
          if (result == 'err') {
            this.presentToast('Cannot connect to server! Action cannot be completed.');
            console.log('SERVER ERROR');
          } else {
            var toastString = item.name+' has been added to your schedule'
            this.presentToast(toastString);
          }
        })
    } else {
      this.presentToast('No Internet Connection! Action cannot be completed.');
    }
  }

  presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 2000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  alertforLogin() {
    var getActive = this.navCtrl.getActive();
    var state = getActive.component.name;
    let alert = this.alertCtrl.create({
      title: 'Authentication Required!',
      message: 'Please Login to LIKE or DISLIKE',
      buttons: [
        {
          text: 'Login',
          handler: () => {
            console.log('Login clicked');
            localStorage.setItem("Page",state);
            this.navCtrl.push(LoginPage);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            //console.log('Cancel clicked');
            //this.navCtrl.setRoot(LoginPage);
          }
        }
      ]
    });
    alert.present();
  }
  gotohomePage() {
    // this.navCtrl.push(HomePage,{});
    console.log('gotohomePage function called');
    this.navCtrl.popToRoot();

  }

  ionViewWillLeave(){
    if(this.shareActionSheet){
      this.shareActionSheet.dismiss();
    }
  }

	gotornotificationPage() {
		this.navCtrl.push(NotificationPage);
		console.log('notification.');
	}
	gotocart() {
		this.navCtrl.push(CartPage);
	}  
}
