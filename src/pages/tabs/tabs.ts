import { Component, ViewChild } from '@angular/core';
import { ToastController } from 'ionic-angular';

import { HomePage } from '../home/home';
import { BookingHistoryPage } from '../booking-history/booking-history';
import { ListPage } from '../list/list';
import { SchedulePage } from '../schedule/schedule';
import { GeofencePage } from '../geofence/geofence';


import { ScrollableTabs } from '../../components/scrollable-tabs/scrollable-tabs';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  // this tells the tabs component which Pages
  // should be each tab's root Page
  HomeRoot: any = HomePage;
  BookingRoot: any = BookingHistoryPage;
  ListRoot: any = ListPage;
  ScheduleRoot: any = SchedulePage;
  GeofenceRoot: any = GeofencePage;




  tabsColor: string = "default";
  tabsMode: string = "md";
  tabsPlacement: string = "top";

  tabToShow: Array<boolean> = [true, true, true, true, true, true, true, true, true];
  scrollableTabsopts: any = {};

  constructor(private toastCtrl: ToastController) {
  }

  ionViewDidEnter() {

  }

  refreshScrollbarTabs() {
    this.scrollableTabsopts = { refresh: true };    
  }

  presentChangeOrendationToast() {
    let toast = this.toastCtrl.create({
      message: 'Rotate screen to rerendering.',
      duration: 2000,
      position: 'middle'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
}
