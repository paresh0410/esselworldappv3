import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InstructionsService } from '../../service/instructions.service';
import { NotificationPage } from '../notification/notification';
import { CartPage } from '../cart/cart';
/**
 * Generated class for the SaftyInstructionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-safty-instruction',
  templateUrl: 'safty-instruction.html',
})
export class SaftyInstructionPage {

  instructions: any = [];
  subInstructions: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private instructionservice: InstructionsService) {

    this.instructions = this.instructionservice.getAll();
    console.log(' this.instructions', this.instructions);

    // for (var i = 0; this.instructions.length > i; i++) {
    //   for (var j = 0; this.instructions[i].List.length > j; j++) {
    //     this.subInstructions.push(this.instructions[i].List[j]);
    //   }
    // }
    // console.log(' this.subInstructions ', this.subInstructions);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SaftyInstructionPage');
  }
	gotornotificationPage() {
		this.navCtrl.push(NotificationPage);
		console.log('notification.');
	}
	gotocart() {
		this.navCtrl.push(CartPage);
	}  

}
