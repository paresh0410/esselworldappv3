import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SaftyInstructionPage } from './safty-instruction';

@NgModule({
  declarations: [
    SaftyInstructionPage,
  ],
  imports: [
    IonicPageModule.forChild(SaftyInstructionPage),
  ],
})
export class SaftyInstructionPageModule {}
