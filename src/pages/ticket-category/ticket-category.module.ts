import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TicketCategoryPage } from './ticket-category';

@NgModule({
  declarations: [
    TicketCategoryPage,
  ],
  imports: [
    IonicPageModule.forChild(TicketCategoryPage),
  ],
})
export class TicketCategoryPageModule {}
