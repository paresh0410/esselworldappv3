import { Component, ViewChild } from '@angular/core';
import {
	IonicPage, NavController, NavParams, Content, AlertController,
	ToastController, LoadingController, Platform, Nav, Events
} from 'ionic-angular';
import { TicketCategoryService } from '../../service/ticketCategory.service';
import { FAndBCategoryPage } from '../../pages/f-and-b-category/f-and-b-category';

import { ENUM } from '../../service/ENUM';
import { webAPIService } from '../../service/webAPIService';
import { checkIfOnlineService } from '../../service/checkIfOnlineService';
import { Device } from '@ionic-native/device';
import { CartPage } from '../cart/cart';
import { NotificationPage } from '../notification/notification';
import { HomePage } from '../home/home';
import { ThemeServiceProvider } from '../../providers/theme-service/theme-service';
import { SelectParkPage } from '../../pages/select-park/select-park';
import { UserDetailsPage } from '../../pages/user-details/user-details';
/**
 * Generated class for the TicketCategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component(
	{
		selector: 'page-ticket-category',
		templateUrl: 'ticket-category.html',
	})
export class TicketCategoryPage {
	@ViewChild(Nav) nav: NavController;

	tktCategory: any = [];
	contentValue: boolean = false;
	cardList: any = [];
	fandbcardList: any = [];
	alert: any;
	pushToBook: any;
	ticketCategory: any;
	totalTicketPrice: number;
	totalTicketCount: number;
	ticketDate: any = [];
	trackerId: any;
	trckrID: any = [];
	tickId: any = [];
	tickPrice: any = [];
	deviceId: any = [];
	userId: any = [];
	tcktDt: any;
	nodataAvailable: boolean = false;
	loader: any;
	forBackButton: any;
	bookingSummary: any = [];
	TotaltPrice: any = 0;
	TTP: any = 0;
	unconfirmedArray: any = [];
	unconfirmLength: any = [];
	uiFlag: boolean;
	red: boolean;
	blue: boolean;
	green: boolean;
	ThemeC: any;

	// itemExpandHeight: number = 100;
	buttonClicked: boolean = false;
	toggle: boolean = false;
	rotate: boolean;
	getColor(hc){
		
		switch(hc){
			case 'red':
				return '1px solid #ff0000 ';
			case 'blue':
				return '1px solid #488aff';
			case 'green':
				return '1px solid #16910b';
		}
	}
	constructor(public navCtrl: NavController, public navParams: NavParams, private tktCategoryservice: TicketCategoryService,
		public webAPI: webAPIService, public checkOnline: checkIfOnlineService,
		private alertCtrl: AlertController, private device: Device, public platform: Platform,public events: Events,
		public themeServices: ThemeServiceProvider,
		private toastCtrl: ToastController, public loadingCtrl: LoadingController) {
			this.ThemeC = this.themeServices.getTheme();
			this.events.subscribe('theme', () => {
				this.ThemeC = this.themeServices.getTheme();
			});
			if(this.ThemeC == 'red'){
				this.red = true;
				this.blue = false;
				this.green = false;
			}else if(this.ThemeC == 'blue'){
				this.blue = true;
				this.red = false;
				this.green = false;
			}else if(this.ThemeC == 'green'){
				this.green = true;
				this.red = false;
				this.blue = false;
			}
			this.pushToBook = null;
			this.pushToBook = this.navParams.get('pushToBook');
			console.log('this.pushToBook ==>', this.pushToBook);
			this.tcktDt = localStorage.getItem('bookingDt');
			console.log('this.tcktDt  ==>', this.tcktDt);
			this.trackerId = localStorage.getItem('trackerId');
			// this.tktCategory = this.tktCategoryservice.getAll();
			this.forBackButton = this.navParams.get('forBackButton');

	}


	onIncrement(mainid, subid, count) {
		//console.log(mainid,subid,count);

		for (let i = 0; i < this.ticketCategory.length; i++) {
			if (mainid == this.ticketCategory[i].id) {
				for (let j = 0; j < this.ticketCategory[i].subCat.length; j++) {
					if (subid == this.ticketCategory[i].subCat[j].id) {
						this.ticketCategory[i].subCat[j].Qnt = count + 1;
						this.addremoveTicke(this.ticketCategory[i].subCat[j]);
					}
				}
			}

		}
	}

	onDecrement(mainid, subid, count) {
		//console.log(mainid,subid,count);

		for (let i = 0; i < this.ticketCategory.length; i++) {
			if (mainid == this.ticketCategory[i].id) {
				for (let j = 0; j < this.ticketCategory[i].subCat.length; j++) {
					if (subid == this.ticketCategory[i].subCat[j].id) {
						if (this.ticketCategory[i].subCat[j].Qnt != 0) {
							this.ticketCategory[i].subCat[j].Qnt = count - 1;
							this.addremoveTicke(this.ticketCategory[i].subCat[j]);
						}
					}
				}
			}

		}
	}
	addremoveTicke(ticket) {
		console.log('ticket', ticket);
		var checkF = this.checkFlag(ticket);
		if (checkF) {

			for (var i = 0; this.cardList.length > i; i++) {
				if (this.cardList[i].id == ticket.id) {
					if (ticket.Qnt == 0) {
						this.cardList.splice(i, 1);
					}
					if (ticket.Qnt > 0) {
						this.cardList[i] = ticket;
						this.cardList[i].totalPrice = ticket.Qnt * ticket.displaytickprice;
					}
				}
			}

		}
		if (!checkF) {
			ticket.totalPrice = ticket.Qnt * ticket.displaytickprice;
			this.cardList.push(ticket);
		}
	}
	TotalTCount() {
		var tCount = 0;
		for (var i = 0; this.cardList.length > i; i++) {
			tCount = tCount + this.cardList[i].Qnt;
		}
		return tCount;
	}

	TotalTPrice() {
		var tPrice = 0;
		for (var i = 0; this.cardList.length > i; i++) {
			tPrice = tPrice + this.cardList[i].totalPrice;
		}
		return tPrice;
	}


	checkFlag(ticket) {
		var flag = false;
		for (var i = 0; this.cardList.length > i; i++) {
			if (this.cardList[i].id == ticket.id) {
				flag = true;
				return flag;
			}
		}
		// return flag;
	}
	DeleteTicket(ticket) {
		for (var i = 0; this.cardList.length > i; i++) {
			if (this.cardList[i].id == ticket.id) {
				this.cardList.splice(i, 1);
				for (let i = 0; i < this.ticketCategory.length; i++) {
					for (let j = 0; j < this.ticketCategory[i].subCat.length; j++) {
						if (ticket.id == this.ticketCategory[i].subCat[j].id) {
							this.ticketCategory[i].subCat[j].Qnt = 0;
						}
					}
				}
			}
		}
	}
	dispCalc() {
		// this.contentValue=this.contentValue ? false : true;
		if (this.contentValue == false) {
			this.contentValue = true;
		}
		else {
			this.contentValue = false;
		}
		console.log(this.contentValue);
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad TicketCategoryPage');

	}
	ionViewDidEnter(){
		console.log('ionViewDidEnter');


		// console.log(this.fetchUnconfirmedItems())
		this.fetchTickets();
		// this.fetchUnconfirmedItems();
	}
	swipeAll(event: any): any {
		console.log('Swipe All', event);
	}

	swipeUp(event: any): any {
		console.log('Swipe Up', event);
	}

	cardHeight(cart, carf) {
		return 100 + 32 * (cart + carf) + 'px';
	}
	alertF(MSG: any) {
		if (this.alert) {
			this.alert.dismiss();
			this.alert = null;
		} else {
			this.alert = this.alertCtrl.create({ cssClass: 'Myalert', buttons: ['OK'] });
			let alertmsg: string = ENUM.alertMessage.replace('####', MSG);
			this.alert.setMessage(alertmsg);
			this.alert.present();
		}
	}

	goBack() {
		// localStorage.removeItem('bookingDt');
		// localStorage.removeItem('trackerId');
		this.navCtrl.popTo('HomePage');
	}

	/* FUNCTION TO FETCH TICKETS BASED ON DATE */
	fetchTickets() {
		let trckrId;
		trckrId = this.trackerId;
		this.uiFlag = false;
		var onlineStatus = this.checkOnline.getIfOnline();
		var url = ENUM.domain + ENUM.url.fetchTickets;
		var params = {
			tcktDt: this.tcktDt,
			ttypId: 1,
			hcid: this.pushToBook.bookHCID,
			trckrId: trckrId
		}
		this.presentLoader();
		if (onlineStatus == true) {
			this.webAPI.getService(url, params)
				.then(result => {
					let res: any;
					res = result;
					if (res.data.length == 0) {
						this.nodataAvailable = true;
						this.closeLoader();
					} else {
						this.nodataAvailable = false;
						console.log('TICKETS FETCHED     ', result);
						this.ticketCategory = res.data;
						this.cardList = [];
						
						for (let i = 0; i < this.ticketCategory.length; i++) {
							for (let j = 0; j < this.ticketCategory[i].subCat.length; j++) {
								if(this.ticketCategory[i].subCat[j].Qnt > 0){
									this.ticketCategory[i].subCat[j].totalPrice = this.ticketCategory[i].subCat[j].Qnt * this.ticketCategory[i].subCat[j].displaytickprice;
									this.cardList.push(	this.ticketCategory[i].subCat[j]);
								}
							if (this.ticketCategory[i].subCat[j].ticketimg) {
								var checkImgpath = this.ticketCategory[i].subCat[j].ticketimg.includes(ENUM.domain + ENUM.url.imgUrl);
								if (!checkImgpath) {
									// item.locimgpath = "";
									// item.locimgpath = ENUM.domain+ENUM.url.imgUrl+item.locimgpath;
									// console.log('ENUM.domain + ENUM.url.imgUrl ->',ENUM.domain + ENUM.url.imgUrl);
									this.ticketCategory[i].subCat[j].ticketimg = ENUM.domain + ENUM.url.imgUrl + this.ticketCategory[i].subCat[j].ticketimg;
								}
								else {	
									console.log('checkImgpath',checkImgpath);
								}
								
							}
						  }
						}
						console.table('this.cardList',this.cardList);
						console.log('this.ticketCategory    from res', this.ticketCategory);
						console.log('this.ticketCategory[0]   ', this.ticketCategory[0]);
						this.closeLoader();
					
						console.log('this.ticketCategory with img', this.ticketCategory);
					}
				})
				.catch(err => {
					console.log('err ==>', err);
					this.presentToast('Something went wrong. Please try again!');
					this.closeLoader();
				});
		} else {
			this.presentToast('No Internet Connection! Action cannot be completed.');
		}

	}

	// FUNCTION TO SAVE THE TICKET DATA, TICKET QUANTITY

	proceed() {
		this.trckrID = [];
		this.tickId = [];
		this.tickPrice = [];
		this.deviceId = [];
		this.userId = [];
		let userDetails: any;
		let userID: any;
		if (localStorage.getItem('UserDetails')) {
			userDetails = JSON.parse(localStorage.getItem('UserDetails'));
			if (userDetails.length > 0) {
				userID = userDetails[0].reguserid;
			}
		} else if (!localStorage.getItem('UserDetails')) {
			userID = 0;
		}
		let param = {
			trckrID: '',
			tickId: '',
			tickPrice: '',
			hcId: '',
			deviceId: '',
			userId: '',
			tcktDt: '',
			ttypId: 1,
			len: ''
		}
		// var trackerId = JSON.parse(localStorage.getItem('trackerId'));
		this.totalTicketPrice = this.TotalTPrice();
		console.log('this.totalTicketPrice   ==>', this.totalTicketPrice);
		this.totalTicketCount = this.TotalTCount();
		console.log('this.totalTicketCount ==>', this.totalTicketCount);
		console.log('this.cardList ==>', this.cardList);
		var cardLength = this.cardList.length;
		// LOOP TO PUSH THE DATA FROM CARDLIST IN ARRAY
		for (let i = 0; i < cardLength; i++) {

			for (let j = 0; j < this.cardList[i].Qnt;) {
				// this.trckrID.push(this.trackerId);
				this.tickId.push(this.cardList[i].id);
				this.tickPrice.push(this.cardList[i].displaytickprice);
				this.deviceId.push(this.device.uuid);
				this.userId.push(userID);
				this.ticketDate.push(this.tcktDt);
				j++;
			}

		}
		// console.log('this.trckrID ==>', this.trckrID);
		console.log('this.tickId   ==>', this.tickId);
		console.log('this.tickPrice  ==>', this.tickPrice);
		console.log('this.userId ==>', this.userId);
		param.trckrID = this.trackerId;
		param.tickId = this.tickId.toString();
		param.tickPrice = this.tickPrice.toString();
		param.hcId = this.pushToBook.bookHCID;
		param.deviceId = this.deviceId.toString();
		param.userId = this.userId.toString();
		param.tcktDt = this.ticketDate.toString();
		param.len = this.tickId.length;
		console.log('trckrID    ', param.trckrID);
		var url = ENUM.domain + ENUM.url.saveUnconfirmed;
		console.log('param ==>', param);
		this.presentLoader();
		var onlineStatus = this.checkOnline.getIfOnline();
		if (onlineStatus == true) {
			this.webAPI.getService(url, param)
				.then(result => {
					console.log('UCLIST RETURN    ', result);
					this.closeLoader();
					if(this.pushToBook.bookHCID == 3){
						// this.navCtrl.push(UserDetailsPage, { 'pushToBook': this.pushToBook, 'ticketDate': this.tcktDt, 'trackerId': this.trackerId, 'totalTicketPrice': this.totalTicketPrice, 'totalTicketCount': this.totalTicketCount });
						this.askOtherPark();
					}
					else{
						this.navCtrl.push(FAndBCategoryPage, { 'pushToBook': this.pushToBook, 'ticketDate': this.tcktDt, 'totalTicketPrice': this.totalTicketPrice, 'totalTicketCount': this.totalTicketCount });
				}
				})
				.catch(err => {
					console.log('err ==>', err);
					this.closeLoader();
				});
		} else {
			this.presentToast('No Internet Connection! Action cannot be completed.');
		}
	}
  askOtherPark() {
    let alert = this.alertCtrl.create({
      title: 'DO YOU WANT TO ADD TICKETS OF OTHER PARK ?',
      buttons: [{
        text: 'Yes',
        handler: () => {
          // user has clicked the alert button
          // begin the alert's dismiss transition
          let navTransition = alert.dismiss();

          // start some async method
          // someAsyncOperation().then(() => {
          // once the async operation has completed
          // then run the next nav transition after the
          // first transition has finished animating out

          navTransition.then(() => {
            // this.alertPark();
            this.navCtrl.push(SelectParkPage);
          });
          // });
          return false;
        }
      },
      {
        text: 'No',
        handler: () => {
          // this.proceed1();
          this.navCtrl.push(UserDetailsPage, { 'pushToBook': this.pushToBook, 'ticketDate': this.tcktDt, 'trackerId': this.trackerId, 'totalTicketPrice': this.totalTicketPrice, 'totalTicketCount': this.totalTicketCount });
        }
      }]
    });

    alert.present();
  }
	// FUNCTION TO FETCH UNCONFIRMED ITEMS

	// fetchUnconfirmedItems() {
	// 	this.uiFlag = true;
	// 	let trckrId = localStorage.getItem('trackerId');
	// 	let hId = this.pushToBook.bookHCID;
	// 	let ttId = 1;

	// 	let param = {
	// 		trackerID: trckrId,
	// 		hcid: hId,
	// 		ttypeId: ttId
	// 	}

	// 	var url = ENUM.domain + ENUM.url.fetchUnconfirmedItems;
	// 	var onlineStatus = this.checkOnline.getIfOnline();
	// 	if (onlineStatus == true) {
	// 		this.webAPI.getService(url, param)
	// 			.then(result => {
	// 				let res: any;
	// 				res = result;
	// 				console.log('UNCONFIRMED TICKETS FETCHED     ', result);
	// 				this.unconfirmedArray = res.data
	// 				console.log('this.ticketCategory     ',this.ticketCategory);
	// 				console.log('this.unconfirmedArray   ',this.unconfirmedArray);
	// 				this.unconfirmLength = this.unconfirmedArray.length;
	// 				if (this.unconfirmLength > 0) {
	// 					console.log('this.ticketCategory[0].subCat.length  ', this.ticketCategory[0].subCat.length);
	// 					console.log('this.ticketCategory[1].subCat.length  ', this.ticketCategory[1].subCat.length);
	// 					console.log('this.ticketCategory[2].subCat.length  ', this.ticketCategory[2].subCat.length);
	// 					// console.log('this.unconfirmedArray[1].subCat[1].Qnt  ', this.unconfirmedArray[1].subCat[1].Qnt);
	// 					for (let i = 0; i < this.ticketCategory.length; i++) {
	// 						for (let j = 0; j < this.ticketCategory[i].subCat.length; j++) {

	// 							for (let k = 0; k < this.unconfirmLength; k++) {
	// 								for (let l = 0; l < this.unconfirmedArray[k].subCat.length; l++) {
	// 									if (this.ticketCategory[i].subCat[j].id == this.unconfirmedArray[k].subCat[l].id) {
	// 										this.ticketCategory[i].subCat[j].Qnt = this.unconfirmedArray[k].subCat[l].Qnt;
	// 										console.log('CHANGED',l);
	// 									}
	// 								}
	// 							}
	// 						}
	// 					}
	// 				}
	// 				console.log(' this.unconfirmLength    from res', this.unconfirmLength);
	// 				console.log('this.ticketCategory    from res', this.ticketCategory);

	// 			})
	// 			.catch(err => {
	// 				console.log('err ==>', err);
	// 				this.presentToast('Something went wrong. Please try again!');
	// 				// this.closeLoader();
	// 			});
	// 	} else {
	// 		this.presentToast('No Internet Connection! Action cannot be completed.');
	// 	}
	// }


	// FUNCTION TO FORMAT DATE
	formatDate(date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('-');
	};

	// FUNCTION TO CREATE TOASTER
	presentToast(text) {
		let toast = this.toastCtrl.create({
			message: text,
			duration: 4000,
			position: 'top'
		});

		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});

		toast.present();
	}

	presentLoader() {
		this.loader = this.loadingCtrl.create({
			content: "Please wait...",
			showBackdrop: true,
		});
		this.loader.present();
	}

	closeLoader() {

		//this.offers = this.array;
		this.loader.dismiss();
	}
	gotornotificationPage() {
		this.navCtrl.push(NotificationPage);
		console.log('notification.');
	}
	gotocart() {
		this.navCtrl.push(CartPage);
	}
	gotohomePage() {
		// this.navCtrl.push(HomePage,{});
		console.log('gotohomePage function called');
		this.navCtrl.setRoot(HomePage);
		// .then(() => this.navCtrl.first().dismiss());
	
	  }
	  expandItem(item){

        // this.items.map((listItem) => {

        //     if(item == listItem){
        //         listItem.expanded = !listItem.expanded;
        //     } else {
        //         listItem.expanded = false;
        //     }

        //     return listItem;

		// });
		console.log('id');
		if(item.expanded === undefined){
			item.expanded = false;	
		}
		item.expanded = !item.expanded;
		this.rotate = item.expanded;
		console.log('this.rotate',this.rotate);
	}
	onButtonClick(cLen){ //cardLen,fLen
		// console.log(cardLen)
		// this.cardHeight(cardLen, fLen);
		//this.buttonClicked = !this.buttonClicked;
		if(this.toggle == false){
			this.toggle = true;
			return 100 + 32 * (cLen) + 'px';
		}
		else{
			this.toggle = false;
			return 0 + 'px';
		}
    }
}
