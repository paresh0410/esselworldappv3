import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,ToastController, LoadingController } from 'ionic-angular';
import { RideDetailsPage } from '../ride-details/ride-details';
import { GameService } from '../../service/game.service';
import { ENUM } from '../../service/ENUM';
import { webAPIService } from '../../service/webAPIService';
import { checkIfOnlineService } from '../../service/checkIfOnlineService';
import { SectionPage } from '../section/section';
import { HomePage } from '../home/home'

/**
 * Generated class for the GamesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-games',
  templateUrl: 'games.html',
})
export class GamesPage {

	games : any = [];
  hcId : any;
  tempArr : any = [];
  loader:any;
  categoryList:any;
  lcId:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public gameservice : GameService,public webAPI: webAPIService, public  checkOnline: checkIfOnlineService,private alertCtrl: AlertController,private toastCtrl: ToastController,public loadingCtrl: LoadingController ) {
    this.presentLoader();
    this.categoryList=this.navParams.get('categoryList')
    this.lcId = this.categoryList.id
    console.log('this.lcId',this.lcId)
    this.hcId = localStorage.getItem('hcId')
    let param = {
      "lcId" :this.lcId,
      "hcId" : this.hcId

    }


    var onlineStatus = this.checkOnline.getIfOnline();
    if(onlineStatus == true){
          var url = ENUM.domain + ENUM.url.attractions;
        this.webAPI.getService(url,param)
        .then(result => {
          
          var temp : any = result

          for(let i=0;i< temp.data.length;i++){
              temp.data[i].locimgpath = 'assets/imgs/home/icons8-game-controller-100.png';
              if(temp.data[i].dislike == null){
                  temp.data[i].dislike = 0;
              }
              if(temp.data[i].like == null){
                  temp.data[i].like = 0;
              }
          }
          this.closeLoader();
         this.games = temp.data
         console.log('Games:', this.games)
        })
        .catch(result =>{
          this.closeLoader();
          console.log("ServerResponseError :",  result);
        })
    }else{
          this.closeLoader();
          this.presentToast();
          this.navCtrl.setRoot(HomePage);
    }
    

  	//this.games = this.gameservice.getAll();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GamesPage');
  }

  gotoDetails(item){
  	this.navCtrl.push(RideDetailsPage,{
  		itemFrom : 'game',
  		item : item.id,
      name : item.locname,
      img : item.locimgpath,
      desc : item.loctext,
      likes : item.like,
      dislikes : item.dislike
  	});
  }

  presentLoader(){
      this.loader = this.loadingCtrl.create({
      content: "Please wait..."
    });     
    this.loader.present();
      console.log('ionViewDidLoad OffersPage');
    //   setTimeout(()=>{
      // this.offers = this.array;
      // loader.dismiss();
    //   },2000)
    }

    closeLoader(){
 
      //this.offers = this.array;
      this.loader.dismiss();
    }

    presentToast() {
    let toast = this.toastCtrl.create({
      message: 'NO INTERNET CONNECTION',
      duration: 3000,
      position: 'top',
      //cssClass:"InternetOffToast"
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

}
