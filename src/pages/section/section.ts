import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, ModalController, Events, AlertController } from 'ionic-angular';
import { NotificationPage } from '../notification/notification';
import { ParkTimingsPage } from '../park-timings/park-timings';
import { RidePage } from '../ride/ride';
import { GuideMapPage } from '../guide-map/guide-map';
import { FoodBeveragesPage } from '../food-beverages/food-beverages';
import { GamesPage } from '../games/games';
import { OffersPage } from '../offers/offers';
import { EventsPage } from '../events/events';
import { BookTicketPage } from '../book-ticket/book-ticket';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { Geolocation } from '@ionic-native/geolocation';
import { Select, } from 'ionic-angular';
import { checkIfOnlineService } from '../../service/checkIfOnlineService';
import { SectionService } from '../../service/SectionService';
import { ENUM } from '../../service/ENUM';
import { webAPIService } from '../../service/webAPIService';
import { BookingPage } from '../booking/booking';
import { LoginPage } from '../login/login';
import { TicketCategoryPage } from '../ticket-category/ticket-category';
import { CartPage } from '../cart/cart';
import { ThemeServiceProvider } from '../../providers/theme-service/theme-service';
import { BookhomecategoryPage } from '../bookhomecategory/bookhomecategory';
// import { ThemeServiceProvider } from '../../providers/theme-service/theme-service';
/**
 * Generated class for the SectionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-section',
	templateUrl: 'section.html',
})
export class SectionPage {
	@ViewChild(Select) select: Select;
	section: any;
	row1: any;
	row2: any;
	promo: any;
	id: any;
	loader:any;
	name: any;
	latitude: number;
	longitude: number;
	destLat: number;
	destLong: number;
	app;
	categoryList: any = [];
	network: boolean;
	localsection: any;
	img: any;
	pushToBook: { bookHCID: any; bookHead: any; };
	forBackButton: boolean;
	backDrop: boolean;

	ThemeC :any;
	slides: any;
	constructor(public navCtrl: NavController, public navParams: NavParams, private launchNavigator: LaunchNavigator,
		public geolocation: Geolocation, public loadingCtrl: LoadingController,
		public checkOnline: checkIfOnlineService, public modalCtrl: ModalController,
		public toastCtrl: ToastController, public sectionService: SectionService, public webAPI: webAPIService, public alertCtrl: AlertController,
		public themeServices : ThemeServiceProvider, public events: Events) {

		this.pushToBook = this.navParams.get('pushToBook');
		this.forBackButton = this.navParams.get('forBackButton');
		let hcItem: any = JSON.parse(localStorage.getItem('hcItem'));
		let id = hcItem.id;
		this.id = id;
		this.section = this.navParams.get('homelist');
		console.log('id =>', id);
		this.name = hcItem.cardHeader;
		this.img = hcItem.imgPath;
		this.destLat = hcItem.destLat;
		this.destLong = hcItem.destLong;
		var onlineStatus = this.checkOnline.getIfOnline();
		// this.row1 = this.sectionService.getSection();
		this.ThemeC = this.themeServices.getTheme();
		this.events.subscribe('theme', () => {
			this.ThemeC = this.themeServices.getTheme();
		});
		var hcid = this.pushToBook.bookHCID
		var param = {
			hc_id: hcid
		}
		
		if (onlineStatus == true) {
			this.network = true;
			this.presentLoader();
			this.fetchBanners();
			var url = ENUM.domain + ENUM.url.fetchSections
			this.webAPI.getService(url, param)
				.then(result => {
					var temp: any = result;
					this.row1 = temp.data;
					console.log('this.row ==>', this.row1);
					for (var i = 0; i < this.row1.length; i++) {
						if (this.row1[i].img !== undefined || this.row1[i].img !== null || this.row1[i].img !== "") {
							var checkImgpath = this.row1[i].img.includes(ENUM.domain + ENUM.url.imgUrl);
							if (checkImgpath == false) {
								// item.locimgpath = "";
								// item.locimgpath = ENUM.domain+ENUM.url.imgUrl+item.locimgpath;
								this.row1[i].img = ENUM.domain + ENUM.url.imgUrl + this.row1[i].img;
							}
							else {
								// this.row1[i].img = ENUM.domain + ENUM.url.imgUrl + '/sec/img/' + this.row[i].name + '_'+ this.themeC + '.png';
								// this.row1[i].img = ENUM.domain + ENUM.url.imgUrl + this.row1[i].img;
							}
						}
					}
					console.log('this.row1 ==>', this.row1);
					this.closeLoader();
				}).catch(error => {
					console.log('fetchSections => ', error);
					this.closeLoader();
				});

		}
		else if (onlineStatus == false) {
			this.network = false;
			if (localStorage.getItem('localsection') === null || localStorage.getItem('localsection') === undefined) {
				this.presentToast('No Internet Connection! Please connect to Internet.');
			}
			else {
				this.localsection = JSON.parse(localStorage.getItem('localsection'));
				console.log('localsection =======>  ====> ', this.localsection);
			}
			if (this.localsection) {
				for (var i = 0; i < this.localsection.length; i++) {
					var checkImgpath = this.localsection[i].img.includes(ENUM.domain + ENUM.url.imgUrl);
					if (this.localsection[i].id == 1) {
						if (!checkImgpath) {
							this.localsection[i].img = "assets/imgs/home/Location.png";
						}
					}
					if (this.localsection[i].id == 2) {
						if (!checkImgpath) {
							this.localsection[i].img = "assets/imgs/home/PT.png";
						}
					}
					if (this.localsection[i].id == 3) {
						if (!checkImgpath) {
							this.localsection[i].img = "assets/imgs/home/Rides.png";
						}
					}
					if (this.localsection[i].id == 4) {
						if (!checkImgpath) {
							this.localsection[i].img = "assets/imgs/home/GM.png";
						}
					}
					if (this.localsection[i].id == 5) {
						if (!checkImgpath) {
							this.localsection[i].img = "assets/imgs/home/FNB.png";
						}
					}
					if (this.localsection[i].id == 6) {
						if (!checkImgpath) {
							this.localsection[i].img = "assets/imgs/home/Games.png";
						}
					}
				}
			}
		}

		console.log('ROW1--->', this.row1);

		// this.section = [
		// 	{
		// 		imgPath: 'assets/imgs/esselworldHome.jpg',
		// 		cardHeader: 'Essel World',
		// 		cardBody: 'Essel World is an amusement park located in Gorai, Mumbai and established in 1989. The park is owned by EsselWorld Leisure Pvt. Ltd. ',
		// 	},
		// 	{
		// 		imgPath: 'assets/imgs/waterkingdomHome.jpg',
		// 		cardHeader: 'Water Kingdom',
		// 		cardBody: 'Family-friendly water park featuring high-speed slides, wave pools, lazy river & kiddie attractions.',
		// 	}
		// ];

		this.promo = [
			{
				id: 1,
				title: 'Promotions & Offers',
				img: 'assets/imgs/home/icons8-discount-100.png'
			},
			{
				id: 2,
				title: 'Book Tickets',
				img: 'assets/imgs/home/icons8-two-tickets-100.png'
			}
		];

		this.geolocation.getCurrentPosition().then(position => {
			this.latitude = position.coords.latitude;
			this.longitude = position.coords.longitude;
		}, error => {
			console.log('error', error);
		});

	}

	fetchBanners(){
		var hcid:any = localStorage.getItem('hcId');
		var param = {
			hc_id: hcid
		}
		var url = ENUM.domain + ENUM.url.fetchSectionBanners
			this.webAPI.getService(url, param)
			.then(result =>{
				console.log('Banners ',result);
				let res:any = result;
				this.slides = res.data[0];
				console.log('this.slides',this.slides);
				for (var i = 0; i < this.slides.length; i++) {
					if (this.slides[i].imgpath !== undefined || this.slides[i].imgpath !== null || this.slides[i].imgpath !== "") {
						var checkImgpath = this.slides[i].imgpath.includes(ENUM.domain + ENUM.url.imgUrl);
						if (checkImgpath == false) {
							// item.locimgpath = "";
							// item.locimgpath = ENUM.domain+ENUM.url.imgUrl+item.locimgpath;
							this.slides[i].imgpath = ENUM.domain + ENUM.url.imgUrl + this.slides[i].imgpath;
						}
						else {
							// this.row1[i].img = ENUM.domain + ENUM.url.imgUrl + '/sec/img/' + this.row[i].name + '_'+ this.themeC + '.png';
							// this.row1[i].img = ENUM.domain + ENUM.url.imgUrl + this.row1[i].img;
						}
					}
				}
			}).catch(err=>{
				console.log('Banner Error',err);
			})
	}

	gotornotificationPage() {
		this.navCtrl.push(NotificationPage);
		console.log('notification.');
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad SectionPage');
	}

	gotoSubpages(index) {
		// console.log(this.row1[index]);
		var onlineStatus = this.checkOnline.getIfOnline();
		if (index == 0) {
			this.navCtrl.push(RidePage, { 'categoryList': this.row1[index] });
		} else if (index == 1) {

			// this.navCtrl.push(ParkTimingsPage);
			this.navCtrl.push(RidePage, { 'categoryList': this.row1[index] });

			// this.presentToast('No Internet Connection!!!');

		} else if (index == 2) {

			this.navCtrl.push(RidePage, { 'categoryList': this.row1[index] });
			

			// this.presentToast('No Internet Connection!!!');

		} else if (index == 3) {

			this.navCtrl.push(EventsPage, { 'categoryList': this.row1[index] });

			// this.presentToast('No Internet Connection!!!');

		} else if (index == 4) {

			this.navCtrl.push(OffersPage, { 'categoryList': this.row1[index] });

			// this.presentToast('No Internet Connection!!!');

		} else if (index == 5) {

			this.navCtrl.push(GuideMapPage, { 'categoryList': this.row1[index] });
			// this.presentToast('No Internet Connection!!!');
		} else if(index == 6){
			window.location.href = ' https://www.fab5shopping.in/';

		} else if(index == 7){
			this.navCtrl.push(RidePage,{'categoryList': this.row1[index]});
			
		} else if(index == 8){
			this.booking();
		}

	}

	gotoinnerpages() { //index
		// if (index == 0) {
		this.navCtrl.push(OffersPage);
		// } 
		// else if (index == 1) {
		// 	this.booking(index)
		// }
	}

	presentToast(text) {
		let toast = this.toastCtrl.create({
			message: text,
			duration: 2000,
			position: 'bottom'
		});

		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});

		toast.present();
	}

	// booking() {
	// 	if (localStorage.getItem('UserDetails')) {
	// 		if (localStorage.getItem('trackerId')) {
	// 			this.navCtrl.push(TicketCategoryPage, { 'pushToBook': this.pushToBook, 'forBackButton': this.forBackButton });
	// 		}
	// 		else {
	// 			this.presentModal();
	// 		}
	// 	}
	// 	else {
	// 		var checkLogin: boolean = true;
	// 		this.navCtrl.push(LoginPage, { 'checkLogin': checkLogin, 'pushToBook': this.pushToBook });
	// 	}
	// }
	// presentModal() {
	// 	// localStorage.setItem('trackerId','');
	// 	// localStorage.setItem('bookingDt','');
	// 	this.backDrop = true;
	// 	const modal = this.modalCtrl.create(BookingPage, { 'pushToBook': this.pushToBook }, { cssClass: 'myModal' });
	// 	modal.present();
	// }

	booking(){
		// if(localStorage.getItem('trackerId')){
		// 	this.resetTicket();
		// }
		// else{

		// }
		this.presentModal();
	}
	resetTicket() {
		
		let alert = this.alertCtrl.create({
		  title: 'Create a new booking?',
		  buttons: [{
			text: 'No',
			handler: () => {
			  // user has clicked the alert button
			  // begin the alert's dismiss transition
			  let navTransition = alert.dismiss();
	
			  // start some async method
			  // someAsyncOperation().then(() => {
			  // once the async operation has completed
			  // then run the next nav transition after the
			  // first transition has finished animating out
	
			  navTransition.then(() => {
				// this.alertPark();
				this.navCtrl.push(TicketCategoryPage, { 'pushToBook': this.pushToBook, 'forBackButton': this.forBackButton });
			  });
			  // });
			  return false;
			}
		  },
		  {
			text: 'Yes',
			handler: () => {
				this.presentModal();
			//   this.navCtrl.push(UserDetailsPage, { 'pushToBook': this.pushToBook, 'ticketDate': this.tcktDt, 'trackerId': this.trackerId, 'totalTicketPrice': this.totalTicketPrice, 'totalTicketCount': this.totalTicketCount });
			}
		  }]
		});
	
		alert.present();
	  }
	  presentModal() {
		// this.backDrop = true;
		// const modal = this.modalCtrl.create(BookhomecategoryPage, { 'pushToBook': this.pushToBook }, { cssClass: 'myModal' });
		// modal.present();
		this.navCtrl.push(BookhomecategoryPage, { 'pushToBook': this.pushToBook });
	}
	gotocart(){
		this.navCtrl.push(CartPage);
	  }
	  presentLoader() {
		this.loader = this.loadingCtrl.create({
			content: "Please wait..."
		});
		this.loader.present();
		console.log('ionViewDidLoad SectionPage');
		// 	setTimeout(()=>{
		// this.offers = this.array;
		// loader.dismiss();
		// 	},2000)
	}

	closeLoader() {

		// this.offers = this.array;
		this.loader.dismiss();
	}

}