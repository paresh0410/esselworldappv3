import { Component, ViewChild } from '@angular/core';
import {
  IonicPage, NavController, NavParams, DateTime,
  ViewController, LoadingController, ToastController, Platform,Nav, Events
} from 'ionic-angular';
import { ENUM } from '../../service/ENUM';
import { bookingService } from '../../service/bookingService';
import { ModalController } from 'ionic-angular';
import { Device } from '@ionic-native/device';
import { webAPIService } from '../../service/webAPIService';
import { TicketCategoryPage } from '../ticket-category/ticket-category';
import { CartPage } from '../cart/cart';
import { NotificationPage } from '../notification/notification';
import { ThemeServiceProvider } from '../../providers/theme-service/theme-service';
import { HomePage } from '../home/home';
/**
 * Generated class for the BookingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-booking',
  templateUrl: 'booking.html',
})
export class BookingPage {
  @ViewChild(Nav) nav: NavController;
  date: any;
  user: any;
  dateTime: any;
  pushToBook: any;
  bk = {
    bookingDt: '',
    trackingId:'',
  }
forBackButton: boolean = false;
ThemeC: any;

  params: { trckrId: string; deviceId: string; userId: string; tcktDt: string; rcrdDT: string; };
  red: boolean;
  blue: boolean;
  green: boolean;

	getColor(hc){
    console.log('hc    ',hc);
		switch(hc){
			case 1:
				return '#ff0000 ';
			case 2:
				return '#488aff';
			case 3:
				return '#16910b';
		}
	}
  constructor(public navCtrl: NavController, public navParams: NavParams,
   /* public bk: bookingService,*/ public modalCtrl: ModalController,
    public viewCtrl: ViewController, public device: Device, public webAPI: webAPIService, public platform: Platform,
    public loadingCtrl: LoadingController, public toastCtrl: ToastController,
    public events: Events,public themeServices : ThemeServiceProvider) {
        ////////  Chnage for theme   ///////////////
				this.ThemeC = this.themeServices.getTheme();
				this.events.subscribe('theme', () => {
					this.ThemeC = this.themeServices.getTheme();
        });
        if(this.ThemeC == 'red'){
          this.red = true;
          this.blue = false;
          this.green = false;
        }else if(this.ThemeC == 'blue'){
          this.blue = true;
          this.red = false;
          this.green = false;
        }else if(this.ThemeC == 'green'){
          this.green = true;
          this.red = false;
          this.blue = false;
        }
      this.pushToBook = this.navParams.get('pushToBook');
      console.log('this.pushToBook',this.pushToBook.bookHCID);
      this.user = JSON.parse(localStorage.getItem('UserDetails'));
      console.log('UUID', this.device.uuid);
  }

  ionViewDidEnter(){

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookingPage');
  }

  // SAVE DATE TO bookingService 
  dateSelected(event) {

    this.bk.bookingDt = '';
      this.bk.bookingDt = this.formatDate(event);
      console.log('this.bk.bookingDt =====>', this.bk.bookingDt);
      this.bk.trackingId = Math.random().toString(36).substr(2, 8).toUpperCase();
      console.log('this.bk.trackingId  ', this.bk.trackingId);

  };

  // TO DISMISS THE MODAL
  cancel() {
    this.viewCtrl.dismiss();
  };

  // SAVE THE DATE AND UPDATE TRACKERID WITH OTHER DETAILS IN THE DATABASE AND REDIRECT TO TICKET CATEGORY
  proceed() {
    this.params = {
      trckrId: '',
      deviceId: '',
      userId: '',
      tcktDt: '',
      rcrdDT: ''
    };
    let userDetails: any;
    let userID: any;
    if (localStorage.getItem('UserDetails')) {
      userDetails = JSON.parse(localStorage.getItem('UserDetails'));
      if (userDetails.length > 0) {
        userID = userDetails[0].reguserid;
      }
    } else if (!localStorage.getItem('UserDetails')) {
      userID = 0;
    }
    var url = ENUM.domain + ENUM.url.insertTrackerId;
    this.params = {
      trckrId: this.bk.trackingId,
      deviceId: this.device.uuid,
      userId: userID,
      tcktDt: this.bk.bookingDt,
      rcrdDT: ''
    }
    if (this.params.tcktDt < this.formatDate(new Date())) {
      this.presentToast('You cannot select previous date');
    }
    else if (this.params.tcktDt == null || this.params.tcktDt == undefined) {
      this.presentToast('Please select a date');
    }
    else {
      this.webAPI.getService(url, this.params)
        .then(result => {
          if (result != 'err') {
            this.forBackButton = false;
            localStorage.setItem('trackerId', this.bk.trackingId);
            localStorage.setItem('bookingDt', this.bk.bookingDt);
            console.log("SUCCESS!! PROCEED NOW", result);
            this.cancel();
            console.log('this.pushToBook  ==>',this.pushToBook);
            this.navCtrl.push(TicketCategoryPage, { 'pushToBook': this.pushToBook, 'ticketDate': this.params.tcktDt, 'forBackButton':this.forBackButton });
          }
        }).catch(err => {
          console.log('Error occurred ==>', err);
        });
    }

  };

  // FUNCTION TO FORMAT DATE
  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  };

  // FUNCTION TO CREATE TOASTER
  presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 2000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  gotornotificationPage() {
    this.navCtrl.push(NotificationPage);
    console.log('notification.');
  }

  gotocart() {
    this.navCtrl.push(CartPage);
  }

  gotohomePage() {
    // this.navCtrl.push(HomePage,{});
    console.log('gotohomePage function called');
    this.navCtrl.setRoot(HomePage)
          // .then(() => this.navCtrl.first().dismiss());

  }

}
