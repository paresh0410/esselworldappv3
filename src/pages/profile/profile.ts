import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, LoadingController, ToastController  } from 'ionic-angular';
import { FormBuilder, FormControl, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { ENUM } from '../../service/ENUM';
import { webAPIService } from '../../service/webAPIService';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
//import { ImagePicker } from '@ionic-native/image-picker';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
	profileForms:FormGroup;
	profileform: any ={
		firstname:'',
		lastname:'',
		email:'',
		mobile:'',
		gender:'',
		bdate:'',
    radioMarried:'',
    img:""
	};
  // profileform : any;
  mobileNoDetails : any;
  userDetails:any;
  proImage = '';
  profiles:any = [];
  array:any = [];
  uData1:any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams,public webAPI: webAPIService,
     public actionSheetController: ActionSheetController,private camera:Camera,private imagePicker: ImagePicker,private loadingCtrl: LoadingController,
     private toastCtrl: ToastController) {
    this.mobileNoDetails = JSON.parse(localStorage.getItem('mobileNumber'));
    this.userDetails = JSON.parse(localStorage.getItem('UserDetails'));
    // this.profileform.firstname = this.userDetails[0].regusername;
    // this.profileform.email = this.userDetails[0].email;
    // this.profileform.mobile = this.userDetails[0].mobileno;
    // this.profileform.gender = this.userDetails[0].gender;
    // this.profileform.bdate = this.userDetails[0].dob;
    // if(!this.userDetails[0].profileImg){
    //     this.proImage  = 'assets/imgs/default-profile-picture.jpg';
    // }else{
    //     this.proImage  = this.userDetails[0].profileImg;
    // }
   
  }
  parseISOString(s) {
    var b = s.split(/\D+/);
    return new Date(Date.UTC(b[0], --b[1], b[2], b[3], b[4], b[5], b[6]));
  }
  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [ year,month,day].join('-');
  };
  ionViewDidLoad() 
  {
    let param = {
      imobileno: this.mobileNoDetails
   }
   let loader = this.loadingCtrl.create({
    content: "Please wait..."
  });
  loader.present();
   this.webAPI.getService(ENUM.domain + ENUM.url.loginUser, param)
				.then(result => {
					console.log("result", result);
					var temp: any = result;
					var uData = temp.data;
          this.uData1 = uData[0];
          console.log("uData1", this.uData1);
          this.profileform.firstname = this.uData1[0].regusername;
          this.profileform.lastname = this.uData1[0].reguserlastname,
          this.profileform.email = this.uData1[0].email;
          this.profileform.mobile = this.uData1[0].mobileno;
          this.profileform.gender = this.uData1[0].gender;
          this.profileform.bdate =  new Date(this.formatDate(this.parseISOString(this.uData1[0].dob))).toISOString(); //this.uData1[0].dob;
          console.log('parseISOString',new Date(this.formatDate(this.parseISOString(this.uData1[0].dob))).toISOString());
          if(this.uData1[0].profileImg === "" || this.uData1[0].profileImg === undefined || this.uData1[0].profileImg === null){
              this.proImage  = 'assets/imgs/default-profile-picture.jpg';
               }else{                 
                this.proImage  = ENUM.domain+ENUM.url.fetchProfileImg+this.uData1[0].profileImg;
                console.log('this.proImage  ',this.proImage );
                }
                loader.dismiss();

        }).catch(result => {
          console.log("Error :", result);
          loader.dismiss();

				})
    console.log('ionViewDidLoad called');

  }
   ionViewWillEnter() 
  {
    console.log('ionViewWillEnter called');
  }

  ngOnInit() {
        this.profileForms = new FormGroup({
            
            firstname: new FormControl('', [Validators.required]),
           lastname: new FormControl('', [Validators.required]),
            email: new FormControl('', [Validators.required,Validators.pattern('^[a-zA-z0-9+-_.]+@[a-zA-Z]+.[a-zA-Z]{2,4}')]),
            gender: new FormControl('', [Validators.required]),
            mobile: new FormControl('',Validators.compose([Validators.maxLength(10),Validators.minLength(10), Validators.pattern('[0-9]*'), Validators.required])),
            bdate:  new FormControl('',[Validators.required]),
           // radioMarried:  new FormControl('',[Validators.requiredTrue]),
        });
    }

    // openImagepicker(){
    //         this.imagePicker.getPictures(options).then((results) => {
    //     for (var i = 0; i < results.length; i++) {
    //         console.log('Image URI: ' + results[i]);
    //     }
    //   }, (err) => { });
    // }

    saveProfile(profileform)
        {
        console.log('edited profile',profileform);
        let param = {
         "regID": this.userDetails[0].reguserid,
         "regName":profileform.firstname,
         "regLastName": profileform.lastname,
         "EID":profileform.email,
         "Gen" : profileform.gender,
         "Dob":profileform.bdate,
         "proImg" : this.proImage,
          
        }
        let loader = this.loadingCtrl.create({
            content:"Please wait..."
        });
        loader.present();  
      var url = ENUM.domain + ENUM.url.updateprofile;
        this.webAPI.getService(url,param)
          .then(result => {
            console.log('Profile:', result);
            let temp :any = result;
            if(temp.type === true){
              let userdata :any = temp.data;
              console.log('userdata =>',userdata);
              loader.dismiss();
              this.presentToast('Profile Updated Successfully!');
              // localStorage.removeItem('UserDetails');
              var userDataString = JSON.stringify(userdata);
              localStorage.setItem('UserDetails', userDataString);
            }
            else{
              this.presentToast('Profile Not Saved');
            }
          }).catch(result => {
            loader.dismiss();
            this.presentToast('Something went wrong!!');
            console.log("Server Response Error :", result);
          });

      }

      selectImg(){
        let actionSheet = this.actionSheetController.create({
          buttons: [
             {
              text: "Click picture",
              icon: 'camera',
              handler: () => {
               this.opneCamera();
                // .catch(() => {
                //   //error
                // });
              }
            },
            {
              text: "Select picture from gallery",
              icon: 'images',
              handler: () => {
                this.openGallery();
                // .catch(() => {
                //   //error
                // });
              }
            },
            {
              text: 'Cancel',
              role: 'cancel',
              icon: 'close-circle',
              handler: () => {
                console.log('Cancel clicked');
              }
            }
          ]
        });
        actionSheet.present();

      }

      opneCamera(){
         const options: CameraOptions = {
          quality: 100,
          sourceType: this.camera.PictureSourceType.CAMERA,
          destinationType: this.camera.DestinationType.DATA_URL,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE,
          saveToPhotoAlbum: true,
          correctOrientation: true
        }
        this.camera.getPicture(options).then((imageData) => {
         // imageData is either a base64 encoded string or a file URI
         // If it's base64 (DATA_URL):
            let base64Image = 'data:image/jpeg;base64,' + imageData;
            this.proImage = base64Image;
         }, (err) => {
           // Handle error
           console.log('error :-', err);
        });
      }

      openGallery(){
        const options: CameraOptions = {
              quality: 100,
              sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
              saveToPhotoAlbum: false,
              destinationType: this.camera.DestinationType.DATA_URL,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE,
              correctOrientation: true,
         }

          this.camera.getPicture(options).then((imageData) => {
            let base64Image = 'data:image/jpeg;base64,' + imageData;
            this.proImage = base64Image;
           }, (err) => { 
             console.log('error :-', err);
          });
      }
      presentToast(text) {
        let toast = this.toastCtrl.create({
          message: text,
          duration: 2000,
          position: 'top'
        });
      
        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });
      
        toast.present();
      }   
}
