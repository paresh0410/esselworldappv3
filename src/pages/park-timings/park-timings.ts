import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { webAPIService } from '../../service/webAPIService';
import { ENUM } from '../../service/ENUM';
import { checkIfOnlineService } from '../../service/checkIfOnlineService';
import { HomePage } from '../home/home'
import { SectionPage } from '../section/section';
import { NotificationPage } from '../notification/notification';
import { CartPage } from '../cart/cart';
/**
 * Generated class for the ParkTimingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-park-timings',
  templateUrl: 'park-timings.html',
})
export class ParkTimingsPage {

	// parkTime : any = {
	// 	startDate:'09th Nov 2018',
	// 	endDate:'30th Dec 2018',
	// 	startTime:'10:00 am',
	// 	endTime:'21.30 pm'
	// };

  parkTimes : any = [];
  loader : any;
  nodataAvailable : boolean = false;
  note : boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,public checkOnline: checkIfOnlineService, public webAPI: webAPIService,public loadingCtrl: LoadingController, private toastCtrl: ToastController) {
    this.presentLoader();
    var hcId = localStorage.getItem('hcId');

    let param = {
      "hId":hcId
    }

    var onlineStatus = this.checkOnline.getIfOnline();
    
    this.webAPI.getService(ENUM.domain + ENUM.url.fetchParkTimings, param)
    .then(result => {
      //  console.log('ParkTimings:',result)
        var data : any = result;
        var temp = data.data;
        if(temp.length > 0){
            this.parkTimes = temp;
            console.log('ParkTimings:',this.parkTimes);
            for(let i = 0;i<this.parkTimes.length;i++){
              if(this.parkTimes[i].note == null){
                this.note = false;
              }else{
                this.note = true;
              }
            }
            this.closeLoader();
        }else{
          this.closeLoader();
          this.nodataAvailable = true;
        }
        
        

    })
    .catch(result => {
      console.log("ServiceResponseError :", result);
      this.closeLoader();
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ParkTimingsPage');
  }

  presentLoader() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.loader.present();
    console.log('ionViewDidLoad OffersPage');
    //   setTimeout(()=>{
    // this.offers = this.array;
    // loader.dismiss();
    //   },2000)
  }

  closeLoader() {
    this.loader.dismiss();
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'NO INTERNET CONNECTION',
      duration: 3000,
      position: 'top',
      //cssClass:"InternetOffToast"
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  gotohomePage(){
    // this.navCtrl.push(HomePage,{});
    this.navCtrl.popToRoot();
    
  }
  gotornotificationPage() {
		this.navCtrl.push(NotificationPage);
		console.log('notification.');
	}
	gotocart() {
		this.navCtrl.push(CartPage);
	}
}
