import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { SwipeTabComponent } from '../../components/swipe-tab/swipe-tab';
import { webAPIService } from '../../service/webAPIService';
import { ENUM } from '../../service/ENUM';
import { checkIfOnlineService } from '../../service/checkIfOnlineService';
import { NoInternetConnectionPage } from '../no-internet-connection/no-internet-connection';
import { HomePage } from '../home/home';
import { GroupsNschoolsDetailsPage } from '../groups-nschools-details/groups-nschools-details';
import { CartPage } from '../cart/cart';
/**
 * Generated class for the GroupsNschoolsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-groups-nschools',
  templateUrl: 'groups-nschools.html',
})

export class GroupsNschoolsPage {

	groups: any = []
	array:any = [];
	dots: any;
	nodataAvailable : boolean = false;
	constructor(public navCtrl: NavController, public navParams: NavParams,
		 public webAPI: webAPIService,
		public loadingCtrl: LoadingController, public  checkOnline: checkIfOnlineService,
		private alertCtrl: AlertController) {
			this.dots = "...";
			console.log('dots',this.dots);
		// this.offers = [
		// 	{
		// 		tabId: '1',
		// 		tabLabel: 'SCHOOLS & COACHING CLASSES',
		// 		contentImg: 'assets/imgs/schools-and-coaching-class.jpg',
		// 		contentHead: 'SCHOOLS & COACHING CLASSES',
		// 		subContentTitle: 'GIVES US A RING; LET THE PICNIC BEGIN',
		// 		subContentP1: 'We believe in educating, inspiring and informing children through fun engagement activities. By educating them we strive to make a positive, lasting and sustainable impact on society.',
		// 		subContentP2: 'Learning is fun for kids when mixed with entertainment. Credited as country’s first green park and committed to environment and encompassed in a zero pollution zone. We have designed a special environment based outbound training program that will help kids understand nature first hand.',
		// 		subContentP3: 'Also, its never-before seen attractions set amidst unique architecture with international standard amenities and hygiene; are truly an experience to look forward to.',
		// 	},
		// 	{
		// 		tabId: '2',
		// 		tabLabel: 'CORPORATES & MICE',
		// 		contentImg: 'assets/imgs/corporatesNmice.jpg',
		// 		contentHead: 'CORPORATES & MICE',
		// 		subContentTitle: 'EsselWorld & Water Kingdom creates a perfect getaway for team building activities, meetings, product launch, dealer meets, conferences and events, all in a refreshingly manner.',
		// 		subContentP1: 'At EsselWorld & Water Kingdom, we understand the value of the time taken out by the different individuals/corporate in attending meeting, conferences & various team building activities. Hence, we endeavor to ensure that we pay attention to each and every aspect of arranging a meeting, conference or any corporate event - right from arranging transfers via ferry or road to speedy entry & other necessary arrangements like well-defined area for activities, Breakfast & Buffet lunch, access to all rides & lots more.',
		// 		subContentP2: 'We at EsselWorld know that success comes from truly inspired work and dedication. We realize that motivated people achieve beyond limits. But most importantly, we know how to recognize and appreciate those achievers.',
		// 		subContentP3: 'Let our parks be your event planning partner. Our experienced and professional planning teams and catering will help you create an unforgettable events that’s guaranteed to build camaraderie.',
		// 	},
		// 	{
		// 		tabId: '3',
		// 		tabLabel: "BIRTHDAY PARTY PACKAGE",
		// 		contentImg: 'assets/imgs/Birthday-Party-PackageEW.jpg',
		// 		contentHead: 'CORPORATES & MICE',
		// 		subContentP1: 'Plan the best outdoor birthday in Mumbai at India’s happiest island. Make your kids ‘next birthday celebration special at EsselWorld and Water Kingdom. We specialize in organizing memorable birthday parties for boys and girls aged from 1yrs to teenagers. We offer a professional bespoke party planning service, taking care of everything your party needs.',
		// 	},
		// 	{
		// 		tabId: 4,
		// 		tabLabel: 'GROUPS',
		// 		contentImg: 'assets/imgs/groupsEW.jpg',
		// 		contentHead: 'GROUPS',
		// 		subContentP1: 'Every year we host hundreds of groups, in sizes from 30 to 30,000. Associations, Religious organizations, Youth groups, Reunions, Social groups & family gatherings. You name it, we have hosted it!',
		// 		subContentP2: 'We offer exceptionally low prices for a wide range of group sizes, from as few as 30 to over 30,000 customers.',
		// 		subContentP3: 'Whatever the size of your group, our friendly and experienced executives will be happy to discuss your requirements, make recommendations and provide you with a highly competitive quote.'
		// 	},
		// 	{
		// 		tabId: 5,
		// 		tabLabel: 'SPECIAL OCCASIONS',
		// 		contentImg: 'assets/imgs/special-occasions.jpg',
		// 		contentHead: 'SPECIAL OCCASIONS',
		// 		subContentTitle: 'WE KNOW HOW TO PUT UP A SHOW!',
		// 		subContentP1: 'If you still do not know it yet, then we at EsselWorld & Water kingdom host some pretty awesome parties. Whatever be the occasion, we make sure it is of equal priority to us as it is to you.',
		// 		subContentP2: 'Your special occasions and events now get all the more special amidst 64 acres of good fun with tailormade solutions. ',
		// 		subContentP3: 'You can celebrate birthday parties, weddings, anniversary, and lots more at our parks.',
		// 	},
		// ]

		var onlineStatus = this.checkOnline.getIfOnline();
		if(onlineStatus == true){
			var url = ENUM.domain + ENUM.url.groupList;
			this.webAPI.getService(url,'')
			.then(result => {
				if(result != 'err'){
					console.log("Success :",  result);
					let temp : any= result;

					if(temp.data.length > 0){
						this.array = temp.data;
						var x=0;
						for(var i=0;i<this.array.length;i++){
							x++
							this.array[i].tabId = "" + x + "";
							if(this.array[i].imgpath !== null || this.array[i].imgpath !== undefined || this.array[i].imgpath !== ''){
								var checkImgpath = this.array[i].imgpath.includes("/img/ego/");
								if(!checkImgpath){
									this.array[i].contentImg = 'assets/imgs/default.png'
								}
								else{
									this.array[i].contentImg = ENUM.domain+ENUM.url.imgUrl+this.array[i].imgpath;
							}
						}
						}
					}else{
						this.nodataAvailable = true;
					}
					
				}else{
					this.serverErrorAlert();
				}
			})
			.catch(result =>{
				console.log("Error :",  result);
			})
		}else{
			this.navCtrl.push(NoInternetConnectionPage);
		}

	}
	
	serverErrorAlert(){
		let alert = this.alertCtrl.create({
			title: 'Server Error!!!',
			message: 'Server Not Responding. Please try again later.',
			buttons: [
				{
					text: 'OK',
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
						this.navCtrl.setRoot(HomePage,{AP: 'Y'});
					}
				},
			]
		});
		alert.present();
	}
	gotogroupsDetails(item){

		this.navCtrl.push(GroupsNschoolsDetailsPage,{


			eventgroupofferId:item.eventgroupofferId,
			contentImg:item.contentImg,
			contentHead:item.contentHead,
			subContentP1:item.subContentP1,
		});
	}
    ionViewDidLoad() {
		let loader = this.loadingCtrl.create({
			content: "Please wait..."
		});     
		loader.present();	
	  	console.log('ionViewDidLoad OffersPage');
	  	setTimeout(()=>{
			this.groups = this.array;
			console.log('this.groups == > ',this.groups);
			loader.dismiss();
	  	},2000)
    }

		gotocart(){
			this.navCtrl.push(CartPage);
		}
}
