import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { OtpPage } from '../otp/otp';
import { FormBuilder, FormControl, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { webAPIService } from '../../service/webAPIService';
import { ENUM } from '../../service/ENUM';
import { AlertController } from 'ionic-angular';
import { checkIfOnlineService } from '../../service/checkIfOnlineService';

/**
 * Generated class for the RegisterNewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-register-new',
	templateUrl: 'register-new.html',
})
export class RegisterNewPage {
	regRes: any = [];
	registerCredentials: any = {};
	registerForm: FormGroup;
	temp: any = [];
	regRes1: any = [];

	neaArray: any = [{
		reguserid: '',
		registerflag: '',
	}];

	constructor(private alertCtrl: AlertController, public toastCtrl: ToastController, 
		public navCtrl: NavController, public navParams: NavParams, public webAPI: webAPIService,
		public  checkOnline: checkIfOnlineService) {
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad RegisterNewPage');
	}

	gotoLogin() {
		this.navCtrl.setRoot(LoginPage);
	}

	gotoOtp() {
		var onlineStatus = this.checkOnline.getIfOnline();
		if(onlineStatus == true){
			var param = {
				iregusername: this.registerCredentials.name,
				ireglastname: this.registerCredentials.lname,
				imobileno: this.registerCredentials.mobile,
				add: this.registerCredentials.addr,
				idob: this.registerCredentials.bdate,
				igen: this.registerCredentials.gender
			}
	
			localStorage.setItem('mobileNumber', this.registerCredentials.mobile)
	
			var otpParam = {
				imobileno: this.registerCredentials.mobile
			}
			this.webAPI.getService(ENUM.domain + ENUM.url.registerUser, param)
			.then(result => {
				console.log('registerResponse', result)
				var temp: any = result;
				var regRes = temp.data[0];
	
				console.log('tempAray1 ', regRes)
	
				var regRes1 = JSON.stringify(regRes)
				localStorage.setItem('UserDetails', regRes1);
	
				if (regRes[0].registerflag == 0) {
					this.existUserAlert();
				} else {
					console.log("Success :", result);
					this.webAPI.otpService(ENUM.domain + ENUM.url.sendOtp, otpParam)
					.then(result => {
						if(result != 'err'){
							var temp: any = result
							var SendOtpResponse = JSON.parse(temp.data)
							console.log('send', SendOtpResponse)
							var fstr = JSON.stringify(SendOtpResponse.Details)
							localStorage.setItem('SendOtpResponse', fstr)
							console.log('sendOtpRespnse', result);
							this.navCtrl.push(OtpPage);
						}else{
							this.serverErrorAlert();
						}
					}).catch(result => {
						console.log("Error :", result);
					})
				}
	
			}).catch(result => {
				console.log("Error :", result);
				this.presentToast('Something went wrong.please try again.');
			})
		}else{
			this.presentToast('No Internet Connection!!!');
		}	
	}

	serverErrorAlert(){
		let alert = this.alertCtrl.create({
			title: 'Server Error!!!',
			message: 'Server Not Responding. Please try again later.',
			buttons: [
				{
					text: 'OK',
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
					}
				},
			]
		});
		alert.present();
	}

	existUserAlert() {
		let alert = this.alertCtrl.create({
			title: 'Registration',
			message: 'User with same mobile number already exists. Login using mobile number?',
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
					}
				},
				{
					text: 'OK',
					handler: () => {
						console.log('Buy clicked');
						this.navCtrl.setRoot(LoginPage);
					}
				}
			]
		});
		alert.present();
	}

	presentToast(text) {
		let toast = this.toastCtrl.create({
			message: text,
			duration: 2000,
			position: 'top'
		});

		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});

		toast.present();
	}



	ngOnInit() {
		this.registerForm = new FormGroup({
			name: new FormControl('', [Validators.required, Validators.minLength(2), Validators.pattern('^[a-zA-Z ]+$')]),
			lname: new FormControl('',[Validators.required, Validators.minLength(1), Validators.pattern('^[a-zA-Z ]+$')]),
			mobile: new FormControl('', Validators.compose([Validators.maxLength(10), Validators.minLength(10), Validators.pattern('[0-9]*'), Validators.required])),
			bdate: new FormControl('', [Validators.required]),
			gender: new FormControl('', [Validators.required]),
			addr: new FormControl(''),
		});
	}

}
