import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events } from 'ionic-angular';
import { NotificationPage } from '../notification/notification';
import { CartPage } from '../cart/cart';
import { ThemeServiceProvider } from '../../providers/theme-service/theme-service';

/**
 * Generated class for the OffersDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-offers-details',
  templateUrl: 'offers-details.html',
})
export class OffersDetailsPage {
  eventgroupofferId: any;
  contentImg: any;
  contentHead: any;
  subContentP1: any;
  ThemeC: string;
  red: boolean;
	blue: boolean;
	green: boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
    public event: Events,
		public themeServices: ThemeServiceProvider) {
      if(localStorage.getItem('theme')){
        var previous = this.navCtrl.getPrevious();
        console.log('previous ', previous);
        this.ThemeC = localStorage.getItem('theme');
      }
      else{
                this.ThemeC = this.themeServices.getTheme();
            this.event.subscribe('theme', () => {
              this.ThemeC = this.themeServices.getTheme();
            });
            if(this.ThemeC == 'red'){
              this.red = true;
              this.blue = false;
              this.green = false;
            }else if(this.ThemeC == 'blue'){
              this.blue = true;
              this.red = false;
              this.green = false;
            }else if(this.ThemeC == 'green'){
              this.green = true;
              this.red = false;
              this.blue = false;
            }
      }
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });     
    loader.present();
    this.eventgroupofferId = this.navParams.get('eventgroupofferId');
    this.contentImg = this.navParams.get('contentImg');
    this.contentHead = this.navParams.get('contentHead');
    this.subContentP1 = this.navParams.get('subContentP1');
    loader.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OffersDetailsPage');
  }
  gotohomePage(){
    // this.navCtrl.push(HomePage,{});
    console.log('gotohomePage function called');
    this.navCtrl.popToRoot();
    
  }

  gotocart() {
		this.navCtrl.push(CartPage);
	}

	gotornotificationPage() {
		this.navCtrl.push(NotificationPage);
		console.log('notification.');
  }


}
