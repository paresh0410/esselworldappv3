import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OffersDetailsPage } from './offers-details';

@NgModule({
  declarations: [
    OffersDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(OffersDetailsPage),
  ],
})
export class OffersDetailsPageModule {}
