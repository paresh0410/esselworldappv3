import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,ToastController, LoadingController, Events } from 'ionic-angular';
import { Slides,App } from 'ionic-angular';
import { RidesService } from '../../service/ride.service';
import { RideDetailsPage } from '../ride-details/ride-details';
import { ENUM } from '../../service/ENUM';
import { checkIfOnlineService } from '../../service/checkIfOnlineService';
import { webAPIService } from '../../service/webAPIService';
import { SectionPage } from '../section/section';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';
import { SchedulePage } from '../schedule/schedule';
import { NotificationPage } from '../notification/notification';
import { CartPage } from '../cart/cart';
import { ThemeServiceProvider } from '../../providers/theme-service/theme-service';

/**
 * Generated class for the RidePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ride',
  templateUrl: 'ride.html',
})
export class RidePage {
	@ViewChild(Slides) slides: Slides;
	relationship: string 
	// = '1';
	currentIndex:number = 0;
	rides:any=[];
	subrides:any=[];
	hcId : any;
	lcId : any;
	loader:any;
	passParams: any;
	categoryList : any;
	attraction: any;
	scheduleBtn : boolean = false;
	nodataAvailable : boolean = false;
	red: boolean = false;
	blue: boolean = false;
	green: boolean = false;
  tabArray:any=[
  	{
  		id: '1',
  		name: 'ADULT RIDES',
  	},
  	{
  		id: '2',
  		name: 'FAMILY RIDES'
  	},
  	{
  		id: '3',
  		name: 'KIDS HAPPY RIDES'
  	}
  ]
	ThemeC: any;
	constructor(public navCtrl: NavController, public navParams: NavParams,
		public rideservice : RidesService,public webAPI: webAPIService,
		public checkOnline: checkIfOnlineService,private alertCtrl: AlertController,
		private toastCtrl: ToastController,public loadingCtrl: LoadingController, public app:App,public events: Events,
		public themeServices: ThemeServiceProvider) {
	//   this.app._setDisableScroll(true);
	////////  Chnage for theme   ///////////////
	this.ThemeC = this.themeServices.getTheme();
	this.events.subscribe('theme', () => {
		this.ThemeC = this.themeServices.getTheme();
	});
	console.log('Theme C', this.ThemeC);
	if(this.ThemeC == 'red'){
		this.red = true;
		this.blue = false;
		this.green = false;
	}else if(this.ThemeC == 'blue'){
		this.blue = true;
		this.red = false;
		this.green = false;
	}else if(this.ThemeC == 'green'){
		this.green = true;
		this.red = false;
		this.blue = false;
	}
	////////////////////////////////////////////
  	this.presentLoader();
  	if(localStorage.getItem('login')=='Y'){
        this.scheduleBtn = true
    }else{
        this.scheduleBtn = false
    }

  	this.hcId = localStorage.getItem('hcId');
  	this.categoryList=this.navParams.get('categoryList');
	this.lcId = this.categoryList.id;
	if(this.lcId == 10 || this.lcId == 19){
		this.lcId = 1;
	}
	else if(this.lcId == 11 || this.lcId == 20){
		this.lcId = 2;
	}
	else if( this.lcId == 12 || this.lcId == 21){
		this.lcId = 3;
	}
	else if(this.lcId == 8 || this.lcId == 17 || this.lcId == 26){
		this.lcId = 7;
	}
	// else if(this.lcId == 14){
	// 	this.lcId = 5;
	// }
	this.attraction = this.categoryList.title;
  	console.log('this.lcId',this.lcId);
  	if(localStorage.getItem('login') == 'Y'){
		var userDetails = JSON.parse(localStorage.getItem('UserDetails'));
	    console.log('userDetails',userDetails);
	    var userid = userDetails[0].reguserid;
	    
	  	let param = {
	  		
	  		lcId : this.lcId,
	  		hcId : this.hcId,
	  		userId :userid
        }
        this.passParams = param;
  	}else{
		let param = {
	  		
	  		lcId : this.lcId,
	  		hcId : this.hcId,
	  		userId:0

	  	}	
	  	this.passParams = param
  	}
  	

  	console.log('params',this.passParams)

  	var onlineStatus = this.checkOnline.getIfOnline();
			var url = ENUM.domain + ENUM.url.attractions;
			this.webAPI.getService(url,this.passParams)
			.then(result => {
					var temp : any = result
					console.log('rideArray:',temp)
					var rides = temp.data

					if(rides.length > 0){
							for(let i=0;i<rides.length;i++){
							var subrides = rides[i].subRides
							//this.subrides = rides[i].subRides
							rides[i].lscid = "" + rides[i].lscid + ""
							for(let j=0;j<subrides.length;j++){
								rides[i].subRides[j].imgpath = ENUM.domain+ENUM.url.imgUrl+rides[i].subRides[j].imgpath;
								// console.log('Ride Imgs Path',rides[i].subRides[j].imgpath);
								// if(rides[i].subRides[j].dislike == null){
								// 	rides[i].subRides[j].dislike = 0
								// }
								// if(rides[i].subRides[j].like == null){
								// 	rides[i].subRides[j].like = 0
								// }
								// if(rides[i].subRides[j].reglikestatus == null){
								// 	this.alertforLogin();
								// }

								if(rides[i].subRides[j].imgpath !== null || rides[i].subRides[j].imgpath !== undefined || rides[i].subRides[j].imgpath !== ''){
									var checkImgpath = rides[i].subRides[j].imgpath.includes("/img/loc/");
	     							 if (!checkImgpath) {
										rides[i].subRides[j].imgpath = "assets/imgs/default.png";
									  }
								} 
							}
							
						}
						this.closeLoader();
						this.rides = rides;
						// for(let i=0;i<rides.length;i++){
							this.relationship = this.rides[0].lscid;
						// }
						
						console.log('Rides:',this.rides)
					}else{
						this.closeLoader();
						this.nodataAvailable = true;
					}

					
			})
			.catch(result =>{
				this.closeLoader();
				console.log("ServerResponseError :",  result);
			})


  	
  	// this.rides = this.rideservice.getAll();
  	//console.log('All Rides:',this.rides);
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad RidePage');
  }

  slideChanged(dataIndex) {
		if(dataIndex == undefined || dataIndex == null){
			this.currentIndex = this.slides.getActiveIndex();
			console.log('Current index is', this.currentIndex);
			if(this.rides.length != this.currentIndex){
				var Index = this.rides[this.currentIndex].lscid
				// this.contentlist=this.tabs[this.currentIndex].tabContent;
				this.relationship = Index;
				var element = this.currentIndex + 1;
				var ID = "content" + '-' + element;
				var elmnt = document.getElementById(ID);
				if(elmnt != null){
					elmnt.scrollIntoView();
				}
			}
		}else{
			this.currentIndex = dataIndex;
			console.log('Current index is', this.currentIndex);
			if(this.rides.length != this.currentIndex){
				Index = this.rides[this.currentIndex].lscid
				// this.contentlist=this.tabs[this.currentIndex].tabContent;
				this.relationship = Index;
				element = this.currentIndex + 1;
				ID = "content" + '-' + element;
				elmnt = document.getElementById(ID);
				if(elmnt != null){
					elmnt.scrollIntoView();
				}
			}
		}
	}

	tabChanged(tabData){
		for(let i=0;i<this.rides.length;i++){
			if(this.rides[i].lscid == tabData.lscid){
				console.log('TABDATA--->',tabData);
				this.slides.slideTo(i);
			}
		}
		
		// this.slideChanged(toIndex);
	}

	gotorideDetails(item){
		console.log('item ==>',item);
		console.log('item.id',item.id);
		this.navCtrl.push(RideDetailsPage,{
			catId:this.lcId,
			item:item.id,
			img:item.imgpath,
			name:item.name,
			desc:item.desc,
			likes:item.like,
			dislikes:item.dislike,
			reglikestatus:item.reglikestatus,
			youtubelink:item.youtubelink,
			title : this.categoryList.title,
		});
	}

	gotoschedule(item){
		console.log('Some Item--->>',item);
		var UD:any = JSON.parse(localStorage.getItem('UserDetails'));
		var param = {
			userId: UD[0].reguserid,
			locId: item.id,
			lSeen: 0,
			actv: 1
		}
		var url = ENUM.domain + ENUM.url.updateMySchedule;
		var onlineStatus = this.checkOnline.getIfOnline();
		if(onlineStatus == true){
			this.webAPI.pushService(url,param)
			.then(result => {
				if(result == 'err'){
					this.presentToast('Cannot connect to server! Action cannot be completed.');
					console.log('SERVER ERROR');
				}else{
					var toastString = item.name+' has been added to your schedule'
					this.presentToast(toastString);
				}
			})
		}else{
			this.presentToast('No Internet Connection! Action cannot be completed.');
		}
	}


	presentLoader(){
    	this.loader = this.loadingCtrl.create({
			content: "Please wait..."
		});     
		this.loader.present();
	  	console.log('ionViewDidLoad OffersPage');
	  // 	setTimeout(()=>{
			// this.offers = this.array;
			// loader.dismiss();
	  // 	},2000)
    }

    closeLoader(){
 
    	//this.offers = this.array;
	    this.loader.dismiss();
    }

    presentToast(text) {
		let toast = this.toastCtrl.create({
		  message: text,
		  duration: 2000,
		  position: 'top'
		});
  
		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});
  
		toast.present();
	}

	gotohomePage(){
		// this.navCtrl.push(HomePage,{});
		console.log('gotohomePage function called');
		this.navCtrl.popToRoot();
		
	  }

	scrolling(event){
		console.log("Scrolling -> ", event);
	}
	scrollComplete(event){
		console.log("Scroll Complete -> ", event);
	}
	gotornotificationPage() {
		this.navCtrl.push(NotificationPage);
		console.log('notification.');
	}
	gotocart() {
		this.navCtrl.push(CartPage);
	}
}
