import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaidAttractionsPage } from './paid-attractions';

@NgModule({
  declarations: [
    PaidAttractionsPage,
  ],
  imports: [
    IonicPageModule.forChild(PaidAttractionsPage),
  ],
})
export class PaidAttractionsPageModule {}
