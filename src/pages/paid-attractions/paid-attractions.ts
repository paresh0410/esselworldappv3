import { Component, ViewChild } from '@angular/core';
import {
  IonicPage, NavController, NavParams, LoadingController, AlertController, ToastController,
  Platform, Nav , Events 
} from 'ionic-angular';
import { ENUM } from '../../service/ENUM';
import { webAPIService } from '../../service/webAPIService';
import { checkIfOnlineService } from '../../service/checkIfOnlineService';
import { Device } from '@ionic-native/device';
import { UserDetailsPage } from '../../pages/user-details/user-details';
import { HomePage } from '../home/home';
import { CartPage } from '../cart/cart';
import { NotificationPage } from '../notification/notification';
import { TicketCategoryPage } from '../ticket-category/ticket-category';
import { SelectParkPage } from '../select-park/select-park';
import { ThemeServiceProvider } from '../../providers/theme-service/theme-service';
import { BookhomecategoryPage } from '../../pages/bookhomecategory/bookhomecategory';

// import { HomePage  } from '../home/home';
/**
 * Generated class for the PaidAttractionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-paid-attractions',
  templateUrl: 'paid-attractions.html',
})
export class PaidAttractionsPage {
  @ViewChild(Nav) nav: NavController;
  todayDate = new Date();
  paCategory: any = [];
  paidAttractionsBucket: any = [];
  tktbucketarray: any = [];
  ticketDate: any = [];
  trackerId: any;
  trckrID: any = [];
  tickId: any = [];
  tickPrice: any = [];
  deviceId: any = [];
  userId: any = [];
  itemValue: boolean = false;
  alert: any;
  pushToBook: any;
  tcktDt: any;
  nodataAvailable: boolean = false;
  loader: any;
  totalTicketPrice: number;
  totalTicketCount: number;
  red: boolean;
  blue: boolean;
  green: boolean;
  ThemeC: any;

  buttonClicked: boolean = false;
  toggle: boolean = false;
  rotate: boolean;
  getColor(hc){
    switch(hc){
      case 'red':
        return '1px solid #ff0000 ';
      case 'blue':
        return '1px solid #488aff';
      case 'green':
        return '1px solid #16910b';
    }
  }

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public webAPI: webAPIService,
    public checkOnline: checkIfOnlineService, private alertCtrl: AlertController, public platform: Platform,
    public themeServices: ThemeServiceProvider,public events: Events,
    private toastCtrl: ToastController, public loadingCtrl: LoadingController, private device: Device) {

    this.ThemeC = this.themeServices.getTheme();
      this.events.subscribe('theme', () => {
        this.ThemeC = this.themeServices.getTheme();
      });
      if(this.ThemeC == 'red'){
        this.red = true;
        this.blue = false;
        this.green = false;
      }else if(this.ThemeC == 'blue'){
        this.blue = true;
        this.red = false;
        this.green = false;
      }else if(this.ThemeC == 'green'){
        this.green = true;
        this.red = false;
        this.blue = false;
      }
      
      this.pushToBook = this.navParams.get('pushToBook');
      console.log('this.pushToBook ==>', this.pushToBook);
      this.tcktDt = this.navParams.get('ticketDate');
      console.log('this.tcktDt  ==>', this.tcktDt);
      this.trackerId = localStorage.getItem('trackerId');
  }

  ionViewDidEnter(){

    this.fetchTickets1();
  }

  onIncrement(mainid, subid, count) {
    console.log(mainid, subid, count);

    for (let i = 0; i < this.paCategory.length; i++) {
      if (mainid == this.paCategory[i].id) {
        for (let j = 0; j < this.paCategory[i].subCat.length; j++) {
          if (subid == this.paCategory[i].subCat[j].id) {
            this.paCategory[i].subCat[j].Qnt = count + 1;
            this.addremoveTicket(this.paCategory[i].subCat[j]);
          }
        }
      }
    }
  }
  onDecrement(mainid, subid, count) {
    console.log(mainid, subid, count);

    for (let i = 0; i < this.paCategory.length; i++) {
      if (mainid == this.paCategory[i].id) {
        for (let j = 0; j < this.paCategory[i].subCat.length; j++) {
          if (subid == this.paCategory[i].subCat[j].id) {
            if (this.paCategory[i].subCat[j].Qnt != 0)
              this.paCategory[i].subCat[j].Qnt = count - 1;
            this.addremoveTicket(this.paCategory[i].subCat[j]);
          }
        }
      }
    }
  }

  addremoveTicket(ticket) {
    console.log('ticket', ticket);
    var checkF = this.checkFlag(ticket);
    if (checkF) {
      for (var i = 0; this.paidAttractionsBucket.length > i; i++) {
        if (this.paidAttractionsBucket[i].id == ticket.id) {
          if (ticket.Qnt == 0) {
            this.paidAttractionsBucket.splice(i, 1);
          }
          if (ticket.Qnt > 0) {
            this.paidAttractionsBucket[i] = ticket;
            this.paidAttractionsBucket[i].totalPrice = ticket.Qnt * ticket.displaytickprice;
          }
        }
      }
    }
    if (!checkF) {
      ticket.totalPrice = ticket.Qnt * ticket.displaytickprice;
      this.paidAttractionsBucket.push(ticket);
    }
  }

  TotalTCount() {
    var tcount = 0;
    for (var i = 0; this.paidAttractionsBucket.length > i; i++) {
      tcount = tcount + this.paidAttractionsBucket[i].Qnt;
    }
    return tcount;
  }

  TotalTPrice() {
    var tprice = 0;
    for (var i = 0; this.paidAttractionsBucket.length > i; i++) {
      tprice = tprice + this.paidAttractionsBucket[i].totalPrice;
    }
    return tprice;
  }

  checkFlag(ticket) {
    var flag = false;
    for (let i = 0; this.paidAttractionsBucket.length > i; i++) {
      if (this.paidAttractionsBucket[i].id == ticket.id) {
        flag = true;
        return flag;
      }
    }
  }

  DeleteTicket(ticket) {
    for (var i = 0; this.paidAttractionsBucket.length > i; i++) {
      if (this.paidAttractionsBucket[i].id == ticket.id) {
        this.paidAttractionsBucket.splice(i, 1);
        for (let i = 0; i < this.paCategory.length; i++) {
          for (let j = 0; j < this.paCategory[i].subCat.length; j++) {
            if (ticket.id == this.paCategory[i].subCat[j].id) {
              this.paCategory[i].subCat[j].Qnt = 0;
            }
          }
        }
      }
    }
  }

  showResult() {
    if (this.itemValue == false) {
      this.itemValue = true;
    }
    else {
      this.itemValue = false;
    }
    console.log(this.itemValue);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FAndBCategoryPage');
  }
  cardHeight(cart, carf) {
    return 100 + 32 * (cart + carf) + 'px';
  }
  alertF(MSG: any) {
    if (this.alert) {
      this.alert.dismiss();
      this.alert = null;
    } else {
      this.alert = this.alertCtrl.create({ cssClass: 'Myalert', buttons: ['OK'] });
      let alertmsg: string = ENUM.alertMessage.replace('####', MSG);
      this.alert.setMessage(alertmsg);
      this.alert.present();
    }
    this.alert = null;
  }

  // FUNCTION TO FETCH PAID ATTRACTION TICKETS

  fetchTickets1() {
    var hcArray: any = [];
    var trackerId: any;
    trackerId = localStorage.getItem('trackerId');
    console.log('trackerId for checkMain ', trackerId);
    var url = ENUM.domain + ENUM.url.checkMainTicket;
    var param = {
      trid: trackerId
    }
    this.webAPI.getService(url, param)
    .then(result => {
      console.log('result for MainTicket =>', result);
      let res: any = result;
      let res1 = res.data;
      let res2 = res1[0];
      let res2Len = res2.length;
      console.log('res2 in checkMain', res2.length);
      if (res2Len) {
        for(let i = 0; i < res2Len; i++){
          hcArray.push(res2[i].hcid);
          console.log('hcArray in loop ',hcArray);
        }
        console.log('hcArray ',hcArray);
        let hcArray1 =  hcArray.includes(1);
        let hcArray2 = hcArray.includes(2);
        console.log('hcArray1',hcArray1);
        console.log('hcArray2',hcArray2);
        // if (hcArray1) {
          console.log('this.pushToBook.bookHCID ======>',this.pushToBook.bookHCID)
          if (this.pushToBook.bookHCID == 1 && hcArray1) {
            this.checkMainTicket(1);
          // }
        } else if(this.pushToBook.bookHCID == 2 && hcArray2){
          this.checkMainTicket(2);
        } else if(this.pushToBook.bookHCID == 3){
          this.bpError(2);
        }
        else {
          this.parkErrorAlert();
        }
      }

    }).catch(error => {
      console.log('Error for main ticket ', error);
    });
}

  // FUNCTION TO SAVE THE PAID ATTRACTIONS DATA, TICKET QUANTITY

  proceed(){
    this.trckrID = [];
    this.tickId = [];
    this.tickPrice = [];
    this.deviceId = [];
    this.userId = [];
    let userDetails: any;
    let userID: any;
    if (localStorage.getItem('UserDetails')) {
      userDetails = JSON.parse(localStorage.getItem('UserDetails'));
      if (userDetails.length > 0) {
        userID = userDetails[0].reguserid;
      }
    } else if (!localStorage.getItem('UserDetails')) {
      userID = 0;
    }
    let param = {
      trckrID: '',
      tickId: '',
      tickPrice: '',
      hcId: '',
      deviceId: '',
      userId: '',
      tcktDt: '',
      ttypId: 3,
      len: ''
    }
    // var trackerId = JSON.parse(localStorage.getItem('trackerId'));
    this.totalTicketPrice = this.TotalTPrice();
    console.log('this.totalTicketPrice   ==>', this.totalTicketPrice);
    this.totalTicketCount = this.TotalTCount();
    console.log('this.totalTicketCount ==>', this.totalTicketCount);
    console.log('this.paidAttractionsBucket ==>', this.paidAttractionsBucket);
    var cardLength = this.paidAttractionsBucket.length;
    // LOOP TO PUSH THE DATA FROM paidAttractionsBucket IN ARRAY
    for (let i = 0; i < cardLength; i++) {

      for (let j = 0; j < this.paidAttractionsBucket[i].Qnt;) {
        // this.trckrID.push(this.trackerId);
        this.tickId.push(this.paidAttractionsBucket[i].id);
        this.tickPrice.push(this.paidAttractionsBucket[i].displaytickprice);
        this.deviceId.push(this.device.uuid);
        this.userId.push(userID);
        this.ticketDate.push(this.tcktDt);
        j++;
      }

    }
    console.log('this.trackerId ==>', this.trackerId);
    console.log('this.tickId   ==>', this.tickId);
    console.log('this.tickPrice  ==>', this.tickPrice);
    console.log('this.userId ==>', this.userId);
    param.trckrID = this.trackerId;
    param.tickId = this.tickId.toString();
    param.tickPrice = this.tickPrice.toString();
    param.hcId = this.pushToBook.bookHCID;
    param.deviceId = this.deviceId.toString();
    param.userId = this.userId.toString();
    param.tcktDt = this.ticketDate.toString();
    param.len = this.tickId.length;
    console.log('trckrID    ', param.trckrID);
    var url = ENUM.domain + ENUM.url.saveUnconfirmed;
    console.log('param ==>', param);
    this.presentLoader();
    this.webAPI.getService(url, param)
      .then(result => {
        console.log('UCLIST RETURN    ', result);
        this.closeLoader();
        // this.askOtherPark();
        this.navCtrl.push(SelectParkPage, { 'pushToBook': this.pushToBook, 'ticketDate': this.tcktDt, 'trackerId': this.trackerId, 'totalTicketPrice': this.totalTicketPrice, 'totalTicketCount': this.totalTicketCount });
        // this.navCtrl.push(UserDetailsPage, { 'pushToBook': this.pushToBook, 'ticketDate': this.tcktDt, 'trackerId': this.trackerId, 'totalTicketPrice': this.totalTicketPrice, 'totalTicketCount': this.totalTicketCount });
        // this.navCtrl.push(SelectParkPage, { 'pushToBook': this.pushToBook, 'ticketDate': this.tcktDt, 'totalTicketPrice': this.totalTicketPrice, 'totalTicketCount': this.totalTicketCount });
      })
      .catch(err => {
        console.log('err ==>', err);
        // this.closeLoader();
      });
  }
  proceed1() {
    this.trckrID = [];
    this.tickId = [];
    this.tickPrice = [];
    this.deviceId = [];
    this.userId = [];
    let userDetails: any;
    let userID: any;
    if (localStorage.getItem('UserDetails')) {
      userDetails = JSON.parse(localStorage.getItem('UserDetails'));
      if (userDetails.length > 0) {
        userID = userDetails[0].reguserid;
      }
    } else if (!localStorage.getItem('UserDetails')) {
      userID = 0;
    }
    let param = {
      trckrID: '',
      tickId: '',
      tickPrice: '',
      hcId: '',
      deviceId: '',
      userId: '',
      tcktDt: '',
      ttypId: 3,
      len: ''
    }
    // var trackerId = JSON.parse(localStorage.getItem('trackerId'));
    this.totalTicketPrice = this.TotalTPrice();
    console.log('this.totalTicketPrice   ==>', this.totalTicketPrice);
    this.totalTicketCount = this.TotalTCount();
    console.log('this.totalTicketCount ==>', this.totalTicketCount);
    console.log('this.paidAttractionsBucket ==>', this.paidAttractionsBucket);
    var cardLength = this.paidAttractionsBucket.length;
    // LOOP TO PUSH THE DATA FROM paidAttractionsBucket IN ARRAY
    for (let i = 0; i < cardLength; i++) {

      for (let j = 0; j < this.paidAttractionsBucket[i].Qnt;) {
        // this.trckrID.push(this.trackerId);
        this.tickId.push(this.paidAttractionsBucket[i].id);
        this.tickPrice.push(this.paidAttractionsBucket[i].displaytickprice);
        this.deviceId.push(this.device.uuid);
        this.userId.push(userID);
        this.ticketDate.push(this.tcktDt);
        j++;
      }

    }
    console.log('this.trackerId ==>', this.trackerId);
    console.log('this.tickId   ==>', this.tickId);
    console.log('this.tickPrice  ==>', this.tickPrice);
    console.log('this.userId ==>', this.userId);
    param.trckrID = this.trackerId;
    param.tickId = this.tickId.toString();
    param.tickPrice = this.tickPrice.toString();
    param.hcId = this.pushToBook.bookHCID;
    param.deviceId = this.deviceId.toString();
    param.userId = this.userId.toString();
    param.tcktDt = this.ticketDate.toString();
    param.len = this.tickId.length;
    console.log('trckrID    ', param.trckrID);
    var url = ENUM.domain + ENUM.url.saveUnconfirmed;
    console.log('param ==>', param);
    this.presentLoader();
    this.webAPI.getService(url, param)
      .then(result => {
        console.log('UCLIST RETURN    ', result);
        this.closeLoader();
        // this.askOtherPark();
        this.navCtrl.push(UserDetailsPage, { 'pushToBook': this.pushToBook, 'ticketDate': this.tcktDt, 'trackerId': this.trackerId, 'totalTicketPrice': this.totalTicketPrice, 'totalTicketCount': this.totalTicketCount });
        // this.navCtrl.push(SelectParkPage, { 'pushToBook': this.pushToBook, 'ticketDate': this.tcktDt, 'totalTicketPrice': this.totalTicketPrice, 'totalTicketCount': this.totalTicketCount });
      })
      .catch(err => {
        console.log('err ==>', err);
        // this.closeLoader();
      });
  }
  // FUNCTION TO SKIP PAID ATTRACTION SELECTION
  skip() {
    this.navCtrl.push(SelectParkPage, { 'pushToBook': this.pushToBook, 'ticketDate': this.tcktDt, 'totalTicketPrice': this.totalTicketPrice, 'totalTicketCount': this.totalTicketCount });
  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  };

  // FUNCTION TO CREATE TOASTER
  presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 4000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  presentLoader() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
      showBackdrop: true,
    });
    this.loader.present();
  }

  closeLoader() {

    //this.offers = this.array;
    this.loader.dismiss();
  }

  askOtherPark() {
    let alert = this.alertCtrl.create({
      title: 'DO YOU WANT TO ADD TICKETS OF OTHER PARK ?',
      buttons: [{
        text: 'Yes',
        handler: () => {
          // user has clicked the alert button
          // begin the alert's dismiss transition
          let navTransition = alert.dismiss();

          // start some async method
          // someAsyncOperation().then(() => {
          // once the async operation has completed
          // then run the next nav transition after the
          // first transition has finished animating out

          navTransition.then(() => {
            this.proceed();
            // this.navCtrl.push(SelectParkPage);
          });
          // });
          return false;
        }
      },
      {
        text: 'No',
        handler: () => {
          this.proceed1();
          // this.navCtrl.push(UserDetailsPage, { 'pushToBook': this.pushToBook, 'ticketDate': this.tcktDt, 'trackerId': this.trackerId, 'totalTicketPrice': this.totalTicketPrice, 'totalTicketCount': this.totalTicketCount });
        }
      }]
    });

    alert.present();
  }

  alertPark() {
    let alert = this.alertCtrl.create({
      title: 'Select a Park',
      buttons: [{
        text: 'Essel World',
        handler: () => {
          // user has clicked the alert button
          // begin the alert's dismiss transition
          let navTransition = alert.dismiss();

          // start some async method
          // someAsyncOperation().then(() => {
          // once the async operation has completed
          // then run the next nav transition after the
          // first transition has finished animating out

          navTransition.then(() => {
            this.pushToBook.bookHCID = 1;
            this.pushToBook.bookHead = 'Essel World'            
            let bookingDt = localStorage.getItem('bookingDt');
            this.navCtrl.push(TicketCategoryPage, { 'pushToBook': this.pushToBook, 'bookingDt': bookingDt });
          });
          // });
          return false;
        }
      },
      {
        text: 'Water Kingdom',
        handler: () => {
          // this.proceed1();
          let navTransition = alert.dismiss();
          // start some async method
          // someAsyncOperation().then(() => {
          // once the async operation has completed
          // then run the next nav transition after the
          // first transition has finished animating out

          navTransition.then(() => {
            this.pushToBook.bookHCID = 2;
            this.pushToBook.bookHead = 'Water Kingdom'
            let bookingDt = localStorage.getItem('bookingDt');
            this.navCtrl.push(TicketCategoryPage, { 'pushToBook': this.pushToBook, 'bookingDt': bookingDt });
          });
          // });
          return false;
        }
      }, {
        text: 'Bird Park',
        handler: () => {
          // this.proceed1();
          let navTransition = alert.dismiss();
          // start some async method
          // someAsyncOperation().then(() => {
          // once the async operation has completed
          // then run the next nav transition after the
          // first transition has finished animating out

          navTransition.then(() => {
            this.pushToBook.bookHCID = 3;
            this.pushToBook.bookHead = 'Bird Park'
            let bookingDt = localStorage.getItem('bookingDt');
            this.navCtrl.push(TicketCategoryPage, { 'pushToBook': this.pushToBook, 'bookingDt': bookingDt });
          });
          // });
          return false;
        }
      }]
    });

    alert.present();
  }

  gotornotificationPage() {
    this.navCtrl.push(NotificationPage);
    console.log('notification.');
  }
  gotocart() {
    this.navCtrl.push(CartPage);
  }
	gotohomePage() {
		// this.navCtrl.push(HomePage,{});
		console.log('gotohomePage function called');
		this.navCtrl.setRoot(HomePage);
		// .then(() => this.navCtrl.first().dismiss());
	
	  }
  expandItem(item){
    if(item.expanded === undefined){
			item.expanded = false;	
		}
		item.expanded = !item.expanded;
  }

  parkErrorAlert() {
		let alert = this.alertCtrl.create({
			title: 'Warning',
			message: 'Please select a Park Ticket before selecting Add Ons',
			buttons: [
				{
					text: 'OK',
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
						this.navCtrl.push(TicketCategoryPage,{'pushToBook': this.pushToBook});
					}
				},
			]
		});
		alert.present();
  }

  checkMainTicket(hcid) {
    let trckrId;
    trckrId = this.trackerId;
    let url = ENUM.domain + ENUM.url.fetchTickets;
    var params = {
      tcktDt: this.tcktDt,
      ttypId: 3,
      hcid: hcid,
      trckrId: trckrId
    }
    this.presentLoader();
    this.webAPI.getService(url, params)
      .then(result => {
        console.log('result   ==>', result);
        var res: any;
        res = result;
        if (!res.data.length || res.data.length == undefined) {
          this.nodataAvailable = true;
          this.closeLoader();
        } else {
          this.paidAttractionsBucket = []
          console.log('TICKETS FETCHED     ', result);
          this.paCategory = res.data;
          console.log('FNBCAT: ', this.paCategory);
          console.log('fnbcat: ', this.paCategory[0].subCat[0].ticketimg);
          for (let i = 0; i < this.paCategory.length; i++) {
            for (let j = 0; j < this.paCategory[i].subCat.length; j++) {
              if(this.paCategory[i].subCat[j].Qnt > 0){
                this.paCategory[i].subCat[j].totalPrice = this.paCategory[i].subCat[j].Qnt * this.paCategory[i].subCat[j].displaytickprice;
                this.paidAttractionsBucket.push(	this.paCategory[i].subCat[j]);
              }
              console.log('fnbimg', this.paCategory[i].subCat[j].ticketimg)
              if (this.paCategory[i].subCat[j].ticketimg) {
                var checkImgpath = this.paCategory[i].subCat[j].ticketimg.includes(ENUM.domain + ENUM.url.imgUrl);
                if (!checkImgpath) {
                  // item.locimgpath = "";
                  // item.locimgpath = ENUM.domain+ENUM.url.imgUrl+item.locimgpath;
                  this.paCategory[i].subCat[j].ticketimg = ENUM.domain + ENUM.url.imgUrl + this.paCategory[i].subCat[j].ticketimg;
                }
                else {
                  console.log('checkImgpath', checkImgpath);
                }
              }
            }
          }
          console.log('this.paCategory    from res', this.paCategory);
          this.closeLoader();
        }
      })
      .catch(err => {
        console.log('err ==>', err);
        this.presentToast('Something went wrong. Please try again!');
        this.closeLoader();
      });

  } 

bpError(item) {

    var type;
    type = item;
    var msg = ''; 
    if(type = 2){
     msg = 'F & B'
    } else if( type = 3){
        msg = 'Paid Attractions'
    }
let alert = this.alertCtrl.create({
  title: 'Sorry!!',
  message: 'We are currently not offering any '+msg+' for Bird Park',
  buttons: [
    {
      text: 'OK',
      role: 'cancel',
      handler: () => {
        console.log('Cancel clicked');
        // this.navCtrl.setRoot(TicketCategoryPage,{'pushToBook': this.pushToBook});
      }
    },
  ]
});
alert.present();
}
}
