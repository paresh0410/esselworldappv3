import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, Events } from 'ionic-angular';
import { SwipeTabComponent } from '../../components/swipe-tab/swipe-tab';
import { webAPIService } from '../../service/webAPIService';
import { ENUM } from '../../service/ENUM';
import { checkIfOnlineService } from '../../service/checkIfOnlineService';
import { NoInternetConnectionPage } from '../no-internet-connection/no-internet-connection';
import { HomePage } from '../home/home';
import { EventsDetailsPage } from '../events-details/events-details';
import { NotificationPage } from '../notification/notification';
import { CartPage } from '../cart/cart';
import { ThemeServiceProvider } from '../../providers/theme-service/theme-service';

/**
 * Generated class for the EventsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-events',
	templateUrl: 'events.html',
})

export class EventsPage {

	
	relationship: string = '1';
	events: any = []
	array: any = [];
	nodataAvailable : boolean = false;
	ThemeC: any;
	red: boolean;
	green: boolean;
	blue: boolean;
	hid: any;
	categoryList: any;
	constructor(public navCtrl: NavController, public navParams: NavParams, public webAPI: webAPIService,
		public loadingCtrl: LoadingController, public checkOnline: checkIfOnlineService,public event: Events,public themeServices: ThemeServiceProvider,
		private alertCtrl: AlertController) {
			this.hid = this.navParams.get('hid');
			if(this.hid == undefined || this.hid == ''){
				this.hid = 0;
			}
			this.categoryList = this.navParams.get('categoryList');
////////  Chnage for theme   ///////////////
this.ThemeC = this.themeServices.getTheme();
this.event.subscribe('theme', () => {
	this.ThemeC = this.themeServices.getTheme();
});
if(this.ThemeC == 'red'){
	this.red = true;
	this.blue = false;
	this.green = false;
}else if(this.ThemeC == 'blue'){
	this.blue = true;
	this.red = false;
	this.green = false;
}else if(this.ThemeC == 'green'){
	this.green = true;
	this.red = false;
	this.blue = false;
}
// if(localStorage.getItem('theme')){
// 		this.ThemeC = localStorage.getItem('theme');
// 		console.log('theme',this.ThemeC);
// }
// else{
// 	this.ThemeC = 'red'
// }
////////////////////////////////////////////


		var onlineStatus = this.checkOnline.getIfOnline();
		if (onlineStatus == true) {
			var params = {
				hid: this.hid
			}
			if(this.categoryList){
				params.hid = this.categoryList.hcid;
			}
			
			var url = ENUM.domain + ENUM.url.eventList;
			this.webAPI.getService(url, params)
				.then(result => {
					if (result != 'err') {
						console.log("Success :", result);
						let temp: any = result;

							if(temp.data.length > 0){
								this.array = temp.data;
								var x = 0;
								for (var i = 0; i < this.array.length; i++) {
									x = x + 1;
									this.array[i].tabId = "" + x + "";
									if (this.array[i].imgpath !== null || this.array[i].imgpath !== undefined || this.array[i].imgpath !== '') {
										var checkImgpath = this.array[i].imgpath.includes("/img/ego/");
										if (!checkImgpath) {
											this.array[i].contentImg = 'assets/imgs/default.png'
										}
										else {
											this.array[i].contentImg = ENUM.domain + ENUM.url.imgUrl + this.array[i].imgpath;
										}
									}
								}
							}else{
								//this.closeLoader();
							    this.nodataAvailable = true;
							    console.log('NO DATA AVAILABLE')
							}
						
					} else {
						this.serverErrorAlert();
					}
				})
				.catch(result => {
					console.log("Error :", result);
				})
		} else {
			this.navCtrl.push(NoInternetConnectionPage);
		}

	}

	serverErrorAlert() {
		let alert = this.alertCtrl.create({
			title: 'Server Error!!!',
			message: 'Server Not Responding. Please try again later.',
			buttons: [
				{
					text: 'OK',
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
						this.navCtrl.setRoot(HomePage, { AP: 'Y' });
					}
				},
			]
		});
		alert.present();
	}
	gotoeventsDetails(item){

		this.navCtrl.push(EventsDetailsPage,{
			eventgroupofferId:item.eventgroupofferId,
			contentImg:item.contentImg,
			contentHead:item.contentHead,
			subContentP1:item.subContentP1,
		});
	}
	ionViewWillLeave(){
		// if(localStorage.getItem('theme')){
		// 	let localTheme = localStorage.getItem('theme');
		// 	if(localTheme == 'green' || localTheme == 'blue'){
		// 		localTheme = 'red';
		// 		localStorage.setItem('theme',localTheme);
		// 	}
		// }
	}
	ionViewDidLoad() {
		var val=this.navCtrl.getViews();
		console.log('val',val);
		let loader = this.loadingCtrl.create({
			content: "Please wait..."
		});
		loader.present();
		console.log('ionViewDidLoad OffersPage');
		setTimeout(() => {
			this.events = this.array;
			loader.dismiss();
		}, 1000)
	}
	gotornotificationPage(){
		this.navCtrl.push(NotificationPage);
		console.log('notification.');
	}
	gotocart(){
		this.navCtrl.push(CartPage);
	  }

}
