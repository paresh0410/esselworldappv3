import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, Nav, ToastController, ViewController, App, AlertController } from 'ionic-angular';
import { bookingSummaryService } from '../../service/bookingSummary.service';
import { ENUM } from '../../service/ENUM';
import { webAPIService } from '../../service/webAPIService';
import { HomePage } from '../home/home';
import { CartPage } from '../cart/cart';
import { NotificationPage } from '../notification/notification';
import { BookingPage } from '../booking/booking';
import { BookingHistoryListPage } from '../booking-history-list/booking-history-list';
// import { PaymentPage } from '../payment/payment';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { TermsAndConditionsPage } from '../terms-and-conditions/terms-and-conditions';
import { Device } from '@ionic-native/device';
import { WebIntent } from '@ionic-native/web-intent';
import { AES256 } from '@ionic-native/aes-256';
import { HttpHeaders, HttpClient } from '@angular/common/http';
// import {Md5} from 'ts-md5/dist/md5';
import * as md5 from '../../../node_modules/blueimp-md5';
/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {
  event: any;
  totalAmount: any;
  bookingSummary: any = [];
  TotaltPrice: any = 0;
  TTP: any = 0;
  TotaltfPrice: any = 0;
  TFP: any = 0;
  TotaltaPrice: any = 0;
  TAP: any = 0;
  saveConfirmed: any;
  cordova: any;
  url: string = 'https://merchant.benow.in/paysdk';
  options: InAppBrowserOptions = {
    location: 'yes',//Or 'no' 
    hidden: 'no', //Or  'yes'
    clearcache: 'yes',
    clearsessioncache: 'yes',
    zoom: 'yes',//Android only ,shows browser zoom controls 
    hardwareback: 'yes',
    mediaPlaybackRequiresUserAction: 'no',
    shouldPauseOnSuspend: 'no', //Android only 
    disallowoverscroll: 'no', //iOS only 
    toolbar: 'yes', //iOS only 
    enableViewportScale: 'no', //iOS only 
    allowInlineMediaPlayback: 'no',//iOS only 
    presentationstyle: 'pagesheet',//iOS only 
    fullscreen: 'yes',//Windows only
    hideurlbar: 'yes'
  };

  param: any;
  modes: string[];
  userdata: any;
  totalPrice: any;
  success: any;
  payOpt: boolean;
  // pay: boolean;
  fail: boolean;
  SuccessTxn: any;
  bookingID: any;
  successSplit: any;
  checkedTick: boolean = false;
  uid: any;
  secureKey: any;
  secureIV: any;
  encryptedData: string;
  browser: any;
  response: any;
  trackerId: string;
  paymentFail: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public platform: Platform, public webAPI: webAPIService, public toast: ToastController, public app: App,
    public viewCtrl: ViewController, private iab: InAppBrowser, private device: Device,
    private webIntent: WebIntent, private aes256: AES256, public alertCtrl: AlertController) {
    this.totalAmount = this.navParams.get('totalAmount');
    console.log('this.totalAmount', this.totalAmount);
    // this.totalAmount = 1200;
    this.userdata = this.navParams.get('userData');
    this.trackerId = localStorage.getItem('trackerId');

  }
  onChangeHandler(event: string) {
    console.log(event);
    this.event = event;
    this.checkedTick = true;
  }

  pay() {

    if (this.event == 'dc') {
      this.payment();
    }
    else if (this.event == 'upi') {

      this.paymentUPI();
    }
    else {
      this.checkedTick = false;
    }
  }
  payment() {
    let trackerId = localStorage.getItem('trackerId');
    let param = {
      trckrID: trackerId
    }
    this.modes = ['DC', 'CC', 'NB'];
    // document.getElementById("sdkForm").onsubmit;
    this.param = {
      askmob: false,
      askadd: false,
      askpan: false,
      askname: false,
      askemail: false,
      askresidence: false,
      mndmob: false,
      mndpan: false,
      mndname: false,
      mndemail: false,
      mndaddress: false,
      readonlymob: false,
      readonlypan: false,
      readonlyname: false,
      readonlyresidence: false,
      readonlyaddr: false,
      readonlyemail: false,
      allowMultiSelect: false,
      mtype: null,
      amount: 1,//this.totalAmount, //pass totalprice here for payment
      language: null,
      sourceId: 1,
      minpanamnt: null,
      merchantType: null,
      campaignTarget: null,
      id: '',
      hash: '',
      successURL: '',
      failureURL: '',
      description: '',
      email: this.userdata.email,
      phone: this.userdata.mobileno,
      title: 'ESSELWORLD LEISURE PVT LTD',
      mccCode: '7996',
      firstName: this.userdata.firstname,
      lastName: '',
      merchantId: 'EHMR9',
      merchantVpa: 'EHMR9@yesbank',
      merchantCode: 'EHMR9',
      businessName: 'ESSELWORLD LEISURE PVT LTD',
      invoiceNumber: 'EW - ' + this.trackerId,
      txnid: this.trackerId,
      supportedModes: this.modes,
      udf1: '',
      udf2: '',
      udf3: '',
      udf4: '',
      udf5: ''
    }
    var pageContent = '<html><head></head><body><form id="sdkForm" #sdkForm ngNoForm action="' + this.url + '" method="post">' +
      '<input type="hidden" name="askmob" value="' + this.param.askmob + '" />' +
      '<input type="hidden" name="askadd" value="' + this.param.askadd + '" />' +
      '<input type="hidden" name="askpan" value="' + this.param.askpan + '" />' +
      '<input type="hidden" name="askname" value="' + this.param.askname + '" />' +
      '<input type="hidden" name="askemail" value="' + this.param.askemail + '" />' +
      '<input type="hidden" name="askresidence" value="' + this.param.askresidence + '" />' +
      '<input type="hidden" name="mndmob" value="' + this.param.mndmob + '" />' +
      '<input type="hidden" name="mndpan" value="' + this.param.mndpan + '" />' +
      '<input type="hidden" name="mndname" value="' + this.param.mndname + '" />' +
      '<input type="hidden" name="mndemail" value="' + this.param.mndemail + '" />' +
      '<input type="hidden" name="mndaddress" value="' + this.param.mndaddress + '" />' +
      '<input type="hidden" name="readonlymob" value="' + this.param.readonlymob + '" />' +
      '<input type="hidden" name="readonlypan" value="' + this.param.readonlypan + '" />' +
      '<input type="hidden" name="readonlyname" value="' + this.param.readonlyname + '" />' +
      '<input type="hidden" name="readonlyresidence" value="' + this.param.readonlyresidence + '" />' +
      '<input type="hidden" name="readonlyaddr" value="' + this.param.readonlyaddr + '" />' +
      '<input type="hidden" name="readonlyemail" value="' + this.param.readonlyemail + '" />' +
      '<input type="hidden" name="allowMultiSelect" value="' + this.param.allowMultiSelect + '" />' +
      '<input type="hidden" name="mtype" value="' + this.param.mtype + '" />' +
      '<input type="hidden" name="amount" value="' + this.param.amount + '" />' + // pass amount here
      '<input type="hidden" name="language" value="' + this.param.language + '" />' +
      '<input type="hidden" name="sourceId" value="' + this.param.sourceId + '" />' +
      '<input type="hidden" name="minpanamnt" value="' + this.param.minpanamnt + '" />' +
      '<input type="hidden" name="merchantType" value="' + this.param.merchantType + '" />' +
      '<input type="hidden" name="campaignTarget" value="' + this.param.campaignTarget + '" />' +
      '<input type="hidden" name="hash" value="' + this.param.hash + '" />' +
      '<input type="hidden" name="successURL" value="' + this.param.successURL + '" />' +
      '<input type="hidden" name="failureURL" value="' + this.param.failureURL + '" /> ' +
      '<input type="hidden" name="email" value="' + this.param.email + '" />' +
      '<input type="hidden" name="phone" value="' + this.param.phone + '" />' +
      '<input type="hidden" name="title" value="' + this.param.title + '" />' +
      '<input type="hidden" name="mccCode" value="' + this.param.mccCode + '" />' +
      '<input type="hidden" name="lastName" value="' + this.param.lastName + '" />' +
      '<input type="hidden" name="firstName" value="' + this.param.firstName + '" />' +
      '<input type="hidden" name="merchantId" value="' + this.param.merchantId + '" />' +
      '<input type="hidden" name="merchantVpa" value="' + this.param.merchantVpa + '" />' +
      '<input type="hidden" name="merchantCode" value="' + this.param.merchantCode + '" />' +
      '<input type="hidden" name="businessName" value="' + this.param.businessName + '" />' +
      '<input type="hidden" name="invoiceNumber" value="' + this.param.invoiceNumber + '" />' +
      '<input type="hidden" name="txnid" value="' + this.param.txnid + '" />' +
      '<input type="hidden" name="supportedModes" value="' + this.modes + '" />' +
      '</form> <script type="text/javascript">document.getElementById("sdkForm").submit();</script></body></html>';
    var pageContentUrl = 'data:text/html;base64,' + btoa(pageContent);

    this.browser = this.iab.create(pageContentUrl, '_blank', this.options);

    this.browser.on('loadstart').subscribe(event => {
      console.log('loadstart', event);
      this.checkTxn(event);
    });

    this.browser.on('loadstop').subscribe(event => {
      console.log('loadstop', event);
      // this.success = event.url.includes('https://merchant.benow.in/sdksuccess');

    });

    this.browser.on('loaderror').subscribe(event => {
      console.log('loaderror', event);
    });

    // browser.on('beforexit').subscribe(event => {
    //   	console.log('beforexit',event);
    // });

    // browser.on('beforeunload').subscribe(event => {
    //   	console.log('beforeunload',event);
    // });

    this.browser.on('exit').subscribe(event => {
      console.log('exit', event);
      if (event.type == 'exit') {
        // this.nav.popToRoot()
        //     .then(() => this.nav.first().dismiss());param
        // this.confirmPay();
        this.navCtrl.popToRoot()
          .then(() => this.navCtrl.first().dismiss());
      }
    });

    this.browser.on('beforeload').subscribe(event => {
      console.log('beforeload', event);
    });

    this.browser.on('message').subscribe(event => {
      console.log('message', event);
    });

  }
  checkTxn(event) {
    this.fail = event.url.includes('https://merchant.benow.in/sdkfailure');
    this.paymentFail = event.url.includes('https://merchant.benow.in/paymentfailure/');
    this.payOpt = event.url.includes('https://secure.payu.in/_payment_options');
    this.pay = event.url.includes('https://secure.payu.in/pay');
    this.SuccessTxn = event.url.includes('https://merchant.benow.in/sdksuccess/' + this.param.merchantCode + '/');
    // let thanks = event.url.includes('https://merchant.benow.in/paysdk/thanks');

    if (this.SuccessTxn) {
      this.success = event.url;
      //split the success URL to get txnID which is after the merchant code and store txnID in this.bookingID.
      this.successSplit = this.success.split("https://merchant.benow.in/sdksuccess/" + this.param.merchantCode + "/");
      // this.successSplit = this.successSplit[1].split("/");
      console.log('this.successSplit ==>', this.successSplit);
      this.bookingID = this.successSplit[1];
      console.log('this.bookingID ==>', this.bookingID);
      if (this.bookingID) {

        this.confirmPay();
      }
    }
    // else if (this.paymentFail) {
    //   let title: any = 'Payment Failed';
    //   let msg: any = 'Something Went Wrong. Please try again later.'
    //   this.browser.close();
    //   this.showAlert(title,msg);
    // }
  }
  paymentUPI() {
    let trackerId = localStorage.getItem('trackerId');
    var param = {
      // merchantCode: 'AF8Y1',
      amount: 1,//this.totalAmount, //pass amount here
      tr: trackerId,
      paymentMethod: 'UPI',
      merchantId: 'EHMR9',
      merchantVpa: 'EHMR9@yesbank',
      merchantCode: 'EHMR9',
      businessName: 'ESSELWORLD LEISURE PVT LTD',
    }
    var url1 = ENUM.url.upilink;
    console.log('params ', param);
    this.webAPI.getService(url1, param)
      .then((result) => {
        console.log("Header Res : ", result);
        let res;
        res = result;
        let url = res.url;
        const options = {
          action: this.webIntent.ACTION_VIEW,
          url: url,
          // type: 'application/vnd.android.package-archive'
        };
        this.webIntent.startActivityForResult(options).then(success => {
          console.log('success', success);
          //response Phonepe = txnId=YBLac8238694fe7456d8618a844ac109480&txnRef=17AN7UF5&Status=Success&responseCode=00
          //response Paytm = txnId=PTMe268ab0b316c4e559cc4d1f46cad80ec&responseCode=0&ApprovalRefNo=913442629179&Status=SUCCESS&txnRef=17AN7UF5
          //response BHIM = txnId=undefined&responseCode=U09&ApprovalRefNo=undefined&Status=FAILURE&txnRef=17AN7UF5
          //response BHIM = txnId=undefined&responseCode=00&ApprovalRefNo=undefined&Status=SUCCESS&txnRef=17AN7UF5
          //response GPay = txnId=AXI58ea5317f4c04d14b54bd83c1868a985&responseCode=00&Status=SUCCESS&txnRef=17AN7UF5
          if(success.extras.Status){
            if (success.extras.Status == 'SUCCESS' || success.extras.Status == 'Success') {
              this.bookingID = success.extras.txnId;
              this.response = success.extras.response;
              this.confirmUPIPay();
            }
          } else if(!success.extras.Status){
            let res:any = success.extras.response;
            let res1:any = res.split("@");
            let res2 = res1[0].split("&");
            let res3 = res2.split("=");
            let response: any = res3[0];
            let response1: any = response.split("=");
            let bkid: any = response1[1];
            this.bookingID = bkid;
            this.response = success.extras.response;
            this.confirmUPIPay();
          } 
        }).catch(error => {
          console.log('error', error);
        })
        console.log('url ', url);
        // this.callWebIntent(url);
      }).catch((result) => {
        console.log("Header Error : ", result);
      });
  }

  callWebIntent(url) {
    const options = {
      action: this.webIntent.ACTION_VIEW,
      url: url,
      // type: 'application/vnd.android.package-archive'
    };

    this.webIntent.startActivity(options).then(success => {
      console.log('success from UPI', success);
    }).catch(error => {
      console.log('error from UPI', error);
    })
  }

  confirmUPIPay() {
    var trackerId = localStorage.getItem('trackerId');
    let param = {
      trckrID: trackerId,
      bookingid: this.bookingID
    };
    console.log('param for saving to confirmlist ', param);
    var url = ENUM.domain + ENUM.url.saveConfirmed;
    this.webAPI.getService(url, param)
      .then(result => {
        console.log('success result ', result);
        localStorage.removeItem('trackerId');
        localStorage.removeItem('bookingDt');
        this.navCtrl.setRoot(BookingHistoryListPage);
        console.log('Details:', result);
        this.saveNotification();
      }).catch(result => {
        this.toaster('Something went wrong! Please try again.');
        console.log("Server Response Error :", result);

      });
  }
  confirmPay() {
    var trackerId = localStorage.getItem('trackerId');
    let param = {
      trckrID: trackerId,
      bookingid: this.bookingID
    };
    console.log('param for saving to confirmlist ', param);
    var url = ENUM.domain + ENUM.url.saveConfirmed;
    this.webAPI.getService(url, param)
      .then(result => {
        console.log('success result ', result);
        localStorage.removeItem('trackerId');
        localStorage.removeItem('bookingDt');
        // this.navCtrl.setRoot(BookingHistoryListPage);
        console.log('Details:', result);
        this.browser.close();
        this.navCtrl.setRoot(BookingHistoryListPage);
        this.saveNotification();
      }).catch(result => {
        this.toaster('Something went wrong! Please try again.');
        console.log("Server Response Error :", result);

      });
  }
  saveNotification() {
    var UserDetails = JSON.parse(localStorage.getItem('UserDetails'));
    this.uid = UserDetails[0].reguserid;
    var uuid = this.device.uuid;
    var txnid = this.bookingID;
    var param = {
      uid: this.uid,
      uuid: uuid,
      txnid: txnid
    }
    var url = ENUM.domain + ENUM.url.savetxnNoti;
    this.webAPI.getService(url, param)
      .then(result => {
        console.log(' result ', result);
        this.sendNotifications();
      }).catch(err => {
        console.log(' error occurred ', err);
      });
  }

  sendNotifications() {
    var param = {
      transactionID: this.bookingID,
      usrid: this.uid
    }
    var url1 = ENUM.domain + ENUM.url.fetchtxnSMS;
    var url2 = ENUM.domain + ENUM.url.fetchtxnMail;
    this.webAPI.getService(url1, param)
      .then(result => {
        console.log(' result ', result);
      }).catch(err => {
        console.log('error occurred while sending sms', err);
      });
    this.webAPI.getService(url2, param)
      .then(result => {
        console.log('result email', result);
      }).catch(err => {
        console.log('Error Occurred while sending email')
      })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentPage');
  }
  gotornotificationPage() {
    this.navCtrl.push(NotificationPage);
    console.log('notification.');
  }

  gotocart() {
    this.navCtrl.push(CartPage);
  }

  gotohomePage() {
    // this.navCtrl.push(HomePage,{});
    console.log('gotohomePage function called');
    this.navCtrl.setRoot(HomePage);
    // .then(() => this.navCtrl.first().dismiss());	
  }
  toaster(text) {
    let toast = this.toast.create({
      message: text,
      duration: 2000,
      position: 'top',
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
  showAlert(title, msg) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present().then(()=>{
      this.navCtrl.setRoot(HomePage);
    });
  }

}

