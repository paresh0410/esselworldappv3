import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroupsNschoolsDetailsPage } from './groups-nschools-details';

@NgModule({
  declarations: [
    GroupsNschoolsDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(GroupsNschoolsDetailsPage),
  ],
})
export class GroupsNschoolsDetailsPageModule {}
