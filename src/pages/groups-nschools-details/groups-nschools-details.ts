import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { CartPage } from '../cart/cart';
import { NotificationPage } from '../notification/notification';
import { HomePage } from '../home/home';
/**
 * Generated class for the GroupsNschoolsDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-groups-nschools-details',
  templateUrl: 'groups-nschools-details.html',
})
export class GroupsNschoolsDetailsPage {
  eventgroupofferId: any;
  contentImg: any;
  contentHead: any;
  subContentP1: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public loadingCtrl: LoadingController) {
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });     
    loader.present();
    this.eventgroupofferId = this.navParams.get('eventgroupofferId');
    this.contentImg = this.navParams.get('contentImg');
    this.contentHead = this.navParams.get('contentHead');
    this.subContentP1 = this.navParams.get('subContentP1');
    loader.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GroupsNschoolsDetailsPage');
  }
  gotohomePage(){
    // this.navCtrl.push(HomePage,{});
    console.log('gotohomePage function called');
    this.navCtrl.setRoot(HomePage);
    
  }		

  gotocart(){
		this.navCtrl.push(CartPage);
  }
  
  gotornotificationPage() {
		this.navCtrl.push(NotificationPage);
		console.log('notification.');
	}
}
