import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login'
import { Slides } from 'ionic-angular';
import { webAPIService } from '../../service/webAPIService';
import { ENUM } from '../../service/ENUM';
import { checkIfOnlineService } from '../../service/checkIfOnlineService';
import { NoInternetConnectionPage } from '../no-internet-connection/no-internet-connection';

/**
 * Generated class for the IntroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
})
export class IntroPage {

	@ViewChild(Slides) slides: Slides;

	public introArray:any=[];
	public length:any;

  	constructor(public navCtrl: NavController, public navParams: NavParams, public webAPI: webAPIService,
		public checkOnline: checkIfOnlineService, private alertCtrl: AlertController) {
	  
		// this.introArray = [
		// 	{
		// 		id: 1,
		// 		imgPath: 'assets/imgs/bignite-offer-EW.png',
		// 		introHeader: "BIGNITE' 18",
		// 		introBody: 'India’s largest open air party. The most amazing New Year Eve Party in town. And it’s getting bigger and better with each passing year.',
		// 	},
		// 	{
		// 		id: 2,
		// 		imgPath: 'assets/imgs/event-christmas-EW.png',
		// 		introHeader: "CHRISTMAS' 2018",
		// 		introBody: 'This Christmas, party away and plan the most amazing season with grand attractions for children and adults alike.'
		// 	}
		// ];

		var onlineStatus = this.checkOnline.getIfOnline();
		if(onlineStatus == true){
			var url = ENUM.domain + ENUM.url.intro;
			this.webAPI.getService(url,'')
			.then(result => {
				if(result != 'err'){
					let temp : any= result;
					this.introArray = temp.data;
					if(this.introArray.length > 0){
						for(var i=0;i<this.introArray.length;i++){
							this.introArray[i].imgPath = ENUM.domain+ENUM.url.imgUrl+this.introArray[i].isimgpath;
						}
					}else{
						if (localStorage.getItem('login') == 'Y') {
							this.navCtrl.setRoot(HomePage);
							}
							else{
								this.navCtrl.setRoot(LoginPage);
							}
					}
					console.log("Success :",  this.introArray);
				}else{
					this.serverErrorAlert();
				}
			})
			.catch(result =>{
				console.log("Error :",  result);
			})
		}else{
			this.navCtrl.push(NoInternetConnectionPage);
		}

		this.length = this.introArray.length;
	
	}

	serverErrorAlert(){
		let alert = this.alertCtrl.create({
			title: 'Server Error!!!',
			message: 'Server Not Responding. Please try again later.',
			buttons: [
				{
					text: 'OK',
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
						if (localStorage.getItem('login') == 'Y') {
							this.navCtrl.setRoot(HomePage,{AP: 'Y'});
							}
							else{
								this.navCtrl.setRoot(LoginPage);
							}
					}
				},
			]
		});
		alert.present();
	}

  	ionViewDidLoad() {
    	console.log('ionViewDidLoad IntroPage');
	}
	  
	goToHome(){
		if (localStorage.getItem('login') == 'Y') {
			this.navCtrl.setRoot(HomePage);
			}
			else{
				this.navCtrl.setRoot(LoginPage);
			}
	}

	gotoSlide(introData,type){
		var toSlideNo = 0;
		if(introData.id != 0 || introData.id != this.length){
			if(type == 0){
				toSlideNo = this.slides.getActiveIndex()-1;
				this.slides.slideTo(toSlideNo);
			}else{
				toSlideNo = this.slides.getActiveIndex()+1;
				this.slides.slideTo(toSlideNo);
			}
		}
	}

}
