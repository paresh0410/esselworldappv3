import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ModalpagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modalpage',
  templateUrl: 'modalpage.html',
})
export class ModalpagePage {
  value: any;
  name: any;
  param: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private view: ViewController) {
    this.param = this.navParams.get('value');
    this.value = this.param.value;
    this.name = this.param.name;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalpagePage');
  }
  closeModal(){
    this.view.dismiss();
  }
}
