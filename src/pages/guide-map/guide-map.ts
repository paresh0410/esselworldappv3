
import {  PinchZoomModule } from 'ngx-pinch-zoom';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, LoadingController, Events } from 'ionic-angular';
import { Slides, App } from 'ionic-angular';
import { RidesService } from '../../service/ride.service';
import { RideDetailsPage } from '../ride-details/ride-details';
import { ENUM } from '../../service/ENUM';
import { checkIfOnlineService } from '../../service/checkIfOnlineService';
import { webAPIService } from '../../service/webAPIService';
import { NotificationPage } from '../notification/notification';
import { CartPage } from '../cart/cart';
import { ThemeServiceProvider } from '../../providers/theme-service/theme-service';
import { GeofencePage } from '../../pages/geofence/geofence';
/**
 * Generated class for the GuideMapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-guide-map',
	templateUrl: 'guide-map.html',
})

export class GuideMapPage {

	
	image: string;
	hcId: any;
	loader: any;
	categoryList: any;
	ThemeC: any;
	red: boolean;
	green: boolean;
	blue: boolean;
	constructor(public navCtrl: NavController, public navParams: NavParams,
		public rideservice: RidesService, public webAPI: webAPIService,
		public checkOnline: checkIfOnlineService, private alertCtrl: AlertController,
		private toastCtrl: ToastController, public loadingCtrl: LoadingController,public events: Events,public themeServices: ThemeServiceProvider,
		public app: App,  private pinchModule: PinchZoomModule) {
				////////  Chnage for theme   ///////////////
				// this.ThemeC = this.themeServices.getTheme();
				// this.events.subscribe('theme', () => {
				// 	this.ThemeC = this.themeServices.getTheme();
				// });
				// if(this.ThemeC == 'red'){
				// 	this.red = true;
				// 	this.blue = false;
				// 	this.green = false;
				// }else if(this.ThemeC == 'blue'){
				// 	this.blue = true;
				// 	this.red = false;
				// 	this.green = false;
				// }else if(this.ThemeC == 'green'){
				// 	this.green = true;
				// 	this.red = false;
				// 	this.blue = false;
				// }
	////////////////////////////////////////////
	if(localStorage.getItem('theme')){
		this.ThemeC = localStorage.getItem('theme');
	}
	else{
						this.ThemeC = this.themeServices.getTheme();
				this.events.subscribe('theme', () => {
					this.ThemeC = this.themeServices.getTheme();
				});
				if(this.ThemeC == 'red'){
					this.red = true;
					this.blue = false;
					this.green = false;
				}else if(this.ThemeC == 'blue'){
					this.blue = true;
					this.red = false;
					this.green = false;
				}else if(this.ThemeC == 'green'){
					this.green = true;
					this.red = false;
					this.blue = false;
				}
	}
	
		this.image = 'assets/imgs/ew-map.jpg';
		this.hcId = localStorage.getItem('hcId');
		this.categoryList = this.navParams.get('categoryList');
		console.log('this.hcId', this.hcId);
		let param = {
			"hId": this.hcId
		}
		this.presentLoader();
		var onlineStatus = this.checkOnline.getIfOnline();

		this.webAPI.getService(ENUM.domain + ENUM.url.fetchGuidMapHcid, param)
			.then(result => {
				// this.closeLoader();
				//  console.log('ParkTimings:',result)
				var data: any = result;
				var temp = data.data;
				this.closeLoader();
				if (temp.length > 0) {
					this.image = ENUM.domain + ENUM.url.imgUrl + temp[0].imgpath;
					console.log('ParkTimings:', temp);
				} else {
					this.image = 'assets/imgs/ew-map.jpg';
				}
			})
			.catch(result => {
				console.log("ServiceResponseError :", result);
				this.closeLoader();
			});
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad GuideMapPage');
	}

	gotohomePage() {
		// this.navCtrl.push(HomePage,{});
		this.navCtrl.popToRoot();

	}
	presentLoader() {
		this.loader = this.loadingCtrl.create({
			content: "Please wait..."
		});
		this.loader.present();
		console.log('ionViewDidLoad OffersPage');
		// 	setTimeout(()=>{
		// this.offers = this.array;
		// loader.dismiss();
		// 	},2000)
	}
	closeLoader() {
		this.loader.dismiss();
	}
	// zoomIn(event) {
	// 	console.log('event', event);
	// 	this.pinch.toggleZoom(event);
	// }

	// zoomOut(event) {
	// 	console.log('event', event);
	// 	this.pinch.toggleZoom(event);
	// }
	gotornotificationPage() {
		this.navCtrl.push(NotificationPage);
		console.log('notification.');
	}

	gotocart(){
		this.navCtrl.push(CartPage);
	}

	goToNearBy(){
		this.navCtrl.push(GeofencePage);
	}
}
