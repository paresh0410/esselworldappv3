import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PinchZoomModule } from 'ngx-pinch-zoom';
// import { GuideMapPage } from './guide-map';
@NgModule({
  declarations: [
    // GuideMapPage,
  ],
  imports: [
    PinchZoomModule,
    // IonicPageModule.forChild(GuideMapPage),
  ],
  exports:[
  ],
  providers:[
  ]
})
export class GuideMapPageModule {}
