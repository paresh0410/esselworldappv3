import { Component, ViewChild } from '@angular/core';
import {
  IonicPage, NavController, NavParams, LoadingController,
  ToastController, AlertController, Platform, Nav
} from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ENUM } from '../../service/ENUM';
import { webAPIService } from '../../service/webAPIService';
import { checkIfOnlineService } from '../../service/checkIfOnlineService';
import { BookingSummaryPage } from '../../pages/booking-summary/booking-summary';
import { LoginPage } from '../../pages/login/login';
import { NotificationPage } from '../notification/notification';
import { CartPage } from '../cart/cart';
import { HomePage } from '../home/home';
/**
 * Generated class for the UserDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-details',
  templateUrl: 'user-details.html',
})
export class UserDetailsPage {
  @ViewChild(Nav) nav: NavController;
  userForm: FormGroup;
  todayDate = new Date(this.formatDate(localStorage.getItem('bookingDt')));
  userArray: any =
    {
      firstname: '',
      mobile: '',
      // lastname: '',
      dob: '',
      emailid: '',
      addrline1: '',
      city: '',
      state: '',
      country: '',
      pincode: ''
    };
  userbookingDetails: any;
  trackerId: string;
  userId: any;
  res: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform,
    private loadingctrl: LoadingController, private toastctrl: ToastController,
    private alertCtrl: AlertController, public webAPI: webAPIService, public checkifol: checkIfOnlineService) {

    // this.platform.registerBackButtonAction(() => {
    //   console.log('Nav', this.nav);
    //   let view = this.nav.getActive();
    //   let previousView = this.navCtrl.last().name;
    //   if (previousView)
    //     // this.nav.setRoot(previousView);
    //     this.nav.popTo(previousView);
    // });

    this.trackerId = localStorage.getItem('trackerId');
    console.log('------------------------------------------------', this.trackerId);
    this.userbookingDetails = JSON.parse(localStorage.getItem('UserDetails'));
    this.userId = this.userbookingDetails[0].reguserid;
    //  var onlinestatus = this.checkifol.getIfOnline();
    //  if(onlinestatus==true){
    if (this.trackerId) {
      let param: any = {
        uid: this.userId,
        trckid: this.trackerId
      }
      var url = ENUM.domain + ENUM.url.fetchuserdetails;
      this.webAPI.getService(url, param)
        .then(result => {
          console.log('result', result);
          this.res = result;
          console.log('this.res.data.length',this.res.data[0].length);
          if(this.res.data[0].length > 0){
            this.userArray.firstname = this.res.data[0][0].firstname;
            this.userArray.lastname = this.res.data[0][0].lastname;
            this.userArray.mobile = this.res.data[0][0].mobileno;
            this.userArray.dob = new Date(this.formatDate(this.parseISOString(this.res.data[0][0].dob))).toISOString();
            this.userArray.emailid = this.res.data[0][0].email;
            this.userArray.addrline1 = this.res.data[0][0].address1;
            this.userArray.pincode = this.res.data[0][0].pincode;
            this.userArray.city = this.res.data[0][0].city;
            this.userArray.state = this.res.data[0][0].state;
            this.userArray.country = this.res.data[0][0].country;
          }
          else if (localStorage.getItem('UserDetails') && this.res.data[0].length == 0) {
            console.log('entered in else if for localstorage')
            this.userbookingDetails = JSON.parse(localStorage.getItem('UserDetails'));
            this.userArray.firstname = this.userbookingDetails[0].regusername;
            this.userArray.lastname = this.userbookingDetails[0].reguserlastname;
            this.userArray.mobile = this.userbookingDetails[0].mobileno;
            this.userArray.dob = new Date(this.formatDate(this.parseISOString(this.userbookingDetails[0].dob))).toISOString();
            this.userArray.emailid = this.userbookingDetails[0].email;
      
          }
          else {
            console.log('Entered in else for blank')
            this.navCtrl.push(LoginPage);
            this.userArray.firstname = '';
            this.userArray.mobile = '';
            this.userArray.lastname = '';
            this.userArray.dob = '';
            this.userArray.emailid = '';
            this.userArray.addrline1 = '';
            // this.userArray.addrline2 = '';
            this.userArray.city = '';
            this.userArray.state = '';
            this.userArray.country = '';
            this.userArray.pincode = '';
          }
        });
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserDetailsPage');
  }

  ngOnInit() {
    this.userForm = new FormGroup({
      firstname: new FormControl('', [Validators.required]),
      mobile: new FormControl('', [Validators.required, Validators.minLength(10), Validators.pattern('[0-9]*')]),
      lastname: new FormControl(''),
      dob: new FormControl('', [Validators.required]),
      emailid: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-z0-9+-_.]+@[a-zA-Z]+.[a-zA-Z.]{2,10}')]),
      addrline1: new FormControl(''),
      // addrline2: new FormControl(''),
      city: new FormControl('', [Validators.pattern('[a-zA-Z]*')]),
      state: new FormControl('', [Validators.pattern('[a-zA-Z]*')]),
      country: new FormControl('', [Validators.pattern('[a-zA-Z]*')]),
      pincode: new FormControl('', [Validators.required]),
    });
  }

  onSave(userArray) {
    // let loader = this.loadingctrl.create({
    //   content:'Please wait...'
    // });
    if (this.userArray.firstname == '' ||  this.userArray.mobile == '' || this.userArray.dob == '' || this.userArray.emailid == '' || this.userArray.pincode == '' ||
      this.userArray.firstname == null || this.userArray.mobile == null || this.userArray.dob == null || this.userArray.emailid == null || this.userArray.pincode == null ||
      this.userArray.firstname == undefined || this.userArray.mobile == undefined || this.userArray.dob == undefined || this.userArray.emailid == undefined || this.userArray.pincode == undefined) {
      let toast1 = this.alertCtrl.create({
        title: 'Alert',
        subTitle: 'Please enter all required details',
        buttons: ['OK']
      });
      toast1.present();
    }

    else if (this.userForm.get('firstname').invalid || this.userForm.get('mobile').invalid || this.userForm.get('dob').invalid
      || this.userForm.get('emailid').invalid || this.userForm.get('country').invalid || this.userForm.get('state').invalid || this.userForm.get('city').invalid || this.userForm.get('pincode').invalid || this.userForm.get('lastname').invalid) {
      let toast = this.alertCtrl.create({
        title: 'Alert',
        subTitle: 'Please enter valid details.',
        buttons: ['OK']
      });
      toast.present();
    }

    else if (this.userForm.get('firstname').valid || this.userForm.get('mobile').valid || this.userForm.get('dob').valid
      || this.userForm.get('emailid').valid || this.userForm.get('country').valid || this.userForm.get('state').valid || this.userForm.get('city').valid || this.userForm.get('pincode').valid || this.userForm.get('lastname').valid) {
      console.log('Booking Details', userArray);
      let param = {
        fName: userArray.firstname,
        lName: userArray.lastname,
        mob: userArray.mobile,
        email: userArray.emailid,
        bDt: userArray.dob,
        addr1: userArray.addrline1,
        // addr2: userArray.addrline2,
        ct: userArray.city,
        st: userArray.state,
        cntry: userArray.country,
        pin: userArray.pincode,
        trckrId: this.trackerId,
        usrId: this.userId,
        tcktDT: this.todayDate
      }
      console.log('userArray',userArray);
      let loader = this.loadingctrl.create({
        content: "Please wait..."
      });
      loader.present();
      var url = ENUM.domain + ENUM.url.saveuserdetails;
      this.webAPI.getService(url, param)
        .then(result => {
          console.log('Details:', result);
          let temp: any = result;
          let userdata: any = temp.data;
          loader.dismiss();
          // localStorage.setItem('UserBookingDetails', JSON.stringify(userdata));
          this.navCtrl.push(BookingSummaryPage);
        }).catch(result => {
          loader.dismiss();
          console.log("Server Response Error :", result);
        });
    }
  }

  gotocart() {
    this.navCtrl.push(CartPage);
  }

  gotohomePage() {
    // this.navCtrl.push(HomePage,{});
    console.log('gotohomePage function called');
    this.navCtrl.setRoot(HomePage);
    // .then(() => this.navCtrl.first().dismiss());

  }

  gotornotificationPage() {
    this.navCtrl.push(NotificationPage);
    console.log('notification.');
  }

  parseISOString(s) {
    console.log('s ',s);
    var b = s.split(/\D+/);
    return new Date(Date.UTC(b[0], --b[1], b[2], b[3], b[4], b[5], b[6]));
  }
  formatDate(date) {
    console.log('date', date);
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  };
}
