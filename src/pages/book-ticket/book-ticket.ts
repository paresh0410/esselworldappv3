import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BookTicketPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-book-ticket',
  templateUrl: 'book-ticket.html',
})
export class BookTicketPage {

	adultCount : any = 0;
	childCount : any = 0;
	srCitizenCount : any = 0;
	amtAdult: any =1000;
	amtChild :any = 500;
	amtsrCitizen : any = 400;
	total : any = 0;
  count : any = 0; 
	date :any= new Date().getFullYear().toString() + "-"+ ("0" + (new Date().getMonth() + 1)).slice(-2).toString() +"-"+("0" + (new Date().getDate())).slice(-2).toString();

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookTicketPage');
  }

  incrementAdult(){
  	//this.total=0;
  	this.adultCount++;
  	//this.amtAdult = 1000;
  	this.grandTotal(this.amtAdult,1);
    var adultC = 1;
    this.grandCount(adultC,1)
  }

  decrementAdult(){
  	if(this.adultCount>0){
  		this.adultCount--;
  		//var totalAdultamt = 1000;
  	    this.grandTotal(this.amtAdult,2);
        var adultC = 1;
        this.grandCount(adultC,2)
  	}else{
  		this.adultCount = '0';
  	}
  }

  incrementChild(){
  	//this.total=0;
  	this.childCount++;
  	var totalChildamt = 500;
  	this.grandTotal(this.amtChild,1);
    var childC = 1;
    this.grandCount(childC,1)

  }

  decrementChild(){
  	if(this.childCount>0){
  		this.childCount--;
  		//var totalChildamt = 500;
  	    this.grandTotal(this.amtChild,2);
        var childC = 1;
        this.grandCount(childC,2)
  	}else{
  		this.childCount = '0';
  	}
  }

  incrementsrCitizen(){
  	//this.total=0;
  	this.srCitizenCount++;
  	//var srCitizenCountamt = 400;
  	this.grandTotal(this.amtsrCitizen,1);
    var srCitizen = 1;
    this.grandCount(srCitizen,1)
  }

  decrementsrCitizen(){
  	if(this.srCitizenCount>0){
  		this.srCitizenCount--;
  		//var srCitizenCountamt = 400;
  	    this.grandTotal(this.amtsrCitizen,2);
        var srCitizen = 1;
        this.grandCount(srCitizen,2)

  	}else{
  		this.srCitizenCount = '0';
  	}
  }

  grandTotal(amount,id){
    	if(id == 1){
    		this.total = this.total + amount;
  	  }else if(id == 2){
  	  	this.total = this.total - amount;
  	  }
  	}

  grandCount(countnum,id){
      if(id == 1){
        this.count = this.count + countnum;
      }else if(id == 2){
        this.count = this.count - countnum;
      }
  }
  	
}
