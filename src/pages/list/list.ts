import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ToastController, Events } from 'ionic-angular';
import { EntryRatesServiceProvider } from '../../service/entryrates-service';
import { checkIfOnlineService } from '../../service/checkIfOnlineService';
import { NoInternetConnectionPage } from '../no-internet-connection/no-internet-connection';
import { HomePage } from '../home/home';
import { NotificationPage } from '../notification/notification';
import { CartPage } from '../cart/cart';
import { BookingPage } from '../booking/booking';
import { ThemeServiceProvider } from '../../providers/theme-service/theme-service';
import { LoginPage } from '../login/login';
import { BookhomecategoryPage } from '../bookhomecategory/bookhomecategory';

@Component({
	selector: 'page-list',
	templateUrl: 'list.html'
})
export class ListPage {
	selectedItem: any;
	icons: string[];
	items: Array<{ title: string, note: string, icon: string }>;
	totalArray: any = [];
	allParks: any = [];
	EsselWorld: any = [];
	WaterKingdom: any = [];
	park: any = 1;
	loader: any;
	noDataAvailable: boolean = false;
	segmentName: any = [];
	segmentData: any = [];
	homelist: any = [];
	pushToBook: { bookHCID: any; bookHead: any; };
	forBackButton: boolean;
	ThemeC :any;

	constructor(public navCtrl: NavController, public navParams: NavParams,
		public entryRates: EntryRatesServiceProvider, public loadingCtrl: LoadingController,
		public checkOnline: checkIfOnlineService, private alertCtrl: AlertController, private toastCtrl: ToastController,
		public events: Events,public themeServices : ThemeServiceProvider) {

		this.presentLoader();
				////////  Chnage for theme   ///////////////
				this.ThemeC = this.themeServices.getTheme();
				this.events.subscribe('theme', () => {
					this.ThemeC = this.themeServices.getTheme();
				});
		// If we navigated to this page, we will have an item available as a nav param
		this.selectedItem = navParams.get('item');

		// Let's populate this page with some filler content for funzies
		this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
			'american-football', 'boat', 'bluetooth', 'build'];

		this.items = [];
		for (let i = 1; i < 11; i++) {
			this.items.push({
				title: 'Item ' + i,
				note: 'This is item #' + i,
				icon: this.icons[Math.floor(Math.random() * this.icons.length)]
			});
		}

		var onlineStatus = this.checkOnline.getIfOnline();
		if (onlineStatus == true) {
			this.entryRates.getentryRatesService()
				.then(result => {
					if (result != 'err') {
						console.log("Entry Rate Success :", result);
						let temp: any = result;
						this.allParks = temp.data;
						console.log('this.allParks', this.allParks)
						this.park = this.allParks[0].hcid;
						console.log('this.park', this.park)
						if (this.allParks.length > 0) {
							// for(var i=0;i<this.allParks.length;i++){
							// 	// this.allParks[i].hcid = '' + this.allParks[i].hcid + '';
							// 	// if(this.allParks[i].hcid == this.park){
							// 	// 	this.EsselWorld = this.allParks[i].subRides;
							// 	// }
							// }
							this.changeArr(this.park);
							// console.log('EW--->',this.EsselWorld);
							// console.log('WK--->',this.park);
							this.closeLoader();
						} else {
							this.noDataAvailable = true;
							this.closeLoader();
						}
					} else {
						this.closeLoader();
						this.serverErrorAlert();
					}
				}).catch(result => {
					console.log("Error :", result);
				})
		} else {
			this.closeLoader();
			this.presentToast();
			this.navCtrl.setRoot(HomePage);
		}

		// this.EsselWorld = [
		// 	{
		// 		ttDesc: "Pay N Play Basic Card",
		// 		ttp: "390",
		// 	},
		// 	{
		// 		ttDesc: "Pay N Play Silver(Adult) Height Above 4'6ft",
		// 		ttp: "980",
		// 	},
		// 	{
		// 		ttDesc: "Pay N Play Silver(Child) Height Between 3'3ft to 4'6ft",
		// 		ttp: '690',
		// 	},
		// 	{
		// 		ttDesc: "Pay N Play Gold(Child) Height Between 3'3ft to 4'6ft",
		// 		ttp: '1290',
		// 	},
		// 	{
		// 		ttDesc: "Annual Pass - Passport Next",
		// 		ttp: '1690',
		// 	},
		// ];

		// this.WaterKingdom=[
		// 	{
		// 		ttDesc: "Adult(Height Above 4'6ft)",
		// 		ttp: "1050",
		// 	},
		// 	{
		// 		ttDesc: "Child(Height Between 3'3ft to 4'6ft)",
		// 		ttp: '696',
		// 	},
		// 	{
		// 		ttDesc: "Sr. Citizen(Age above 60 years)",
		// 		ttp: '690',
		// 	},
		// ]

	}

	serverErrorAlert() {
		let alert = this.alertCtrl.create({
			title: 'Server Error!!!',
			message: 'Server Not Responding. Please try again later.',
			buttons: [
				{
					text: 'OK',
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
						this.navCtrl.setRoot(HomePage, { AP: 'Y' });
					}
				},
			]
		});
		alert.present();
	}

	itemTapped(event, item) {
		// That's right, we're pushing to ourselves!
		this.navCtrl.push(ListPage, {
			item: item
		});
	}

	presentLoader() {
		this.loader = this.loadingCtrl.create({
			content: "Please wait..."
		});
		this.loader.present();
		console.log('ionViewDidLoad OffersPage');
		//   setTimeout(()=>{
		// this.offers = this.array;
		// loader.dismiss();
		//   },2000)
	}

	closeLoader() {
		this.loader.dismiss();
	}

	presentToast() {
		let toast = this.toastCtrl.create({
			message: 'NO INTERNET CONNECTION',
			duration: 3000,
			position: 'top',
			//cssClass:"InternetOffToast"
		});

		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});

		toast.present();
	}

	changeArr(ID) {
		console.log('ID--->', ID);
		this.EsselWorld = [];
		if (this.allParks.length > 0) {
			for (var i = 0; i < this.allParks.length; i++) {
				if (this.allParks[i].hcid == ID) {
					this.park = this.allParks[i].hcid;
					this.EsselWorld = this.allParks[i].subRides;
				}
			}
		} else {
			this.noDataAvailable = true;
			this.closeLoader();
		}
	}

	gotocart() {
		this.navCtrl.push(CartPage);
	}

	gotornotificationPage() {
		this.navCtrl.push(NotificationPage);
		console.log('notification.');
	}

	goToHomeCategories(){
		this.navCtrl.push(BookhomecategoryPage);
	}
	  resetTicket() {
			
			let alert = this.alertCtrl.create({
			  title: 'Create a new booking?',
			  buttons: [{
				text: 'No',
				handler: () => {
				  // user has clicked the alert button
				  // begin the alert's dismiss transition
				  let navTransition = alert.dismiss();
		
				  // start some async method
				  // someAsyncOperation().then(() => {
				  // once the async operation has completed
				  // then run the next nav transition after the
				  // first transition has finished animating out
		
				  navTransition.then(() => {
					// this.alertPark();
					// this.navCtrl.push(TicketCategoryPage, { 'pushToBook': this.pushToBook, 'forBackButton': this.forBackButton });
				  });
				  // });
				  return false;
				}
			  },
			  {
				text: 'Yes',
				handler: () => {
			// this.presentModal();
			this.navCtrl.push(BookingPage, { 'pushToBook': this.pushToBook });
				//   this.navCtrl.push(UserDetailsPage, { 'pushToBook': this.pushToBook, 'ticketDate': this.tcktDt, 'trackerId': this.trackerId, 'totalTicketPrice': this.totalTicketPrice, 'totalTicketCount': this.totalTicketCount });
				}
			  }]
			});
		
			alert.present();
		  }

}
