import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CartPage } from '../cart/cart';
import { NotificationPage } from '../notification/notification';
import { webAPIService } from '../../service/webAPIService';
import { ENUM } from '../../service/ENUM';
/**
 * Generated class for the RewardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reward',
  templateUrl: 'reward.html',
})
export class RewardPage {
  totalAmount: any = 100;
  mobile: any;
  value: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public webAPI: webAPIService, public changeDetRef: ChangeDetectorRef) {
  
    this.fetchPoints();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RewardPage');
  }
  fetchPoints(){
    let mob;
    mob = localStorage.getItem('mobileNumber');
    let param = {
      mob: mob
    }
    var url = ENUM.domain + ENUM.url.fetchPoints;
    this.webAPI.getService(url, param)
    .then(res =>{
      console.log('points fetched ',res);
      let result;
      result = res;
      result = result.data[0];
      this.mobile = result.mobileno;
      this.value = result.value;
      console.log('result ',result);
    }).catch(err =>{
      console.log('points fetched ',err);
    });

  }
  gotornotificationPage() {
    this.navCtrl.push(NotificationPage);
    console.log('notification.');
  }

  gotocart() {
    this.navCtrl.push(CartPage);
  }
}
