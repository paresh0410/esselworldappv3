import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { RegisterNewPage } from '../register-new/register-new';
import { OtpPage } from '../otp/otp';
import { FormBuilder, FormControl, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { webAPIService } from '../../service/webAPIService';
import { ENUM } from '../../service/ENUM';
import { AlertController } from 'ionic-angular';
import { LoggedInUserService } from '../../service/loggedInUser.service';
import { checkIfOnlineService } from '../../service/checkIfOnlineService';
import { HomePage } from '../home/home';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-login',
	templateUrl: 'login.html',
})
export class LoginPage {

	loginCredentials: any = {};
	loginForm: FormGroup;
	checkLogin: boolean;
	pushToBook: any;
	uData1: any = [];
	constructor(private alertCtrl: AlertController, public toastCtrl: ToastController, public navCtrl: NavController,
		public navParams: NavParams, public webAPI: webAPIService, public loadingCtrl: LoadingController,
		public loggedinuserservice: LoggedInUserService, public checkOnline: checkIfOnlineService) {
		this.checkLogin = this.navParams.get('checkLogin');
		this.pushToBook = this.navParams.get('pushToBook');
		this.navCtrl.getViews();
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad LoginPage');
	}

	ionViewWillEnter() {
		var isPageFound = localStorage.getItem('checkState');
		if (isPageFound) {
			localStorage.removeItem('checkState');
			localStorage.removeItem('Page');
			this.navCtrl.pop();
		}
	}

	gotosignUp() {
		this.navCtrl.setRoot(RegisterNewPage);
	}

	gotoOtp() {
		var onlineStatus = this.checkOnline.getIfOnline();
		if (onlineStatus == true) {
			let loader = this.loadingCtrl.create({
				content: "Please wait..."
			});
			loader.present();
			var param = {
				imobileno: this.loginCredentials.mobile,
			}
			var otpParam = {
				imobileno: this.loginCredentials.mobile
			}

			localStorage.setItem('mobileNumber', this.loginCredentials.mobile)
			// if (param.imobileno == '9819015186') {
			// 	var userdata = [{
			// 		reguserid: 1,
			// 		regusername: 'Harshavardhan',
			// 		reguserlastname: 'Malve',
			// 		mobileno: '9819015186',
			// 		regdate: '2019-05-30T17:57:11.000Z',
			// 		isOtpVerified: 'Y',
			// 		email: null,
			// 		gender: 1,
			// 		dob: '1993-11-25T18:30:00.000Z',
			// 		profileImg: null
			// 	}];
			// 	var string = JSON.stringify(userdata)
			// 	localStorage.setItem('UserDetails', string);
			// 	this.navCtrl.setRoot(HomePage, { 'checkLogin': this.checkLogin, 'pushToBook': this.pushToBook });
			// 	localStorage.setItem("login", 'Y');
			// 	loader.dismiss();
			// }
			// else {
				this.webAPI.getService(ENUM.domain + ENUM.url.loginUser, param)
					.then(result => {
						console.log("loginSuccess :", result);
						var temp: any = result;
						var uData = temp.data;
						console.log("loginSuccess :", result);
						this.uData1 = uData[0];
						console.log('this.uData1', this.uData1);
						console.log('this.udata1.length', this.uData1.length);
						// this.uData1[0].profileImg = '';
						this.loggedinuserservice.userdetails = this.uData1;
						console.log('userDetails:', this.loggedinuserservice.userdetails);
						if (this.uData1.length != 0) {
							this.uData1[0].profileImg = '';
							var string = JSON.stringify(this.uData1)
							localStorage.setItem('UserDetails', string);
							this.webAPI.otpService(ENUM.domain + ENUM.url.sendOtp, otpParam)
								.then(result => {
									if (result != 'err') {
										var temp: any = result
										var SendOtpResponse = JSON.parse(temp.data)
										console.log('send', SendOtpResponse)
										var fstr = JSON.stringify(SendOtpResponse.Details)
										localStorage.setItem('SendOtpResponse', fstr)
										console.log('sendOtpRespnse', result);
										this.navCtrl.push(OtpPage, { 'checkLogin': this.checkLogin, 'pushToBook': this.pushToBook });
										loader.dismiss();
									} else {
										loader.dismiss();
										this.serverErrorAlert();
									}
								}).catch(result => {
									console.log("sendOtpError :", result);
									this.presentToast('Something went wrong.please try again.');
									loader.dismiss();
								})
						} else {
							this.newUserAlert();
							loader.dismiss();
						}
					}).catch(result => {
						console.log("Error :", result);
						loader.dismiss();
					})
			}
		// }
		else {
			this.presentToast('No Internet Connection!!!');
		}
	}

	serverErrorAlert() {
		let alert = this.alertCtrl.create({
			title: 'Server Error!!!',
			message: 'Server Not Responding. Please try again later.',
			buttons: [
				{
					text: 'OK',
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
					}
				},
			]
		});
		alert.present();
	}

	newUserAlert() {
		let alert = this.alertCtrl.create({
			title: 'Login Error',
			message: 'No user found. Register as a new user?',
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					handler: () => {
						//console.log('Cancel clicked');
					}
				},
				{
					text: 'OK',
					handler: () => {
						//console.log('Buy clicked');
						this.navCtrl.setRoot(RegisterNewPage);
					}
				}
			]
		});
		alert.present();
	}

	presentToast(text) {
		let toast = this.toastCtrl.create({
			message: text,
			duration: 2000,
			position: 'top'
		});

		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});

		toast.present();
	}

	ngOnInit() {
		this.loginForm = new FormGroup({
			mobile: new FormControl('', Validators.compose([Validators.maxLength(10), Validators.minLength(10), Validators.pattern('[0-9]*'), Validators.required])),
		});
	}


}
