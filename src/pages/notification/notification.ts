import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NotificationService } from '../../service/notification.service';
import { webAPIService } from '../../service/webAPIService';
import { ENUM } from '../../service/ENUM';
import { checkIfOnlineService } from '../../service/checkIfOnlineService';
import { NoInternetConnectionPage } from '../no-internet-connection/no-internet-connection';
import { Device } from '@ionic-native/device';
import { HomePage } from '../home/home';
/**
 * Generated class for the NotificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-notification',
	templateUrl: 'notification.html',
})
export class NotificationPage {

	notifications: any = [];

	// nodata: boolean = false;
	title: any;
	body: any;
	res: any;
	date: Date;
	nodataAvailable: boolean = false;

	constructor(public navCtrl: NavController, public navParams: NavParams,
		private notiservice: NotificationService,
		public webAPI: webAPIService, private device: Device, public checkOnline: checkIfOnlineService) {
		//   this.notifications = this.notiservice.getAll();
		console.log('Device UUID is: ' + this.device.uuid);
		var onlineStatus = this.checkOnline.getIfOnline();
		let userDetails = JSON.parse(localStorage.getItem('UserDetails'));
		let userId = userDetails[0].reguserid;
		var params = {
			usrid: userId
		}
		if (onlineStatus == true) {
			var url = ENUM.domain + ENUM.url.myNotifications;

			this.webAPI.getService(url, params)
				.then(data => {
					let result: any = data;
					if (result) {
						console.log('result ', result);
						this.res = result.data;
						if(this.res.length > 0){
							console.log('res ', this.res);
							this.title = this.res.title;
							this.body = this.res.message;
							this.date = this.res.date;
							this.nodataAvailable = false;
						}
						else{
							this.nodataAvailable = true;
						}
					}
					else{
						console.log('No Data');
					}
				})
		} else {
			this.navCtrl.push(NoInternetConnectionPage);
		}


	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad NotificationPage');
	}

	// delete(data){
	// 	var match = data.id;
	// 	for(var i=match;i<this.notifications.length;i++){
	// 		if(i!=this.notifications.length-1 && this.notifications.length!=2){
	// 			this.notifications[i] = this.notifications[i+1];
	// 		}else{
	// 			if(this.notifications.length==2){
	// 				if(data.id == this.notifications[0].id){
	// 					this.notifications[0] = this.notifications[1];
	// 				}
	//   			}	
	// 		} 
	// 	}
	// 	this.notifications.length = this.notifications.length-1;
	// }

	gotohomePage() {
		// this.navCtrl.push(HomePage,{});
		console.log('gotohomePage function called');
		this.navCtrl.popToRoot();
	
	  }

}
