import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, LoadingController } from 'ionic-angular';
import { ENUM } from '../../service/ENUM';
import { webAPIService } from '../../service/webAPIService';

import { UserDetailsPage } from '../../pages/user-details/user-details';

import { TicketCategoryPage } from '../../pages/ticket-category/ticket-category';
import { PaidAttractionsPage } from '../../pages/paid-attractions/paid-attractions';
import { FAndBCategoryPage } from '../../pages/f-and-b-category/f-and-b-category';
/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-cart',
    templateUrl: 'cart.html',
})
export class CartPage {

    bookingCart: any = [];
    cardList: any = [];
    TotaltincPrice: any = 0;
    TotaltfincPrice: any = 0;
    TotaltaincPrice: any = 0;
    TotaltdecPrice: any = 0;
    TotaltfdecPrice: any = 0;
    TotaltadecPrice: any = 0;
    loader: any;
    trackerId: any;
    nodataAvailable: boolean = false;
    ew: boolean;
    bp: boolean;
    constructor(public navCtrl: NavController, public navParams: NavParams, public webAPI: webAPIService,
        private alertCtrl: AlertController, public loadingCtrl: LoadingController, private toastCtrl: ToastController) {

    }

    ionViewDidEnter() {
        this.bookingCart = [];
        this.fetchBookingSummary();
    }

    fetchBookingSummary() {
        this.presentLoader();
        this.trackerId = localStorage.getItem('trackerId');
        let param = {
            TrackID: this.trackerId
        }
        var url = ENUM.domain + ENUM.url.fetchbookingsummary;
        this.webAPI.getService(url, param)
            .then(result => {
                console.log('Details:', result);
                let temp: any = result;
                let userdata: any = temp.data;
                this.closeLoader();
                this.bookingCart.push(userdata);
                if (!this.bookingCart[0].id) {
                    this.nodataAvailable = true;
                }
                console.log('this.bookingSummary', this.bookingCart);
                console.log('fetchbookingsummaryREs:', userdata);
            }).catch(result => {
                this.closeLoader();
                console.log("Server Response Error :", result);
                this.nodataAvailable = true;
            });
    }

    onIncrement(mainid, subid, count) {
        if (mainid == 1) {
            for (let j = 0; j < this.bookingCart[0].tickets.length; j++) {
                if (subid == this.bookingCart[0].tickets[j].ticketId) {
                    this.AddNewTicket(this.bookingCart[0].id, this.bookingCart[0].tickets[j].ttypeId, this.bookingCart[0].tickets[j].ticketId, (function (summary) {
                        console.log('summary', summary);
                        if (summary == "InsertSucsess") {
                            this.bookingCart[0].tickets[j].ticCount = count + 1;
                            this.bookingCart[0].tickets[j].totalPirce = this.bookingCart[0].tickets[j].ticketprice * this.bookingCart[0].tickets[j].ticCount;
                        }
                    }).bind(this));
                }
            }
        }
        if (mainid == 2) {
            for (let j = 0; j < this.bookingCart[0].FandB.length; j++) {
                if (subid == this.bookingCart[0].FandB[j].ticketId) {
                    this.AddNewTicket(this.bookingCart[0].id, this.bookingCart[0].FandB[j].ttypeId, this.bookingCart[0].FandB[j].ticketId, (function (summary) {
                        console.log('summary', summary);
                        if (summary == "InsertSucsess") {
                            this.bookingCart[0].FandB[j].ticCount = count + 1;
                            this.bookingCart[0].FandB[j].totalPirce = this.bookingCart[0].FandB[j].ticketprice * this.bookingCart[0].FandB[j].ticCount;
                        }
                    }).bind(this));

                }
            }
        }
        if (mainid == 3) {
            for (let j = 0; j < this.bookingCart[0].paidAttr.length; j++) {
                if (subid == this.bookingCart[0].paidAttr[j].ticketId) {
                    this.AddNewTicket(this.bookingCart[0].id, this.bookingCart[0].paidAttr[j].ttypeId, this.bookingCart[0].paidAttr[j].ticketId, (function (summary) {
                        console.log('summary', summary);
                        if (summary == "InsertSucsess") {
                            this.bookingCart[0].paidAttr[j].ticCount = count + 1;
                            this.bookingCart[0].paidAttr[j].totalPirce = this.bookingCart[0].paidAttr[j].ticketprice * this.bookingCart[0].paidAttr[j].ticCount;
                        }
                    }).bind(this));
                }
            }
        }
    }

    AddNewTicket(id, typeId, ticketId, cb) {

        this.presentLoader();
        let trackerId = localStorage.getItem('trackerId');
        let param = {
            TrackID: id,
            typeId: typeId,
            ticketId: ticketId
        }
        var url = ENUM.domain + ENUM.url.addNewTicket;
        this.webAPI.getService(url, param)
            .then(result => {
                this.closeLoader();
                console.log('Details:', result);
                let temp: any = result;
                cb("InsertSucsess");
            }).catch(result => {
                this.closeLoader();
                console.log("Server Response Error :", result);
                cb(null);
            });

    }


    onDecrement(mainid, subid, count) {
        if (mainid == 1) {
            for (let j = 0; j < this.bookingCart[0].tickets.length; j++) {
                if (subid == this.bookingCart[0].tickets[j].ticketId) {
                    if (this.bookingCart[0].tickets[j].ticCount > 1) {
                        this.DeleteOneTicket(this.bookingCart[0].id, this.bookingCart[0].tickets[j].ttypeId, this.bookingCart[0].tickets[j].ticketId, (function (summary) {
                            console.log('summary', summary);
                            if (summary == "DeletedOne") {
                                this.bookingCart[0].tickets[j].ticCount = count - 1;
                                this.bookingCart[0].tickets[j].totalPirce = this.bookingCart[0].tickets[j].ticketprice * this.bookingCart[0].tickets[j].ticCount;
                            }
                        }).bind(this));
                    }
                    if (this.bookingCart[0].tickets[j].ticCount == 1) {
                        this.presentToast("");
                    }
                }
            }
        }
        if (mainid == 2) {
            for (let j = 0; j < this.bookingCart[0].FandB.length; j++) {
                if (subid == this.bookingCart[0].FandB[j].ticketId) {
                    if (this.bookingCart[0].FandB[j].ticCount > 1) {
                        this.DeleteOneTicket(this.bookingCart[0].id, this.bookingCart[0].FandB[j].ttypeId, this.bookingCart[0].FandB[j].ticketId, (function (summary) {
                            console.log('summary', summary);
                            if (summary == "DeletedOne") {
                                this.bookingCart[0].FandB[j].ticCount = count - 1;
                                this.bookingCart[0].FandB[j].totalPirce = this.bookingCart[0].FandB[j].ticketprice * this.bookingCart[0].FandB[j].ticCount;
                            }
                        }).bind(this));
                    }
                    if (this.bookingCart[0].FandB[j].ticCount == 1) {
                        this.presentToast("");
                    }
                }
            }
        }
        if (mainid == 3) {
            for (let j = 0; j < this.bookingCart[0].paidAttr.length; j++) {
                if (subid == this.bookingCart[0].paidAttr[j].ticketId) {
                    if (this.bookingCart[0].paidAttr[j].ticCount > 1) {
                        this.DeleteOneTicket(this.bookingCart[0].id, this.bookingCart[0].paidAttr[j].ttypeId, this.bookingCart[0].paidAttr[j].ticketId, (function (summary) {
                            console.log('summary', summary);
                            if (summary == "DeletedOne") {
                                this.bookingCart[0].paidAttr[j].ticCount = count - 1;
                                this.bookingCart[0].paidAttr[j].totalPirce = this.bookingCart[0].paidAttr[j].ticketprice * this.bookingCart[0].paidAttr[j].ticCount;
                            }
                        }).bind(this));
                    }
                    if (this.bookingCart[0].paidAttr[j].ticCount == 1) {
                        this.presentToast("");
                    }
                }
            }
        }
    }

    DeleteOneTicket(id, typeId, ticketId, cb) {

        this.presentLoader();
        let trackerId = localStorage.getItem('trackerId');
        let param = {
            TrackID: id,
            typeId: typeId,
            ticketId: ticketId
        }
        var url = ENUM.domain + ENUM.url.deleteOneTicket;
        this.webAPI.getService(url, param)
            .then(result => {
                this.closeLoader();
                console.log('Details:', result);
                let temp: any = result;
                cb("DeletedOne");
            }).catch(result => {
                this.closeLoader();
                console.log("Server Response Error :", result);
                cb(null);
            });

    }

    TotalTPrice() {
        var tPrice = 0;
        for (var i = 0; this.bookingCart.length > i; i++) {
            for (let j = 0; j < this.bookingCart[i].tickets.length; j++) {
                tPrice = tPrice + this.bookingCart[i].tickets[j].totalPirce;
            }
        }
        return tPrice;
    }
    TotalFPrice() {
        var tPrice = 0;
        for (var i = 0; this.bookingCart.length > i; i++) {
            for (let j = 0; j < this.bookingCart[i].FandB.length; j++) {
                tPrice = tPrice + this.bookingCart[i].FandB[j].totalPirce;
            }
        }
        return tPrice;
    }
    TotalPAPrice() {
        var tPrice = 0;
        for (var i = 0; this.bookingCart.length > i; i++) {
            for (let j = 0; j < this.bookingCart[i].paidAttr.length; j++) {
                tPrice = tPrice + this.bookingCart[i].paidAttr[j].totalPirce;
            }
        }
        return tPrice;
    }

    DeleteAllTicket(mainid, subid, count, hid) {
        var hcid = hid;
        var tickets: any = [];
        var msg = '';
        var trackerId = localStorage.getItem('trackerId');
        // let ticketlength = 0;
        var ticketlength = this.bookingCart[0].tickets.length
        
        
        if (mainid == 1) {
            for(let j = 0; ticketlength > j; j++){
                // hcid.push(this.bookingCart[0].tickets[j].hcid);
                if(this.bookingCart[0].tickets[j].hcid == hcid){
                    tickets.push(this.bookingCart[0].tickets[j]);
                }
            }
            if(tickets.length == 1){
                msg = 'Are you sure you want to remove this item? Removing this item will also remove all the F & B and Paid Attractions associated with this item';
            }
        }
        else {
            msg = 'Are you sure you want to remove this item?';
        }
        const confirm = this.alertCtrl.create({
            title: 'Remove Item',
            message: msg,
            buttons: [{
                text: 'Cancel',
                handler: () => {
                    console.log('cancel clicked');
                }
            },
            {
                text: 'Remove',
                handler: () => {

                    if (mainid == 1) {
                        for (let j = 0; this.bookingCart[0].tickets.length > j; j++) {
                            if (this.bookingCart[0].tickets[j].ticketId == subid) {
                                // hcid.push(this.bookingCart[0].tickets[j].hcid);
                                this.RemoveAllTicket(this.bookingCart[0].id, this.bookingCart[0].tickets[j].ttypeId, this.bookingCart[0].tickets[j].ticketId, (function (summary) {
                                    console.log('summary', summary);
                                    if (summary == "RemoveAll") {
                                        this.bookingCart[0].tickets.splice(j, 1);
                                    }
                                }).bind(this));
                            }
                            console.log('hcid for delete => ', hcid);
                            if (tickets.length == 1) {
                                if(this.bookingCart[0].tickets[j].hcid == hcid){
                                    this.deleteOnMain(trackerId, hcid, (function (summary) {
                                        console.log('this.bookingCart[0].tickets.length    ', this.bookingCart[0].tickets.length);
    
                                        if (summary == "RemoveAll") {
                                            var arr =[];
                                            var arr1 =[];
                                            for(let i=0; i<this.bookingCart[0].FandB.length; i++){
                                                if(this.bookingCart[0].FandB[i].hcid == hcid){
                                                    arr.push(i);
                                                    // this.bookingCart[0].FandB.splice(i, 1);

                                                }
                                            }
                                            for(let i= arr.length -1; i>=0; i--){
                                                this.bookingCart[0].FandB.splice(arr[i], 1);
                                            }
                                            for(let k=0; k<this.bookingCart[0].paidAttr.length; k++){
                                                if(this.bookingCart[0].paidAttr[k].hcid == hcid){
                                                    // this.bookingCart[0].paidAttr.splice(k, 1);
                                                    arr1.push(k);
                                                }
                                            }
                                            for(let k= arr1.length -1; k>=0; k--){
                                                this.bookingCart[0].paidAttr.splice(arr1[k], 1);
                                            }
                                            
                                        }
                                    }).bind(this));
                                }

                            }
                        }

                    }
                    if (mainid == 2) {
                        for (let j = 0; this.bookingCart[0].FandB.length > j; j++) {
                            if (this.bookingCart[0].FandB[j].ticketId == subid) {
                                this.RemoveAllTicket(this.bookingCart[0].id, this.bookingCart[0].FandB[j].ttypeId, this.bookingCart[0].FandB[j].ticketId, (function (summary) {
                                    console.log('summary', summary);
                                    
                                    if (summary == "RemoveAll") {
                                        this.bookingCart[0].FandB.splice(j, 1);
                                    }
                                }).bind(this));
                            }
                        }
                    }
                    if (mainid == 3) {
                        for (let j = 0; this.bookingCart[0].paidAttr.length > j; j++) {
                            if (this.bookingCart[0].paidAttr[j].ticketId == subid) {
                                this.RemoveAllTicket(this.bookingCart[0].id, this.bookingCart[0].paidAttr[j].ttypeId, this.bookingCart[0].paidAttr[j].ticketId, (function (summary) {
                                    console.log('summary', summary);
                                    if (summary == "RemoveAll") {
                                        this.bookingCart[0].paidAttr.splice(j, 1);
                                    }
                                }).bind(this));
                            }
                        }
                    }

                }
            }
            ]
        });
        confirm.present();
    }

    RemoveAllTicket(id, typeId, ticketId, cb) {

        this.presentLoader();
        let trackerId = localStorage.getItem('trackerId');
        let param = {
            TrackID: id,
            typeId: typeId,
            ticketId: ticketId
        }
        var url = ENUM.domain + ENUM.url.removeAllTicket;
        this.webAPI.getService(url, param)
            .then(result => {
                this.closeLoader();
                console.log('Details:', result);
                let temp: any = result;
                cb("RemoveAll");
            }).catch(result => {
                this.closeLoader();
                console.log("Server Response Error :", result);
                cb(null);
            });

    }


    DeleteTicket(ticket) {
        const confirm = this.alertCtrl.create({
            title: 'Remove Item',
            message: 'Are you sure you want to remove this item?',
            buttons: [{
                text: 'Cancel',
                handler: () => {
                    console.log('cancel clicked');
                }
            },
            {
                text: 'Remove',
                handler: () => {
                    for (var i = 0; this.bookingCart.length > i; i++) {
                        for (let j = 0; this.bookingCart[i].tickets.length > j; j++) {
                            if (this.bookingCart[i].tickets[j].ticketId == ticket.ticketId) {
                                this.bookingCart[i].tickets.splice(j, 1);
                            }
                        }
                    }
                }
            }
            ]
        });
        confirm.present();
    }

    DeleteFnB(ticket) {
        const confirm = this.alertCtrl.create({
            title: 'Remove Item',
            message: 'Are you sure you want to remove this item?',
            buttons: [{
                text: 'Cancel',
                handler: () => {
                    console.log('cancel clicked');
                }
            },
            {
                text: 'Remove',
                handler: () => {
                    for (var i = 0; this.bookingCart.length > i; i++) {
                        for (let j = 0; this.bookingCart[i].FandB.length > j; j++) {
                            if (this.bookingCart[i].FandB[j].ticketId == ticket.ticketId) {
                                this.bookingCart[i].FandB.splice(j, 1);
                            }
                        }
                    }
                }
            }
            ]
        });
        confirm.present();
    }

    DeletePaidAtr(ticket) {
        const confirm = this.alertCtrl.create({
            title: 'Remove Item',
            message: 'Are you sure you want to remove this item?',
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                        console.log('cancel clicked');
                    }
                },
                {
                    text: 'Remove',
                    handler: () => {
                        for (var i = 0; this.bookingCart.length > i; i++) {
                            for (let j = 0; this.bookingCart[i].paidAttr.length > j; j++) {
                                if (this.bookingCart[i].paidAttr[j].ticketId == ticket.ticketId) {
                                    this.bookingCart[i].paidAttr.splice(j, 1);
                                }
                            }
                        }
                    }
                }
            ]
        });
        confirm.present();
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad CartPage');
    }
    presentLoader() {
        this.loader = this.loadingCtrl.create({
            content: "Please wait...",
            showBackdrop: true,
        });
        this.loader.present();
    }

    closeLoader() {
        this.loader.dismiss();
    }

    presentToast(text) {
        let toast = this.toastCtrl.create({
            message: text,
            duration: 2000,
            position: 'top'
        });

        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });

        toast.present();
    }

    Continue() {
        this.navCtrl.push(UserDetailsPage, { 'trackerId': this.trackerId });
    }

    AddMore(type) {
        let alert = this.alertCtrl.create({
            title: 'Select a Park',
            buttons: [{
                text: 'Essel World',
                handler: () => {
                    let navTransition = alert.dismiss();
                    navTransition.then(() => {
                        var pushto = {
                            bookHCID: 1,
                            bookHead: 'Essel World'
                        }
                        let bookingDt = localStorage.getItem('bookingDt');
                        if (type == 1) {
                            this.navCtrl.push(TicketCategoryPage, { 'pushToBook': pushto, 'ticketDate': bookingDt });
                        }
                        if (type == 2) {
                            this.navCtrl.push(FAndBCategoryPage, { 'pushToBook': pushto, 'ticketDate': bookingDt });
                        }
                        if (type == 3) {
                            this.navCtrl.push(PaidAttractionsPage, { 'pushToBook': pushto, 'ticketDate': bookingDt });
                        }

                    });
                    return false;
                }
            },
            {
                text: 'Water Kingdom',
                handler: () => {
                    let navTransition = alert.dismiss();
                    navTransition.then(() => {
                        var pushto = {
                            bookHCID: 2,
                            bookHead: 'Water Kingdom'
                        }
                        let bookingDt = localStorage.getItem('bookingDt');
                        if (type == 1) {
                            this.navCtrl.push(TicketCategoryPage, { 'pushToBook': pushto, 'ticketDate': bookingDt });
                        }
                        if (type == 2) {
                            this.navCtrl.push(FAndBCategoryPage, { 'pushToBook': pushto, 'ticketDate': bookingDt });
                        }
                        if (type == 3) {
                            this.navCtrl.push(PaidAttractionsPage, { 'pushToBook': pushto, 'ticketDate': bookingDt });
                        }
                    });
                    return false;
                }
            },
            {
                text: 'Bird Park',
                handler: () => {
                    let navTransition = alert.dismiss();

                    navTransition.then(() => {
                        var pushto = {
                            bookHCID: 3,
                            bookHead: 'Bird Park'
                        }
                        let bookingDt = localStorage.getItem('bookingDt');
                        if (type == 1) {
                            this.navCtrl.push(TicketCategoryPage, { 'pushToBook': pushto, 'ticketDate': bookingDt });
                        }
                        if (type == 2) {
                            this.parkErrorAlert(type);
                        }
                        if (type == 3) {
                            this.parkErrorAlert(type);
                        }
                    });
                    return false;
                }
            }]
        });

        alert.present();
    }

    parkErrorAlert(item) {

        var type;
        type = item;
        var msg;
        if (type = 2) {
            msg = 'F & B'
        } else if (type = 3) {
            msg = 'Paid Attractions'
        }
        let alert = this.alertCtrl.create({
            title: 'Sorry!!',
            message: 'We are currently not offering any ' + msg + ' for Bird Park',
            buttons: [
                {
                    text: 'OK',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                        // this.navCtrl.setRoot(TicketCategoryPage,{'pushToBook': this.pushToBook});
                    }
                },
            ]
        });
        alert.present();
    }

    deleteOnMain(trkid, hcid, cb) {
        let hid = hcid.toString();
        let url = ENUM.domain + ENUM.url.DeleteAllTicketsOnMain;
        var param = {
            trkrid: trkid,
            hid: hid
        }
        this.webAPI.getService(url, param)
            .then(result => {
                console.log('result on all delete ', result);
                let url = ENUM.domain + ENUM.url.fetchbookingsummary;
                let param = {
                    TrackID: this.trackerId
                }
                this.webAPI.getService(url, param)
                    .then(res => {
                        console.log('res booking summary after delete =>', res);
                    }).catch(err => {
                        console.log('err booking summary after delete =>', err);
                    })
                cb("RemoveAll");
            }).catch(err => {
                console.log('error on all delete ', err);
            });

    }
}
