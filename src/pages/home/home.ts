import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, LoadingController, AlertController, NavParams, Events, Modal, ViewController } from 'ionic-angular';
import { NotificationPage } from '../notification/notification';
import { SectionPage } from '../section/section';
import { HomeServiceProvider } from '../../service/home-service';
import { webAPIService } from '../../service/webAPIService';
import { checkIfOnlineService } from '../../service/checkIfOnlineService';
import { ENUM, NUMBER } from '../../service/ENUM';
import { MenuServiceProvider } from '../../service/menu-service';
import { getLocationService } from '../../service/getlocation.service';
import { MyApp } from '../../app/app.component';
import { LocationInfoService } from '../../service/LocationInfoService';
import { NotificationService } from '../../service/notification.service';
import { BookingPage } from '../booking/booking';
import { ModalController } from 'ionic-angular';
import { OffersPage } from '../offers/offers';
import { EventsPage } from '../events/events';
import { LoginPage } from '../login/login';
import { TicketCategoryPage } from '../ticket-category/ticket-category';
import { CartPage } from '../cart/cart';
import { ThemeServiceProvider } from '../../providers/theme-service/theme-service';
import { BookhomecategoryPage } from '../bookhomecategory/bookhomecategory';
@Component({
	selector: 'page-home',
	templateUrl: 'home.html',
})
export class HomePage {

	public number: any;

	public alertPresented: any;
	array: any;
	section: any;
	local: boolean = false;
	defaultJSON: any = [{
		cardBody: 'EsselWorld text , if any appears here.',
		cardHeader: 'EsselWorld',
		id: 1,
		imgPath: '../assets/imgs/esselworldHome.jpg',
		isActive: 1,
	},
	{
		cardBody: 'WaterKgdom text , if any appears here.',
		cardHeader: 'WaterKingdom',
		id: 2,
		imgPath: '../assets/imgs/waterkingdomHome.jpg',
		isActive: 1,
	}];
	homelist: any = [];
	categoryList: any = [];
	getlocationsonHome: any = [];
	backDrop: boolean = false;
	pushToBook: { bookHCID: any; bookHead: any; };
	forBackButton: boolean = false;
	trackerId: any;
	bookingDt: any;
	ThemeC: any;
	hid: any;
	currentPage: string;

	constructor(public navCtrl: NavController, public viewCtrl: ViewController, public webAPI: webAPIService,
		public menuService: MenuServiceProvider,
		public checkOnline: checkIfOnlineService, public homeservice: HomeServiceProvider,
		public loadingCtrl: LoadingController, public getlocationservice: getLocationService,
		private alertCtrl: AlertController, public navParams: NavParams,
		public locationService: LocationInfoService, public changeDetRef: ChangeDetectorRef,
		public notiService: NotificationService, public events: Events,
		public modalCtrl: ModalController, public themeServices: ThemeServiceProvider
	) {
		////////  Chnage for theme   ///////////////
		this.ThemeC = this.themeServices.getTheme();
		this.events.subscribe('theme', () => {
			this.ThemeC = this.themeServices.getTheme();
		});
		// this.trackerId = this.navParams.get('trackerId');
		// this.bookingDt = localStorage.getItem('bookingDt');
		this.events.subscribe('number', () => {
			// this.notiService.setNumber(this.NUMBER);
			this.number = this.notiService.getNumber();
		});
		this.currentPage = this.viewCtrl.name;
		console.log('this.currentPage    ', this.currentPage);
		console.log('this.number', this.number);
		this.fetchLocations();
		var AP = this.navParams.data.AP;
		if (AP == 'Y') {
			this.alertPresented = true;
		} else {
			this.alertPresented = false;
		}
		//this.getlocationsonHome = this.getlocationservice.locations;
		//console.log('getlocationsonHome',this.getlocationsonHome);

		if (localStorage.getItem('login') == 'Y') {
			this.menuService.enableMenu("login");
		} else {
			this.menuService.enableMenu("logout");
		}
		this.homelist = this.homeservice.getLocalList();
		console.log('this.homelist   ', this.homelist);
		let param = {};
		var onlineStatus = this.checkOnline.getIfOnline();
		console.log(onlineStatus);
		if (onlineStatus == true) {
			this.homeservice.getHomeListService(param)
				.then(result => {
					if (result != 'err') {
						console.log("SuccessHomeService :", result);
						let temp: any = result;
						let temp2 = temp.data
						for (let i = 0; i < temp2.length; i++) {
							temp2[i].imgPath = ENUM.domain + ENUM.url.imgUrl + temp2[i].imgPath
						}
						this.homelist = temp2;
						// this.categoryList = temp.data[1];
						// this.homelist[0].imgPath = 'assets/imgs/esselworldHome.jpg';
						// this.homelist[1].imgPath = 'assets/imgs/waterkingdomHome.jpg';
						// loader.dismiss();
					} else {
						// loader.dismiss();
						this.serverErrorAlert();
						this.homelist = this.homeservice.getLocalList();
					}
				}).catch(result => {
					console.log("Error :", result);
					this.local = false;
					this.homelist = this.homeservice.getLocalList();
					// loader.dismiss();
				})
		} else {
			this.local = false;
			this.homelist = this.homeservice.getLocalList();
			console.log(this.homelist);
			//loader.dismiss();		
		}

		this.section = [
			{
				id: 1,
				name: 'Essel World',

			},
			{
				id: 2,
				name: 'Water Kingdom',

			}
		]
	}

	ionViewDidEnter() {
		this.trackerId = this.navParams.get('trackerId');
		this.bookingDt = localStorage.getItem('bookingDt');
	}

	gotornotificationPage() {
		this.navCtrl.push(NotificationPage);
		console.log('notification.');
	}


	serverErrorAlert() {
		let vm = this;
		if (!vm.alertPresented) {
			vm.alertPresented = true;
			vm.alertCtrl.create({
				title: 'Server Error!!!',
				message: 'Server Not Responding. Please try again later.',
				buttons: [
					{
						text: 'OK',
						role: 'cancel',
						handler: () => {
							console.log('Cancel clicked');
						}
					},
				]
			}).present();
		}
	}

	gotoEW(index) {
		for (let i = 0; i <= this.homelist.length; i++) {
			if (i == index) {
				this.pushToBook = {
					bookHCID: this.homelist[index].id,
					bookHead: this.homelist[index].cardHeader
				}
				var hcItem = JSON.stringify(this.homelist[index]);
				localStorage.setItem('hcItem', hcItem);
				let ew: any = 1
				localStorage.setItem('hcId', this.homelist[index].id);
				// console.log('Essel World');
				console.log('this.pushToBook   for SectionPage  ', this.pushToBook);
				console.log('this.forBackButton for SectionPage ', this.forBackButton);
				if (index === 0) {
					this.themeServices.setTheme('red');
				} else if (index === 1) {
					this.themeServices.setTheme('blue');
				} else if (index === 2) {
					this.themeServices.setTheme('green');
				}
				this.navCtrl.push(SectionPage, { 'homelist': this.homelist, 'pushToBook': this.pushToBook, 'forBackButton': this.forBackButton });
				break;
			}

		}
	}

	getImage(path) {
		//var onlineStatus = this.checkOnline.getIfOnline();
		if (path && this.local) {
			this.webAPI.getImages(path).then(image => {
				return image;
			});
		} else {
			return path;
		}
	}

	fetchLocations() {
		var userDetails = JSON.parse(localStorage.getItem('UserDetails'));
		var userId = {
			"userId": 0
		}
		if (userDetails) {
			console.log('userDetails', userDetails)
			userId.userId = userDetails[0].reguserid
			console.log('var userid = ', userId);
		}
		this.webAPI.getLocation(ENUM.domain + ENUM.url.fetchLocations, userId)
			.then(result => {
				var temp: any = result
				console.log('getLocationResponse:', result);
				this.getlocationservice.locations = temp.data;
				this.locationService.setLocationList(temp.data);
			}).catch(error => {
				console.log('fetchlocation => ', error);
				this.locationService.setLocationList([]);
			})
	}

	booking(hcid) {
		if (localStorage.getItem('UserDetails')) {
			for (let i = 0; i <= this.homelist.length; i++) {
				if (i == hcid) {
					this.pushToBook = {
						bookHCID: this.homelist[hcid].id,
						bookHead: this.homelist[hcid].cardHeader
					}
					var bookHCID = JSON.stringify(this.homelist[hcid]);
					let ew: any = 1
					this.forBackButton = true;
					if (localStorage.getItem('trackerId')) {
						this.resetTicket();
					}
					else {
						this.presentModal();
					}
					break;
				}
			}
		}
		else {
			var checkLogin: boolean = true;
			this.forBackButton = true;
			this.navCtrl.push(LoginPage, { 'checkLogin': checkLogin, 'pushToBook': this.pushToBook, 'forBackButton': this.forBackButton });
		}
	}
	presentModal() {
		// localStorage.setItem('trackerId','');
		// localStorage.setItem('bookingDt','');
		this.backDrop = true;
		const modal = this.modalCtrl.create(BookhomecategoryPage, { 'pushToBook': this.pushToBook }, { cssClass: 'myModal' });
		modal.present();
	}

	resetTicket() {

		let alert = this.alertCtrl.create({
			title: 'Create a new booking?',
			buttons: [{
				text: 'No',
				handler: () => {
					// user has clicked the alert button
					// begin the alert's dismiss transition
					let navTransition = alert.dismiss();

					// start some async method
					// someAsyncOperation().then(() => {
					// once the async operation has completed
					// then run the next nav transition after the
					// first transition has finished animating out

					navTransition.then(() => {
						// this.alertPark();
						this.navCtrl.push(TicketCategoryPage, { 'pushToBook': this.pushToBook, 'forBackButton': this.forBackButton });
					});
					// });
					return false;
				}
			},
			{
				text: 'Yes',
				handler: () => {
					this.presentModal();
					//   this.navCtrl.push(UserDetailsPage, { 'pushToBook': this.pushToBook, 'ticketDate': this.tcktDt, 'trackerId': this.trackerId, 'totalTicketPrice': this.totalTicketPrice, 'totalTicketCount': this.totalTicketCount });
				}
			}]
		});

		alert.present();
	}
	goToOffers() {
		var hid: number;
		if (this.currentPage == 'HomePage') {
			hid = 0;
		}
		this.navCtrl.push(OffersPage, { 'hid': hid });
	}

	goToEvents() {
		var hid: number;
		if (this.currentPage == 'HomePage') {
			hid = 0;
		}
		this.navCtrl.push(EventsPage, { 'hid': hid });
	}

	gotocart() {
		this.navCtrl.push(CartPage);
	}

	goToHomeCategories() {
		this.navCtrl.push(BookhomecategoryPage);
	}
}
