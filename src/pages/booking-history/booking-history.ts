import { Component } from '@angular/core';
import {   IonicPage, NavController, NavParams, AlertController,
  LoadingController, ToastController, Platform, Nav } from 'ionic-angular';
import { CartPage } from '../cart/cart';
import { NotificationPage } from '../notification/notification';
import { HomePage } from '../home/home';
import { ENUM } from '../../service/ENUM';
import { webAPIService } from '../../service/webAPIService';

/**
 * Generated class for the BookingHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-booking-history',
  templateUrl: 'booking-history.html',
})
export class BookingHistoryPage {


	public imagePath:any;
	public HeadDetail:any;
	public DateDetail:any;
	public TimeDetail:any;
	public createdCode:any;
	bookingSummary: any = [];
  bookdetails: any =[];
  TotaltPrice: any = 0;
  TTP: any = 0;
  TotaltfPrice: any = 0;
  TFP: any = 0;
  TotaltaPrice: any = 0;
  TAP: any = 0;
  loader: any;
  nodataAvailable: boolean = false;
  totalPrice: number = 0;
	TQuantity: number = 0;
	FQuantity: number = 0;
	AQuantity: number = 0;
	totalTickets: number = 0;
	redeemedTickets: number;
	trackerid: any;
  	constructor(public navCtrl: NavController, public navParams: NavParams, public webAPI: webAPIService, 
			private alertCtrl: AlertController,public loadingCtrl: LoadingController) {
			this.trackerid = this.navParams.get('trackerid');
			console.log('this.trackerid  ',this.trackerid);
			this.fetchBookingSummary();
  		this.imagePath = 'assets/imgs/EsselWorldTicket.jpg';
  		this.HeadDetail = 'Essel World';
  		var MList = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  		var DList = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
  		var tDate = new Date();
  		var tDateYear = tDate.getFullYear();
  		var tDateMonth = tDate.getMonth();
  		var tDateDate = ("0" + tDate.getDate()).slice(-2);
  		var tDateDay = tDate.getDay();
  		this.DateDetail = DList[tDateDay] + ', ' + tDateDate + ' ' + MList[tDateMonth] + ' ' + tDateYear;
  		console.log(this.DateDetail);
  		var time = new Date();
	    var hours = time.getHours();
	    var minutes = time.getMinutes();
	    var state;
	    if (hours > 12) {
	        hours -= 12;
	        state = 'PM'
	    } else if (hours === 0) {
	        hours = 12;
	        state = 'AM'
	    }else{
	    	state = 'AM'
	    }
	    this.TimeDetail = hours + ' : ' + minutes + ' ' + state;
	    this.createdCode = localStorage.getItem('trackerId');
		}

		ionViewDidEnter(){
			// this.fetchBookingSummary();
		}

		ionViewDidLoad() {	
			console.log('ionViewDidLoad BookingHistoryListPage');
		}

		fetchBookingSummary() {
			let UserDetails = JSON.parse(localStorage.getItem('UserDetails'));
			let UserId: any;
			UserId = UserDetails[0].reguserid
			let param = {
				userid: UserId,
				trackerid: this.trackerid
			}
	
			var url = ENUM.domain + ENUM.url.fetchhConfirmedItemsDetails;
			this.presentLoader();
			this.webAPI.getService(url, param)
				.then(result => {
					console.log('Details:', result);
					let temp: any = result;
					console.log('temp  ',temp);
					if (!temp.data) {
						this.nodataAvailable = true;
						this.closeLoader();
					}
					else{
						this.nodataAvailable = false;
						let userdata: any = temp.data;
						console.log('fetchbookingsummaryREs:', userdata);
						this.bookingSummary.push(userdata);
						console.log('this.bookingSummary', this.bookingSummary);
						this.bookdetails = this.bookingSummary[0];
						console.log('this.bookdetails ',this.bookdetails);
						this.calculateTicket();
						this.calculateFnB();
						this.calculateAttr();
						this.totalPrice = this.TAP + this.TFP + this.TTP;
						console.log('this.totalPrice ',this.totalPrice);
						this.totalTickets = this.TQuantity + this.FQuantity + this.AQuantity;
						console.log('this.totalTickets ',this.totalTickets);
						this.closeLoader();
				}
				}).catch(result => {
					console.log("Server Response Error :", result);
					this.closeLoader();
				});
		}
	
		calculateTicket() {
			let Tprice = 0;
			let TQuantity = 0;
			let TRedeemed = 0;
	
			for (let i = 0; i < this.bookingSummary[0].tickets.length; i++) {
				// for(let j = 0; j < this.bookingSummary[0].tickets[i].qty;){
				Tprice = this.bookingSummary[0].tickets[i].ticketprice;
				TQuantity = this.bookingSummary[0].tickets[i].ticCount;
				TRedeemed = this.bookingSummary[0].tickets[i].redeemed;
				this.TotaltPrice = Tprice * TQuantity;
				this.TQuantity = this.TQuantity + TQuantity;
				this.TTP = this.TTP + this.TotaltPrice;
				// console.log('TRedeemed',TRedeemed);
			}
			console.log('this.TQuantity  ',this.TQuantity);
			if(this.TTP == '' || this.TTP == undefined){
				this.TTP = 0;
			}
			if(this.TQuantity == undefined){
				this.TQuantity = 0;
			}
		}
	
		calculateFnB() {
			let Fprice = 0;
			let FQuantity = 0;
			var FRedeemed: number = 0;
	
			for (let i = 0; i < this.bookingSummary[0].FandB.length; i++) {
				// for(let j = 0; j < this.bookingSummary[0].FandB[i].qty;){
				Fprice = this.bookingSummary[0].FandB[i].ticketprice;
				FQuantity = this.bookingSummary[0].FandB[i].ticCount;
				FRedeemed = this.bookingSummary[0].FandB[i].redeemed;
				this.TotaltfPrice = Fprice * FQuantity;
				this.FQuantity = this.FQuantity + FQuantity;
				this.TFP = this.TFP + this.TotaltfPrice;
			}
			console.log('this.FQuantity  ',this.FQuantity);
			if(this.TFP == '' || this.TFP == undefined){
				this.TFP = 0;
			}
			if(this.FQuantity == undefined){
				this.FQuantity = 0;
			}
		}
	
		calculateAttr() {
			let Aprice = 0;
			let AQuantity = 0;
			let ARedeemed = 0;
	
			for (let i = 0; i < this.bookingSummary[0].paidAttr.length; i++) {
				// for(let j = 0; j < this.bookingSummary[0].FandB[i].qty;){
				Aprice = this.bookingSummary[0].paidAttr[i].ticketprice;
				AQuantity = this.bookingSummary[0].paidAttr[i].ticCount;
				ARedeemed = this.bookingSummary[0].paidAttr[i].redeemed;
				this.TotaltaPrice = Aprice * AQuantity;
				this.AQuantity = this.AQuantity + AQuantity;
				this.TAP = this.TAP + this.TotaltaPrice;
			}
			console.log('this.AQuantity  ',this.AQuantity);
			if(this.TAP == '' || this.TAP == undefined){
				this.TAP = 0;
			}
			if(this.AQuantity == undefined){
				this.AQuantity = 0;
			}
		}
	
		// totalPrice(){
		//   // console.log(this.TAP + this.TFP + this.TTP);
		//   return this.TAP + this.TFP + this.TTP;
		// }
	
		goToHistoryDetails(){
			this.navCtrl.push(BookingHistoryPage);
		}
	
		gotornotificationPage(){
			this.navCtrl.push(NotificationPage);
			console.log('notification.');
		}
	
		gotocart(){
			this.navCtrl.push(CartPage);
		}
	
		presentLoader() {
			this.loader = this.loadingCtrl.create({
				content: "Please wait...",
				showBackdrop: true,
			});
			this.loader.present();
		}
	
		closeLoader() {
	
			//this.offers = this.array;
			this.loader.dismiss();
		}
	
		alertforLogin() {
			let alert = this.alertCtrl.create({
				title: 'Authentication Error!!!',
				message: 'Please login to check your Booking History',
				buttons: [
					{
						text: 'OK',
						role: 'cancel',
						handler: () => {
							//console.log('Cancel clicked');
							this.navCtrl.setRoot(HomePage);
						}
					},
				]
			});
			alert.present();
		}

}
