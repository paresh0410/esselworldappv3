import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TermsAndConditionsService } from '../../service/termsAndConditions.service';

/**
 * Generated class for the TermsAndConditionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-terms-and-conditions',
  templateUrl: 'terms-and-conditions.html',
})
export class TermsAndConditionsPage {

  termsAndCondt: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private tandcService:TermsAndConditionsService) {
    this.termsAndCondt = this.tandcService.getAll();
    console.log('this.termsAndCondt', this.termsAndCondt);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TermsAndConditionsPage');
  }

}
