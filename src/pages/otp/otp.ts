import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { webAPIService } from '../../service/webAPIService';
import { ENUM } from '../../service/ENUM';
import { OneSignalService } from '../../service/oneSignalService';
import { Device } from '@ionic-native/device';
import { RideDetailsPage } from '../ride-details/ride-details';
/**
 * Generated class for the OtpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-otp',
	templateUrl: 'otp.html',
})
export class OtpPage {

	passcode: any = "";
	password: any = "";
	pincode: any = [null, null, null, null, null, null];
	confirmpin: any = "";
	confirmpassword: any = "";
	pinshow: boolean = false;
	authPassFlag: boolean = true;
	backpassword: number = 0;
	userDetails: any;
	checkLogin: boolean;
	pushToBook: any;
	constructor(public navCtrl: NavController, public navParams: NavParams,
		public toastCtrl: ToastController, public webAPI: webAPIService,
		public loadingCtrl: LoadingController, private onesignalservice: OneSignalService,
		public device: Device) {
			this.checkLogin = this.navParams.get('checkLogin');
			this.pushToBook = this.navParams.get('pushToBook');
	}

	init() {
		this.passcode = "";
		this.password = "";
	}
	add(value) {
		if (this.confirmpin.length < 6) {
			this.confirmpin = this.confirmpin + value;
			this.confirmpassword += value;
			if (this.confirmpin.length == 6) {
				// $timeout(function() {
				//     console.log("The four digit code was entered");
				// }, 500);
				// setTimeout(data =>{
				console.log("The six digit code was entered");
				this.login();
				// },500)
			}
			this.replacing();
		}

	}
	delete() {
		if (this.confirmpin.length > 0) {
			this.confirmpin = this.confirmpin.substring(0, this.confirmpin.length - 1);
			this.confirmpassword = this.confirmpassword.substring(0, this.confirmpassword.length - 1);
		}
		// if(this.confirmpin.length == 0){
		// 	this.backpassword++;
		// 	if(this.backpassword > 1){
		// 		this.authPassFlag = !this.authPassFlag;
		// 	}

		// }
	}

	showpassword() {
		this.pinshow = true;
		this.replacing();
	}
	hidepassword() {
		this.pinshow = false;
		this.replacing();
	}
	replacing() {
		if (!this.pinshow) {
			this.passcode = this.passcode.replace(/./g, "*");
		} else {
			this.passcode = this.password;
		}

		if (!this.pinshow) {
			this.confirmpin = this.confirmpin.replace(/./g, "*");
		} else {
			this.confirmpin = this.confirmpassword;
		}
	}
	authenticate() {
		if (this.password.length == 6) {
			// setTimeout(data => {
			this.authPassFlag = true;
			// },100)
		}

	}
	login() {

		if (this.confirmpassword.length == 6) {
			let loader = this.loadingCtrl.create({
				content: "Please wait..."
			});
			loader.present();
			// if(this.password == this.confirmpassword){

			var mobileNumber = JSON.parse(localStorage.getItem(mobileNumber));
			var userDetails = JSON.parse(localStorage.getItem('UserDetails'));
			var session = JSON.parse(localStorage.getItem('SendOtpResponse'));
			var otpStatus = userDetails[0].isOtpVerified;
			var param = {
				ireguserid: userDetails[0].reguserid,
			}
			var paramOtp = {
				// mobile : localStorage.getItem('mobileNumber'),
				// otp : this.confirmpassword
				session: session,
				verOTP: this.confirmpassword
			}
			var UpOtpparam = {
				imobileno: localStorage.getItem('mobileNumber'),
			}
			this.webAPI.otpService(ENUM.domain + ENUM.url.verifyOtp, paramOtp)
				.then(result => {
					console.log('verifyOTPresponse', result);
					var optRes: any = result;
					var OTPDATA = optRes.data;
					var otpRes = JSON.parse(OTPDATA)
					if (otpRes.Status == 'Success') {
						this.webAPI.getService(ENUM.domain + ENUM.url.loginHistory, param)
							.then(result => {
								console.log('loginhistoryRes', result)
								this.userDetails;
								var userID = null;
								if (otpStatus == 'N') {
									userDetails = JSON.parse(localStorage.getItem('UserDetails'));
									userDetails[0].isOtpVerified = "Y";
									 var string =  JSON.stringify(userDetails);
									 localStorage.setItem('UserDetails',string)
									this.webAPI.getService(ENUM.domain + ENUM.url.updateOtpStatus, UpOtpparam)
										.then(result => {
											console.log("Success :", result);
											this.presentToast('Login Successfully');
											// var isPageFound = localStorage.getItem('Page');
											// if(isPageFound){
											// 	this.navCtrl.popTo(isPageFound);
											// }
											// else{
												this.navCtrl.setRoot(HomePage,{'checkLogin':this.checkLogin,'pushToBook':this.pushToBook});
											// }
											localStorage.setItem("login", 'Y');
											loader.dismiss();
											if (result) {
												var pushIds = this.onesignalservice.getPushIds();	this.onesignalservice.getPushIds();
												console.log('this.pushIds ', this.onesignalservice.getPushIds());

												if (localStorage.getItem('UserDetails')) {
													userDetails = JSON.parse(localStorage.getItem('UserDetails'));
													if (userDetails.length > 0) {
														userID = userDetails[0].reguserid;
													}
												}
												let param: any = {
													uId: null,
													appId: ENUM.oneSignal.configId,
													model: this.device.model,
													platforms: this.device.platform,
													version: this.device.version,
													pushId: pushIds.pushToken, // This token varies 
													pushUserId: pushIds.userId, //This ID is constant
													uuId: this.device.uuid
												};
												if (userDetails[0].reguserid) {
													param.uId = userDetails[0].reguserid;
													var newurl = ENUM.domain + ENUM.url.insertPushUser;
													this.webAPI.getService(newurl, param)
													.then(result => {
														console.log('Update result',result);
													}).catch(result =>{
														console.log('Error ',result);
													});
												}
												else {
													this.webAPI.getService(newurl, param)
													.then(result => {
														console.log('Insert result',result);
													}).catch(result =>{
														console.log('Error ',result);
													});
												}												
											}
										}).catch(result => {
											console.log("updateOTPresponseError :", result);
											this.presentToast('Something went wrong.please try again.');
											loader.dismiss();
										});
								} else {
									console.log("Success :", result);
									this.presentToast('Login Successfully');
									var isPageFound = localStorage.getItem('Page');
									if(isPageFound){
										if(isPageFound == 'RideDetailsPage'){
											localStorage.setItem("checkState",'Y');
										this.navCtrl.pop()
										// this.navCtrl.popTo()
										.then(() => this.navCtrl.first().dismiss());
									}
									if(isPageFound == 'BookingHistoryListPage'){
										localStorage.setItem("checkState",'Y');
									this.navCtrl.pop()
									// this.navCtrl.popTo()
									.then(() => this.navCtrl.first().dismiss());
								}
									}
									else{
									this.navCtrl.setRoot(HomePage,{'checkLogin':this.checkLogin,'pushToBook':this.pushToBook});
									}
									localStorage.setItem("login", 'Y');
									loader.dismiss();
									if (result) {
										var pushIds = this.onesignalservice.getPushIds();
										console.log('this.pushIds ', pushIds);
										if (localStorage.getItem('UserDetails')) {
											userDetails = JSON.parse(localStorage.getItem('UserDetails'));
											if (userDetails.length > 0) {
												userID = userDetails[0].reguserid;
											}
										}
										let param: any = {
											uId: null,
											appId: ENUM.oneSignal.configId,
											model: this.device.model,
											platforms: this.device.platform,
											version: this.device.version,
											pushId: pushIds.pushToken, // This token varies 
											pushUserId: pushIds.userId, //This ID is constant
											uuId: this.device.uuid
										};
										if (userDetails[0].reguserid) {
											param.uId = userDetails[0].reguserid;
											var newurl = ENUM.domain + ENUM.url.insertPushUser;
											this.webAPI.getService(newurl, param)
											.then(result => {
												console.log('Update result',result);
											}).catch(result =>{
												console.log('Error ',result);
											});
										}
										else {
											this.webAPI.getService(newurl, param)
											.then(result => {
												console.log('Insert result',result);
											}).catch(result =>{
												console.log('Error ',result);
											});
										}
									}
								}



							}).catch(result => {
								console.log("loginHistoryError :", result);
								this.presentToast('Something went wrong.please try again.');
								loader.dismiss();
							})
					} else {
						this.presentToast('Invalid OTP.');
						loader.dismiss();
					}


				}).catch(result => {
					console.log("verifyOtpError :", result);
					this.presentToast('Something went wrong.please try again.');
					loader.dismiss();
				})

			// }else{
			// 	this.presentToast('Wrong PIN');
			// }
		} else {
			this.presentToast('Invalid OTP.');
		}
	}

	presentToast(text) {
		let toast = this.toastCtrl.create({
			message: text,
			duration: 2000,
			position: 'top'
		});

		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});

		toast.present();
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad OtpPage');
	}

	resendOTP() {
		var otpParam = {
			imobileno: localStorage.getItem('mobileNumber')
		}

		this.webAPI.otpService(ENUM.domain + ENUM.url.sendOtp, otpParam)
			.then(result => {
				console.log('sendOtpRespnoseResend', result);
				//localStorage.clear('SendOtpResponse');
				var temp: any = result
				var SendOtpResponse = JSON.parse(temp.data)
				console.log('send', SendOtpResponse)
				var fstr = JSON.stringify(SendOtpResponse.Details)
				localStorage.setItem('SendOtpResponse', fstr)
				//this.navCtrl.push(OtpPage);
			}).catch(result => {
				console.log("sendOtpErrorResend :", result);
				this.presentToast('Something went wrong.please try again.');
			})
	}

}
