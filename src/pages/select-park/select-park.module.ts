import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectParkPage } from './select-park';

@NgModule({
  declarations: [
    SelectParkPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectParkPage),
  ],
}) 
export class SelectParkPageModule {}
