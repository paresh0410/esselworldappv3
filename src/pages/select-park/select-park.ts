import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { TicketCategoryPage } from '../ticket-category/ticket-category';
import { UserDetailsPage } from '../user-details/user-details';
import { ThemeServiceProvider } from '../../providers/theme-service/theme-service';
import { CartPage } from '../cart/cart';
import { NotificationPage } from '../notification/notification';
import { ENUM } from '../../service/ENUM';
import { checkIfOnlineService } from '../../service/checkIfOnlineService';
import { HomeServiceProvider } from '../../service/home-service';
import { LoginPage } from '../login/login';
import { BookingPage } from '../booking/booking';
/**
 * Generated class for the SelectParkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-select-park',
  templateUrl: 'select-park.html',
})
export class SelectParkPage {
  trackerId: any;
  pushToBook: any;
  tcktDt: any;
  totalTicketPrice: number;
  totalTicketCount: number;
  red: boolean;
  blue: boolean;
  green: boolean;
  ThemeC: any;

  buttonClicked: boolean = false;
  toggle: boolean = false;
  rotate: boolean;
  homelist: any;
	iconImg: { icon: string; }[];
  local: boolean;
  forBackButton: boolean;
  getColor(hc){
    switch(hc){
      case 'red':
        return '1px solid #ff0000 ';
      case 'blue':
        return '1px solid #488aff';
      case 'green':
        return '1px solid #16910b';
    }
  }

  constructor(public navCtrl: NavController,public themeServices: ThemeServiceProvider,
    public events: Events, public navParams: NavParams, public checkOnline: checkIfOnlineService,public homeservice: HomeServiceProvider) {
	
  this.iconImg = [
    {
      icon: "/assets/imgs/ewNew.png"
    },
  {
    icon: "/assets/imgs/wkNew1.png"
  },{
    icon: "/assets/imgs/bpNew.png"
  }]
    this.ThemeC = this.themeServices.getTheme();
      this.events.subscribe('theme', () => {
        this.ThemeC = this.themeServices.getTheme();
      });
      if(this.ThemeC == 'red'){
        this.red = true;
        this.blue = false;
        this.green = false;
      }else if(this.ThemeC == 'blue'){
        this.blue = true;
        this.red = false;
        this.green = false;
      }else if(this.ThemeC == 'green'){
        this.green = true;
        this.red = false;
        this.blue = false;
      }

      
    this.pushToBook = this.navParams.get('pushToBook');
    console.log('this.pushToBook ==>', this.pushToBook);
    this.tcktDt = this.navParams.get('ticketDate');
    console.log('this.tcktDt  ==>', this.tcktDt);
    this.trackerId = localStorage.getItem('trackerId');
    this.totalTicketPrice = this.navParams.get('totalTicketPrice');
    console.log('this.totalTicketPrice  ==>', this.totalTicketPrice);
    this.totalTicketCount = this.navParams.get('totalTicketCount');
    console.log('this.totalTicketCount  ==>', this.totalTicketCount);
    var onlineStatus = this.checkOnline.getIfOnline();
    // if (onlineStatus == true) {
      let param = {};
      this.homeservice.getHomeListService(param)
        .then(result => {
          if (result != 'err') {
            console.log("SuccessHomeService :", result);
            let temp: any = result;
            let temp2 = temp.data
            for (let i = 0; i < temp2.length; i++) {
              temp2[i].imgPath = ENUM.domain + ENUM.url.imgUrl + temp2[i].imgPath
            }
            this.homelist = temp2;
						console.log('this.homelist',this.homelist);
						for(let i = 0; i < this.homelist.length; i++){
							this.homelist[i].icon = this.iconImg[i].icon;
						}
						console.log('this.homeList',this.homelist);
            // this.categoryList = temp.data[1];
            // this.homelist[0].imgPath = 'assets/imgs/esselworldHome.jpg';
            // this.homelist[1].imgPath = 'assets/imgs/waterkingdomHome.jpg';
            // loader.dismiss();
          }
        }).catch(result => {
          console.log("Error :", result);
          this.local = false;
          // this.homelist = this.homeservice.getLocalList();
          // loader.dismiss();
        })
    // }
  }

  booking(hcid) {
    console.log('hcid ',hcid);
		if (localStorage.getItem('UserDetails')) {
			for (let i = 0; i <= this.homelist.length; i++) {
				if (i == hcid) {
					this.pushToBook = {
						bookHCID: this.homelist[hcid].id,
						bookHead: this.homelist[hcid].cardHeader
          }
          if(hcid === 0){
            this.themeServices.setTheme('red');
          }else if(hcid === 1){
            this.themeServices.setTheme('blue');
          }else if(hcid === 2){
            this.themeServices.setTheme('green');
          }
					var bookHCID = JSON.stringify(this.homelist[hcid]);
					let ew: any = 1
					this.forBackButton = true;
					if (localStorage.getItem('trackerId')) {
						// this.resetTicket();
						// localStorage.removeItem('trackerId');
            // let bookingDt = localStorage.getItem('bookingDt');
            console.log('trackerId in select park ',localStorage.getItem('trackerId'));
						this.navCtrl.push(TicketCategoryPage, { 'pushToBook': this.pushToBook });
					}
					break;
				}
			}
		}
		else {
			var checkLogin: boolean = true;
			this.forBackButton = true;
			this.navCtrl.push(LoginPage, { 'checkLogin': checkLogin, 'pushToBook': this.pushToBook, 'forBackButton': this.forBackButton });
		}
  }
  gotoEW(){
    this.pushToBook.bookHCID = 1;
    this.pushToBook.bookHead = 'Essel World'            
    let bookingDt = localStorage.getItem('bookingDt');
    this.navCtrl.push(TicketCategoryPage, { 'pushToBook': this.pushToBook, 'bookingDt': bookingDt });
  }
  gotoWK(){ 
    this.pushToBook.bookHCID = 2;
    this.pushToBook.bookHead = 'Water Kingdom'
    let bookingDt = localStorage.getItem('bookingDt');
    this.navCtrl.push(TicketCategoryPage, { 'pushToBook': this.pushToBook, 'bookingDt': bookingDt });
  }
  gotoBP(){
    this.pushToBook.bookHCID = 3;
    this.pushToBook.bookHead = 'Bird Park'
    let bookingDt = localStorage.getItem('bookingDt');
    this.navCtrl.push(TicketCategoryPage, { 'pushToBook': this.pushToBook, 'bookingDt': bookingDt });
  }
  skip(){
    this.navCtrl.push(UserDetailsPage, { 'pushToBook': this.pushToBook, 'ticketDate': this.tcktDt, 'totalTicketPrice': this.totalTicketPrice, 'totalTicketCount': this.totalTicketCount});
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectParkPage');
  }
  gotornotificationPage() {
    this.navCtrl.push(NotificationPage);
    console.log('notification.');
  }

  gotocart() {
    this.navCtrl.push(CartPage);
  }

  gotohomePage() {
    // this.navCtrl.push(HomePage,{});
    console.log('gotohomePage function called');
    this.navCtrl.popToRoot();

  }
}
