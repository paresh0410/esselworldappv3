import { Component,ChangeDetectorRef } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { NavController, LoadingController, ToastController,Events } from 'ionic-angular';
import { NotificationPage } from '../notification/notification';
import { AlertController } from 'ionic-angular';
import { ENUM } from '../../service/ENUM';
import { webAPIService } from '../../service/webAPIService';
import { checkIfOnlineService } from '../../service/checkIfOnlineService';
import { NoInternetConnectionPage } from '../no-internet-connection/no-internet-connection';
import { HomePage } from '../home/home'
import { RideDetailsPage } from '../ride-details/ride-details'
import { CartPage } from '../cart/cart';

import { ThemeServiceProvider } from '../../providers/theme-service/theme-service';

@IonicPage({
  name: 'schedule',
  priority: 'high'
})
@Component({
  selector: 'page-schedule',
  templateUrl: 'schedule.html',
})
export class SchedulePage {
	temp: any = [];
	temp2: any = [];
	attr: any = [];
	dist: any = [];

  // Pull to refresh code 
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  // Declarations for rides,games and f&b

  titles: string[];
  avatars: string[];
  items: Array<{ title: string, avatar: string }>;
  outerArray : any=[];
  innerArray : any=[];
  headerArray : any=[];
  rides: any[];
  fb: any[];
  games: any[];
  nodataAvailable : boolean = false;
  header : any;

  status: boolean;
  public category: string = 'suggested';
  public categories: Array<string> = ['suggested', 'ride', 'fnb', 'game'];
	ThemeC :any;
	red: boolean = false;
	blue: boolean = false;
	green: boolean = false;
	// getColor: any;

	getColor(hc){
		switch(hc){
			case 'EsselWorld':
				return '#ff0000 ';
			case 'Water Kingdom':
				return '#488aff';
			case 'Bird Park':
				return '#16910b';
		}
	}
	  constructor(public navCtrl: NavController, private alertCtrl: AlertController, public webAPI: webAPIService,
		public checkOnline: checkIfOnlineService, public loadingCtrl: LoadingController, public changeDetRef: ChangeDetectorRef,
		public toastCtrl : ToastController, public themeServices : ThemeServiceProvider,public events: Events,) {
			////////  Chnage for theme   ///////////////
			this.ThemeC = this.themeServices.getTheme();
			this.events.subscribe('theme', () => {
			this.ThemeC = this.themeServices.getTheme();
		});	
		if(this.ThemeC == 'red'){
			this.red = true;
			this.blue = false;
			this.green = false;
		}else if(this.ThemeC == 'blue'){
			this.blue = true;
			this.red = false;
			this.green = false;
		}else if(this.ThemeC == 'green'){
			this.green = true;
			this.red = false;
			this.blue = false;
		}
	  	if(localStorage.getItem('login')=='Y'){
			this.fetchSchedule();
	  		

	  	}else{
	  		this.innerArray = [];
	    	this.outerArray = [];
			this.headerArray = [];
             this.alertforLogin();
	  	}

	}

	fetchSchedule(){
		this.innerArray = [];
		this.outerArray = [];
		this.headerArray = [];
		let loader = this.loadingCtrl.create({
			content: "Please wait..."
		});     
		loader.present();
		var UD:any = JSON.parse(localStorage.getItem('UserDetails'));
		// var hcID:any = localStorage.getItem('hcId');
		var param = {
			userId: UD[0].reguserid,
		}
		var url = ENUM.domain + ENUM.url.mySchedule;
		var onlineStatus = this.checkOnline.getIfOnline();
		if(onlineStatus == true){
			this.webAPI.getService(url,param)
			.then(result => {
				if(result != 'err'){
					console.log('result',result);
					this.temp = result;
					console.log('temp',this.temp);
					var temp:any = this.temp.data;
					var attr1:any = temp[0];
					var dist:any = temp[1];
					let this_ref=this;

					this.getMySchduleInJSON(attr1, function(RidesList){

								this_ref.outerArray = RidesList;
								console.log('this_ref.outerArray',this_ref.outerArray);

	        		});
					// dist.forEach(distele => {
					// 	this_ref.headerArray.push(distele);
					// 	console.log('this_ref.headerArray',this_ref.headerArray)
					// 	attr1.forEach(attrele => {
					// 		if(distele.Locations == attrele.Locations){
					// 			attrele.locimgpath = ENUM.domain+ENUM.url.imgUrl+attrele.locimgpath;
					// 			this_ref.innerArray.push(attrele);
					// 		}
					// 	});
					// 	//this_ref.innerArray.name.push(distele.Locations);
					// 	this_ref.outerArray.push(this_ref.innerArray);
					// 	this_ref.innerArray = [];
					// });

					// for(let i=0;i<this.outerArray.length;i++){
					// 	if(this.outerArray[i].HomeCat == null){
					// 		this.outerArray[i].HomeCat = 'Common';
					// 	}
					// }
				
					console.log('this.outerArray',this.outerArray)
					loader.dismiss();
				}else{
					loader.dismiss();
					//this.serverErrorAlert();
					this.nodataAvailable = true;

				}
			})	
		}else{
			loader.dismiss();
			// this.presentToast('No Internet Connection!!!');
			this.navCtrl.push(NoInternetConnectionPage);
		}
	}
	  
	serverErrorAlert(){
		let alert = this.alertCtrl.create({
			title: 'Server Error!!!',
			message: 'Server Not Responding. Please try again later.',
			buttons: [
				{
					text: 'OK',
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
					}
				},
			]
		});
		alert.present();
	}

  	// code for changing the tab name (unused)
  	onTabChanged(tabName) {
    	this.category = tabName;
  	}

  	// code to change the status of tab changed (unused)
  	updateupdateStatusr() {
    	console.log('Cucumbers new state:' + this.status);
  	}

  	// code for click on an item of the list (unused)
  	test() {
    	alert('Click handler works!');
  	}

  	// function for navigating to notification page
  	gotornotificationPage() {
    	this.navCtrl.push(NotificationPage);
    	console.log('notification.');
  	}

  	// function for deleting an item in rides list
  	removeItemRides(item) {
		let loader = this.loadingCtrl.create({
			content: "Please wait..."
		});     
		loader.present();
		this.innerArray = [];
		this.outerArray = [];
		this.headerArray = [];
		console.log(item);
		var UD:any = JSON.parse(localStorage.getItem('UserDetails'));
		var param = {
			userId: UD[0].reguserid,
			locId: item.id,
			lSeen: item.flag,
			actv: 0
		}
		var url = ENUM.domain + ENUM.url.updateMySchedule;
		var onlineStatus = this.checkOnline.getIfOnline();
		if(onlineStatus == true){
			this.webAPI.pushService(url,param)
			.then(result => {
				if(result == 'err'){
					loader.dismiss();
					this.presentToast('Could not connect to server! Action cannot be completed.');
					console.log('SERVER ERROR');
				}else{
					if(result != 'err'){
						console.log('result',result);
						this.temp = result;
						console.log('temp',this.temp);
						var temp:any = this.temp.data;
						var attr1:any = temp[0];
						console.log('att1',attr1);
						var dist:any = temp[1];
						console.log('dist',dist)
					
						let this_ref=this;

						this.getMySchduleInJSON(attr1, function(RidesList){

							this_ref.outerArray = RidesList;

						});
						console.log('this_ref.outerArray ',this_ref.outerArray);
			
						//console.log('this.outerArray',this.outerArray);
						loader.dismiss();
					}else{
						loader.dismiss();
						//this.serverErrorAlert();
						this.nodataAvailable = true;
	
					}
				}
				this.navCtrl.setRoot(SchedulePage);
			})
		}else{
			this.presentToast('No Internet Connection! Action cannot be completed.');
		}
		
  	}

  	// function for deleting an item in F&B list
  	removeItemFB(item) {
    	for (let i = 0; i < this.fb.length; i++) {
      		if (this.fb[i] == item) {
        		this.fb.splice(i, 1);
      		}
    	}
  	}

  	// function for deleting an item in Games list
  	removeItemGames(item) {
    	for (let i = 0; i < this.games.length; i++) {
      		if (this.games[i] == item) {
        		this.games.splice(i, 1);
      		}
    	}
  	}

	// function for alerting the details about the item (unused)
  	presentAlert(item) {
    	if (this.rides == item) {
      		let alert = this.alertCtrl.create({
        		title: 'Low battery',
        		subTitle: '10% of battery remaining',
        		buttons: ['Dismiss']
      		});
      		alert.present();
    	}else if (this.fb == item) {
      		let alert = this.alertCtrl.create({
        		title: 'Low battery',
        		subTitle: '10% of battery remaining',
        		buttons: ['Dismiss']
      		});
      		alert.present();
    	}else {
      		let alert = this.alertCtrl.create({
        		title: 'Low battery',
        		subTitle: '10% of battery remaining',
        		buttons: ['Dismiss']
      		});
      		alert.present();
    	}
	}
	  
	ifSeen(attrData){
		console.log('DATA====>',attrData);
		var UD:any = JSON.parse(localStorage.getItem('UserDetails'));
		if(attrData.flag == true){
			var LS = 1;
		}else{
			LS = 0;
		}
		var param = {
			userId: UD[0].reguserid,
			locId: attrData.id,
			lSeen: LS,
			actv: attrData.isactive
		}
		var url = ENUM.domain + ENUM.url.updateMySchedule;
		var onlineStatus = this.checkOnline.getIfOnline();
		if(onlineStatus == true){
			this.webAPI.pushService(url,param)
			.then(result => {
				if(result == 'err'){
					this.presentToast('Cannot connect to server! Action cannot be completed.');
					console.log('SERVER ERROR');
				}
			})
		}else{
			this.presentToast('No Internet Connection! Action cannot be completed.');
		}
	}

	presentToast(text) {
		let toast = this.toastCtrl.create({
		  message: text,
		  duration: 2000,
		  position: 'top'
		});
  
		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});
  
		toast.present();
	}

	alertforLogin(){
    	let alert = this.alertCtrl.create({
      		title: 'Authentication Required!',
      		message: 'Login to access your Schedule',
      		buttons: [
        		{
          			text: 'OK',
          			role: 'cancel',
          			handler: () => {
            			//console.log('Cancel clicked');
            			this.navCtrl.setRoot(HomePage);
          			}
        		},
      		]
    	});
    	alert.present();
	}
	  
	getMySchduleInJSON(result, cb) {
  		
			console.log(result.length);
   
        var SchduleList :any= [];
        var Schdules :any= {};
        
        var Schdule = [];
		var schdule = {};
		
        if(result.length > 0) {
					var lcid = result[0].lcid;
						var title ;
						var Locations;
						if(result[0].hcid == 0){
							title = "COMMON";
						}else{
							title =result[0].hctitle;
						}
            Schdules = {
            	lcid : result[0].lcid,
							Locations: result[0].Locations,
							HomeCat : title,
							hcid : result[0].hcid,
            	subSchdule : []
            };
            var i = 0;
            for (i = 0; i < result.length; i++) {
							if(result[i].hcid == 3){
								if(result[i].lcid == 2){
									result[i].Locations = 'Our Heroes';
								}else if(result[i].lcid == 3){
									result[i].Locations = 'Shopping Nest';
								}
						}
                if(result[i].lcid == lcid){
                	schdule = {
										Locations : result[i].Locations,
										HomeCat : result[i].hctitle,
                		RegLikeStatus : result[i].RegLikeStatus,
                		desc : result[i].desc,
                		dislike : result[i].dislike,
                		flag : result[i].flag,
                		id : result[i].id,
                		isactive : result[i].isactive,
                		lcid : result[i].lcid,
                		like : result[i].like,
                		locimgpath : ENUM.domain+ENUM.url.imgUrl+result[i].locimgpath,
                		msid : result[i].msid,
                		reguserid : result[i].reguserid,
                		title : result[i].title,
                		ytlink : result[i].ytlink
                		
									};

                	Schdule.push(schdule);
                }
                if (lcid !== result[i].lcid || i === result.length) { 
                    
                    Schdules.subSchdule = Schdule;
					
                    SchduleList.push(Schdules);
                    
                    Schdule =[];
						
										schdule = {};
										title ='';
										Locations = '';
										if(result[i].hcid == 0){
											title = "COMMON";
										} 
										else{
											title =result[i].hctitle;
										}
                Schdules = {
		            	lcid : result[i].lcid,
									Locations: result[i].Locations,
									HomeCat : result[i].hctitle,
		            	subSchdule : []
								};
								
		            schdule = {
										Locations : result[i].Locations,
										HomeCat : result[i].hctitle,
										hcid: result[i].hcid,
                		RegLikeStatus : result[i].RegLikeStatus,
                		desc : result[i].desc,
                		dislike : result[i].dislike,
                		flag : result[i].flag,
                		id : result[i].id,
                		isactive : result[i].isactive,
                		lcid : result[i].lcid,
                		like : result[i].like,
                		locimgpath : ENUM.domain+ENUM.url.imgUrl+result[i].locimgpath,
                		msid : result[i].msid,
                		reguserid : result[i].reguserid,
                		title : result[i].title,
                		ytlink : result[i].ytlink
                	};
                	Schdule.push(schdule);
									lcid = result[i].lcid;
					// lscname = result[i].lscname;
                }
            }
			
            Schdules.subSchdule = Schdule;
            SchduleList.push(Schdules);
            
        }  
        cb(SchduleList);
    }

	goToDetails(data){
		console.log('PUSH-DATA====>',data);
		// var img = ENUM.domain + ENUM.url.imgUrl + data.locimgpath;
		this.navCtrl.push(RideDetailsPage,{
			item:data.id,
			img:data.locimgpath,
			name:data.title,
			desc:data.desc,
			likes:data.like,
			dislikes:data.dislike,
			reglikestatus:data.reglikestatus,
			catId : data.lcid
		});
	}
  gotocart(){
    this.navCtrl.push(CartPage);
	}

}
