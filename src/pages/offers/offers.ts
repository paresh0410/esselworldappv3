import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ToastController, Events } from 'ionic-angular';
import { SwipeTabComponent } from '../../components/swipe-tab/swipe-tab';
import { webAPIService } from '../../service/webAPIService';
import { ENUM } from '../../service/ENUM';
import { checkIfOnlineService } from '../../service/checkIfOnlineService';
import { NoInternetConnectionPage } from '../no-internet-connection/no-internet-connection';
import { HomePage } from '../home/home';
import { OffersDetailsPage } from '../offers-details/offers-details';
import { NotificationPage } from '../notification/notification';
import { CartPage } from '../cart/cart';
import { ThemeServiceProvider } from '../../providers/theme-service/theme-service';
import { BookhomecategoryPage } from '../bookhomecategory/bookhomecategory';
import { DoorgetsTruncateService } from 'doorgets-ng-truncate';
/**
 * Generated class for the OffersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-offers',
	templateUrl: 'offers.html',
})

export class OffersPage {


	offers: any = [];
	array: any = [];
	loader: any;
	nodataAvailable : boolean = false;
	ThemeC: any;
	red: boolean;
	blue: boolean;
	green: boolean;
	hid: any;
	categoryList: any;
	getColor(hc){
		switch(hc){
			case 'red':
				return '#ff0000 ';
			case 'blue':
				return '#488aff';
			case 'green':
				return '#16910b';
		}
	}
	constructor(public navCtrl: NavController, public navParams: NavParams, public webAPI: webAPIService,
		public loadingCtrl: LoadingController, public checkOnline: checkIfOnlineService,
		private alertCtrl: AlertController, private toastCtrl: ToastController,public events: Events,
		public themeServices: ThemeServiceProvider, private _truncateService: DoorgetsTruncateService) {
			this.hid = this.navParams.get('hid');
			if(this.hid == undefined || this.hid == ''){
				this.hid = 0;
			}
			this.categoryList = this.navParams.get('categoryList');
			this._truncateService.init({
				trail: '...',
				limit: 50,
				position: 'left',
				words: true
			  });
		this.presentLoader();
	////////  Chnage for theme   ///////////////
	// this.ThemeC = this.themeServices.getTheme();
	// this.events.subscribe('theme', () => {
	// 	this.ThemeC = this.themeServices.getTheme();
	// });
	// if(this.ThemeC == 'red'){
	// 	this.red = true;
	// 	this.blue = false;
	// 	this.green = false;
	// }else if(this.ThemeC == 'blue'){
	// 	this.blue = true;
	// 	this.red = false;
	// 	this.green = false;
	// }else if(this.ThemeC == 'green'){
	// 	this.green = true;
	// 	this.red = false;
	// 	this.blue = false;
	// }
	var theme = localStorage.getItem('theme');
	if(theme){
		this.ThemeC = localStorage.getItem('theme');
		console.log('theme',this.ThemeC);
				if(this.ThemeC == 'red'){
			this.red = true;
			this.blue = false;
			this.green = false;
		}else if(this.ThemeC == 'blue'){
			this.blue = true;
			this.red = false;
			this.green = false;
		}else if(this.ThemeC == 'green'){
			this.green = true;
			this.red = false;
			this.blue = false;
		}
}
else{
	this.ThemeC = 'red'
}
	////////////////////////////////////////////
		var onlineStatus = this.checkOnline.getIfOnline();
		if (onlineStatus == true) {
			var params = {
				hid: this.hid
			}
			if(this.categoryList){
				params.hid = this.categoryList.hcid;
			}
			var url = ENUM.domain + ENUM.url.offerList;
			this.webAPI.getService(url, params)
				.then(result => {

					if (result != 'err') {
						console.log("Success :", result);
						let temp: any = result;
						if(temp.data.length > 0){
							this.array = temp.data;
							var x = 0;
							for (var i = 0; i < this.array.length; i++) {
								x++;
								this.array[i].tabId = "" + x + "";
								if (this.array[i].imgpath !== null || this.array[i].imgpath !== undefined || this.array[i].imgpath !== '') {
									var checkImgpath = this.array[i].imgpath.includes("/img/ego/");
									if (!checkImgpath) {
										this.array[i].contentImg = 'assets/imgs/default.png'
									}
									else {
										this.array[i].contentImg = ENUM.domain + ENUM.url.imgUrl + this.array[i].imgpath;
									}
								}
							}
							this.closeLoader();
						}else{
							this.closeLoader();
							this.nodataAvailable = true;
						}
						
					} else {
						this.serverErrorAlert();
					}
				})
				.catch(result => {
					console.log("Error :", result);
				})
		} else {
			this.closeLoader();
			this.presentToast();
			this.navCtrl.setRoot(HomePage);
		}

	}

	serverErrorAlert() {
		let alert = this.alertCtrl.create({
			title: 'Server Error!!!',
			message: 'Server Not Responding. Please try again later.',
			buttons: [
				{
					text: 'OK',
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
						this.navCtrl.setRoot(HomePage, { AP: 'Y' });
					}
				},
			]
		});
		alert.present();
	}
	gotoofferssDetails(item){

		this.navCtrl.push(OffersDetailsPage,{
			eventgroupofferId:item.eventgroupofferId,
			contentImg:item.contentImg,
			contentHead:item.contentHead,
			subContentP1:item.subContentP1,
		});
	}
	ionViewDidLoad() {
		//this.presentLoader();
	}

	booknow(){
		this.navCtrl.push(BookhomecategoryPage);
	}
	presentLoader() {
		this.loader = this.loadingCtrl.create({
			content: "Please wait..."
		});
		this.loader.present();
		console.log('ionViewDidLoad OffersPage');
		// 	setTimeout(()=>{
		// this.offers = this.array;
		// loader.dismiss();
		// 	},2000)
	}

	closeLoader() {

		this.offers = this.array;
		this.loader.dismiss();
	}


	presentToast() {
		let toast = this.toastCtrl.create({
			message: 'NO INTERNET CONNECTION',
			duration: 3000,
			position: 'top',
			//cssClass:"InternetOffToast"
		});

		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});

		toast.present();
	}
	gotocart() {
		this.navCtrl.push(CartPage);
	}

	gotornotificationPage() {
		this.navCtrl.push(NotificationPage);
		console.log('notification.');
	}

	
}
