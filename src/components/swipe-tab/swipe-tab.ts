import { Component, ViewChild, Input, OnChanges, SimpleChange } from '@angular/core';
import { Slides } from 'ionic-angular'

/**
 * Generated class for the SwipeTabComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

@Component({
  	selector: 'swipe-tab',
  	templateUrl: 'swipe-tab.html'
})

export class SwipeTabComponent   {

	@Input('tabs') tabs;
	@ViewChild(Slides) slides: Slides;
	placeholder = 'assets/imgs/default.png';
	text: string;
	relationship: string = '1';
	contentlist:any = [];
	currentIndex:number = 0;
	isFallback: boolean;
	
	onLoaded(isFallback) {
		// make somthing based on 'isFallback'
		this.isFallback = true;
	  }
 	constructor() {
    	// console.log('Hello SwipeTabComponent Component');
		// this.text = 'Hello World';
		console.log(this.tabs);
		this.currentIndex = 0;
	}

	ngAfterViewInit() {
		console.log(this.tabs);
	}
	  
	ngOnChanges(changes: { [propName: string]: SimpleChange }) {
    	// Todo: update necessary info
  	}

	slideChanged(dataIndex) {
		if(dataIndex == undefined || dataIndex == null){
			this.currentIndex = this.slides.getActiveIndex();
			console.log('Current index is', this.currentIndex);
			if(this.tabs.length != this.currentIndex){
				var Index = this.tabs[this.currentIndex].tabId
				// this.contentlist=this.tabs[this.currentIndex].tabContent;
				this.relationship = Index;
				var element = parseInt(Index);
				var ID = "content" + '-' + element;
				var elmnt = document.getElementById(ID);
				if(elmnt != null){
					elmnt.scrollIntoView();
				}
			}
		}else{
			this.currentIndex = dataIndex;
			console.log('Current index is', this.currentIndex);
			if(this.tabs.length != this.currentIndex){
				var Index = this.tabs[this.currentIndex].tabId
				// this.contentlist=this.tabs[this.currentIndex].tabContent;
				this.relationship = Index;
				var element = parseInt(Index);
				var ID = "content" + '-' + element;
				var elmnt = document.getElementById(ID);
				if(elmnt != null){
					elmnt.scrollIntoView();
				}
			}
		}
	}

	tabChanged(tabData){
		console.log('TABDATA--->',tabData);
		var toIndex = parseInt(tabData.tabId)-1;
		this.slides.slideTo(toIndex);
		// this.slideChanged(toIndex);
	}

}
