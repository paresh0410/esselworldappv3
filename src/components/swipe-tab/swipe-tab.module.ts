import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';

import { SwipeTabComponent } from '../swipe-tab/swipe-tab';
import { ImgFallbackModule } from 'ngx-img-fallback';

@NgModule({
	imports: [
		IonicModule
	],
	declarations: [
         SwipeTabComponent,
         ImgFallbackModule
	],
	exports: [
		 SwipeTabComponent
	]
})
export class SwipeTabComponentModule {}