import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';

// import { ScrollableTabComponent } from './scrollable-tab';

@NgModule({
	imports: [
		IonicModule
	],
	declarations: [
		// ScrollableTabComponent
	],
	exports: [
		// ScrollableTabComponent
	]
})
export class ScrollableTabComponentModule {}