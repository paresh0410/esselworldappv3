import { NgModule } from '@angular/core';
// import { ScrollableTabComponent } from './scrollable-tab/scrollable-tab';
import { GeodetailsComponent } from './geodetails/geodetails';
@NgModule({
	declarations: [GeodetailsComponent],
	imports: [],
	// exports: [ScrollableTabComponent]
})
export class ComponentsModule {}
