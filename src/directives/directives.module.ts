import { NgModule } from '@angular/core';
// import { SwipeSegmentDirective } from './swipe-segment/swipe-segment';
import { ParallaxHeader } from '../directives/parallax-header/parallax-header';
@NgModule({
	declarations: [ParallaxHeader],
	imports: [],
	// exports: [SwipeSegmentDirective]
})
export class DirectivesModule {}
