import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';

/*
  Generated class for the ThemeServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ThemeServiceProvider {
	ThemeC :any;
    constructor(public http: HttpClient, public events: Events) {
        // if(!localStorage.getItem('theme')){
            this.ThemeC = 'red';
        // }else{
        //     this.ThemeC = localStorage.getItem('theme');
        // }
        console.log('Hello ThemeServiceProvider Provider');
    }

    getTheme(){
        return this.ThemeC;
    }
    setTheme(theme){
        localStorage.setItem('theme', theme);
        this.ThemeC = theme;
        this.events.publish('theme', this.ThemeC);
    }
}
